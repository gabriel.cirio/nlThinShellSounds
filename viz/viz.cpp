
#include "multiscale_sounds.h"

#include <iostream>
#include <thread>
#include <mutex>
#include <igl/viewer/Viewer.h>
#include <igl/png/render_to_png.h>

MultiScaleSounds g_sim;
std::thread g_sim_thread;
std::mutex g_data_mutex;
bool g_run = false;
bool g_step = false;
bool g_thread_running = false;
bool g_show_nodal_vectors = true;
bool g_show_world_vectors = true;
int g_frame_stepper = 0;
bool g_render_to_png = false;
int g_png_counter = 0;

std::vector<std::pair<MatrixX, MatrixXi>> g_data_meshes;
std::vector<MatrixX> g_data_nodal_vectors;
std::vector<std::pair<MatrixX, MatrixX>> g_data_world_vectors;

bool g_data_dirty = false;

//#define THICK_LINES

void get_data_from_sim()
{
    if (g_data_mutex.try_lock()) // non-blocking, keep simulating otherwise
    {
		g_data_meshes.clear();
        g_data_nodal_vectors.clear();
        g_data_world_vectors.clear();
		g_sim.get_data_meshes(g_data_meshes);
		g_sim.get_data_nodal_vectors(g_data_nodal_vectors);
        g_sim.get_data_world_vectors(g_data_world_vectors);
		g_data_dirty = true;
        g_data_mutex.unlock();
    }
}

void sim_thread()
{
	g_thread_running = true;
	
	get_data_from_sim();
	
	while (g_sim.is_valid())
	{
		if (g_run || g_step)
		{
			g_sim.simulate_step();
			
			get_data_from_sim();
			
			if (g_step) g_step = false;
		}
		else 
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	}
	
	g_thread_running = false;
}

void toggle_sim()
{
	g_run = !g_run;
}

void step_sim()
{
	g_step = true;
}

void setup_sim()
{
	g_sim_thread = std::thread(sim_thread);
	g_sim_thread.detach();
}

bool key_down(igl::viewer::Viewer& viewer, unsigned char key, int modifier)
{
	if (key == 'P')
    {
		toggle_sim();
    }
    else if (key == 'S')
	{
		step_sim();
	}
	else if (key == 'F')
	{
		g_show_nodal_vectors = !g_show_nodal_vectors;
	}
	else if (key == 'H')
	{
		g_render_to_png = !g_render_to_png;
	}
	else
	{
		g_sim.set_key(key);
	}
	
	
    return true;
}

void render_data(igl::viewer::Viewer & viewer)
{
	if (g_data_dirty)
	{
		MatrixX V;
		MatrixXi F;
		MatrixX C;
		MatrixX P1;
		MatrixX P2;
		MatrixX Cp;
        bool has_nodal_vectors = false;
		
		viewer.data.clear();
		
		g_data_mutex.lock();
		
			if (g_data_meshes.size())
			{
				unsigned int tot_vert = 0;
				unsigned int tot_ind = 0;
				for (unsigned int i=0; i<g_data_meshes.size(); i++)
				{
					tot_vert += g_data_meshes[i].first.rows();
					tot_ind += g_data_meshes[i].second.rows();
				}
				
				V.resize(tot_vert, 3);
				F.resize(tot_ind, 3);
				P1.resize(tot_vert, 3);
				P2.resize(tot_vert, 3);
				Cp.resize(tot_vert, 3);
				Cp.col(0).setOnes();
				
				int cur_vert = 0;
				int cur_ind = 0;
				Real force_viz_scale = 5; // 0.1
				
				for (unsigned int i=0; i<g_data_meshes.size(); i++)
				{
					if (g_data_meshes[i].first.rows())
					{
						V.block(cur_vert, 0, g_data_meshes[i].first.rows(), 3) = g_data_meshes[i].first;
						F.block(cur_ind, 0, g_data_meshes[i].second.rows(), 3) = g_data_meshes[i].second + MatrixXi::Constant(g_data_meshes[i].second.rows(), 3, cur_vert);
						
						if (g_data_nodal_vectors.size() && g_data_nodal_vectors[i].rows())
						{
                            has_nodal_vectors = true;
							P1.block(cur_vert, 0, g_data_meshes[i].first.rows(), 3) = g_data_meshes[i].first;
							P2.block(cur_vert, 0, g_data_meshes[i].first.rows(), 3) = g_data_meshes[i].first + g_data_nodal_vectors[i]*force_viz_scale;
						}
						
						cur_vert += g_data_meshes[i].first.rows();
						cur_ind += g_data_meshes[i].second.rows();
					}
				}
			}
			
            if (V.rows()) viewer.data.set_mesh(V, F);
            if (g_show_nodal_vectors && has_nodal_vectors) viewer.data.add_edges(P1, P2, Cp);
            
            if (g_show_world_vectors)
                for (int i=0; i<g_data_world_vectors.size(); i++)
                    if (g_data_world_vectors[i].first.rows())
                    {
                        MatrixX Cv = MatrixX::Zero(g_data_world_vectors[i].first.rows(), 3);
                        Cv.col(0).setOnes();
                        viewer.data.add_edges(g_data_world_vectors[i].first, g_data_world_vectors[i].second, Cv);
                    }
                    
            g_data_dirty = false;
                
        g_data_mutex.unlock();
                
	}
	
	if (g_frame_stepper < viewer.core.animation_max_fps*3) g_frame_stepper++;
	else
	{
		if (g_run)
		{
			if (g_sim.getElapsedTime())
			{
				std::cout << "Elasped: " << g_sim.getElapsedTime() << std::endl;
				g_frame_stepper = 0;
			}
		}
	}

}

bool callback_post_draw(igl::viewer::Viewer & viewer)
{
    if (g_render_to_png)
    {
        std::string number;
        if (g_png_counter < 10)
            number = "000"+std::to_string(g_png_counter);
        else if (g_png_counter < 100)
            number = "00"+std::to_string(g_png_counter);
        else if (g_png_counter < 1000)
            number = "0"+std::to_string(g_png_counter);
        else
            number = std::to_string(g_png_counter);
        
        igl::png::render_to_png("frame_"+number+".png", viewer.core.viewport(2), viewer.core.viewport(3),false,false);
        g_png_counter++;
    }

	return false;
}
    
bool callback_pre_draw(igl::viewer::Viewer & viewer)
{
	render_data(viewer);
	
	return false;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
	{
		std::cout << "Usage: ./multiscale_sounds <filename>" << std::endl;
		return 0;
	}

	std::string conf_filename = argv[1];
    
	if (!g_sim.load_config(conf_filename)) return 1;
    
	setup_sim();
	
	// Show mesh
	igl::viewer::Viewer viewer;
	while (!g_data_dirty)
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	render_data(viewer);
    viewer.callback_key_down = &key_down;
	viewer.callback_pre_draw = callback_pre_draw;
    viewer.callback_post_draw = callback_post_draw;
	viewer.core.is_animating = true;
    viewer.core.background_color.setOnes();
    #ifdef THICK_LINES
    viewer.core.line_width = 4.;
    #endif
    viewer.launch();
	
	g_sim.set_valid(false);
	while (g_thread_running)
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	
	return 0;
}
