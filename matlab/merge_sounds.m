function merge_turbulence(json_config_file)

    config = parseJSON(json_config_file);
    
    if isfield(config, 'ReducedThinShells') == 0
        disp('No ReducedThinShells to work with in configuration file...');
        return;
    end
    
    if isfield(config, 'Radiation') == 0
        disp('No Radiation config...');
    end
    
    [sound1, sampling1] = audioread(soundfile1);
    [sound2, sampling2] = audioread(soundfile2);
    
    if (sampling1 ~= sampling2)
        disp('Different sampling rates...');
    end
    
    sound_final = vol1*sound1 + vol2*sound2;
    
    sound_final = sound_final / max(abs(sound_final));
    
    audiowrite('merged_sounds.wav', sound_final, sampling1);
    
end

