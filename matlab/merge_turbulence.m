function merge_turbulence(json_config_file)

    config = parseJSON(json_config_file);
    
    if isfield(config, 'ReducedThinShells') == 0
        disp('No ReducedThinShells to work with in configuration file...');
        return;
    end
    
    [base_folder,~,~] = fileparts(json_config_file);
    
    for i = 1:size(config.ReducedThinShells,2)
    
        name = config.ReducedThinShells{i}.ThinShell.filename(1:end-4);
        
        disp(name);
        
        if isfield(config.ReducedThinShells{i}, 'Turbulence') == 0
            disp('No Turbulence. Skipping.');
            continue;
        end
        
        scale_turb = 1;
        if isfield(config.ReducedThinShells{i}.Turbulence, 'merge') ~= 0
            scale_turb = config.ReducedThinShells{i}.Turbulence.merge;
        end
        
        soundfilename = strcat(base_folder, '/', name, '.binaudio');
        soundfilename_turb = strcat(base_folder, '/', name, '_turbulence.binaudio');
        
        if exist(soundfilename, 'file') ~= 2
            disp('No sound file. Skipping.');
            continue;
        end
        
        if exist(soundfilename_turb, 'file') ~= 2
            disp('No sound file for turbulent sound. Skipping.');
            continue;
        end
        
        fileID = fopen(soundfilename, 'r');
        numf = fread(fileID,1,'int');
        numdt = fread(fileID,1,'int');
        modal_positions = fread(fileID,[numf numdt],'double');
        
        fileID = fopen(soundfilename_turb, 'r');
        numf = fread(fileID,1,'int');
        numdt = fread(fileID,1,'int');
        modal_positions_turb = fread(fileID,[numf numdt],'double');

        length = min(size(modal_positions,2), size(modal_positions_turb,2));
        modal_positions_final = modal_positions(:,1:length) + scale_turb*modal_positions_turb(:,1:length);

        sampling = 44100;
        finalsoundfilename = strcat(base_folder, '/', name, '_merged.wav');
        finalsoundfilename_bin = strcat(base_folder, '/', name, '_merged.binaudio');
        
        x = sum(modal_positions_final);
        x = x/max(abs(x));
        audiowrite(finalsoundfilename, x, sampling);
    
        fileID = fopen(finalsoundfilename_bin, 'w');
        fwrite(fileID, size(modal_positions_final,1), 'int');
        fwrite(fileID, size(modal_positions_final,2), 'int');
        fwrite(fileID, modal_positions_final, 'double');
        fclose(fileID);
        
    end
    
end

