function fastbem_assemble(fastbem_folder, plot_data)

if (nargin == 1)
    plot_data = 0;
end

files = dir(strcat(fastbem_folder, '/result_chunk*.dat'));

freq_array = cell(size(files,1), 1);
pressure_array = cell(size(files,1), 1);

for i = 1 : size(files,1)

  fprintf('Reading file %s...\n', files(i).name);
  
  [~, name, ~] = fileparts(files(i).name);
  
  % open the info file to know the number of frequencies, elements and field points
  info_data = dlmread(strcat(fastbem_folder, '/', name(8:end), '.info')); % remove the "result_" in front of the filename
  num_elems = int32(info_data(1));
  num_field_points = int32(info_data(2));
  freq_values = info_data(3:size(info_data,1));
  num_frequencies = size(freq_values, 1);
  pressure_values = zeros(num_frequencies, num_field_points);
  
  % now open the dat file to get the frequencies and the pressure
  fid = fopen(strcat(fastbem_folder, '/', files(i).name));
  
  % jump to the first freq line
  fgetl(fid);
  fgetl(fid);
  
  for j = 1:num_frequencies
    
    % jump to the field points
    for l=1:5
        fgetl(fid);
    end
    fseek(fid,num_elems*97,'cof');
    for l=1:5
        fgetl(fid);
    end
    
    % parse all field points
    for k = 1:num_field_points
    
        txt = fgetl(fid);
      
        pressure_real = str2double(strtrim(txt(14:25)));
        pressure_imag = str2double(strtrim(txt(27:38)));
        pressure_complex = complex(pressure_real, pressure_imag);
      
        % save
        pressure_values(j,k) = abs(pressure_complex);
        
    end
    
    % jump to the next frequency
    for l=1:5
        fgetl(fid);
    end
 
  end
  
  fclose(fid);
  
  freq_array{i} = freq_values;
  pressure_array{i} = pressure_values;

end

% count the number of value pairs
freq_counter = 0;
for i = 1 : size(freq_array,1)
  freq_counter = freq_counter + size(freq_array{i}, 1);
end

pressures = zeros(freq_counter, num_field_points);
frequencies = zeros(freq_counter, 1);

counter = 1;
for i = 1 : size(pressure_array,1) % for each file
    for j = 1 : size(pressure_array{i},1) % for each freq
        pressures(counter, :) = pressure_array{i}(j, :); % all field points
        frequencies(counter, 1) = freq_array{i}(j, 1); % frequency for sorting
        counter = counter + 1;
    end
end

[frequencies, I] = sort(frequencies);
pressures = pressures(I,:);

fileID = fopen(strcat(fastbem_folder, '/fastbem_pressures.bin'),'w');
fwrite(fileID,size(pressures,1),'int');
fwrite(fileID,size(pressures,2),'int');
fwrite(fileID,pressures,'double');
fclose(fileID);

fileID = fopen(strcat(fastbem_folder, '/fastbem_frequencies.bin'),'w');
fwrite(fileID,size(frequencies,1),'int');
fwrite(fileID,frequencies,'double');
fclose(fileID);

if (plot_data ~= 0)
    for i=1:8
      figure;
      plot(frequencies, pressures(:,i)); % plot first 8 field points
    end
end

end
