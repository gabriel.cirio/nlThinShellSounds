
function [] = turbulence(json_config_file, plot_results)

    if (nargin == 1)
        plot_results = 0;
    end
    
    config = parseJSON(json_config_file);
    
    if isfield(config, 'ReducedThinShells') == 0
        disp('No ReducedThinShells to work with in configuration file...');
        return;
    end
    
    [base_folder,~,~] = fileparts(json_config_file);
    
    for r = 1:size(config.ReducedThinShells,2)
    
        if isfield(config.ReducedThinShells{r}, 'Turbulence') == 0
            disp('No Turbulence config. Skipping.');
            continue;
        end
    
        name = config.ReducedThinShells{r}.ThinShell.filename(1:end-4);
    
        turb_data_path = strcat(base_folder, '/', name, '_turbulence_data.bin');
    
        % We call pdepe several times for chunks of time, and apply damping and energy injection between calls
    
        [lowf, highf, ~, num_timesteps, modal_freqs, modal_damping, powerspectrum_sim, modal_velocities, turbulence_grad] = load_sound_data(turb_data_path, 0);
    
        % Remove negative values (artifacts) in turbulence gradient
        turbulence_grad(turbulence_grad < 0) = 0;
        % normalize => THIS IS NOT CORRET SINCE IT BOOSTS TURBGRAD IF IT IS SMALL,
        % WE SHOULD PROBABLY USE A SCALING COEFFICIENT IDEALLY CONSTANT THROUGHOUT SIMS
        turbulence_grad = turbulence_grad/max(turbulence_grad);
        
        % sort frequencies (SHOULD APPLY SORTING TO FREQUENCY DEPENDENT ARRAYS!)
        [modal_freqs, freqsI] = sort(floor(modal_freqs));
        modal_damping = modal_damping(freqsI);

        % round up freqs so that we can do reverse indexing
        modal_freqs = floor(modal_freqs);

        totalf = lowf+highf;%+extraf;
        modal_freqs = modal_freqs(1:totalf);
        modal_damping = modal_damping(1:totalf);
        
        % make a uniform frequency sampling
        df = 10;
        minsimfreq = modal_freqs(1);
        uniformfreqs = minsimfreq:df:modal_freqs(end)+df;
        totaluniformf = size(uniformfreqs,2);

        % some frequencies might overlap, especially after rounding. 
        % This is bad for the computations that will follow.
        % Find and perturb them. This is hacky, but collisions only seem to happen at
        % very high frequencies, so it's fine
        for i=1:totalf-1
            if modal_freqs(i) == modal_freqs(i+1)
                if (i < totalf-1)
                    modal_freqs(i+1) = ceil((modal_freqs(i+1) + modal_freqs(i+2))/2);
                else
                    modal_freqs(i+1) = modal_freqs(i) + (modal_freqs(i) - modal_freqs(i-1));
                end
            end
        end

        % compute frequency map that maps every discrete frequency to its
        % closest values in freqs
        num_discrete_freqs = uniformfreqs(end)+df;
        freqs_interpolants = zeros(num_discrete_freqs,2);
        for j=1:uniformfreqs(1)
            freqs_interpolants(j,1) = 1;
            freqs_interpolants(j,2) = 1;
        end
        for i=1:totaluniformf-1
            for j=uniformfreqs(i):uniformfreqs(i+1)
                freqs_interpolants(j,1) = i;
                freqs_interpolants(j,2) = i+1;
            end
        end
        for j=uniformfreqs(totaluniformf):size(freqs_interpolants,1)
            freqs_interpolants(j,1) = totaluniformf;
            freqs_interpolants(j,2) = totaluniformf;
        end

        % build discrete damping (damping values for the entire discrete spectrum)
        discrete_range = 0:num_discrete_freqs-1;
        damping_discrete = interp1(modal_freqs, modal_damping, discrete_range); % must start at 0 for correct indexing
        damping_discrete(1:modal_freqs(1)) = modal_damping(1); % includes freq(1) since that value is also set as NaN by interp1
        damping_discrete(modal_freqs(totalf):num_discrete_freqs) = modal_damping(totalf);
        
        % uniform damping: damping for each uniformfreq
        uniformdamping = interp1(discrete_range, damping_discrete, uniformfreqs);

        %%%%%% Parameters %%%%%%%
        D = config.ReducedThinShells{r}.Turbulence.diffusion_coeff; % Diffusion coefficient
        %%%%%%% Controls %%%%%%%%
        time_chunk_size = 2000;
        %%%%%%% Switches %%%%%%%%
        use_spectrogram = 0;
        use_velocity_spectrogram = 1; % otherwise amplitude spectrogram
        synth_using_modal_frequencies = 1;

        % The energy scale and the diffusion coefficient can be merged, since D' = D*scale^2
        % The result is also scaled down by "scale" since the solution is the time derivative.

        num_chunks = floor(num_timesteps/time_chunk_size);

        % compute IC (sim energies) %%%%%%%%%%%%%%%%%
        
        disp('Computing IC...');
        
        if (use_spectrogram == 1)
            
            if (use_velocity_spectrogram)
                sound = sum(modal_velocities);
            else
                binaudio_filename = strcat(base_folder, '/', name, '.binaudio');
                fileID = fopen(binaudio_filename, 'r');
                numf = fread(fileID,1,'int');
                numdt = fread(fileID,1,'int');
                modal_positions = fread(fileID,[numf numdt],'double');
                sound = sum(modal_positions);
            end

            sound = sound/max(abs(sound));

            powerspectrum = zeros(totaluniformf,num_chunks);
            
            %spectrogram(sound,[],[],freqs,44100,'yaxis'); % This us just to plot, will plot amplitude per frequency per unit frequency (db/Hz)
            [~,~,~,ps] = spectrogram(sound, time_chunk_size*2, time_chunk_size, uniformfreqs, 44100, 'power');
            powerspectrum(:,2:end) = ps; % ps does not have the content for t=0 (which is 0)
            
        else
            
            powerspectrum = zeros(totaluniformf,num_chunks);
            
            for t_chunk = 1:num_chunks-1

                timestep = t_chunk*time_chunk_size;
                powerspectrum(:,t_chunk+1) = interp1(modal_freqs, powerspectrum_sim(timestep,:), uniformfreqs); % 0 at t=0
                powerspectrum(isnan(powerspectrum(:,t_chunk+1)),t_chunk+1) = 0; % set energies outside of simulated spectrum to 0
                
            end
            
        end

        % compute turbulence %%%%%%%%%%%%%%%%%
        
        disp('Computing turbulence...');
        
        sampling = 44100;
        dt = 1/sampling;
         % save spectrum at the end of each chunk, we will interpolate later
        E = zeros(totaluniformf, num_chunks);
        spectrum_diff = zeros(totaluniformf, num_chunks);
        uniformpowerspectrum = zeros(totaluniformf, num_chunks);
        next_IC = zeros(totaluniformf, 1);
        last_t = 0;
        tic;
        for t_chunk = 1:num_chunks

            timestep = (t_chunk-1)*time_chunk_size + 1;
            t = [0, time_chunk_size/2, time_chunk_size]*dt; % 3 steps because it's the minimum for pdepe
            if t_chunk ~= 1
                next_IC = spectrum_diff(:,t_chunk-1);
            end
            
            uniformpowerspectrum(:,t_chunk) = powerspectrum(:,t_chunk);
            injected_power = turbulence_grad(timestep)*uniformpowerspectrum(:,t_chunk);
            next_IC = next_IC + injected_power;

            sol = pdepe(0,@DiffusionPDEfun,@DiffusionICfun,@DiffusionBCfun,uniformfreqs,t,[]);
            E(:,t_chunk) = sol(size(t,2),:)';

            % compute INJECTED spectrum evolution subjected to damping only (for drift correction)
            powerspectrum_nodiffusion = injected_power.*(1-2*uniformdamping*time_chunk_size/2*dt)';
            powerspectrum_nodiffusion = powerspectrum_nodiffusion.*(1-2*uniformdamping*time_chunk_size/2*dt)';

            % save the spectrum difference to be reinjected in next call
            spectrum_diff(:,t_chunk) = E(:,t_chunk) - powerspectrum_nodiffusion;

            % Remove negative values
            spectrum_diff(spectrum_diff(:,t_chunk) < 0,t_chunk) = 0;
            % If we get negative values in the tail, we need to take smaller
            % steps (it's because high frequencies get overdamped due to linearization)

            last_t = last_t + time_chunk_size*dt;
        end
        toc;

        % plot results %%%%%%%%%%%%%%%%%
        
        if (plot_results == 1)
            
            % plot combined spectrum
            plot_data(uniformfreqs,E,'f','E(f,t)',0);

        end

        % Write spectrogram %%%%%%%%%%%%%%%%%
         
        fileID = fopen(strcat(base_folder, '/', name, '_turbulent_spectrogram.bin'), 'w');
        
        maxf_idx = find(modal_freqs < uniformfreqs(end), 1, 'last');
        output_freqs = modal_freqs(1:maxf_idx);
        
        fwrite(fileID,time_chunk_size,'int');
        fwrite(fileID,size(output_freqs,1),'int');
        fwrite(fileID,size(E,2)+1,'int'); % acutal size plus time t=0 (with 0 energy)

        fwrite(fileID,zeros(size(output_freqs,1),1),'double');
        for t_chunk = 1:num_chunks
            E_chunk = interp1(uniformfreqs, E(:,t_chunk), output_freqs);
            fwrite(fileID,E_chunk,'double');
        end
        
        fclose(fileID);
        
        % Write sound %%%%%%%%%%%%%%%%%
        
%         disp('Computing sound...');
%         
%         if (synth_using_modal_frequencies == 1) % write sound using modal frequencies (not uniform spectrum)
% 
%             minf_idx = 1;
%             %minf_idx = find(modal_freqs > modal_freqs(lowf), 1, 'first');
%             maxf_idx = find(modal_freqs < uniformfreqs(end), 1, 'last');
%             output_freqs = modal_freqs(minf_idx:maxf_idx);
%             output_numf = size(output_freqs,1);
%             
%         else % write sound using uniform spectrum
%             
%             output_freqs = uniformfreqs';
%             output_numf = size(output_freqs,1);
%             
%         end
% 
%         rng(0); % use pseudo-random to always use the same values
%         phase = rand(output_numf, 1)*2*pi;
%         t = (1:num_timesteps)*dt;
%         waves = zeros(output_numf, num_timesteps);
%         for i = 1:output_numf
%             F = output_freqs(i);
%             phi = phase(i);
%             waves(i,:) = sin(2*pi*F*t + phi);
%         end
% 
%         x = zeros(num_timesteps,1);
%         binaudio = zeros(output_numf, num_timesteps);
% 
%         last_idxt = 1;
%         for t_chunk = 0:num_chunks-1 % the 0 index is just zero energy
% 
%             if t_chunk == 0 
%                 Eleft = zeros(output_numf,1);
%             else
%                 Eleft = interp1(uniformfreqs, spectrum_diff(:,t_chunk), output_freqs);
%             end
% 
%             Eright = interp1(uniformfreqs, spectrum_diff(:,t_chunk+1), output_freqs);
% 
%             for idxt = 0:(time_chunk_size-1)
% 
%                 Et = (1-idxt/time_chunk_size)*Eleft + (idxt/time_chunk_size)*Eright;
%                 
%                 A = sqrt(Et);
%                 A = A ./ output_freqs; % go from modal velocities to modal amplitudes
% 
%                 binaudio(:, idxt + last_idxt) = A.*waves(:,idxt + last_idxt);
% 
%                 x(idxt + last_idxt) = dot(A,waves(:,idxt + last_idxt));
% 
%             end
% 
%             last_idxt = last_idxt + time_chunk_size;
% 
%         end
% 
%         x = x/max(abs(x));
% 
%         audiowrite(strcat(base_folder, '/', name, '_turbulence.wav'), x, 44100);
% 
%         fileID = fopen(strcat(base_folder, '/', name, '_turbulence.binaudio'), 'w');
%         fwrite(fileID,size(binaudio,1),'int');
%         fwrite(fileID,size(binaudio,2),'int');
%         fwrite(fileID,binaudio,'double');
%         fclose(fileID);
        
    end
    
    % apply radiation %%%%%%%%%%%%%%%%%
    %disp('Computing acoustic radiation...');
    %acoustic_radiation(json_config_file);
    
    
    %%%%%% PDE FUNCTIONS %%%%%%%
    
    function [c,flux,source] = DiffusionPDEfun(f,t,Ef,dEfdx)

        % nonlinear diffusion
        c = 1;
        flux = D*f*Ef*Ef*dEfdx;
        
        % damping
        grad_Edamping = -2*damping_discrete(floor(f))*Ef;
        source = grad_Edamping;

    end

    function Ef0 = DiffusionICfun(f)
        
         % Initial conditions
         
         mode = floor(f);
         modes_interp = freqs_interpolants(mode,:);
         weight = 1;
         if (modes_interp(1) ~= modes_interp(2))
             weight = 1 - (mode-uniformfreqs(modes_interp(1)))/(uniformfreqs(modes_interp(2))-uniformfreqs(modes_interp(1)));
         end
         Ef0 = weight*next_IC(modes_interp(1)) + (1-weight)*next_IC(modes_interp(2));

    end

    function [pl,ql,pr,qr] = DiffusionBCfun(fl,Efl,fr,Efr,t)
        
        % Boundary conditions for f = 0 and f = maxf;

        % left: no flow allowed
        pl = 0;
        ql = 1;

        % right: free flow into oblivion...
        pr = Efr;
        qr = 0;

    end

    %%%%%% UTILITY FUNCTIONS %%%%%%%
    
    function [] = plot_data(xvec,ymat,xname,yname,type)
        
        % plot simulated (input) spectrum
        figure;
        plot_interval = floor(size(ymat,2)/10);
        for ti=0:10
            time_to_plot = ti*plot_interval;
            if (time_to_plot == 0)
                time_to_plot = 1;
            end
            if type == 0
                plot(xvec,ymat(:,time_to_plot));
            elseif type == 1
                semilogx(xvec,ymat(:,time_to_plot));
            else
                loglog(xvec,ymat(:,time_to_plot));
            end
            hold on;
        end
        xlabel(xname);
        ylabel(yname);
        
    end

end

