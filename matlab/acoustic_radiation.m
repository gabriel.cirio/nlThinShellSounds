
function [] = acoustic_radiation(json_config_file)

    config = parseJSON(json_config_file);
    
    if isfield(config, 'ReducedThinShells') == 0
        disp('No ReducedThinShells to work with in configuration file...');
        return;
    end
    
    if isfield(config, 'Radiation') == 0
        disp('No Radiation config...');
    end
    
    listener_position = config.Radiation.listener_idx;

    sampling = 44100;
    
    [base_folder,~,~] = fileparts(json_config_file);
    
    for i = 1:size(config.ReducedThinShells,2)
    
        name = config.ReducedThinShells{i}.ThinShell.filename(1:end-4);
        
        disp(name);
        
        fastbem_pressures_file = strcat(base_folder, '/fastbem_', name, '/fastbem_pressures.bin');
        if exist(fastbem_pressures_file, 'file') ~= 2 % check if the pressures file exist
            % if it doesn't
            if exist(strcat(base_folder, '/fastbem_', name), 'file') == 7 % check if the folder exists, and call assemble if it does
                fastbem_assemble(strcat(base_folder, '/fastbem_', name), 0)
            else
                disp('No fastbem data available. Aborting...');
                return;
            end
        end
        
        fileID = fopen(fastbem_pressures_file, 'r');
        rows = fread(fileID,1,'int');
        cols = fread(fileID,1,'int');
        fastbem_pressures = fread(fileID,[rows cols],'double');
            
        add_radiation_to_binaudio(strcat(base_folder, '/', name, '.binaudio'), name); % sound
        
        % turbulence
        if exist(strcat(base_folder, '/', name, '_turbulence.binaudio'), 'file') == 2  % turbulence
            add_radiation_to_binaudio(strcat(base_folder, '/', name, '_turbulence.binaudio'), strcat(name, '_turbulence'));
        end
        
        % merged
        if exist(strcat(base_folder, '/', name, '_merged.binaudio'), 'file') == 2  % turbulence
            add_radiation_to_binaudio(strcat(base_folder, '/', name, '_merged.binaudio'), strcat(name, '_merged'));
        end
        
    end
    
    function [] = add_radiation_to_binaudio(binaudio_filename, base_name)
       
        fileID = fopen(binaudio_filename, 'r');
        numf = fread(fileID,1,'int');
        numdt = fread(fileID,1,'int');
        modal_positions = fread(fileID,[numf numdt],'double');

        if (listener_position == 0)
            for l=1:size(fastbem_pressures,2)
                x = fastbem_pressures(1:numf,l)'*modal_positions;
                x = x/max(abs(x));
                audiowrite(strcat(base_folder,'/',base_name,'_radiation_pos',num2str(l),'.wav'), x, sampling);
            end
        else
            x = fastbem_pressures(1:numf,listener_position)'*modal_positions;
            x = x/max(abs(x));
            audiowrite(strcat(base_folder,'/',base_name,'_radiation_pos',num2str(listener_position),'.wav'), x, sampling);
        end
        
    end
    
end