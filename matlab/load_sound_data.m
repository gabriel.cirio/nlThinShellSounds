function [lowf, highf, extraf, num_timesteps, freqs, damping, powerspectrum, modal_velocities, turbulence_grad] = load_sound_data(data_file, recompute)

    if (nargin == 1)
        recompute = 0;
    end
    
    disp('Loading simulation data...');

    fileID = fopen(data_file);
    lowf = fread(fileID,1,'int');
    highf = fread(fileID,1,'int');
    extraf = fread(fileID,1,'int');
    num_timesteps = fread(fileID,1,'int');
    freqs = fread(fileID,lowf+highf+extraf,'double');
    damping = fread(fileID,lowf+highf+extraf,'double');
    modal_velocities = fread(fileID,[lowf+highf num_timesteps],'double');
    
    computed_name = strcat(data_file, 'computed');
    
    if ( (exist(computed_name, 'file') == 0) || recompute)
    
        lyapunov = fread(fileID, num_timesteps, 'double');

        disp('Computing turbulence measurement...');
        smoothed_lyapunov = smooth(lyapunov,0.1);
        turbulence_grad = gradient(smoothed_lyapunov);
        turbulence_grad(turbulence_grad < 0) = 0;
        turbulence_grad = smooth(turbulence_grad,0.1);
        
        disp('Computing velocity power spectrum...');
        powerspectrum = zeros(num_timesteps, lowf+highf);
        for mode = 1:(lowf+highf)
            powerspectrum(:,mode) = smooth(modal_velocities(mode,:)'.*modal_velocities(mode,:)',0.2);
            %plot(powerspectrum_lf(:,mode));
            %hold on;
        end
        
        disp('Saving...');
        fileID2 = fopen(computed_name,'w');
        fwrite(fileID2,turbulence_grad,'double');
        fwrite(fileID2,powerspectrum,'double');
        
    else
        
        disp('Loading computed data...');
        fileID2 = fopen(computed_name);
        turbulence_grad = fread(fileID2,num_timesteps,'double');
        powerspectrum = fread(fileID2,[num_timesteps lowf+highf],'double');
        %plot(powerspectrum(:,mode));
        %hold on;
        
    end
    
    disp('...done');
    
end

