
#ifndef MULTISCALESOUNDS_TYPEDEFS_H
#define MULTISCALESOUNDS_TYPEDEFS_H

#include <Eigen/Dense>

#include <vector>

typedef double Real;

typedef Eigen::Matrix<Real, 1, 1> Matrix1;
typedef Eigen::Matrix<Real, 2, 2> Matrix2;
typedef Eigen::Matrix<Real, 3, 3> Matrix3;
typedef Eigen::Matrix<Real, 4, 4> Matrix4;

typedef Eigen::Matrix<Real, 1, 1> Vector1;
typedef Eigen::Matrix<Real, 2, 1> Vector2;
typedef Eigen::Matrix<Real, 3, 1> Vector3;
typedef Eigen::Matrix<Real, 1, 3> Vector3T;

typedef Eigen::Matrix<int, 2, 1> Vector2i;
typedef Eigen::Matrix<int, 3, 1> Vector3i;
typedef Eigen::Matrix<int, 4, 1> Vector4i;

typedef Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic> MatrixX;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> VectorX;
typedef Eigen::Matrix<Real, 1, Eigen::Dynamic> VectorXT;

typedef Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic> MatrixXi;
typedef Eigen::Matrix<int, Eigen::Dynamic, 1> VectorXi;

typedef Eigen::Quaternion<Real> Quaternion;

typedef Eigen::AngleAxis<Real> AngleAxis;

// Geometry
typedef Eigen::AlignedBox<Real,3> AlignedBox;
typedef Eigen::Transform<Real, 3, Eigen::Affine> Transform;

const Real PI = 3.14159265358979323846;
const Real DEG_TO_RAD = 0.017453292519943295;
const Real RAD_TO_DEG = 57.295779513082323;

inline Matrix3 makeSkewSymmetric(const Vector3 & v)
{
	Matrix3 out;
	out <<     0, -v(2),  v(1),
			v(2),     0, -v(0),
		-v(1),  v(0),     0;

	return out;
}

struct Jacobian
{
	int index_i;
	int index_j;
	MatrixX jacobian;
};

struct ValuesForDof
{
	std::vector<std::vector<std::vector<Real>>> cubic;
	std::vector<std::vector<Real>> quadratic;
	std::vector<Real> linear;
	Real constant;
};

#endif // MULTISCALESOUNDS_TYPEDEFS_H

 
 
