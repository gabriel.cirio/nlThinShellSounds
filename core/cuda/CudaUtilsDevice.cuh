
#ifndef CUDA_UTILS_DEVICE_CUH
#define CUDA_UTILS_DEVICE_CUH

#define PI 3.14159265358979323846

////////////////////////////////////////////////////////////////////////////////
// constructors
////////////////////////////////////////////////////////////////////////////////

inline __host__ __device__ void make_double3(double3 & vec, const double s)
{
    vec.x = s;
    vec.y = s;
    vec.z = s;
}
inline __host__ __device__ void make_double3(double3 & vec, const double2 & a)
{
    vec.x = a.x;
    vec.y = a.y;
    vec.z = 0.;
}
inline __host__ __device__ void make_double3(double3 & vec, const double2 & a, const double s)
{
    vec.x = a.x;
    vec.y = a.y;
    vec.z = s;
}
inline __host__ __device__ void make_double3(double3 & vec, const double4 & a)
{
    vec.x = a.x;
    vec.y = a.y;
    vec.z = a.z;
}
inline __host__ __device__ double3 make_double3(const double4 & a)
{
    return make_double3(a.x, a.y, a.z);
}
inline __host__ __device__ void make_double3(double3 & vec, const int3 & a)
{
	vec.x = double(a.x);
    vec.y = double(a.y);
    vec.z = double(a.z);
}
inline __host__ __device__ void make_double3(double3 & vec, const uint3 & a)
{
    vec.x = double(a.x);
    vec.y = double(a.y);
    vec.z = double(a.z);
}


inline __host__ __device__ void make_double4(double4 & vec, const double s)
{
	vec.x = s;
    vec.y = s;
    vec.z = s;
    vec.w = s;
}
inline __host__ __device__ void make_double4(double4 & vec, const double3 & a)
{
    vec.x = a.x;
    vec.y = a.y;
    vec.z = a.z;
    vec.w = 0.;
}
inline __host__ __device__ double4 make_double4(const double3 & a, const double s)
{
	return make_double4(a.x, a.y, a.z, s);
}
inline __host__ __device__ void make_double4(double4 & vec, const double3 & a, const double s)
{
    vec.x = a.x;
    vec.y = a.y;
    vec.z = a.z;
    vec.w = s;
}
inline __host__ __device__ void make_double4(double4 & vec, const int4 & a)
{
    vec.x = double(a.x);
    vec.y = double(a.y);
    vec.z = double(a.z);
    vec.w = double(a.w);
}
inline __host__ __device__ void make_double4(double4 & vec, const uint4 & a)
{
    vec.x = double(a.x);
    vec.y = double(a.y);
    vec.z = double(a.z);
    vec.w = double(a.w);
}

////////////////////////////////////////////////////////////////////////////////
// negate
////////////////////////////////////////////////////////////////////////////////

inline __host__ __device__ double3 operator-(const double3 & a)
{
    return make_double3(-a.x, -a.y, -a.z);
}
inline __host__ __device__ double4 operator-(const double4 & a)
{
    return make_double4(-a.x, -a.y, -a.z, -a.w);
}
inline __host__ __device__ void negate(double3 & a)
{
    a.x = -a.x;
    a.y = -a.y;
    a.z = -a.z;
}

////////////////////////////////////////////////////////////////////////////////
// addition
////////////////////////////////////////////////////////////////////////////////

inline __host__ __device__ void operator+=(double2 & a, const double2 & b)
{
    a.x += b.x;
    a.y += b.y;
}
inline __host__ __device__ double3 operator+(const double3 & a, const double3 & b)
{
    return make_double3(a.x + b.x, a.y + b.y, a.z + b.z);
}
inline __host__ __device__ void operator+=(double3 & a, const double3 & b)
{
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
}
inline __host__ __device__ double4 operator+(const double4 & a, const double4 & b)
{
    return make_double4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
}

inline __host__ __device__ double4 operator+(const double4 & a, const double3 & b)
{
    return make_double4(a.x + b.x, a.y + b.y, a.z + b.z, a.w);
}

inline __host__ __device__ void operator+=(double4 & a, const double4 & b)
{
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
    a.w += b.w;
}
inline __host__ __device__ void operator+=(double4 & a, const double3 & b)
{
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
}

////////////////////////////////////////////////////////////////////////////////
// subtract
////////////////////////////////////////////////////////////////////////////////

inline __host__ __device__ double3 operator-(const double3 & a, const double3 & b)
{
    return make_double3(a.x - b.x, a.y - b.y, a.z - b.z);
}
inline __host__ __device__ void operator-=(double3 & a, const double3 & b)
{
    a.x -= b.x;
    a.y -= b.y;
    a.z -= b.z;
}
inline __host__ __device__ double4 operator-(const double4 & a, const double4 & b)
{
    return make_double4(a.x - b.x, a.y - b.y, a.z - b.z,  a.w - b.w);
}
inline __host__ __device__ void operator-=(double4 & a, const double4 & b)
{
    a.x -= b.x;
    a.y -= b.y;
    a.z -= b.z;
    a.w -= b.w;
}
inline __host__ __device__ void operator-=(double4 & a, const double3 & b)
{
    a.x -= b.x;
    a.y -= b.y;
    a.z -= b.z;
}
inline __host__ __device__ void operator-=(double3 & a, const double4 & b)
{
    a.x -= b.x;
    a.y -= b.y;
    a.z -= b.z;
}

////////////////////////////////////////////////////////////////////////////////
// multiply
////////////////////////////////////////////////////////////////////////////////

inline __host__ __device__ double operator*(const double3 & a, const double3 & b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

inline __host__ __device__ double3 operator*(const double3 & a, const double b)
{
    return make_double3(a.x * b, a.y * b, a.z * b);
}
inline __host__ __device__ double3 operator*(const double b, const double3 & a)
{
    return make_double3(b * a.x, b * a.y, b * a.z);
}
inline __host__ __device__ void operator*=(double3 & a, const double b)
{
    a.x *= b;
    a.y *= b;
    a.z *= b;
}

inline __host__ __device__ double4 operator*(const double4 & a, const double b)
{
    return make_double4(a.x * b, a.y * b, a.z * b,  a.w * b);
}
inline __host__ __device__ double4 operator*(const double b, const double4 & a)
{
    return make_double4(b * a.x, b * a.y, b * a.z, b * a.w);
}
inline __host__ __device__ void operator*=(double4 & a, const double b)
{
    a.x *= b;
    a.y *= b;
    a.z *= b;
    a.w *= b;
}

////////////////////////////////////////////////////////////////////////////////
// divide
////////////////////////////////////////////////////////////////////////////////

inline __host__ __device__ double3 operator/(const double3 & a, const double3 & b)
{
    return make_double3(a.x / b.x, a.y / b.y, a.z / b.z);
}
inline __host__ __device__ void operator/=(double3 & a, const double3 & b)
{
    a.x /= b.x;
    a.y /= b.y;
    a.z /= b.z;
}
inline __host__ __device__ double3 operator/(const double3 & a, const double b)
{
    return make_double3(a.x / b, a.y / b, a.z / b);
}
inline __host__ __device__ void operator/=(double3 & a, const double b)
{
    a.x /= b;
    a.y /= b;
    a.z /= b;
}
inline __host__ __device__ double3 operator/(const double b, const double3 & a)
{
    return make_double3(b / a.x, b / a.y, b / a.z);
}

inline __host__ __device__ double4 operator/(const double4 & a, const double4 & b)
{
    return make_double4(a.x / b.x, a.y / b.y, a.z / b.z,  a.w / b.w);
}
inline __host__ __device__ void operator/=(double4 & a, const double4 & b)
{
    a.x /= b.x;
    a.y /= b.y;
    a.z /= b.z;
    a.w /= b.w;
}
inline __host__ __device__ double4 operator/(const double4 & a, const double & b)
{
    return make_double4(a.x / b, a.y / b, a.z / b,  a.w / b);
}
inline __host__ __device__ void operator/=(double4 & a, const double b)
{
    a.x /= b;
    a.y /= b;
    a.z /= b;
    a.w /= b;
}
inline __host__ __device__ double4 operator/(const double b, const double4 & a)
{
    return make_double4(b / a.x, b / a.y, b / a.z, b / a.w);
}

////////////////////////////////////////////////////////////////////////////////
// dot product
////////////////////////////////////////////////////////////////////////////////

// if double4, only consider its double3 part

inline __host__ __device__ double dot(const double3 & a, const double3 & b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline __host__ __device__ double dot(const double4 & a, const double3 & b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline __host__ __device__ double dot(const double3 & a, const double4 & b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline __host__ __device__ double dot(const double4 & a, const double4 & b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}


////////////////////////////////////////////////////////////////////////////////
// length
////////////////////////////////////////////////////////////////////////////////

// if double4, only consider its double3 part

inline __host__ __device__ double length(const double3 & v)
{
    return sqrt(dot(v, v));
}

inline __host__ __device__ double length(const double4 & v)
{
    return sqrt(dot(v, v));
}

inline __host__ __device__ double squared_length(const double3 & v)
{
    return v.x*v.x + v.y*v.y + v.z*v.z;
}

////////////////////////////////////////////////////////////////////////////////
// sqlength
////////////////////////////////////////////////////////////////////////////////

// if double4, only consider its double3 part

inline __host__ __device__ double sqlength(const double3 & v)
{
    return dot(v, v);
}

inline __host__ __device__ double sqlength(const double4 & v)
{
    return dot(v, v);
}

////////////////////////////////////////////////////////////////////////////////
// normalize
////////////////////////////////////////////////////////////////////////////////

inline __host__ __device__ double3 normalize(const double3 & v)
{
    return v * rsqrt(dot(v, v));
}

inline __host__ __device__ void normalize_self(double3 & v)
{
    v *= rsqrt(dot(v, v));
}

////////////////////////////////////////////////////////////////////////////////
// cross product
////////////////////////////////////////////////////////////////////////////////

inline __host__ __device__ double3 cross(const double3 & a, const double3 & b)
{
    return make_double3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}
inline __host__ __device__ void cross(const double3 & a, const double3 & b, double3 & out)
{
	out.x = a.y*b.z - a.z*b.y;
	out.y = a.z*b.x - a.x*b.z;
	out.z = a.x*b.y - a.y*b.x;
}
inline __host__ __device__ double3 cross(const double4 & a, const double4 & b)
{
    return make_double3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}
inline __host__ __device__ double3 cross(const double3 & a, const double4 & b)
{
    return make_double3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}

////////////////////////////////////////////////////////////////////////////////
// matrix functions
////////////////////////////////////////////////////////////////////////////////

inline __host__ __device__ void outter_product_sym(const double3 & a, const double3 & b, double3 & diag, double3 & off_diag)
{
    diag.x = a.x*b.x;
    diag.y = a.y*b.y;
    diag.z = a.z*b.z;
    
    off_diag.x = a.x*b.y;
    off_diag.y = a.y*b.z;
    off_diag.z = a.x*b.z;
}

inline __host__ __device__ void outter_product(const double3 & a, const double3 & b, double3 & diag, double3 & off_diag_up, double3 & off_diag_lo)
{
    diag.x = a.x*b.x;
    diag.y = a.y*b.y;
    diag.z = a.z*b.z;
    
    off_diag_up.x = a.x*b.y;
    off_diag_up.y = a.y*b.z;
    off_diag_up.z = a.x*b.z;
    
    off_diag_lo.x = a.y*b.x;
    off_diag_lo.y = a.z*b.y;
    off_diag_lo.z = a.z*b.x;
}

inline __host__ __device__ void matrix_identity_sym(double3 & diag, double3 & off_diag)
{
    make_double3(diag, 1.);
    make_double3(off_diag, 0.);
}

inline __host__ __device__ void matrix_identity(double3 & diag, double3 & off_diag_up, double3 & off_diag_lo)
{
    make_double3(diag, 1.);
    make_double3(off_diag_up, 0.);
    make_double3(off_diag_lo, 0.);
}


inline __host__ __device__ void scalar_matrix_prod_self_sym(const double scalar, double3 & diag, double3 & off_diag)
{
    diag *= scalar;
    off_diag *= scalar;
}

inline __host__ __device__ void scalar_matrix_prod_sym(const double scalar, const double3 & in_diag, const double3 & in_off_diag, double3 & out_diag, double3 & out_off_diag)
{
    out_diag = in_diag*scalar;
    out_off_diag = in_off_diag*scalar;
}

inline __host__ __device__ void scalar_matrix_prod_self(const double scalar, double3 & diag, double3 & off_diag_up, double3 & off_diag_lo)
{
    diag *= scalar;
    off_diag_up *= scalar;
    off_diag_lo *= scalar;
}

inline __host__ __device__ void scalar_matrix_prod(const double scalar, const double3 & in_diag, const double3 & in_off_diag_up, const double3 & in_off_diag_lo, double3 & out_diag, double3 & out_off_diag_up, double3 & out_off_diag_lo)
{
    out_diag = in_diag*scalar;
    out_off_diag_up = in_off_diag_up*scalar;
    out_off_diag_lo = in_off_diag_lo*scalar;
}

inline __host__ __device__ void matrix_sum_self_sym(double3 & left_diag, double3 & left_off_diag, const double3 & right_diag, const double3 & right_off_diag)
{    
    left_diag += right_diag;
    left_off_diag += right_off_diag;
}

inline __host__ __device__ void matrix_sum_self_nosym_sym(double3 & left_diag, double3 & left_off_diag_up, double3 & left_off_diag_lo, const double3 & right_diag, const double3 & right_off_diag)
{
    left_diag += right_diag;
    left_off_diag_up += right_off_diag;
    left_off_diag_lo += right_off_diag;
}

inline __host__ __device__ void matrix_sum_self(double3 & left_diag, double3 & left_off_diag_up, double3 & left_off_diag_lo, const double3 & right_diag, const double3 & right_off_diag_up, const double3 & right_off_diag_lo)
{
    left_diag += right_diag;
    left_off_diag_up += right_off_diag_up;
    left_off_diag_lo += right_off_diag_lo;
}

inline __host__ __device__ void matrix_subs_self_sym(double3 & left_diag, double3 & left_off_diag, const double3 & right_diag, const double3 & right_off_diag)
{    
    left_diag -= right_diag;
    left_off_diag -= right_off_diag;
}

inline __host__ __device__ void matrix_subs_self(double3 & left_diag, double3 & left_off_diag_up, double3 & left_off_diag_lo, const double3 & right_diag, const double3 & right_off_diag_up, const double3 & right_off_diag_lo)
{
    left_diag -= right_diag;
    left_off_diag_up -= right_off_diag_up;
    left_off_diag_lo -= right_off_diag_lo;
}

inline __host__ __device__ void matrix_subs_self_nosym_sym(double3 & left_diag, double3 & left_off_diag_up, double3 & left_off_diag_lo, const double3 & right_diag, const double3 & right_off_diag)
{
    left_diag -= right_diag;
    left_off_diag_up -= right_off_diag;
    left_off_diag_lo -= right_off_diag;
}

inline __host__ __device__ void matrix_assign_sym(double3 & out_diag, double3 & out_off_diag, const double3 & in_diag, const double3 & in_off_diag)
{    
    out_diag = in_diag;
    out_off_diag = in_off_diag;
}

inline __host__ __device__ void matrix_transpose(const double3 & in_diag, const double3 & in_off_diag_up, const double3 & in_off_diag_lo, double3 & out_diag, double3 & out_off_diag_up, double3 & out_off_diag_lo)
{
    out_diag = in_diag;
    out_off_diag_up = in_off_diag_lo;
    out_off_diag_lo = in_off_diag_up;
}

inline __host__ __device__ void matrix_transpose_self(double3 & diag, double3 & off_diag_up, double3 & off_diag_lo)
{
    double3 temp = off_diag_up;
    off_diag_up = off_diag_lo;
    off_diag_lo = temp;
}

inline __host__ __device__ void matrix_vector_prod_sym(const double3 & diag, const double3 & off_diag, const double3 & v_in, double3 & v_out)
{
    v_out.x = diag.x*v_in.x + off_diag.x*v_in.y + off_diag.z*v_in.z;
    v_out.y = off_diag.x*v_in.x + diag.y*v_in.y + off_diag.y*v_in.z;
    v_out.z = off_diag.z*v_in.x + off_diag.y*v_in.y + diag.z*v_in.z;
}

inline __host__ __device__ void matrix_vector_prod(const double3 & diag, const double3 & off_diag_up, const double3 & off_diag_lo, const double3 & v_in, double3 & v_out)
{
    v_out.x = diag.x*v_in.x + off_diag_up.x*v_in.y + off_diag_up.z*v_in.z;
    v_out.y = off_diag_lo.x*v_in.x + diag.y*v_in.y + off_diag_up.y*v_in.z;
    v_out.z = off_diag_lo.z*v_in.x + off_diag_lo.y*v_in.y + diag.z*v_in.z;
}

inline __host__ __device__ void vector_matrix_prod(const double3 & v_in, const double3 & diag, const double3 & off_diag_up, const double3 & off_diag_lo, double3 & v_out)
{
    v_out.x = diag.x*v_in.x + off_diag_lo.x*v_in.y + off_diag_lo.z*v_in.z;
    v_out.y = off_diag_up.x*v_in.x + diag.y*v_in.y + off_diag_lo.y*v_in.z;
    v_out.z = off_diag_up.z*v_in.x + off_diag_up.y*v_in.y + diag.z*v_in.z;
}

inline __host__ __device__ void matrix_matrix_prod(const double3 & d, const double3 & u, const double3 & l, 
						    const double3 & d2, const double3 & u2, const double3 & l2,
						    double3 & out_diag, double3 & out_off_diag_up, double3 & out_off_diag_lo)
{
    out_diag.x = d.x*d2.x + u.x*l2.x + u.z*l2.z;
    out_off_diag_lo.x = l.x*d2.x + d.y*l2.x + u.y*l2.z;
    out_off_diag_lo.z = l.z*d2.x + l.y*l2.x + d.z*l2.z;
    
    out_off_diag_up.x = d.x*u2.x + u.x*d2.y + u.z*l2.y;
    out_diag.y = l.x*u2.x + d.y*d2.y + u.y*l2.y;
    out_off_diag_lo.y = l.z*u2.x + l.y*d2.y + d.z*l2.y;
    
    out_off_diag_up.z = d.x*u2.z + u.x*u2.y + u.z*d2.z;
    out_off_diag_up.y = l.x*u2.z + d.y*u2.y + u.y*d2.z;
    out_diag.z = l.z*u2.z + l.y*u2.y + d.z*d2.z;
}

inline __host__ __device__ void matrix_matrix_prod_self(double3 & d, double3 & u, double3 & l, 
                                                        const double3 & d2, const double3 & u2, const double3 & l2)
{
    double3 temp;
    
    // first row
    temp.x = d.x*d2.x + u.x*l2.x + u.z*l2.z;
    temp.y = d.x*u2.x + u.x*d2.y + u.z*l2.y;
    temp.z = d.x*u2.z + u.x*u2.y + u.z*d2.z;
    
    d.x = temp.x;
    u.x = temp.y;
    u.z = temp.z;
    
    // second row
    temp.x = l.x*d2.x + d.y*l2.x + u.y*l2.z;
    temp.y = l.x*u2.x + d.y*d2.y + u.y*l2.y;
    temp.z = l.x*u2.z + d.y*u2.y + u.y*d2.z;
    
    l.x = temp.x;
    d.y = temp.y;
    u.y = temp.z;
    
    // third row
    temp.x = l.z*d2.x + l.y*l2.x + d.z*l2.z;
    temp.y = l.z*u2.x + l.y*d2.y + d.z*l2.y;
    temp.z = l.z*u2.z + l.y*u2.y + d.z*d2.z;
    
    l.z = temp.x;
    l.y = temp.y;
    d.z = temp.z;
}

inline __host__ __device__ void matrix_skewsymmetric(const double3 v, double3 & out_diag, double3 & out_off_diag_up, double3 & out_off_diag_lo)
{
    make_double3(out_diag, 0.);
    out_off_diag_up = make_double3(-v.z, -v.x, v.y);
    out_off_diag_lo = make_double3(v.z, v.x, -v.y);
}


////////////////////////////////////////////////////////////////////////////////
// atomic functions
////////////////////////////////////////////////////////////////////////////////

/*
// AtomicAdd for doubles is not supported in hardware. Must use this software version.
inline __device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull =
                              (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                        __double_as_longlong(val +
                               __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}

inline __device__ void atomicAdd(double* array, unsigned int index, const double3 & val)
{
    atomicAdd(&array[index], val.x);
    atomicAdd(&array[index+1], val.y);
    atomicAdd(&array[index+2], val.z);
}

inline __device__ void atomicAdd(double4* array, unsigned int index, const double4 & val)
{
    atomicAdd(&array[index].x, val.x);
    atomicAdd(&array[index].y, val.y);
    atomicAdd(&array[index].z, val.z);
    atomicAdd(&array[index].w, val.w);
}*/


#endif // CUDA_UTILS_DEVICE_CUH
