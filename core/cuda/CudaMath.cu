
#include "CudaMath.cuh"

#include "CudaUtilsDevice.cuh"

#define BLOCK_SIZE 1024

__global__ void d_xpky(unsigned int length, double* x, double* y, double k, double* out)
{
	unsigned int index = threadIdx.x + blockDim.x * blockIdx.x;
    
	if (index > length-1) return;
	
	out[index] = x[index] + k*y[index];
}

__global__ void d_kxpkypz(unsigned int length, double* x, double* y, double k, double* z, double* out)
{
	unsigned int index = threadIdx.x + blockDim.x * blockIdx.x;
    
	if (index > length-1) return;
	
	out[index] = k*(x[index] + y[index]) + z[index];
}

void cuda_xpky(unsigned int length, double* x, double* y, double k, double* out)
{
	unsigned int blockSize = BLOCK_SIZE;
    unsigned int gridSize = length/blockSize + 1;
    d_xpky<<<gridSize, blockSize>>>(length, x, y, k, out);
}

void cuda_kxpkypz(unsigned int length, double* x, double* y, double k, double* z, double* out)
{
	unsigned int blockSize = BLOCK_SIZE;
    unsigned int gridSize = length/blockSize + 1;
    d_kxpkypz<<<gridSize, blockSize>>>(length, x, y, k, z, out);
}
