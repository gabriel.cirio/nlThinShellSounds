
#ifndef DESILICO_CUDA_MATH_H
#define DESILICO_CUDA_MATH_H

void cuda_xpky(unsigned int length, double* x, double* y, double k, double* out);
void cuda_kxpkypz(unsigned int length, double* x, double* y, double k, double* z, double* out);

#endif // DESILICO_CUDA_MATH_H
