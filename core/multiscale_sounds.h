
#ifndef MULTISCALE_SOUNDS_H
#define MULTISCALE_SOUNDS_H

#include "typedefs.h"

#include <vector>

class DynamicObject;
class Solver;

class MultiScaleSounds
{
	public:
		
		MultiScaleSounds(bool is_cli=false);
		~MultiScaleSounds() {}
		
		bool load_config(std::string config_filename);
        
		void prepare();
		void simulate_step();
		bool is_valid() { return m_is_valid; }
		void set_valid(bool valid) { m_is_valid = valid; }
		
		void get_data_meshes(std::vector<std::pair<MatrixX, MatrixXi> > & meshes);
		void get_data_nodal_vectors(std::vector<MatrixX> & vectors);
        void get_data_world_vectors(std::vector<std::pair<MatrixX, MatrixX>> & vectors);
		
		void set_key(unsigned char key);
		
		Real getElapsedTime() { return m_elapsed_time; }
		
	private:
		
		bool m_is_valid;
		bool m_is_prepared;
		std::vector<DynamicObject*> m_dynamic_objects;
		Solver* m_solver;
		Real m_elapsed_time;
		int m_dt_substeps;
		
		// control
		bool m_is_cli;
		Real m_duration;
        
        // render meshes
        bool m_mesh_write;
        Real m_mesh_write_fps;
        int m_mesh_write_last_frame;
};

#endif // MULTISCALE_SOUNDS_H
