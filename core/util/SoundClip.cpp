
#include "SoundClip.h"

#include <sndfile.hh>

#include <iostream>
#include <algorithm>

Real SoundClip::writeWAV(std::string filename, std::vector<double> data, int srate, int channels)
{
	// first normalize the input data
	auto max_it = std::max_element(data.begin(), data.end(), [] (double a, double b) -> bool { return (std::abs(a) < std::abs(b)); });
	double max_val = *max_it;
	
	for (int i=0; i<data.size(); i++)
		data[i] /= max_val;
	
	int format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;

	SndfileHandle file = SndfileHandle (filename.c_str(), SFM_WRITE, format, channels, srate);

	file.write(data.data(), data.size());
    
    return max_val;

}

