
#include "Plotter.h"

#include <mgl2/mgl.h>
#include <mgl2/data.h>

#include <iostream>
#include <cstdlib>

void Plotter::plotToFile(const std::vector<Real> & input_data, std::string filename, std::string title, std::string x_axis, std::string y_axis)
{
	MatrixX data(input_data.size(), 1);
	for (int i=0; i<data.rows(); i++)
		data(i,0) = input_data[i];
	
	plotToFile(data, filename, title, x_axis, y_axis);
}

void Plotter::plotToFile(const std::vector<VectorX> & input_data, std::string filename, std::string title, std::string x_axis, std::string y_axis)
{
	MatrixX data(input_data.size(), input_data[0].rows());
	for (int i=0; i<data.rows(); i++)
		data.row(i) = input_data[i];
	
	plotToFile(data, filename, title, x_axis, y_axis);
}

void Plotter::plotToFile(const MatrixX & input_data, std::string filename, std::string title, std::string x_axis, std::string y_axis)
{
	mglData data;
	int length = input_data.rows();
	int curves = input_data.cols();
	data.Create(length, curves);
	for (int i=0; i<curves; i++)
		for (int j=0; j<length; j++)
			data.a[i*length + j] = input_data(j,i);
		
	mglGraph gr(0,1280,720);
	
	gr.SetRange('y', data); // automatically compute ranges
	
	gr.Title(title.c_str());
	//gr->Box();  
	//gr.Axis("xy");
	gr.Axis("y");
	//gr.Label('x', x_axis.c_str());
	gr.Label('y', y_axis.c_str());
	
	gr.Plot(data);
	
	gr.WriteFrame(filename.c_str());
}

void Plotter::showPlot(std::string filename)
{
	std::string call = "gwenview --geometry +1920+100 " + filename + " &";
	int ret = std::system(call.c_str()); // & for non-blocking, --geometry to open in second screen
	if (ret != 0)
		std::cout << "Error while opening plot file!" << std::endl;
}

