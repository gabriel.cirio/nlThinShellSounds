#ifndef JSONCONFIGURATION_H
#define JSONCONFIGURATION_H

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/error/en.h"
#include <iostream>

class JSONConfiguration
{
    
public:
    
    typedef rapidjson::GenericObject<true, rapidjson::GenericValue<rapidjson::UTF8<> > > Object;
    
    JSONConfiguration();
    ~JSONConfiguration();
    
    bool loadSceneFile(std::string file_path);
    const rapidjson::Document& getSceneConfigurationTree(){return m_scene_configuration_tree;}
    
private:
    
    bool parseFile(std::string file_path);
    bool loadSceneJSON(std::string json_string);

protected:
    
    std::string m_json_string;
    rapidjson::Document m_scene_configuration_tree;
};

#endif //JSONCONFIGURATION_H
