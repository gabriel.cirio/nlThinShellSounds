#ifndef HASHGRID_H 
#define HASHGRID_H 
// The original hash table code was provided by: 
// Bruno Heidelberger, Matthias Müller, Matthias Teschner
 
#include "typedefs.h" 

#include <iostream> 
#include <vector>
#include <set> 
 
template <class T> 
class HashGrid 
{ 
    public:  
         
        HashGrid(void) 
        { 
            mTime=1; 
 
            mHashIndex=new Root[mHashIndexSize]; 
            for(unsigned int i = 0; i < mHashIndexSize; i++) { 
                mHashIndex[i].first = -1; 
                mHashIndex[i].timeStamp = 0; 
            } 
            mSpacing = 1.; 
            mInvSpacing = 1./mSpacing; 
        } 
 
        ~HashGrid(void) 
        { 
            delete[] mHashIndex; 
        } 
 
        void SetGridSpacing(Real spacing) 
        { 
            mSpacing = spacing; 
            mInvSpacing = 1./mSpacing; 
            Reset(); 
        } 
 
        void Reset(void) 
        { 
            mTime++; 
            mEntries.resize(0); 
        } 
 
        bool AddPoint(const Vector3 & point, T value) 
        { 
            int x,y,z; 
            cellCoordOf(point, x,y,z); 
 
            Entry entry(value); 
            unsigned int h; 
            int n; 
 
            h = hashFunction(x,y,z); 
            Root &r = mHashIndex[h]; 
            n = (int)mEntries.size(); 
            entry.next = (r.timeStamp != mTime || r.first < 0) ? -1 : r.first; 
            r.first = n; 
            r.timeStamp = mTime; 
            mEntries.push_back(entry); 
                 
            return true; 
        } 
 
        bool AddBox(const Vector3 & min, const Vector3 & max, T value) 
        { 
            int x1,y1,z1; 
            int x2,y2,z2; 
            cellCoordOf(min, x1,y1,z1); 
            cellCoordOf(max, x2,y2,z2); 
 
            int size = (x2-x1)*(y2-y1)*(z2-z1); 
            if (fabs((double)size) > 1e6) 
            { 
                std::cerr << "Don't add box to grid! Way too large!" << std::endl; 
                return false; 
            } 
 
            Entry entry(value); 
            unsigned int h; 
            int n; 
            for (int x = x1; x <= x2; x++) 
            { 
                for (int y = y1; y <= y2; y++) 
                { 
                    for (int z = z1; z <= z2; z++) 
                    { 
                        h = hashFunction(x,y,z); 
                        Root &r = mHashIndex[h]; 
                        n = (int)mEntries.size(); 
                        entry.next = (r.timeStamp != mTime || r.first < 0) ? -1 : r.first; 
                        r.first = n; 
                        r.timeStamp = mTime; 
                        mEntries.push_back(entry); 
                    } 
                } 
            } 
 
            return true; 
        } 
 
        bool QueryPoint(const Vector3& p, std::vector<T> & values) const 
        { 
            int x,y,z; 
            cellCoordOf(p, x,y,z); 
 
            unsigned int h = hashFunction(x,y,z); 
            Root &r = mHashIndex[h]; 
             
            if (r.timeStamp != mTime) return false; 
             
            for(int i = r.first; i >= 0; ) 
            { 
                const Entry &entry = mEntries[i]; 
                values.push_back(entry.value); 
                 
                i = entry.next; 
            } 
 
            return true; 
        } 
 
        bool QueryBox(const Vector3 & min, const Vector3 & max, std::vector<T> & values) 
        { 
            int x1,y1,z1; 
            int x2,y2,z2; 
            cellCoordOf(min, x1,y1,z1); 
            cellCoordOf(max, x2,y2,z2); 
       
            int size = (x2-x1)*(y2-y1)*(z2-z1); 
       
            if (fabs((double)size) > 1e6) 
            { 
                //cerr << "Don't query box! Way too large!" << endl; 
                return false; 
            } 
       
            return QueryBox(x1, x2, y1, y2, z1, z2, values); 
        } 

        bool QueryBoxParallel(const Vector3 & min, const Vector3 & max, std::vector<T> & values)
        {
            int x1,y1,z1;
            int x2,y2,z2;
            cellCoordOf(min, x1,y1,z1);
            cellCoordOf(max, x2,y2,z2);

            int size = (x2-x1)*(y2-y1)*(z2-z1);

            if (fabs((double)size) > 1e6)
            {
                //cerr << "Don't query box! Way too large!" << endl;
                return false;
            }

            return QueryBoxParallel(x1, x2, y1, y2, z1, z2, values);
        }
 
        bool QueryBox(int x1, int x2, int y1, int y2, int z1, int z2, std::vector<T> & values) 
        { 
            std::vector<int> read_entries; 
            for (int x = x1; x <= x2; x++) 
            { 
                for (int y = y1; y <= y2; y++) 
                { 
                    for (int z = z1; z <= z2; z++) 
                    { 
                        unsigned int h = hashFunction(x,y,z); 
                        Root &r = mHashIndex[h]; 
                        if (r.timeStamp != mTime) continue; 
                        int i = r.first; 
                        while (i >= 0) 
                        { 
                            Entry &entry = mEntries[i]; 
                            if(!entry.flag)
                            { 
                                entry.flag = true; 
                                read_entries.push_back(i); 
                                values.push_back(entry.value); 
                            } 
                            i = entry.next; 
                        } 
                    } 
                } 
            } 
 
            for (unsigned int i = 0; i < read_entries.size(); i++) 
            { 
                mEntries[read_entries[i]].flag = false; 
            } 
 
            return true; 
        } 

        bool QueryBoxParallel(int x1, int x2, int y1, int y2, int z1, int z2, std::vector<T> & values)
        {
            std::set<int> read_entries;

            for (int x = x1; x <= x2; x++)
            {
                for (int y = y1; y <= y2; y++)
                {
                    for (int z = z1; z <= z2; z++)
                    {
                        unsigned int h = hashFunction(x,y,z);
                        Root &r = mHashIndex[h];
                        if (r.timeStamp != mTime) continue;
                        int i = r.first;
                        while (i >= 0)
                        {
                            Entry &entry = mEntries[i];
                            if (read_entries.find(i) == read_entries.end())
                            {
                                read_entries.insert(i);
                                values.push_back(entry.value);
                            }
                            i = entry.next;
                        }
                    }
                }
            }

            return true;
        }
 
        inline unsigned int hashFunction(int xi, int yi, int zi) const 
        {  
            unsigned int h = (xi * 92837111)^(yi * 689287499)^(zi * 283923481); 
            return h % mHashIndexSize; 
        } 
 
        inline void cellCoordOf(const Vector3 &v, int &xi, int &yi, int &zi) const 
        { 
            xi = (int)(v[0] * mInvSpacing); if (v[0] < 0.) xi--; 
            yi = (int)(v[1] * mInvSpacing); if (v[1] < 0.) yi--; 
            zi = (int)(v[2] * mInvSpacing); if (v[2] < 0.) zi--; 
        } 
         
    protected: 
         
        struct Root 
        { 
            int first; 
            unsigned int timeStamp; 
        }; 
 
    struct Entry 
        { 
            T value; 
            int next; 
            bool flag; 
 
            Entry(void) : flag(false) {} 
            Entry(T & val) : value(val), flag(false) {} 
 
        }; 
 
    protected: 
         
        static const unsigned int mHashIndexSize = 1349533; 
 
        Real mSpacing; 
        Real mInvSpacing; 
        unsigned int mTime; 
 
        Root *mHashIndex; 
        std::vector<Entry> mEntries; 
 
}; 
 
//-------------------------------------------------------------- 
// constants 
//-------------------------------------------------------------- 
 
//const unsigned int HashGrid::mHashIndexSize = 17011; 
//const unsigned int HashGrid::mHashIndexSize = 155921; 
 
//const unsigned int HashGrid<T>::mHashIndexSize = 1349533; 
 
//const unsigned int HashGrid::mHashIndexSize = 4652353; 
 
//const unsigned int HashGrid::mHashIndexSize = 122164747; 
 
//r320 for 120 
//const unsigned int HashGrid::mHashIndexSize = 46080000; 
//r100 for 120 
//const unsigned int HashGrid::mHashIndexSize = 14400000; 
//r320 for 80 
//const unsigned int HashGrid::mHashIndexSize = 2048000; 
//r320 for 40 
//const unsigned int HashGrid::mHashIndexSize = 512000; 
//r320 for 20 
//const unsigned int HashGrid::mHashIndexSize = 128000; 
//r320 for 10 
//const unsigned int HashGrid::mHashIndexSize = 32000; 
 
 
#endif 
