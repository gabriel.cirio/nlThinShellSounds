
#ifndef PLOTTER_H
#define PLOTTER_H

#include "typedefs.h"
	
class Plotter
{
	public:
		
		Plotter() {}
		~Plotter() {}
		
		static void plotToFile(const std::vector<Real> & input_data, std::string filename, std::string title="", std::string x_axis="", std::string y_axis="");
		static void plotToFile(const std::vector<VectorX> & input_data, std::string filename, std::string title="", std::string x_axis="", std::string y_axis="");
		static void plotToFile(const MatrixX & input_data, std::string filename, std::string title="", std::string x_axis="", std::string y_axis="");
		static void showPlot(std::string filename);

};

#endif // PLOTTER_H

