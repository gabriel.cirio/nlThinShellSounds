
#ifndef MULTISCALESOUNDS_TIMER_H
#define MULTISCALESOUNDS_TIMER_H

#include <iostream>
#include <vector>
#include <chrono>
#include <ctime>

class Timer
{
	public:
		
		static void tic(std::string msg="");
		static double toc(bool print=true);
		static void disable_print() { m_disable_print = true; }
		static void enable_print() { m_disable_print = false; }

	public:

		Timer() {}
		~Timer() {}
		
	private:
		
		static std::vector<std::chrono::time_point<std::chrono::system_clock> > s_start;
		static std::vector<std::string> s_msg;
		static bool m_disable_print;
		

};

#endif // MULTISCALESOUNDS_TIMER_H
