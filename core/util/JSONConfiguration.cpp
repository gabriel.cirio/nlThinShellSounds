#include "JSONConfiguration.h"
#include <string>
#include <fstream>

JSONConfiguration::JSONConfiguration()
{
}

bool JSONConfiguration::loadSceneFile(std::string file_path)
{
	bool result = parseFile(file_path);
	if (result) result = loadSceneJSON(m_json_string);
	return result;
}

JSONConfiguration::~JSONConfiguration()
{
}

bool JSONConfiguration::parseFile(std::string file_path)
{

	std::ifstream file;
	file.open(file_path);
	if (file.is_open()) {
		m_json_string.clear();
		std::string str;
		bool inside_comment_block = false;
		while (std::getline(file, str))
		{
			if (!inside_comment_block) {
				size_t pos = str.find("/*");
				if (pos != std::string::npos) {
					str = str.substr(0, pos);
					inside_comment_block = true;
				}
				else {
					pos = str.find("//");
					if (pos != std::string::npos) {
						str = str.substr(0, pos);
					}
				}
			}
			else {
				size_t pos = str.find("*/");
				if (pos != std::string::npos) {
					str = str.substr(pos+2);
					inside_comment_block = false;
				}
				else {
					str = "";
				}
			}
			m_json_string += str;
		}
		file.close();
		return true;
	}
	else {
		std::cerr << "Configuration file not found on " << file_path << ".\n";
		std::cerr << "Empty simulation loaded.\n";
	}
	return false;
}

bool JSONConfiguration::loadSceneJSON(std::string json_string)
{
	rapidjson::ParseResult ok = m_scene_configuration_tree.Parse(m_json_string.c_str());
	if (!ok) {
		std::cerr << "JSON parse error: " << rapidjson::GetParseError_En(ok.Code()) << " In position " << ok.Offset() << "\n";
		int init_pos, length;
		init_pos = (int)(ok.Offset()) - 20;
		if (init_pos < 0) init_pos = 0;
		length = 40;
		if (length + init_pos > m_json_string.size()) length = m_json_string.size() - init_pos;
		std::cerr << "Text around error: " << m_json_string.substr(init_pos, length) << "\n";
		std::cerr << "Empty simulation loaded.\n";
		return false;
	}
	return true;
}
