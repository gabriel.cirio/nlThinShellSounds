
#ifndef SOUNDCLIP_H
#define SOUNDCLIP_H

#include "typedefs.h"
#include <string>
#include <vector>
	
class SoundClip
{
	public:
		
		SoundClip() {}
		~SoundClip() {}
		
		static Real writeWAV(std::string filename, std::vector<double> data, int srate = 44100, int channels = 1); // data is copied intentionally
		

};

#endif // SOUNDCLIP_H

