
#include "Timer.h"

std::vector<std::chrono::time_point<std::chrono::system_clock> > Timer::s_start;
std::vector<std::string> Timer::s_msg;
bool Timer::m_disable_print = false;

void Timer::tic(std::string msg)
{
	s_start.push_back(std::chrono::system_clock::now());
	s_msg.push_back(msg);
}

double Timer::toc(bool print)
{
	std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now() - s_start.back();
	s_start.pop_back();
	
	std::string msg = s_msg.back();
	s_msg.pop_back();
	
	if (print && !m_disable_print) 
		std::cout << msg << elapsed_seconds.count() << std::endl;
	
	return elapsed_seconds.count();
}
