
#ifndef GRAVITY_ENERGY_H
#define GRAVITY_ENERGY_H

#include "Energy.h"

class GravityEnergy : public Energy
{
	public:
		
		GravityEnergy(VectorX & node_rest_area, Real density, Real thickness, Vector3 gravity_vector);
		~GravityEnergy() {}
		
		void setup() {}
		void prepareForStep() {}
		void computeForces(std::vector<MatrixX> & force_omp_arrays);
		void computeDfDx(std::vector<std::vector<Jacobian> > & dfdx) {}
		
	private:
		
		VectorX & m_node_rest_area;
		Real m_density;
		Real m_thickness;
		Vector3 m_gravity_vector;
	
};

#endif // GRAVITY_ENERGY_H



