
#ifndef MULTISCALESOUNDS_FASTBEMFILEGENERATOR_H
#define MULTISCALESOUNDS_FASTBEMFILEGENERATOR_H

#include "typedefs.h"

class FastBEMFileGenerator
{
	
	public:
		
		static void writeFiles(std::string path_prefix, const MatrixX & V, const MatrixXi & F, const MatrixX & evectors, const VectorX & evalues, const MatrixX & fieldpoints);
		
	private:
		
		FastBEMFileGenerator() {} // so that we cannot instantiate this object
};

#endif // MULTISCALESOUNDS_FASTBEMFILEGENERATOR_H
