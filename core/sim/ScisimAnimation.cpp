
#include "ScisimAnimation.h"

#include "util/HashGrid.h"

#include <igl/readOBJ.h>
#include <igl/writeOBJ.h>
#include <igl/decimate.h>

#include <iostream>
#include <fstream>

void ScisimAnimationObject::addTimestep(const Vector3 & pos, const Matrix3 & rot, const Vector3 & vel, const Vector3 & w)
{
    state.push_back(State());
    state.back().pos = pos;
    state.back().rot = rot;
    state.back().vel = vel;
    state.back().w = w;
    collisions.push_back(std::vector<Collision>());
}
        
void ScisimAnimationObject::addCollisionAtCurrentStep(const Collision & col)
{
    collisions.back().push_back(col);
}

void ScisimAnimationObject::computeCollisionLocalData(Real force_scale)
{
    // converts world data from scisim into local data from this project (converts to local and then rotates by R)
    
    HashGrid<int> grid;
    
    grid.SetGridSpacing(0.01); // 1cm
    for (int i=0; i<restV.rows(); i++)
        grid.AddPoint(restV.row(i).transpose(), i);
    
    for (int t=0; t<state.size(); t++)
    {
        if (collisions[t].size())
        {
            for (auto & col : collisions[t])
            {
                Vector3 local_point = state[t].rot.transpose()*(col.world_point - state[t].pos);
                
                // a cube with 2 cm sides to account for errors in the distance field of the scisim collision detection
                Vector3 box_min = local_point - 0.01*Vector3::Ones();
                Vector3 box_max = local_point + 0.01*Vector3::Ones();
                std::vector<int> potential_closest_nodes;
                grid.QueryBox(box_min, box_max, potential_closest_nodes);
                
                int closest_node = -1;
                Real closest_sqdist = 99999999.;
                for (int i=0; i<potential_closest_nodes.size(); i++)
                {
                    Real sqdist = (restV.row(potential_closest_nodes[i]).transpose() - local_point).squaredNorm();
                    if (sqdist < closest_sqdist) 
                    {
                        closest_sqdist = sqdist;
                        closest_node = potential_closest_nodes[i];
                    }
                }
                
                // just in case...
                if (closest_sqdist > 0.1)
                {
                    std::cout << "WARNING: HashGrid point search failed, falling back to brute force search." << std::endl;
                    closest_node = -1;
                    closest_sqdist = 99999999.;
                    for (int i=0; i<restV.rows(); i++)
                    {
                        Real sqdist = ((state[t].pos + state[t].rot*restV.row(i).transpose())-col.world_point).squaredNorm();
                        if (sqdist < closest_sqdist) 
                        {
                            closest_sqdist = sqdist;
                            closest_node = i;
                        }
                    }
                }
                
                col.node = closest_node;
                col.local_point = R*local_point;
                col.local_normal = R*state[t].rot.transpose()*col.world_normal;
                col.local_force = R*state[t].rot.transpose()*col.world_force*force_scale;
            }
        }
    }
}

ScisimAnimation::ScisimAnimation() : m_timestep_nb(0), m_force_scale(1.)
{
}

void ScisimAnimation::configure(const JSONConfiguration::Object & config, const std::string & base_path)
{
    std::string animation_filename = config["name"].GetString();
    if (config.HasMember("force_scale")) m_force_scale = config["force_scale"].GetDouble();
    
    this->read(base_path + animation_filename + ".bin");
}

void ScisimAnimation::read(std::string sbf_filename)
{
    std::ifstream sbf(sbf_filename, std::ios::binary);
    if (!sbf)
    {
        std::cout << "Could not load " << sbf_filename << std::endl;
    }
    
    Real to_meters = 0.01;
    
    int num_objects;
    sbf.read((char*)(&num_objects),sizeof(num_objects));
    
    m_objects.resize(num_objects);
    m_animation_timesteps = 0;
    
    for (int i=0; i<num_objects; i++)
    {
        int Vrows;
        sbf.read((char*)(&Vrows),sizeof(Vrows));
        m_objects[i].restV.resize(Vrows,3);
        sbf.read((char*)(m_objects[i].restV.data()),Vrows*3*sizeof(double));
        m_objects[i].restV *= to_meters;
        
        int Frows;
        sbf.read((char*)(&Frows),sizeof(Frows));
        m_objects[i].F.resize(Frows,3);
        sbf.read((char*)(m_objects[i].F.data()),Frows*3*sizeof(int));
        
        sbf.read((char*)(&m_objects[i].cm),m_objects[i].cm.rows()*sizeof(double));
        sbf.read((char*)(&m_objects[i].R),m_objects[i].R.rows()*m_objects[i].R.cols()*sizeof(double));
        m_objects[i].cm *= to_meters;
        
        // TEMP!
        //igl::readOBJ("trash_can.obj", m_objects[i].restV, m_objects[i].F);
        
        // create coarse mesh for rendering
        Eigen::VectorXi J,I;
		igl::decimate(m_objects[i].restV, m_objects[i].F, 6000, m_objects[i].coarse_restV, m_objects[i].coarse_F, J, I);
        //m_objects[i].coarse_restV = m_objects[i].restV;
        //m_objects[i].coarse_F = m_objects[i].F;
    }
    
    // header
    int qsize;
    sbf.read((char*)(&qsize),sizeof(qsize));
    int vsize;
    sbf.read((char*)(&vsize),sizeof(vsize));
    
    while (sbf)
    {
        // timestep
        sbf.read((char*)(&m_animation_dt),sizeof(m_animation_dt));
        unsigned int iteration;
        sbf.read((char*)(&iteration),sizeof(iteration));
        double time;
        sbf.read((char*)(&time),sizeof(time));
        
        m_animation_timesteps++;
        
        VectorX q(qsize);
        VectorX v(vsize);
        sbf.read((char*)q.data(),qsize*sizeof(double)); // q
        sbf.read((char*)v.data(),vsize*sizeof(double)); // v
        
        for (unsigned int obj=0; obj<num_objects; obj++)
        {
            Vector3 pos = q.segment(obj*3,3)*to_meters;
            Matrix3 rot;
            rot << q(num_objects*3+obj*9+0), q(num_objects*3+obj*9+1), q(num_objects*3+obj*9+2), q(num_objects*3+obj*9+3), q(num_objects*3+obj*9+4), q(num_objects*3+obj*9+5), q(num_objects*3+obj*9+6), q(num_objects*3+obj*9+7), q(num_objects*3+obj*9+8);
            Vector3 vel = v.segment(obj*3,3)*to_meters;
            Vector3 w = v.segment((num_objects+obj)*3,3);
            m_objects[obj].addTimestep(pos, rot, vel, w);
        }
        
        // collisions
        unsigned int ncons;
        sbf.read((char*)(&ncons),sizeof(ncons));
        
        if (ncons && sbf)
        {
            // indices of all bodies involved
            VectorXi col_indices(ncons*2);
            sbf.read((char*)col_indices.data(),col_indices.rows()*sizeof(int));
            
            // world space contact points
            VectorX col_points(ncons*3);
            sbf.read((char*)col_points.data(),col_points.rows()*sizeof(double));
            
            // world space contact normals
            VectorX col_normals(ncons*3);
            sbf.read((char*)col_normals.data(),col_normals.rows()*sizeof(double));
            
            // collision forces
            VectorX col_forces(ncons*3);
            sbf.read((char*)col_forces.data(),col_forces.rows()*sizeof(double));
            
            // save
            for (int i=0; i<ncons; i++)
            {       
                ScisimAnimationObject::Collision col;
                col.world_point = col_points.segment(i*3,3)*to_meters;
                col.world_normal = col_normals.segment(i*3,3);
                if (col_indices(i*2+0) >= 0)
                {
                    col.other_body_index = col_indices(i*2+1);
                    col.world_force = col_forces.segment(i*3,3)*to_meters;
                    m_objects[col_indices(i*2+0)].addCollisionAtCurrentStep(col);
                }
                if (col_indices(i*2+1) >= 0)
                {
                    col.other_body_index = col_indices(i*2+0);
                    col.world_force = -col_forces.segment(i*3,3)*to_meters;
                    m_objects[col_indices(i*2+1)].addCollisionAtCurrentStep(col);
                }
            }
        }
    }
    
    std::cout << "Computing collision local data..." << std::endl;
    
    for (auto & obj : m_objects)
    {
        obj.dt = m_animation_dt;
        obj.computeCollisionLocalData(m_force_scale);
    }
    
    std::cout << "...done" << std::endl;
}

void ScisimAnimation::prepareForSimulation()
{   
}

void ScisimAnimation::prepareForStep(Real current_time, Real dt)
{
    if (current_time <= m_animation_timesteps*m_animation_dt)
        m_timestep_nb = std::round(current_time/m_animation_dt);
    else
        m_timestep_nb = m_animation_timesteps-1;
}

void ScisimAnimation::getMeshData(std::pair<MatrixX, MatrixXi> & mesh_data)
{
    int tot_Vrows = 0;
    int tot_Frows = 0;
    
    for (int obj=0; obj<m_objects.size(); obj++)
    {
        tot_Vrows += m_objects[obj].coarse_restV.rows();
        tot_Frows += m_objects[obj].coarse_F.rows();
    }
    
    mesh_data.first.resize(tot_Vrows,3);
    int current_row = 0;
    std::vector<int> cumul_size;
    for (int obj=0; obj<m_objects.size(); obj++)
    {
        cumul_size.push_back(current_row);
        for (int i=0; i<m_objects[obj].coarse_restV.rows(); i++)
        {
            mesh_data.first.row(current_row) = m_objects[obj].state[m_timestep_nb].pos + m_objects[obj].state[m_timestep_nb].rot*m_objects[obj].coarse_restV.row(i).transpose();
            //mesh_data.first.row(current_row) = m_objects[obj].R*m_objects[obj].coarse_restV.row(i).transpose();
            //mesh_data.first.row(current_row) = m_objects[obj].coarse_restV.row(i).transpose();
            current_row++;
        }
    }
    
    mesh_data.second.resize(tot_Frows,3);
    int current_size = 0;
    for (int obj=0; obj<m_objects.size(); obj++)
    {
        mesh_data.second.block(current_size, 0, m_objects[obj].coarse_F.rows(), 3) = m_objects[obj].coarse_F;
        mesh_data.second.block(current_size, 0, m_objects[obj].coarse_F.rows(), 3).array() += cumul_size[obj]; //.cwiseSum(current_size);
        current_size += m_objects[obj].coarse_F.rows();
    }
    
}

void ScisimAnimation::writeMeshes(std::string base_filename)
{
    for (int obj=0; obj<m_objects.size(); obj++)
    {
        MatrixX V(m_objects[obj].restV.rows(), 3);
        
        for (int i=0; i<m_objects[obj].restV.rows(); i++)
        {
            V.row(i) = m_objects[obj].state[m_timestep_nb].pos + m_objects[obj].state[m_timestep_nb].rot*m_objects[obj].restV.row(i).transpose();
        }
        
        igl::writeOBJ("anim_" + std::to_string(obj) + "_" + base_filename + ".obj", V, m_objects[obj].F);
        //igl::writeOBJ(base_filename + ".obj", V, m_objects[obj].F);
    }
    
}

void ScisimAnimation::getWorldVectors(std::pair<MatrixX, MatrixX> & vectors)
{
    //static std::vector<Vector3> start;
    //static std::vector<Vector3> end;
    std::vector<Vector3> start;
    std::vector<Vector3> end;
    
    for (int obj=0; obj<m_objects.size(); obj++)
    {
        if (m_objects[obj].collisions[m_timestep_nb].size())
        {
            //std::cout << "It " << m_timestep_nb << ": Obj " << obj << " " << m_objects[obj].collisions[m_timestep_nb].size() << " collisions." << std::endl;
            for (const auto & col : m_objects[obj].collisions[m_timestep_nb])
            {
                start.push_back(col.world_point);
                end.push_back(start.back()+col.world_force);
                //start.push_back(m_objects[obj].state[m_timestep_nb].rot.transpose()*(col.world_point - m_objects[obj].state[m_timestep_nb].pos));
                //end.push_back(start.back()+m_objects[obj].state[m_timestep_nb].rot.transpose()*(col.world_force));
            }
        }
    }
    
    vectors.first.resize(start.size(), 3);
    vectors.second.resize(end.size(), 3);
    
    for (int i=0; i<start.size(); i++)
    {
        vectors.first.row(i) = start[i];
        vectors.second.row(i) = end[i];
    }
}

void ScisimAnimation::getNodalVectors(MatrixX & vectors)
{
    
}

