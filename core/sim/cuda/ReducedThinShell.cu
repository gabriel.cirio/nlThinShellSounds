
#include "ReducedThinShell.cuh"

#include "cuda/CudaUtilsDevice.cuh"

__global__ void d_compute_cubic_elem_vector(int num_fake_dofs, double* fake_dofs_gpu, double* cubic_elem_vector_gpu)
{
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	int j = threadIdx.y + blockDim.y * blockIdx.y;
	int k = threadIdx.z + blockDim.z * blockIdx.z;
	if ( (i > (num_fake_dofs-1)) || (j > (num_fake_dofs-1)) || (k > (num_fake_dofs-1)) ) return;
	if ( (i > j) || (j > k) ) return; // NOT GREAT! WE ARE SACRIFICING 3/4 OR THE THREADS. USE A LOOKUP TABLE INSTEAD TO KNOW WHAT i,j,k WE ARE FROM A SINGLE INDEX??
	
	int index = (num_fake_dofs)*((num_fake_dofs)*i - 0.5*i*(i-1)) - 0.5*(num_fake_dofs-1)*(num_fake_dofs)*i + 0.5*i*(i-1)*(2.*i-1)/6. - 0.5*0.5*i*(i-1)
				+ (num_fake_dofs)*(j-i) - 0.5*(j)*(j-1) + 0.5*i*(i-1) 
				+ (k-j);
    
	cubic_elem_vector_gpu[index] = fake_dofs_gpu[i]*fake_dofs_gpu[j]*fake_dofs_gpu[k];
}

void cuda_compute_cubic_elem_vector(int num_fake_dofs, double* fake_dofs_gpu, double* cubic_elem_vector_gpu)
{
	dim3 threadsPerBlock(8, 8, 8); // so we get 512 threads per block
    dim3 numBlocks(num_fake_dofs/threadsPerBlock.x+1, num_fake_dofs/threadsPerBlock.y+1, num_fake_dofs/threadsPerBlock.z+1);
    
	d_compute_cubic_elem_vector<<<numBlocks, threadsPerBlock>>>(num_fake_dofs, fake_dofs_gpu, cubic_elem_vector_gpu);
}
