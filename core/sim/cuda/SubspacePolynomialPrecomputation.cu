
#include "SubspacePolynomialPrecomputation.cuh"

#include "cuda/CudaUtilsDevice.cuh"

#include <iostream>

#define NUM_THREADS_PER_BLOCK 512 // must be higher than nb_red_dofs

__global__ void d_to_mat_cubic(int nb_threads, int nb_red_dofs, int dof0, int dof1, int dof2, double valx, double valy, double valz, int3* thread_map, double* Ut, double* Cr_v)
{
	int index = threadIdx.x + blockDim.x * blockIdx.x;
	
	extern __shared__ double U012[];
	
	double* U0 = &U012[0];
	double* U1 = &U012[nb_red_dofs];
	double* U2 = &U012[nb_red_dofs*2];
	
	if (threadIdx.x < nb_red_dofs)
	{
		U0[threadIdx.x] = Ut[dof0*nb_red_dofs+threadIdx.x];
		U1[threadIdx.x] = Ut[dof1*nb_red_dofs+threadIdx.x];
		U2[threadIdx.x] = Ut[dof2*nb_red_dofs+threadIdx.x];
	}
	
	__syncthreads();
	
	if (index > nb_threads-1) return;
	int3 coords = thread_map[index];
	
	double U0x = U0[coords.x];
	double U1y = U1[coords.y];
	double U2z = U2[coords.z];

	double val_red = U0x*U1y*U2z;
	
	if (!( (coords.x == coords.y) && (coords.y == coords.z) )) // not all the same
	{
		double U1x = U1[coords.x];
		double U2x = U2[coords.x];
		
		double U0y = U0[coords.y];
		double U2y = U2[coords.y];
		
		double U0z = U0[coords.z];
		double U1z = U1[coords.z];
		
		if (coords.x == coords.y)
		{
			val_red += U0x*U1z*U2x + U0z*U1x*U2x;
		}
		else if (coords.x == coords.z)
		{
			val_red += U0x*U1x*U2y + U0y*U1x*U2x;
		}
		else if (coords.y == coords.z)
		{
			val_red += U0y*U1y*U2x + U0y*U1x*U2y;
		}
		else // all different
		{
			val_red += U0x*U1z*U2y + U0y*U1x*U2z + U0y*U1z*U2x + U0z*U1x*U2y + U0z*U1y*U2x;
		}
	}
	
	/*Cr_v[index*3+0] += valx*val_red;
	Cr_v[index*3+1] += valy*val_red;
	Cr_v[index*3+2] += valz*val_red;*/
	
	Cr_v[index] += valx*val_red;
	Cr_v[nb_threads + index] += valy*val_red;
	Cr_v[2*nb_threads + index] += valz*val_red;
	
}

__global__ void d_cubic_prod_Ut(int nb_threads, int nb_red_dofs, int dof_start, double* Ut, double* Cr_v, double* Cr_v_array)
{
	int index = threadIdx.x + blockDim.x * blockIdx.x;
	
	extern __shared__ double Udof012[];
	
	double* Utdof_start0 = &Udof012[0];
	double* Utdof_start1 = &Udof012[nb_red_dofs];
	double* Utdof_start2 = &Udof012[nb_red_dofs*2];
	
	if (threadIdx.x < nb_red_dofs)
	{
		Utdof_start0[threadIdx.x] = Ut[(dof_start+0)*nb_red_dofs + threadIdx.x];
		Utdof_start1[threadIdx.x] = Ut[(dof_start+1)*nb_red_dofs + threadIdx.x];
		Utdof_start2[threadIdx.x] = Ut[(dof_start+2)*nb_red_dofs + threadIdx.x];
	}

	__syncthreads();
	
	if (index > nb_threads-1) return;
	
	//double3 vals = ((double3*)(&Cr_v[index*3]))[0];
	double valx = Cr_v[index];
	double valy = Cr_v[nb_threads + index];
	double valz = Cr_v[2*nb_threads + index];
	
	for (int i=0; i<nb_red_dofs; i++)
	{
		double Ut_vals0 = Utdof_start0[i];
		double Ut_vals1 = Utdof_start1[i];
		double Ut_vals2 = Utdof_start2[i];
		
		//Cr_v_array[index*nb_red_dofs+i] += Ut_vals0*vals.x + Ut_vals1*vals.y + Ut_vals2*vals.z;
		//Cr_v_array[index*nb_red_dofs+i] += Ut_vals0*valx + Ut_vals1*valy + Ut_vals2*valz;
		Cr_v_array[i*nb_threads + index] += Ut_vals0*valx + Ut_vals1*valy + Ut_vals2*valz;
	}
	
}

__global__ void d_to_mat_quadratic(int nb_threads, int nb_red_dofs, int dof0, int dof1, double valx, double valy, double valz, int2* thread_map, double* Ut, double* Qr_v)
{
	int index = threadIdx.x + blockDim.x * blockIdx.x;
	if (index > nb_threads-1) return;
	
	int2 coords = thread_map[index];
	
	double val_red = Ut[dof0*nb_red_dofs+coords.x]*Ut[dof1*nb_red_dofs+coords.y];

	if (coords.x != coords.y)
	{
		val_red += Ut[dof0*nb_red_dofs+coords.y]*Ut[dof1*nb_red_dofs+coords.x];
	}
	
	/*Qr_v[index*3+0] += valx*val_red;
	Qr_v[index*3+1] += valy*val_red;
	Qr_v[index*3+2] += valz*val_red;*/
	
	Qr_v[index] += valx*val_red;
	Qr_v[nb_threads + index] += valy*val_red;
	Qr_v[2*nb_threads + index] += valz*val_red;
	
}

__global__ void d_quadratic_prod_Ut(int nb_threads, int nb_red_dofs, int dof_start, double* Ut, double* Qr_v, double* Qr_v_array)
{
	int index = threadIdx.x + blockDim.x * blockIdx.x;
	
	extern __shared__ double Udof012[];
	
	double* Utdof_start0 = &Udof012[0];
	double* Utdof_start1 = &Udof012[nb_red_dofs];
	double* Utdof_start2 = &Udof012[nb_red_dofs*2];
	
	if (threadIdx.x < nb_red_dofs)
	{
		Utdof_start0[threadIdx.x] = Ut[(dof_start+0)*nb_red_dofs + threadIdx.x];
		Utdof_start1[threadIdx.x] = Ut[(dof_start+1)*nb_red_dofs + threadIdx.x];
		Utdof_start2[threadIdx.x] = Ut[(dof_start+2)*nb_red_dofs + threadIdx.x];
	}

	__syncthreads();
	
	if (index > nb_threads-1) return;
	
	//double3 vals = ((double3*)(&Qr_v[index*3]))[0];
	double valx = Qr_v[index];
	double valy = Qr_v[nb_threads + index];
	double valz = Qr_v[2*nb_threads + index];
	
	for (int i=0; i<nb_red_dofs; i++)
	{
		double Ut_vals0 = Utdof_start0[i];
		double Ut_vals1 = Utdof_start1[i];
		double Ut_vals2 = Utdof_start2[i];
		
		//Qr_v_array[index*nb_red_dofs+i] += Ut_vals0*vals.x + Ut_vals1*vals.y + Ut_vals2*vals.z;
		//Qr_v_array[index*nb_red_dofs+i] += Ut_vals0*valx + Ut_vals1*valy + Ut_vals2*valz;
		Qr_v_array[i*nb_threads + index] += Ut_vals0*valx + Ut_vals1*valy + Ut_vals2*valz;
	}
	
}

__global__ void d_to_mat_linear(int nb_threads, int nb_red_dofs, int dof0, double valx, double valy, double valz, double* Ut, double* Lr_v)
{
	int index = threadIdx.x + blockDim.x * blockIdx.x;
	if (index > nb_threads-1) return;
	
	double val_red = Ut[dof0*nb_red_dofs+index];
	
	/*Lr_v[index*3+0] += valx*val_red;
	Lr_v[index*3+1] += valy*val_red;
	Lr_v[index*3+2] += valz*val_red;*/
	
	Lr_v[index] += valx*val_red;
	Lr_v[nb_threads + index] += valy*val_red;
	Lr_v[2*nb_threads + index] += valz*val_red;
}

__global__ void d_linear_prod_Ut(int nb_threads, int nb_red_dofs, int dof_start, double* Ut, double* Lr_v, double* Lr_v_array)
{
	int index = threadIdx.x + blockDim.x * blockIdx.x;
	
	extern __shared__ double Udof012[];
	
	double* Utdof_start0 = &Udof012[0];
	double* Utdof_start1 = &Udof012[nb_red_dofs];
	double* Utdof_start2 = &Udof012[nb_red_dofs*2];
	
	if (threadIdx.x < nb_red_dofs)
	{
		Utdof_start0[threadIdx.x] = Ut[(dof_start+0)*nb_red_dofs + threadIdx.x];
		Utdof_start1[threadIdx.x] = Ut[(dof_start+1)*nb_red_dofs + threadIdx.x];
		Utdof_start2[threadIdx.x] = Ut[(dof_start+2)*nb_red_dofs + threadIdx.x];
	}

	__syncthreads();
	
	if (index > nb_threads-1) return;
	
	//double3 vals = ((double3*)(&Lr_v[index*3]))[0];
	double valx = Lr_v[index];
	double valy = Lr_v[nb_threads + index];
	double valz = Lr_v[2*nb_threads + index];
	
	for (int i=0; i<nb_red_dofs; i++)
	{
		double Ut_vals0 = Utdof_start0[i];
		double Ut_vals1 = Utdof_start1[i];
		double Ut_vals2 = Utdof_start2[i];
		
		//Lr_v_array[index*nb_red_dofs+i] += Ut_vals0*vals.x + Ut_vals1*vals.y + Ut_vals2*vals.z;
		//Lr_v_array[index*nb_red_dofs+i] += Ut_vals0*valx + Ut_vals1*valy + Ut_vals2*valz;
		Lr_v_array[i*nb_threads + index] += Ut_vals0*valx + Ut_vals1*valy + Ut_vals2*valz;
	}
	
}

void cuda_to_mat_cubic(int nb_threads, int nb_red_dofs, int dof0, int dof1, int dof2, double valx, double valy, double valz, int* thread_map, double* Ut, double* Cr_v)
{
	int threadsPerBlock = NUM_THREADS_PER_BLOCK;
    int numBlocks = nb_threads/threadsPerBlock+1;
    
    if (threadsPerBlock < nb_red_dofs)
        std::cout << "ERROR: Number of threads per block is smaller than the number of reduced dofs. Kernel won't work." << std::endl;
    
	d_to_mat_cubic<<<numBlocks, threadsPerBlock, nb_red_dofs*3*sizeof(double)>>>(nb_threads, nb_red_dofs, dof0, dof1, dof2, valx, valy, valz, (int3*)thread_map, Ut, Cr_v);
}

void cuda_cubic_prod_Ut(int nb_threads, int nb_red_dofs, int dof_start, double* Ut, double* Cr_v, double* Cr_v_array)
{
	int threadsPerBlock = NUM_THREADS_PER_BLOCK;
    int numBlocks = nb_threads/threadsPerBlock+1;
    
    if (threadsPerBlock < nb_red_dofs)
        std::cout << "ERROR: Number of threads per block is smaller than the number of reduced dofs. Kernel won't work." << std::endl;
    
	d_cubic_prod_Ut<<<numBlocks, threadsPerBlock, nb_red_dofs*3*sizeof(double)>>>(nb_threads, nb_red_dofs, dof_start, Ut, Cr_v, Cr_v_array);
}

void cuda_to_mat_quadratic(int nb_threads, int nb_red_dofs, int dof0, int dof1, double valx, double valy, double valz, int* thread_map, double* Ut, double* Qr_v)
{
	int threadsPerBlock = NUM_THREADS_PER_BLOCK;
    int numBlocks = nb_threads/threadsPerBlock+1;
    
	d_to_mat_quadratic<<<numBlocks, threadsPerBlock>>>(nb_threads, nb_red_dofs, dof0, dof1, valx, valy, valz, (int2*)thread_map, Ut, Qr_v);
}

void cuda_quadratic_prod_Ut(int nb_threads, int nb_red_dofs, int dof_start, double* Ut, double* Qr_v, double* Qr_v_array)
{
	int threadsPerBlock = NUM_THREADS_PER_BLOCK;
    int numBlocks = nb_threads/threadsPerBlock+1;
    
	d_quadratic_prod_Ut<<<numBlocks, threadsPerBlock, nb_red_dofs*3*sizeof(double)>>>(nb_threads, nb_red_dofs, dof_start, Ut, Qr_v, Qr_v_array);
}

void cuda_to_mat_linear(int nb_threads, int nb_red_dofs, int dof0, double valx, double valy, double valz, double* Ut, double* Lr_v)
{
	int threadsPerBlock = NUM_THREADS_PER_BLOCK;
    int numBlocks = nb_threads/threadsPerBlock+1;
    
	d_to_mat_linear<<<numBlocks, threadsPerBlock>>>(nb_threads, nb_red_dofs, dof0, valx, valy, valz, Ut, Lr_v);
}

void cuda_linear_prod_Ut(int nb_threads, int nb_red_dofs, int dof_start, double* Ut, double* Lr_v, double* Lr_v_array)
{
	int threadsPerBlock = NUM_THREADS_PER_BLOCK;
    int numBlocks = nb_threads/threadsPerBlock+1;
    
	d_linear_prod_Ut<<<numBlocks, threadsPerBlock, nb_red_dofs*3*sizeof(double)>>>(nb_threads, nb_red_dofs, dof_start, Ut, Lr_v, Lr_v_array);
}
