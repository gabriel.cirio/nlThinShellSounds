
#ifndef CUDA_SUBSPACE_POLYNOMIAL_PRECOMPUATION_H
#define CUDA_SUBSPACE_POLYNOMIAL_PRECOMPUATION_H

void cuda_to_mat_cubic(int nb_threads, int nb_red_dofs, int dof0, int dof1, int dof2, double valx, double valy, double valz, int* thread_map, double* Ut, double* Cr_v);
void cuda_cubic_prod_Ut(int nb_threads, int nb_red_dofs, int dof_start, double* Ut, double* Cr_v, double* Cr_v_array);

void cuda_to_mat_quadratic(int nb_threads, int nb_red_dofs, int dof0, int dof1, double valx, double valy, double valz, int* thread_map, double* Ut, double* Qr_v);
void cuda_quadratic_prod_Ut(int nb_threads, int nb_red_dofs, int dof_start, double* Ut, double* Qr_v, double* Qr_v_array);

void cuda_to_mat_linear(int nb_threads, int nb_red_dofs, int dof0, double valx, double valy, double valz, double* Ut, double* Lr_v);
void cuda_linear_prod_Ut(int nb_threads, int nb_red_dofs, int dof_start, double* Ut, double* Lr_v, double* Lr_v_array);

#endif // CUDA_SUBSPACE_POLYNOMIAL_PRECOMPUATION_H
