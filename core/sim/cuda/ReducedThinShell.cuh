
#ifndef CUDA_REDUCED_THINSHELL_H
#define CUDA_REDUCED_THINSHELL_H

void cuda_compute_cubic_elem_vector(int num_fake_dofs, double* fake_dofs_gpu, double* cubic_elem_vector_gpu);

#endif // CUDA_REDUCED_THINSHELL_H


