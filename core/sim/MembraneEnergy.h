
#ifndef MEMBRANE_ENERGY_H
#define MEMBRANE_ENERGY_H

#include "Energy.h"

#include <Eigen/Sparse>

class MembraneEnergy : public Energy
{
	public:
		
		MembraneEnergy(MatrixX & rest_pos, MatrixX & pos, MatrixXi & F, std::vector<int> & per_node_dofs, VectorX & triangle_rest_area, Real young_modulus, Real poisson_ratio);
		~MembraneEnergy() {}
		
		void setup();
		void prepareForStep();
		void computeForces(std::vector<MatrixX> & force_omp_arrays);
		
		void computeDfDx(std::vector<std::vector<Jacobian> > & dfdx);
		
		void prepareForSubspacePolynomialPrecomputation(std::vector<std::set<int>> & stencil_dofs_per_node);
		void subspacePolynomialPrecomputationNodei(int node_i, const std::vector<int> & stencil_dofs_vec, ValuesForDof values_for_dof[3]);
		
	private:
		
		MatrixX & m_rest_pos;
		MatrixX & m_pos;
		MatrixXi & m_F;
		std::vector<int> & m_per_node_dofs;
		VectorX & m_triangle_rest_area;
		
		MatrixX m_triangle_normals;
		
		Real m_young_modulus;
		Real m_poisson_ratio;
		Matrix3 m_membrane_material_tensor;
		MatrixX m_membrane_ru;
		MatrixX m_membrane_rv;
		
		std::vector<std::vector<int>> m_triangles_per_node;
	
};

#endif // MEMBRANE_ENERGY_H


