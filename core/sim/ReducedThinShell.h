
#ifndef REDUCED_THINSHELL_H
#define REDUCED_THINSHELL_H

#include "ThinShell.h"

#include "ScisimAnimation.h"

#ifdef HAVE_CUDA
#include <cublas_v2.h>
#endif

#include <Eigen/Sparse>
#include <vector>

struct Impact
{
	Impact(Real _time, Vector3 _center, Real _radius, Real _magnitude, Real _spread) : 
			time(_time), center(_center), radius(_radius), magnitude(_magnitude), spread(_spread), local_time(0.)
	{}
	
	Real time;
	Vector3 center;
	Real radius;
	Real magnitude;
	Real spread;
	
	Real local_time;
	std::vector<int> nodes;
	std::vector<Vector3> directions;
};

class ReducedThinShell : public DynamicObject
{
	public:
		
		ReducedThinShell();
		~ReducedThinShell();
		
        void configure(const JSONConfiguration::Object & config, const std::string & base_path);
        
		void getMeshData(std::pair<MatrixX, MatrixXi> & mesh_data);
        void getNodalVectors(MatrixX & vectors);
        void getWorldVectors(std::pair<MatrixX, MatrixX> & vectors);
        
		int fixNodes(Vector3 pos, Real radius) { return m_shell->fixNodes(pos, radius); }
		void fixNode(int index) { m_shell->fixNode(index); }
		
		void prepareForSimulation();
		void prepareForStep(Real current_time, Real dt);
		void computeInternalForces();
		void computeInternalForceJacobians() {}
		void computeInternalDampingJacobians() {}
		void computeMass() {}
		void postStep();
		void setDtSubsteps(int dt_substeps) { m_dt_substeps = dt_substeps; }
		
		void getDofState(Eigen::Ref<VectorX> positions, Eigen::Ref<VectorX> velocities);
		void setDofState(const Eigen::Ref<const VectorX> & positions, const Eigen::Ref<const VectorX> & velocities);
		void getDofForces(Eigen::Ref<VectorX> forces);
		void getDofMasses(std::vector<std::vector<Jacobian> >* & masses) { masses = &m_masses_lowfreq; }
		void getDofForceJacobians(std::vector<std::vector<Jacobian> >* & force_jacobians) {}
		void getDofDampingJacobians(std::vector<std::vector<Jacobian> >* & damping_jacobians) {}
		unsigned int getNbDof() { return m_evalues_lowfreq.rows() + m_evalues_highfreq.rows(); }
        
		bool is_reduced() { return true; }
		void set_key(unsigned char key);
		
		Real getSecondsToSave() { return m_seconds_to_save; }
		
		void writeToDisk() { this->writeAudio(); }
		
		ThinShell* getThinShell() { return m_shell; }
		
		void setAnimationObject(const ScisimAnimationObject & object) { m_animation_object = object; }
		void setSecondsToSave(Real seconds_to_save);
        void setComputationSpace(int space);
		void enableLyapunovExponents(bool enable) { m_compute_lyapunov_exponents = enable; }
		void renderToScreen(bool render) { m_render_to_screen = render; }
		
		void writeFastBEMFiles();
        void writeMeshes(std::string base_filename);
		
	private:
		
		void computeUnreducedMatrices(Eigen::SparseMatrix<Real> & M_full, VectorX & M_full_diag, Eigen::SparseMatrix<Real> & K_full);
		
		void computeModes(int num_modes, Eigen::SparseMatrix<Real> & M_full, Eigen::SparseMatrix<Real> & K_full, VectorX & evalues, MatrixX & evecs);
		void loadModes(int num_modes, VectorX & evalues, MatrixX & evecs);
		void loadOrComputeModes(int num_modes, Eigen::SparseMatrix<Real> & M_full, Eigen::SparseMatrix<Real> & K_full, VectorX & evalues, MatrixX & evecs);
		void pruneRigidModes(Real rigid_modes_threshold, VectorX & evalues, MatrixX & evecs);
		void selectModalRange(int first_mode, int num_modes, const VectorX & evalues, const MatrixX & evecs, VectorX & selected_evalues, MatrixX & selected_evectors, bool verbose=true);
		
		void computeReducedRayleighMatrix(const VectorX & evalues, VectorX & reduced_rayleigh_vec);
		void setupLyapunovExponents();
		void updateLyapunovExponents();
		
		void convertReducedForceVectorsToSingleMatrix();
		
		void writeAudio();
		void writeTurbulenceData();
		void plotLyapunov();
		void plotModes();
        
        void getNodalForces(MatrixX & forces);
		void getNodalImpulses(MatrixX & forces);
        
        void setMaterial(std::string material_name);
		void setNumModesToCompute(int num_modes_to_compute) { m_num_modes_to_compute = num_modes_to_compute; }
		void setNumLowFrequencyModes(int num_lowfreq_modes) { m_num_lowfreq_modes = num_lowfreq_modes; }
		void setNumHighFrequencyModes(int num_highfreq_modes) { m_num_highfreq_modes = num_highfreq_modes; }
		
		void setLowFreqModesToPlot(int first_mode_to_plot, int last_mode_to_plot) { m_first_lowfreq_mode_to_plot = first_mode_to_plot; m_last_lowfreq_mode_to_plot = last_mode_to_plot; }
		void setHighFreqModesToPlot(int first_mode_to_plot, int last_mode_to_plot) { m_first_highfreq_mode_to_plot = first_mode_to_plot; m_last_highfreq_mode_to_plot = last_mode_to_plot; }
		void setPruneRigidBodyModes(bool prune) { m_prune_rigid_modes = prune; }
		void setPruneRigidBodyModesThreshold(Real threshold) { m_prune_rigid_modes_threshold = threshold; }
		void doNotComputeStretchForces() { m_no_stretch_forces = true; }
		void doNotComputeBendingForces() { m_no_bending_forces = true; }
		void addImpact(Impact impact);
        
		void setSoundBaseName(std::string sound_name) { m_sound_base_name = sound_name; }
		
	private:
		
		ThinShell* m_shell;
		
		VectorX m_rest_positions;
		VectorX m_rest_velocities;
		
		Real m_rayleigh_alpha;
		Real m_rayleigh_beta;
		
		int m_dt_substeps;
		
		int m_num_modes_to_compute;
		bool m_prune_rigid_modes;
		Real m_prune_rigid_modes_threshold;
		bool m_no_stretch_forces;
		bool m_no_bending_forces;
		
		int m_show_mode_nb;
		Real m_show_modes_t;
		Real m_show_modes_freq;
		Vector3 m_bb_min;
		Vector3 m_bb_max;
		Real m_span;
		
		std::vector<VectorX> m_saved_modal_positions_lowfreq;
		std::vector<VectorX> m_saved_modal_positions_highfreq;
		std::vector<VectorX> m_saved_modal_velocities_lowfreq;
		std::vector<VectorX> m_saved_modal_velocities_highfreq;
		Real m_seconds_to_save;
		int m_first_lowfreq_mode_to_plot;
		int m_last_lowfreq_mode_to_plot;
		int m_first_highfreq_mode_to_plot;
		int m_last_highfreq_mode_to_plot;
		
		// low frequency
		int m_num_lowfreq_modes;
		VectorX m_evalues_lowfreq;
		MatrixX m_evecs_lowfreq;
		std::vector<std::vector<Jacobian> > m_masses_lowfreq;
		VectorX m_q_lowfreq; // reduced displacement
		VectorX m_qv_lowfreq; // reduced relative velocities (IS THIS CORRECT??)
		VectorX m_qf_lowfreq; // reduced forces
		
		// high frequency
		int m_num_highfreq_modes;
		VectorX m_evalues_highfreq;
		MatrixX m_evecs_highfreq;
		std::vector<std::vector<Jacobian> > m_masses_highfreq;
		VectorX m_q_highfreq; // reduced displacement
		VectorX m_qv_highfreq; // reduced relative velocities (IS THIS CORRECT??)
		VectorX m_qf_highfreq; // reduced forces
		
		// all frequencies
		VectorX m_evalues_allfreq;
		MatrixX m_evecs_allfreq;
		MatrixX m_evecs_allfreq_transposed;
		VectorX m_rayleigh_reduced_vec_allfreq;
		
		// extra (remaining) frequencies
		VectorX m_evalues_extrafreq;
		MatrixX m_evecs_extrafreq;
		VectorX m_rayleigh_reduced_vec_extrafreq;
		
		// Reduced force space
		VectorX m_Kr; // constant
		std::vector<VectorX> m_Lr; // linear
		std::vector<std::vector<VectorX>> m_Qr; // quadratic
		std::vector<std::vector<std::vector<VectorX>>> m_Cr; // cubic
		MatrixX m_reduced_forces_matrix;
		
		// external impact
        std::list<Impact> m_impacts;
		Eigen::SparseVector<Real> m_external_impact_sparse;
		
		// switches
		int m_computation_space; // 0 - full space, 1 - linear space, 2 - subspace
		bool m_compute_lyapunov_exponents;
		std::string m_sound_base_name;
        std::string m_base_folder;
		
		#ifdef HAVE_CUDA
		cublasHandle_t m_cublas_handle;
		double* m_reduced_forces_matrix_gpu;
		double* m_fake_dofs_gpu;
		double* m_cubic_elem_vector_gpu;
		double* m_ret_gpu;
		#endif
        
        bool m_render_to_screen;
		
		bool m_post_step_plot_modes;
		bool m_post_step_write_audio;
		bool m_post_step_plot_lyapunov;
		
		// runtime prunning
        std::vector<Real> m_Lr_norms;
		std::vector<std::vector<Real>> m_Qr_norms;
		std::vector<std::vector<std::vector<Real>>> m_Cr_norms;
        VectorX m_qpeaks;
        VectorX m_prevq;
        VectorXi m_osc_dir;
        Real m_prunning_threshold;
            
		
		// lyapunov stuff
		Real m_dt; // dt from solver
		VectorX m_up; // perturbation vector
		VectorX m_vp; // perturbation velocity vector
		Real m_up_rest_norm;
		Real m_accum_perturb_ln_norm; // ln(d) = ln(d1*d2*...*dn) = ln(d1) + ln(d2) + ... + ln(dn)
		VectorX m_accum_perturb_ln_all;
		std::vector<Real> m_lyapunov;
		std::vector<VectorX> m_lyapunov_all;
        
        // turbulence stuff
        MatrixX m_spectrogram;
        Real m_spectrogram_scale;
        int m_timechunk_size;
        VectorX m_prevprevvel;
        VectorX m_prevvel;
        VectorX m_last_vel_per_freq;
        VectorXi m_last_timestep_per_freq;
        
        // animation stuff
        ScisimAnimationObject m_animation_object;
        bool m_contact_in_this_timestep;
        Real m_contact_damping_scale;
	
};

#endif // REDUCED_THINSHELL_H

