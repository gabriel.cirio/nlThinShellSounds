
#include "ExplicitSymplecticEulerSolver.h"

#include "DynamicObject.h"

#include <Eigen/Sparse>

#include <iostream>

ExplicitSymplecticEulerSolver::ExplicitSymplecticEulerSolver()
{
    // default tolerance for Explicit Euler
    m_cg_tolerance = 1e-16;
	m_cg_max_iterations = 500;
	m_dt = 1./44100;
}

void ExplicitSymplecticEulerSolver::setDtSubsteps(int dt_substeps)
{
	m_dt /= Real(dt_substeps);
	std::cout << "Timestep is " << m_dt << std::endl;
}

void ExplicitSymplecticEulerSolver::solve(std::vector<DynamicObject*> & dyn_objects)
{
	// This is Symplectic Euler (since using v_{i+1} when computing x_{i+1})
	// Compute dv_{i+1} = a_{i+1}*h = inv(M)*f(v_{i}, x_{i})*h
	// update v and x:
	// v_{i+1} = v_{i} + dv_{i+1})
	// x_{i+1} = x_{i} + h*v_{i+1}
	
    std::vector<std::pair<unsigned int, unsigned int> > ids;
    unsigned int total_nb_dof = 0;
    unsigned int nb_free_dof = 0;
    
	// prepare items
	for (auto & dyn_object : dyn_objects)
	{
		nb_free_dof = dyn_object->getNbDof();
		ids.push_back(std::make_pair(total_nb_dof, nb_free_dof));
		total_nb_dof += nb_free_dof;
	}
	
	// dv
	VectorX dv(total_nb_dof);
	
	for (unsigned int i=0; i<dyn_objects.size(); i++)
	{
		dyn_objects[i]->computeInternalForces();
		dyn_objects[i]->computeMass();
	}
	    
    // set global state vectors
    VectorX positions(total_nb_dof);
    positions.setZero();
    VectorX velocities(total_nb_dof);
    velocities.setZero();
    VectorX forces(total_nb_dof);
    forces.setZero();
	
	// gather global state vector
	for (unsigned int i=0; i<dyn_objects.size(); i++)
	{
		dyn_objects[i]->getDofState(positions.segment(ids[i].first, ids[i].second), velocities.segment(ids[i].first, ids[i].second));
		dyn_objects[i]->getDofForces(forces.segment(ids[i].first, ids[i].second));
	}

	// b
	VectorX b = forces*m_dt;
	
	bool all_reduced = true;
	for (unsigned int i=0; i<dyn_objects.size(); i++)
		if (!dyn_objects[i]->is_reduced()) 
			all_reduced = false;

	if (all_reduced) // masses are identity
	{
		dv = b;
	}
	else
	{
		std::vector<std::vector< std::vector<Jacobian> >* > masses;
		masses.resize(dyn_objects.size());
		
		// gather global state vector
		for (unsigned int i=0; i<dyn_objects.size(); i++)
			dyn_objects[i]->getDofMasses(masses[i]);
		
		Eigen::SparseMatrix<Real> M(total_nb_dof, total_nb_dof); // mass matrix
		std::vector<Eigen::Triplet<Real> > tripletList;
		
		// finish by adding the mass entries
		for (unsigned int j=0; j<masses.size(); j++)
		{
			unsigned int base_index = ids[j].first;
			
			for (unsigned int a=0; a<masses[j]->size(); a++)
			{
				for (unsigned int k=0; k<masses[j]->at(a).size(); k++)
				{
					MatrixX mass = masses[j]->at(a)[k].jacobian;
					
					int index_i = base_index + masses[j]->at(a)[k].index_i;
					int index_j = base_index + masses[j]->at(a)[k].index_j;
					
					// get dimension of jacobian (3x3, 3x1, 1x1, ...)
					unsigned int size_jac_i = mass.rows();
					unsigned int size_jac_j = mass.cols();
					
					for (unsigned int l=0; l<size_jac_i; l++)
					{
						for (unsigned int m=0; m<size_jac_j; m++)
						{
							tripletList.push_back(Eigen::Triplet<Real>(index_i+l, index_j+m, mass(l,m)));
						}
					}
				}
			}
		}
		
		// build sparse mass matrix M from mass triplets
		M.setFromTriplets(tripletList.begin(), tripletList.end());
		
		Eigen::ConjugateGradient<Eigen::SparseMatrix<Real>, Eigen::Upper> cg;
		cg.setMaxIterations(m_cg_max_iterations);
		cg.setTolerance(m_cg_tolerance);
		cg.compute(M);
		dv.setZero();
		dv = cg.solveWithGuess(b, dv);
		
		//std::cout << "Eigen CG error: " << cg.error() << std::endl;
		//std::cout << "Eigen CG iteration count: " << cg.iterations() << std::endl;
	}
    
    velocities += dv;
    positions += velocities*m_dt;
	
    // redistribute results back to items
    for (unsigned int i=0; i<dyn_objects.size(); i++)
		dyn_objects[i]->setDofState(positions.segment(ids[i].first, ids[i].second), velocities.segment(ids[i].first, ids[i].second));
    
}
