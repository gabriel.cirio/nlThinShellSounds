
#include "BendingEnergy_Isometric.h"

#include <iostream>

#include <omp.h>

#define USE_OMP

IsometricBendingEnergy::IsometricBendingEnergy(MatrixX & rest_pos, MatrixX & pos, MatrixXi & F, std::vector<int> & per_node_dofs, VectorX & triangle_rest_area, MatrixXi & E_unique, MatrixXi & per_unique_edge_triangles, MatrixXi & per_unique_edge_triangles_local_corners, Real young_modulus, Real poisson_ratio, Real thickness)
:m_rest_pos(rest_pos), m_pos(pos), m_F(F), m_per_node_dofs(per_node_dofs), m_triangle_rest_area(triangle_rest_area), m_E_unique(E_unique), m_per_unique_edge_triangles(per_unique_edge_triangles), m_per_unique_edge_triangles_local_corners(per_unique_edge_triangles_local_corners), m_young_modulus(young_modulus), m_poisson_ratio(poisson_ratio), m_thickness(thickness)
{
}

void IsometricBendingEnergy::setup()
{
	m_bending_stiffness = m_young_modulus*m_thickness*m_thickness*m_thickness/(24.*(1.-m_poisson_ratio*m_poisson_ratio));
	
	std::vector<Eigen::Triplet<Real>> LtripletList;
	std::vector<std::vector<Eigen::Triplet<Real>>> Qi_tripletList(m_rest_pos.rows()*3);
	
	auto l_comp_common = [&] (int e, int idx[4])
	{
		Vector3 x0 = m_rest_pos.row(idx[0]);
		Vector3 x1 = m_rest_pos.row(idx[1]);
		Vector3 x2 = m_rest_pos.row(idx[2]);
		Vector3 x3 = m_rest_pos.row(idx[3]);
		
		Vector3 e0 = x1 - x0;
		Vector3 e1 = x2 - x0;
		Vector3 e2 = x3 - x0;
		Vector3 e3 = x2 - x1;
		Vector3 e4 = x3 - x1;
		
		Real e0_norm = e0.norm();
		Real e1_norm = e1.norm();
		Real e2_norm = e2.norm();
		Real e3_norm = e3.norm();
		Real e4_norm = e4.norm();
		
		e0 /= e0_norm;
		e1 /= e1_norm;
		e2 /= e2_norm;
		e3 /= e3_norm;
		e4 /= e4_norm;
		
		Real c01 = e0.dot(e1)/e0.cross(e1).norm();
		Real c02 = e0.dot(e2)/e0.cross(e2).norm();
		Real c03 = -e0.dot(e3)/e0.cross(e3).norm();
		Real c04 = -e0.dot(e4)/e0.cross(e4).norm();
		
		m_cot(e,1) = c01;
		m_cot(e,2) = c02;
		m_cot(e,3) = c03;
		m_cot(e,4) = c04;
		
		Vector3 t0_0 = -c03*e1_norm*e1 - c01*e3_norm*e3;
		Vector3 t0_1 = -c04*e2_norm*e2 - c02*e4_norm*e4;
		Real costheta = -t0_0.dot(t0_1)/(e0_norm*e0_norm);
		
		Real restareas = m_triangle_rest_area(m_per_unique_edge_triangles(e,0)) + m_triangle_rest_area(m_per_unique_edge_triangles(e,1));
		
		m_flat_common(e) = (3.*m_bending_stiffness/restareas)*costheta;
				
		// Rederived manually. Paper is wrong??
		Real beta = (1./e0_norm)*(c01+c03)*(c02+c04);
		m_nonflat_common(e) = (3.*m_bending_stiffness/restareas)*beta*(beta*((e0_norm*e0).cross(e1_norm*e1).dot(e2_norm*e2))*(-1./(e0_norm*e0_norm)));
	};
	
	auto l_comp_Le = [&] (int e, int idx[4])
	{
		MatrixX Le(4,4);
		
		VectorX k(4);
		k << m_cot(e,3)+m_cot(e,4), m_cot(e,1)+m_cot(e,2), -m_cot(e,1)-m_cot(e,3),-m_cot(e,2)-m_cot(e,4);
		
		Le = k*k.transpose();
		
		Le *= m_flat_common(e);
		
		for (unsigned int i=0; i<4; i++)
			for (unsigned int j=0; j<4; j++)
				LtripletList.push_back(Eigen::Triplet<Real>(idx[i], idx[j], Le(i,j)));
	};
	
	m_nonflat_common.resize(m_E_unique.rows());
	m_nonflat_common.setZero();
	
	m_flat_common.resize(m_E_unique.rows());
	m_flat_common.setZero();
	
	m_cot.resize(m_E_unique.rows(), 5);
	m_cot.setZero();
	
	for (unsigned int e=0; e<m_E_unique.rows(); e++)
	{
		if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges
		
		int idx[4];
		idx[0] = m_E_unique(e,0);
		idx[1] = m_E_unique(e,1);
		idx[2] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
		idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
		
		l_comp_common(e, idx);
		l_comp_Le(e, idx); // linear term
	}
	
	int num_nodes = m_rest_pos.rows();
	
	m_L.resize(num_nodes, num_nodes);
	m_L.setFromTriplets(LtripletList.begin(), LtripletList.end());
	
	//std::cout << "Isometric Bending stiffness: " << m_bending_stiffness << std::endl;
	
	// compute edges per node
	m_edges_per_node.resize(m_rest_pos.rows(), std::vector<int>());
	for (int e=0; e<m_E_unique.rows(); e++)
	{
		if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges

		m_edges_per_node[m_E_unique(e,0)].push_back(e);
		m_edges_per_node[m_E_unique(e,1)].push_back(e);
		m_edges_per_node[m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0))].push_back(e);
		m_edges_per_node[m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1))].push_back(e);
	}
	
}

void IsometricBendingEnergy::computeForces(std::vector<MatrixX> & force_omp_arrays)
{
	// thin plate part
	force_omp_arrays[0] += -m_L*m_pos;
	
	auto l_bending_force_stencil_nonflat = [this] (int e, int idx[4], Vector3 forces[4])
	{
		Vector3 x0 = m_pos.row(idx[0]);
		Vector3 x1 = m_pos.row(idx[1]);
		Vector3 x2 = m_pos.row(idx[2]);
		Vector3 x3 = m_pos.row(idx[3]);
		Vector3 e0 = x1 - x0;
		Vector3 e1 = x2 - x0;
		Vector3 e2 = x3 - x0;
		
		forces[1] = -m_nonflat_common(e)*(e1.cross(e2));
		forces[2] = -m_nonflat_common(e)*(e2.cross(e0));
		forces[3] = -m_nonflat_common(e)*(e0.cross(e1));
		forces[0] = -forces[1] - forces[2] - forces[3];
	};
	
	#ifdef USE_OMP
	#pragma omp parallel
	#endif
	{
		int omp_thread_index = omp_get_thread_num();

		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (unsigned int e=0; e<m_E_unique.rows(); e++)
		{
			if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges

			int idx[4];
			idx[0] = m_E_unique(e,0);
			idx[1] = m_E_unique(e,1);
			idx[2] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
			idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
		
			Vector3 forces[4];
			l_bending_force_stencil_nonflat(e, idx, forces);
			
			force_omp_arrays[omp_thread_index].row(idx[0]) += forces[0].transpose();
			force_omp_arrays[omp_thread_index].row(idx[1]) += forces[1].transpose();
			force_omp_arrays[omp_thread_index].row(idx[2]) += forces[2].transpose();
			force_omp_arrays[omp_thread_index].row(idx[3]) += forces[3].transpose();
		}
	}
	
}

void IsometricBendingEnergy::computeDfDx(std::vector<std::vector<Jacobian> > & dfdx)
{
	auto l_bending_jacobian_stencil_nonflat = [this] (int e, int idx[4], Matrix3 jacobians[4][4])
	{
		Vector3 x0 = m_pos.row(idx[0]);
		Vector3 x1 = m_pos.row(idx[1]);
		Vector3 x2 = m_pos.row(idx[2]);
		Vector3 x3 = m_pos.row(idx[3]);
		Vector3 e0 = x1 - x0;
		Vector3 e1 = x2 - x0;
		Vector3 e2 = x3 - x0;
		
		Real common = m_nonflat_common(e);
		
		jacobians[0][0].setZero();
		jacobians[1][1].setZero();
		jacobians[2][2].setZero();
		jacobians[3][3].setZero();
		jacobians[0][1] = common*makeSkewSymmetric(e1-e2);
		jacobians[0][2] = common*makeSkewSymmetric(e2-e0);
		jacobians[0][3] = common*makeSkewSymmetric(e0-e1);
		jacobians[1][2] = common*makeSkewSymmetric(-e2);
		jacobians[1][3] = common*makeSkewSymmetric(e1);
		jacobians[2][3] = common*makeSkewSymmetric(-e0);
		jacobians[1][0] = -jacobians[0][1];
		jacobians[2][0] = -jacobians[0][2];
		jacobians[2][1] = -jacobians[1][2];
		jacobians[3][0] = -jacobians[0][3];
		jacobians[3][1] = -jacobians[1][3];
		jacobians[3][2] = -jacobians[2][3];
	};
		
	#ifdef USE_OMP
	#pragma omp parallel
	#endif
	{
		int omp_thread_index = omp_get_thread_num();
		
		// flat part (-> constant K)
		
		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (int k=0; k<m_L.outerSize(); ++k)
		{
			Jacobian jac;
			jac.jacobian.resize(1,1);
		
			for (Eigen::SparseMatrix<Real>::InnerIterator it(m_L,k); it; ++it)
			{
				jac.jacobian << -it.value();
				
				int node_i = it.row();
				int node_j = it.col();
				
				if ( (m_per_node_dofs[node_i] != -1) && (m_per_node_dofs[node_j] != -1) )
				{
					jac.index_i = m_per_node_dofs[node_i] + 0;
					jac.index_j = m_per_node_dofs[node_j] + 0;
					dfdx[omp_thread_index].push_back(jac);
					
					jac.index_i = m_per_node_dofs[node_i] + 1;
					jac.index_j = m_per_node_dofs[node_j] + 1;
					dfdx[omp_thread_index].push_back(jac);
					
					jac.index_i = m_per_node_dofs[node_i] + 2;
					jac.index_j = m_per_node_dofs[node_j] + 2;
					dfdx[omp_thread_index].push_back(jac);
				}
			}
		}
		
		// nonflat part (-> linear K)
		
		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (unsigned int e=0; e<m_E_unique.rows(); e++)
		{
			if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges

			int idx[4];
			idx[0] = m_E_unique(e,0);
			idx[1] = m_E_unique(e,1);
			idx[2] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
			idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
		
			Matrix3 jacobians[4][4];
			l_bending_jacobian_stencil_nonflat(e, idx, jacobians);
			
			Jacobian jac;
		
			for (unsigned int i=0; i<4; i++)
			{
				if (m_per_node_dofs[idx[i]] == -1) continue;
				
				jac.index_i = m_per_node_dofs[idx[i]];
				
				for (unsigned int j=0; j<4; j++)
				{
					if (m_per_node_dofs[idx[j]] == -1) continue;
					
					jac.index_j = m_per_node_dofs[idx[j]];
					jac.jacobian = -jacobians[i][j];
					
					dfdx[omp_thread_index].push_back(jac);
				}
			}
		}
	}
	
}

void IsometricBendingEnergy::prepareForSubspacePolynomialPrecomputation(std::vector<std::set<int>> & stencil_dofs_per_node)
{
	for (int i=0; i<m_edges_per_node.size(); i++)
	{
		auto & stencil_dofs = stencil_dofs_per_node[i];
		
		for (auto e : m_edges_per_node[i])
		{
			int idx[4];
			idx[0] = m_E_unique(e,0);
			idx[1] = m_E_unique(e,1);
			idx[2] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
			idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
				
			stencil_dofs.insert(idx[0]*3+0);
			stencil_dofs.insert(idx[0]*3+1);
			stencil_dofs.insert(idx[0]*3+2);
			stencil_dofs.insert(idx[1]*3+0);
			stencil_dofs.insert(idx[1]*3+1);
			stencil_dofs.insert(idx[1]*3+2);
			stencil_dofs.insert(idx[2]*3+0);
			stencil_dofs.insert(idx[2]*3+1);
			stencil_dofs.insert(idx[2]*3+2);
			stencil_dofs.insert(idx[3]*3+0);
			stencil_dofs.insert(idx[3]*3+1);
			stencil_dofs.insert(idx[3]*3+2);
		}
	}
	
}

void IsometricBendingEnergy::subspacePolynomialPrecomputationNodei(int node_i, const std::vector<int> & stencil_dofs_vec, ValuesForDof values_for_dof[3])
{
	// TODO: WARNING: THIS FUNCTION HAS NOT BEEN TESTED SINCE CONVERTED TO THE NEW SUBSPACE PRECOMPUTATION PIPELINE!!
		
	std::vector<int> dofs_vec_rev(m_rest_pos.rows()*3, 0);
	for (int i=0; i<stencil_dofs_vec.size(); i++)
		dofs_vec_rev[stencil_dofs_vec[i]] = i;

	for (auto e : m_edges_per_node[node_i])
	{
		int idx[4];
		idx[0] = m_E_unique(e,0);
		idx[1] = m_E_unique(e,1);
		idx[2] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
		idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
		
		// flat part (produces linear and constant terms when using displacements instead of positions)
		
		MatrixX Le(4,4);
		
		VectorX k_vec(4);
		k_vec << m_cot(e,3)+m_cot(e,4), m_cot(e,1)+m_cot(e,2), -m_cot(e,1)-m_cot(e,3),-m_cot(e,2)-m_cot(e,4);
		
		Le = m_flat_common(e)*k_vec*k_vec.transpose();
		
		int i;
		if (node_i == idx[0]) i = 0;
		else if (node_i == idx[1]) i = 1;
		else if (node_i == idx[2]) i = 2;
		else i = 3;
			
		for (unsigned int j=0; j<4; j++)
		{
			for (unsigned int d=0; d<3; d++) // for each dimension (3)
			{
				// force = -L*u = -L*(x - x0) = -L*x + L*x0
				cumul(dofs_vec_rev, values_for_dof[d], Le(i,j)*m_rest_pos(idx[j],d)); // constant term
				cumul(dofs_vec_rev, values_for_dof[d], idx[j]*3+d, -Le(i,j)); // linear term
			}
		}
		
		// non-flat part (produces a constant, a linear and a quadratic part when using displacements instead of positions)

		int ux0 = idx[0]*3+0;
		int ux1 = idx[1]*3+0;
		int ux2 = idx[2]*3+0;
		int ux3 = idx[3]*3+0;
		int uy0 = idx[0]*3+1;
		int uy1 = idx[1]*3+1;
		int uy2 = idx[2]*3+1;
		int uy3 = idx[3]*3+1;
		int uz0 = idx[0]*3+2;
		int uz1 = idx[1]*3+2;
		int uz2 = idx[2]*3+2;
		int uz3 = idx[3]*3+2;
		
		Real rx0 = m_rest_pos(idx[0], 0);
		Real rx1 = m_rest_pos(idx[1], 0);
		Real rx2 = m_rest_pos(idx[2], 0);
		Real rx3 = m_rest_pos(idx[3], 0);
		Real ry0 = m_rest_pos(idx[0], 1);
		Real ry1 = m_rest_pos(idx[1], 1);
		Real ry2 = m_rest_pos(idx[2], 1);
		Real ry3 = m_rest_pos(idx[3], 1);
		Real rz0 = m_rest_pos(idx[0], 2);
		Real rz1 = m_rest_pos(idx[1], 2);
		Real rz2 = m_rest_pos(idx[2], 2);
		Real rz3 = m_rest_pos(idx[3], 2);
		
		Real k = -m_nonflat_common(e);

		if (node_i == idx[0])
		{
			// fx0
			cumul(dofs_vec_rev, values_for_dof[0], uz1, -(k*(ry0-ry2)-k*(ry0-ry3)));
			cumul(dofs_vec_rev, values_for_dof[0], uz2, (k*(ry0-ry1)-k*(ry0-ry3)));
			cumul(dofs_vec_rev, values_for_dof[0], uz3, -(k*(ry0-ry1)-k*(ry0-ry2)));
			cumul(dofs_vec_rev, values_for_dof[0], uy1, (k*(rz0-rz2)-k*(rz0-rz3)));
			cumul(dofs_vec_rev, values_for_dof[0], uy2, -(k*(rz0-rz1)-k*(rz0-rz3)));
			cumul(dofs_vec_rev, values_for_dof[0], uy3, (k*(rz0-rz1)-k*(rz0-rz2)));
			cumul(dofs_vec_rev, values_for_dof[0], -k*((ry0-ry1)*(rz0-rz2)-(ry0-ry2)*(rz0-rz1))+k*((ry0-ry1)*(rz0-rz3)-(ry0-ry3)*(rz0-rz1))-k*((ry0-ry2)*(rz0-rz3)-(ry0-ry3)*(rz0-rz2)));
			cumul(dofs_vec_rev, values_for_dof[0], uz0, (k*(ry1-ry2)-k*(ry1-ry3)+k*(ry2-ry3)));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, -(k*(rz1-rz2)-k*(rz1-rz3)+k*(rz2-rz3)));
			cumul(dofs_vec_rev, values_for_dof[0], uy1, uz2, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uy2, uz1, k);
			cumul(dofs_vec_rev, values_for_dof[0], uy1, uz3, k);
			cumul(dofs_vec_rev, values_for_dof[0], uy3, uz1, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uy2, uz3, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uy3, uz2, k);
			
			// fy0
			cumul(dofs_vec_rev, values_for_dof[1], uz1, (k*(rx0-rx2)-k*(rx0-rx3)));
			cumul(dofs_vec_rev, values_for_dof[1], uz2, -(k*(rx0-rx1)-k*(rx0-rx3)));
			cumul(dofs_vec_rev, values_for_dof[1], uz3, (k*(rx0-rx1)-k*(rx0-rx2)));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, -(k*(rz0-rz2)-k*(rz0-rz3)));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, (k*(rz0-rz1)-k*(rz0-rz3)));
			cumul(dofs_vec_rev, values_for_dof[1], ux3, -(k*(rz0-rz1)-k*(rz0-rz2)));
			cumul(dofs_vec_rev, values_for_dof[1], k*((rx0-rx1)*(rz0-rz2)-(rx0-rx2)*(rz0-rz1))-k*((rx0-rx1)*(rz0-rz3)-(rx0-rx3)*(rz0-rz1))+k*((rx0-rx2)*(rz0-rz3)-(rx0-rx3)*(rz0-rz2)));
			cumul(dofs_vec_rev, values_for_dof[1], uz0, -(k*(rx1-rx2)-k*(rx1-rx3)+k*(rx2-rx3)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, (k*(rz1-rz2)-k*(rz1-rz3)+k*(rz2-rz3)));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, uz2, k);
			cumul(dofs_vec_rev, values_for_dof[1], ux2, uz1, -k);
			cumul(dofs_vec_rev, values_for_dof[1], ux1, uz3, -k);
			cumul(dofs_vec_rev, values_for_dof[1], ux3, uz1, k);
			cumul(dofs_vec_rev, values_for_dof[1], ux2, uz3, k);
			cumul(dofs_vec_rev, values_for_dof[1], ux3, uz2, -k);
			
			// fz0
			cumul(dofs_vec_rev, values_for_dof[2], uy1, -(k*(rx0-rx2)-k*(rx0-rx3)));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, (k*(rx0-rx1)-k*(rx0-rx3)));
			cumul(dofs_vec_rev, values_for_dof[2], uy3, -(k*(rx0-rx1)-k*(rx0-rx2)));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, (k*(ry0-ry2)-k*(ry0-ry3)));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, -(k*(ry0-ry1)-k*(ry0-ry3)));
			cumul(dofs_vec_rev, values_for_dof[2], ux3, (k*(ry0-ry1)-k*(ry0-ry2)));
			cumul(dofs_vec_rev, values_for_dof[2], -k*((rx0-rx1)*(ry0-ry2)-(rx0-rx2)*(ry0-ry1))+k*((rx0-rx1)*(ry0-ry3)-(rx0-rx3)*(ry0-ry1))-k*((rx0-rx2)*(ry0-ry3)-(rx0-rx3)*(ry0-ry2)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, (k*(rx1-rx2)-k*(rx1-rx3)+k*(rx2-rx3)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, -(k*(ry1-ry2)-k*(ry1-ry3)+k*(ry2-ry3)));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, uy2, -k);
			cumul(dofs_vec_rev, values_for_dof[2], ux2, uy1, k);
			cumul(dofs_vec_rev, values_for_dof[2], ux1, uy3, k);
			cumul(dofs_vec_rev, values_for_dof[2], ux3, uy1, -k);
			cumul(dofs_vec_rev, values_for_dof[2], ux2, uy3, -k);
			cumul(dofs_vec_rev, values_for_dof[2], ux3, uy2, k);
		}
		else if (node_i == idx[1])
		{
			// fx1
			cumul(dofs_vec_rev, values_for_dof[0], k*((ry0-ry2)*(rz0-rz3)-(ry0-ry3)*(rz0-rz2)));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, uz2, k);
			cumul(dofs_vec_rev, values_for_dof[0], uy2, uz0, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uy0, uz3, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uy3, uz0, k);
			cumul(dofs_vec_rev, values_for_dof[0], uy2, uz3, k);
			cumul(dofs_vec_rev, values_for_dof[0], uy3, uz2, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uz0, -k*(ry2-ry3));
			cumul(dofs_vec_rev, values_for_dof[0], uz2, k*(ry0-ry3));
			cumul(dofs_vec_rev, values_for_dof[0], uz3, -k*(ry0-ry2));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, k*(rz2-rz3));
			cumul(dofs_vec_rev, values_for_dof[0], uy2, -k*(rz0-rz3));
			cumul(dofs_vec_rev, values_for_dof[0], uy3, k*(rz0-rz2));
			
			// fy1
			cumul(dofs_vec_rev, values_for_dof[1], -k*((rx0-rx2)*(rz0-rz3)-(rx0-rx3)*(rz0-rz2)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, uz2, -k);
			cumul(dofs_vec_rev, values_for_dof[1], ux2, uz0, k);
			cumul(dofs_vec_rev, values_for_dof[1], ux0, uz3, k);
			cumul(dofs_vec_rev, values_for_dof[1], ux3, uz0, -k);
			cumul(dofs_vec_rev, values_for_dof[1], ux2, uz3, -k);
			cumul(dofs_vec_rev, values_for_dof[1], ux3, uz2, k);
			cumul(dofs_vec_rev, values_for_dof[1], uz0, k*(rx2-rx3));
			cumul(dofs_vec_rev, values_for_dof[1], uz2, -k*(rx0-rx3));
			cumul(dofs_vec_rev, values_for_dof[1], uz3, k*(rx0-rx2));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, -k*(rz2-rz3));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, k*(rz0-rz3));
			cumul(dofs_vec_rev, values_for_dof[1], ux3, -k*(rz0-rz2));
			
			// fz1
			cumul(dofs_vec_rev, values_for_dof[2], k*((rx0-rx2)*(ry0-ry3)-(rx0-rx3)*(ry0-ry2)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, uy2, k);
			cumul(dofs_vec_rev, values_for_dof[2], ux2, uy0, -k);
			cumul(dofs_vec_rev, values_for_dof[2], ux0, uy3, -k);
			cumul(dofs_vec_rev, values_for_dof[2], ux3, uy0, k);
			cumul(dofs_vec_rev, values_for_dof[2], ux2, uy3, k);
			cumul(dofs_vec_rev, values_for_dof[2], ux3, uy2, -k);
			cumul(dofs_vec_rev, values_for_dof[2], uy0, -k*(rx2-rx3));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, k*(rx0-rx3));
			cumul(dofs_vec_rev, values_for_dof[2], uy3, -k*(rx0-rx2));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, k*(ry2-ry3));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, -k*(ry0-ry3));
			cumul(dofs_vec_rev, values_for_dof[2], ux3, k*(ry0-ry2));
		}
		else if (node_i == idx[2])
		{
			// fx2
			cumul(dofs_vec_rev, values_for_dof[0], -k*((ry0-ry1)*(rz0-rz3)-(ry0-ry3)*(rz0-rz1)));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, uz1, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uy1, uz0, k);
			cumul(dofs_vec_rev, values_for_dof[0], uy0, uz3, k);
			cumul(dofs_vec_rev, values_for_dof[0], uy3, uz0, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uy1, uz3, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uy3, uz1, k);
			cumul(dofs_vec_rev, values_for_dof[0], uz0, k*(ry1-ry3));
			cumul(dofs_vec_rev, values_for_dof[0], uz1, -k*(ry0-ry3));
			cumul(dofs_vec_rev, values_for_dof[0], uz3, k*(ry0-ry1));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, -k*(rz1-rz3));
			cumul(dofs_vec_rev, values_for_dof[0], uy1, k*(rz0-rz3));
			cumul(dofs_vec_rev, values_for_dof[0], uy3, -k*(rz0-rz1));
		
			// fy2
			cumul(dofs_vec_rev, values_for_dof[1], k*((rx0-rx1)*(rz0-rz3)-(rx0-rx3)*(rz0-rz1)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, uz1, k);
			cumul(dofs_vec_rev, values_for_dof[1], ux1, uz0, -k);
			cumul(dofs_vec_rev, values_for_dof[1], ux0, uz3, -k);
			cumul(dofs_vec_rev, values_for_dof[1], ux3, uz0, k);
			cumul(dofs_vec_rev, values_for_dof[1], ux1, uz3, k);
			cumul(dofs_vec_rev, values_for_dof[1], ux3, uz1, -k);
			cumul(dofs_vec_rev, values_for_dof[1], uz0, -k*(rx1-rx3));
			cumul(dofs_vec_rev, values_for_dof[1], uz1, k*(rx0-rx3));
			cumul(dofs_vec_rev, values_for_dof[1], uz3, -k*(rx0-rx1));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, k*(rz1-rz3));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, -k*(rz0-rz3));
			cumul(dofs_vec_rev, values_for_dof[1], ux3, k*(rz0-rz1));
			
			// fz2
			cumul(dofs_vec_rev, values_for_dof[2], -k*((rx0-rx1)*(ry0-ry3)-(rx0-rx3)*(ry0-ry1)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, uy1, -k);
			cumul(dofs_vec_rev, values_for_dof[2], ux1, uy0, k);
			cumul(dofs_vec_rev, values_for_dof[2], ux0, uy3, k);
			cumul(dofs_vec_rev, values_for_dof[2], ux3, uy0, -k);
			cumul(dofs_vec_rev, values_for_dof[2], ux1, uy3, -k);
			cumul(dofs_vec_rev, values_for_dof[2], ux3, uy1, k);
			cumul(dofs_vec_rev, values_for_dof[2], uy0, k*(rx1-rx3));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, -k*(rx0-rx3));
			cumul(dofs_vec_rev, values_for_dof[2], uy3, k*(rx0-rx1));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, -k*(ry1-ry3));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, k*(ry0-ry3));
			cumul(dofs_vec_rev, values_for_dof[2], ux3, -k*(ry0-ry1));
		}
		else if (node_i == idx[3])
		{
			// fx3
			cumul(dofs_vec_rev, values_for_dof[0], k*((ry0-ry1)*(rz0-rz2)-(ry0-ry2)*(rz0-rz1)));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, uz1, k);
			cumul(dofs_vec_rev, values_for_dof[0], uy1, uz0, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uy0, uz2, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uy2, uz0, k);
			cumul(dofs_vec_rev, values_for_dof[0], uy1, uz2, k);
			cumul(dofs_vec_rev, values_for_dof[0], uy2, uz1, -k);
			cumul(dofs_vec_rev, values_for_dof[0], uz0, -k*(ry1-ry2));
			cumul(dofs_vec_rev, values_for_dof[0], uz1, k*(ry0-ry2));
			cumul(dofs_vec_rev, values_for_dof[0], uz2, -k*(ry0-ry1));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, k*(rz1-rz2));
			cumul(dofs_vec_rev, values_for_dof[0], uy1, -k*(rz0-rz2));
			cumul(dofs_vec_rev, values_for_dof[0], uy2, k*(rz0-rz1));
			
			// fy3
			cumul(dofs_vec_rev, values_for_dof[1], -k*((rx0-rx1)*(rz0-rz2)-(rx0-rx2)*(rz0-rz1)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, uz1, -k);
			cumul(dofs_vec_rev, values_for_dof[1], ux1, uz0, k);
			cumul(dofs_vec_rev, values_for_dof[1], ux0, uz2, k);
			cumul(dofs_vec_rev, values_for_dof[1], ux2, uz0, -k);
			cumul(dofs_vec_rev, values_for_dof[1], ux1, uz2, -k);
			cumul(dofs_vec_rev, values_for_dof[1], ux2, uz1, k);
			cumul(dofs_vec_rev, values_for_dof[1], uz0, k*(rx1-rx2));
			cumul(dofs_vec_rev, values_for_dof[1], uz1, -k*(rx0-rx2));
			cumul(dofs_vec_rev, values_for_dof[1], uz2, k*(rx0-rx1));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, -k*(rz1-rz2));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, k*(rz0-rz2));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, -k*(rz0-rz1));
			
			// fz3
			cumul(dofs_vec_rev, values_for_dof[2], k*((rx0-rx1)*(ry0-ry2)-(rx0-rx2)*(ry0-ry1)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, uy1, k);
			cumul(dofs_vec_rev, values_for_dof[2], ux1, uy0, -k);
			cumul(dofs_vec_rev, values_for_dof[2], ux0, uy2, -k);
			cumul(dofs_vec_rev, values_for_dof[2], ux2, uy0, k);
			cumul(dofs_vec_rev, values_for_dof[2], ux1, uy2, k);
			cumul(dofs_vec_rev, values_for_dof[2], ux2, uy1, -k);
			cumul(dofs_vec_rev, values_for_dof[2], uy0, -k*(rx1-rx2));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, k*(rx0-rx2));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, -k*(rx0-rx1));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, k*(ry1-ry2));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, -k*(ry0-ry2));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, k*(ry0-ry1));
		}
	}
	
}
