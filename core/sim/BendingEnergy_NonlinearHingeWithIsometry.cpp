
#define EIGEN_NO_DEBUG
#include "util/Autodiff.h"
DECLARE_DIFFSCALAR_BASE();

#include "BendingEnergy_NonlinearHingeWithIsometry.h"
#include "BendingEnergy_NonlinearHingeWithIsometry_force_polynomial.h"
#include "CubicPolynomialScalar.h"

#include <iostream>
#include <fstream>

#include <omp.h>

#define USE_OMP

// this only applies to full space computations. For reduced space, isometric assumptions and quartic-to-cubic reductions are mandatory (otherwise not cubic)
#define APPLY_ISOMETRIC_ASSUMPTIONS
#define QUARTIC_TO_CUBIC

BendingEnergy_NonlinearHingeWithIsometry::BendingEnergy_NonlinearHingeWithIsometry(MatrixX & rest_pos, MatrixX & pos, MatrixXi & F, std::vector<int> & per_node_dofs, VectorX & triangle_rest_area, MatrixXi & E_unique, MatrixXi & per_unique_edge_triangles, MatrixXi & per_unique_edge_triangles_local_corners, Real young_modulus, Real poisson_ratio, Real thickness)
:m_rest_pos(rest_pos), m_pos(pos), m_F(F), m_per_node_dofs(per_node_dofs), m_triangle_rest_area(triangle_rest_area), m_E_unique(E_unique), m_per_unique_edge_triangles(per_unique_edge_triangles), m_per_unique_edge_triangles_local_corners(per_unique_edge_triangles_local_corners), m_young_modulus(young_modulus), m_poisson_ratio(poisson_ratio), m_thickness(thickness)
{	
}

void BendingEnergy_NonlinearHingeWithIsometry::setup()
{
	m_bending_stiffness = m_young_modulus*m_thickness*m_thickness*m_thickness/(24.*(1.-m_poisson_ratio*m_poisson_ratio));
	
	//std::cout << "Isometric Bending stiffness: " << m_bending_stiffness << std::endl;
	
	m_cot.resize(m_E_unique.rows(), 5);
	m_cot.setZero();
	
	m_rest_angle.resize(m_E_unique.rows());
	m_rest_angle.setZero();
	
	m_nl_common.resize(m_E_unique.rows());
	m_nl_common.setZero();
	
	for (unsigned int e=0; e<m_E_unique.rows(); e++)
	{
		if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges
		
		int idx[4];
		idx[0] = m_E_unique(e,0);
		idx[1] = m_E_unique(e,1);
		idx[2] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
		idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
		
		Vector3 x0 = m_rest_pos.row(idx[0]);
		Vector3 x1 = m_rest_pos.row(idx[1]);
		Vector3 x2 = m_rest_pos.row(idx[2]);
		Vector3 x3 = m_rest_pos.row(idx[3]);
		
		Vector3 e0 = x1 - x0;
		Vector3 e1 = x2 - x0;
		Vector3 e2 = x3 - x0;
		Vector3 e3 = x2 - x1;
		Vector3 e4 = x3 - x1;
		
		Real e0_norm = e0.norm();
		Real e1_norm = e1.norm();
		Real e2_norm = e2.norm();
		Real e3_norm = e3.norm();
		Real e4_norm = e4.norm();
		
		e0 /= e0_norm;
		e1 /= e1_norm;
		e2 /= e2_norm;
		e3 /= e3_norm;
		e4 /= e4_norm;
		
		Real c01 = e0.dot(e1)/e0.cross(e1).norm();
		Real c02 = e0.dot(e2)/e0.cross(e2).norm();
		Real c03 = -e0.dot(e3)/e0.cross(e3).norm();
		Real c04 = -e0.dot(e4)/e0.cross(e4).norm();
		
		m_cot(e,1) = c01;
		m_cot(e,2) = c02;
		m_cot(e,3) = c03;
		m_cot(e,4) = c04;
		
		Real restareas = m_triangle_rest_area(m_per_unique_edge_triangles(e,0)) + m_triangle_rest_area(m_per_unique_edge_triangles(e,1));
		
		// rest angle
		
		Vector3 v0 = x1 - x0;
		Vector3 v1 = x2 - x1;
		Vector3 v1_tilde = x3 - x1;
		
		Vector3 n = v0.cross(v1);
		n.normalize();
		Vector3 n_tilde = -v0.cross(v1_tilde);
		n_tilde.normalize();
		
		m_rest_angle(e) = std::atan2((n.cross(n_tilde)).dot(v0.normalized()), n.dot(n_tilde));
		
		m_nl_common(e) = 0.5*(3.*m_bending_stiffness/restareas)*e0_norm*e0_norm;
	}
	
	// compute edges per node
	m_edges_per_node.resize(m_rest_pos.rows(), std::vector<int>());
	for (int e=0; e<m_E_unique.rows(); e++)
	{
		if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges

		m_edges_per_node[m_E_unique(e,0)].push_back(e);
		m_edges_per_node[m_E_unique(e,1)].push_back(e);
		m_edges_per_node[m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0))].push_back(e);
		m_edges_per_node[m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1))].push_back(e);
	}
	
	//if (true) this->generate_polynomial_code("BendingEnergy_NonlinearHingeWithIsometry_force_polynomial");

}

void BendingEnergy_NonlinearHingeWithIsometry::computeForces(std::vector<MatrixX> & force_omp_arrays)
{
	static int frame = 0;
	frame++;
	
	auto l_quartic_to_cubic = [this] (int a, int b, int c, int d, int idx[4]) -> Vector3
	{
		// remove the quartic displacement component from ea*eb.transpose()*ec.cross(ed)
		
		Vector3 x0 = m_pos.row(idx[0]);
		Vector3 x1 = m_pos.row(idx[1]);
		Vector3 x2 = m_pos.row(idx[2]);
		Vector3 x3 = m_pos.row(idx[3]);
		
		Vector3 x0r = m_rest_pos.row(idx[0]);
		Vector3 x1r = m_rest_pos.row(idx[1]);
		Vector3 x2r = m_rest_pos.row(idx[2]);
		Vector3 x3r = m_rest_pos.row(idx[3]);
		
		Vector3 e[5] = { x1-x0, x2-x0, x3-x0, x2-x1, x3-x1 };
		Vector3 er[5] = { x1r-x0r, x2r-x0r, x3r-x0r, x2r-x1r, x3r-x1r };
		Vector3 u[5] = { e[0] - er[0], e[1] - er[1], e[2] - er[2], e[3] - er[3], e[4] - er[4] };
		
		Vector3 res = er[a]*e[b].transpose()*e[c].cross(e[d]) + u[a]*er[b].transpose()*e[c].cross(e[d]) + u[a]*u[b].transpose()*(er[c].cross(e[d]) + u[c].cross(er[d]));
		
		return res;
	};
	
	#ifdef USE_OMP
	#pragma omp parallel
	#endif
	{
		int omp_thread_index = omp_get_thread_num();
		
		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (unsigned int e=0; e<m_E_unique.rows(); e++)
		{
			if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges

			int idx[4];
			idx[0] = m_E_unique(e,0);
			idx[1] = m_E_unique(e,1);
			idx[2] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
			idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
		
			Vector3 x0 = m_pos.row(idx[0]);
			Vector3 x1 = m_pos.row(idx[1]);
			Vector3 x2 = m_pos.row(idx[2]);
			Vector3 x3 = m_pos.row(idx[3]);
			
			Vector3 e0 = x1-x0;
			Vector3 e1 = x2-x0;
			Vector3 e2 = x3-x0;
			Vector3 e3 = x2-x1;
			Vector3 e4 = x3-x1;
			
			Real costheta0 = std::cos(m_rest_angle(e));
			Real sintheta0 = std::sin(m_rest_angle(e));
			
			#ifdef APPLY_ISOMETRIC_ASSUMPTIONS
			
				Vector3 x0_rest = m_rest_pos.row(idx[0]);
				Vector3 x1_rest = m_rest_pos.row(idx[1]);
				Vector3 x2_rest = m_rest_pos.row(idx[2]);
				Vector3 x3_rest = m_rest_pos.row(idx[3]);
				
				Vector3 e0_rest = x1_rest - x0_rest;
				Vector3 e1_rest = x2_rest - x0_rest;
				Vector3 e2_rest = x3_rest - x0_rest;
				Vector3 e3_rest = x2_rest - x1_rest;
				Vector3 e4_rest = x3_rest - x1_rest;
				
				Real c01 = e0_rest.dot(e1_rest)/e0_rest.cross(e1_rest).norm();
				Real c02 = e0_rest.dot(e2_rest)/e0_rest.cross(e2_rest).norm();
				Real c03 = -e0_rest.dot(e3_rest)/e0_rest.cross(e3_rest).norm();
				Real c04 = -e0_rest.dot(e4_rest)/e0_rest.cross(e4_rest).norm();
				
				Real e0_norm = e0_rest.norm();
				Real e1_norm = e1_rest.norm();
				Real e2_norm = e2_rest.norm();
				Real e3_norm = e3_rest.norm();
				Real e4_norm = e4_rest.norm();
				Real e0xe1norm = e0_rest.cross(e1_rest).norm();
				Real e0dote1 = e0_rest.dot(e1_rest);
				Real e0xe2norm = e0_rest.cross(e2_rest).norm();
				Real e0dote2 = e0_rest.dot(e2_rest);
				Real e0xe3norm = e0_rest.cross(e3_rest).norm();
				Real e0dote3 = e0_rest.dot(e3_rest);
				Real e0xe4norm = e0_rest.cross(e4_rest).norm();
				Real e0dote4 = e0_rest.dot(e4_rest);
				Real e4xe2_norm = e4_rest.cross(e2_rest).norm();
				Real e4dote2 = e4_rest.dot(e2_rest);
				
			#else
				
				Real c01 = e0.dot(e1)/e0.cross(e1).norm();
				Real c02 = e0.dot(e2)/e0.cross(e2).norm();
				Real c03 = -e0.dot(e3)/e0.cross(e3).norm();
				Real c04 = -e0.dot(e4)/e0.cross(e4).norm();
				
				Real e0_norm = e0.norm();
				Real e1_norm = e1.norm();
				Real e2_norm = e2.norm();
				Real e3_norm = e3.norm();
				Real e4_norm = e4.norm();
				Real e0xe1norm = e0.cross(e1).norm();
				Real e0dote1 = e0.dot(e1);
				Real e0xe2norm = e0.cross(e2).norm();
				Real e0dote2 = e0.dot(e2);
				Real e0xe3norm = e0.cross(e3).norm();
				Real e0dote3 = e0.dot(e3);
				Real e0xe4norm = e0.cross(e4).norm();
				Real e0dote4 = e0.dot(e4);
				Real e4dote2 = e4.dot(e2);
				Real e4xe2_norm = e4.cross(e2).norm();
				
			#endif
				
			Vector3 t0 = -c03*e1 - c01*e3;
			Vector3 t1 = -c04*e2 - c02*e4;
			
			// dE/dx0
			{
				Vector3 der_c01_x0 = -e1*(1./e0xe1norm + e0dote1*(e0dote1 - e0_norm*e0_norm)/(e0xe1norm*e0xe1norm*e0xe1norm)) - e0*(1./e0xe1norm + e0dote1*(-e1_norm*e1_norm + e0dote1)/(e0xe1norm*e0xe1norm*e0xe1norm));
				Vector3 der_c02_x0 = -e2*(1./e0xe2norm + e0dote2*(e0dote2 - e0_norm*e0_norm)/(e0xe2norm*e0xe2norm*e0xe2norm)) - e0*(1./e0xe2norm + e0dote2*(-e2_norm*e2_norm + e0dote2)/(e0xe2norm*e0xe2norm*e0xe2norm));
				Vector3 der_c03_x0 = e3*(1./e0xe3norm + e0dote3*e0dote3/(e0xe3norm*e0xe3norm*e0xe3norm)) - e0*e0dote3*e3_norm*e3_norm/(e0xe3norm*e0xe3norm*e0xe3norm);
				Vector3 der_c04_x0 = e4*(1./e0xe4norm + e0dote4*(e0dote4)/(e0xe4norm*e0xe4norm*e0xe4norm)) - e0*e0dote4*e4_norm*e4_norm/(e0xe4norm*e0xe4norm*e0xe4norm);
				
				Matrix3 der_t0_x0 = -(e1*der_c03_x0.transpose() - c03*Matrix3::Identity() + e3*der_c01_x0.transpose());
				Matrix3 der_t1_x0 = -(e2*der_c04_x0.transpose() - c04*Matrix3::Identity() + e4*der_c02_x0.transpose());
				Vector3 der_t0dott1_x0 = der_t0_x0.transpose()*t1 + der_t1_x0.transpose()*t0;
				Vector3 der_costheta_x0 = -(der_t0dott1_x0*e0_norm*e0_norm + 2.*t0.dot(t1)*e0)/(e0_norm*e0_norm*e0_norm*e0_norm);
				
				#ifdef QUARTIC_TO_CUBIC
					Vector3 temp1 = -e4dote2*(c03*l_quartic_to_cubic(4,1,4,2,idx) + c01*l_quartic_to_cubic(4,3,4,2,idx)) + e4_norm*e4_norm*(c03*l_quartic_to_cubic(2,1,4,2,idx) + c01*l_quartic_to_cubic(2,3,4,2, idx));
					Vector3 temp4 = (-e1_norm*e1_norm + e0dote1)/e0xe1norm*l_quartic_to_cubic(0,3,4,2,idx) + (e0dote1- e0_norm*e0_norm)/e0xe1norm*l_quartic_to_cubic(1,3,4,2,idx);
					Vector3 temp3 = -e3_norm*e3_norm/e0xe3norm*l_quartic_to_cubic(0,1,4,2,idx) + e0dote3/e0xe3norm*l_quartic_to_cubic(3,1,4,2,idx);
					Vector3 temp5 = -c03/e0_norm/e4xe2_norm*l_quartic_to_cubic(0,1,4,2,idx) - c01/e0_norm/e4xe2_norm*l_quartic_to_cubic(0,3,4,2,idx);
					Vector3 temp2 = -(l_quartic_to_cubic(3,1,4,2,idx)/(e4xe2_norm*e0xe3norm) + e0dote3/e4xe2_norm/(e0xe3norm*e0xe3norm)*temp3 -c03*(e4.cross(e2)/e4xe2_norm) + -(l_quartic_to_cubic(1,3,4,2,idx) + l_quartic_to_cubic(0,3,4,2,idx))/(e0xe1norm*e4xe2_norm) - e0dote1/(e0xe1norm*e0xe1norm)/e4xe2_norm*temp4);
					
					Vector3 der_n_x0_transp_t0 = (e4xe2_norm*e4.cross(t0) - temp1/e4xe2_norm)/(e4xe2_norm*e4xe2_norm);
					Vector3 der_t0dotn_x0 = temp2 + der_n_x0_transp_t0;
					Vector3 der_sintheta_x0 = (der_t0dotn_x0*e0_norm + temp5)/(e0_norm*e0_norm);
				#else
					Vector3 der_e4xe2norm_x0 = -(e4.cross(e2)).cross(e4)/e4xe2_norm;
					Vector3 der_n_x0_transp_t0 = ( (e4xe2_norm*e4.cross(t0).transpose() - t0.transpose()*(e4.cross(e2))*der_e4xe2norm_x0.transpose())/(e4xe2_norm*e4xe2_norm) ).transpose();
					Vector3 der_t0dotn_x0 = der_t0_x0.transpose()*(e4.cross(e2)/e4xe2_norm) + der_n_x0_transp_t0;
					Vector3 der_sintheta_x0 = (der_t0dotn_x0*e0_norm + t0.dot((e4.cross(e2)/e4xe2_norm))*(e0/e0_norm))/(e0_norm*e0_norm);
				#endif
				
				Vector3 force = m_nl_common(e)*(costheta0*der_costheta_x0 + sintheta0*der_sintheta_x0);
				force_omp_arrays[omp_thread_index].row(idx[0]) += force.transpose();
			}
			
			// dE/dx1
			{
				Vector3 der_c01_x1 = e1/e0xe1norm - e0dote1*(e1_norm*e1_norm*e0 - e0dote1*e1)/(e0xe1norm*e0xe1norm*e0xe1norm);
				Vector3 der_c02_x1 = e2/e0xe2norm - e0dote2*(e2_norm*e2_norm*e0 - e0dote2*e2)/(e0xe2norm*e0xe2norm*e0xe2norm);
				Vector3 der_c03_x1 = (e0-e3)/e0xe3norm + e0dote3*(e3_norm*e3_norm*e0 + e0dote3*(e0-e3) - e0_norm*e0_norm*e3)/(e0xe3norm*e0xe3norm*e0xe3norm);
				Vector3 der_c04_x1 = (e0-e4)/e0xe4norm + e0dote4*(e4_norm*e4_norm*e0 + e0dote4*(e0-e4) - e0_norm*e0_norm*e4)/(e0xe4norm*e0xe4norm*e0xe4norm);
				
				Matrix3 der_t0_x1 = -(e1*der_c03_x1.transpose() + e3*der_c01_x1.transpose() - c01*Matrix3::Identity());
				Matrix3 der_t1_x1 = -(e2*der_c04_x1.transpose() + e4*der_c02_x1.transpose() - c02*Matrix3::Identity());
				Vector3 der_t0dott1_x1 = der_t0_x1.transpose()*t1 + der_t1_x1.transpose()*t0;
				Vector3 der_costheta_x1 = -(der_t0dott1_x1*e0_norm*e0_norm - 2.*t0.dot(t1)*e0)/(e0_norm*e0_norm*e0_norm*e0_norm);
				
				#ifdef QUARTIC_TO_CUBIC
					Vector3 temp1 = c03*e2_norm*e2_norm/e4xe2_norm*l_quartic_to_cubic(4,1,4,2,idx) - c03*e4dote2/e4xe2_norm*l_quartic_to_cubic(2,1,4,2,idx) + c01*e2_norm*e2_norm/e4xe2_norm*l_quartic_to_cubic(4,3,4,2,idx) - c01*e4dote2/e4xe2_norm*l_quartic_to_cubic(2,3,4,2,idx);
					Vector3 temp2 = (1. + e0dote3*(e3_norm*e3_norm+e0dote3)/(e0xe3norm*e0xe3norm))/e0xe3norm*l_quartic_to_cubic(0,1,4,2,idx) - (1. + e0dote3*(e0dote3+e0_norm*e0_norm)/(e0xe3norm*e0xe3norm))/e0xe3norm*l_quartic_to_cubic(3,1,4,2,idx);
					Vector3 temp3 = (1. + e0dote1*e0dote1/(e0xe1norm*e0xe1norm))/e0xe1norm*l_quartic_to_cubic(1,3,4,2,idx) - e0dote1*e1_norm*e1_norm/(e0xe1norm*e0xe1norm*e0xe1norm)*l_quartic_to_cubic(0,3,4,2,idx);
					Vector3 temp4 = -(temp2 + temp3 - c01*e4.cross(e2))/e4xe2_norm;
					Vector3 temp5 = -c03*l_quartic_to_cubic(0,1,4,2,idx) + -c01*l_quartic_to_cubic(0,3,4,2,idx);
					
					Vector3 der_n_x1_transp_t0 = (t0.cross(e2)*e4xe2_norm - temp1)/(e4xe2_norm*e4xe2_norm);
					Vector3 der_t0dotn_x1 = temp4 + der_n_x1_transp_t0;
					Vector3 der_sintheta_x1 = (der_t0dotn_x1*e0_norm - temp5/e0_norm/e4xe2_norm)/(e0_norm*e0_norm);
                    //Vector3 der_sintheta_x1 = der_t0dotn_x1/e0_norm - temp5/(e4xe2_norm*e0_norm*e0_norm*e0_norm);
				#else
					Vector3 der_e4xe2norm_x1 = e4.cross(e2).cross(e2)/e4xe2_norm;
					Vector3 der_n_x1_transp_t0 = (t0.cross(e2)*e4xe2_norm - der_e4xe2norm_x1*(e4.cross(e2)).transpose()*t0)/(e4xe2_norm*e4xe2_norm);
					Vector3 der_t0dotn_x1 = der_t0_x1.transpose()*(e4.cross(e2)/e4xe2_norm) + der_n_x1_transp_t0;
					Vector3 der_sintheta_x1 = (der_t0dotn_x1*e0_norm - t0.dot(e4.cross(e2)/e4xe2_norm)*(e0/e0_norm))/(e0_norm*e0_norm);
                    //Vector3 der_sintheta_x1 = der_t0dotn_x1/e0_norm - t0.dot(e4.cross(e2)/e4xe2_norm)*e0/(e0_norm*e0_norm*e0_norm);
				#endif
				
				Vector3 force = m_nl_common(e)*(costheta0*der_costheta_x1 + sintheta0*der_sintheta_x1);
				force_omp_arrays[omp_thread_index].row(idx[1]) += force.transpose();
			}
			
			// dE/dx2
			{
				Vector3 der_c01_x2 = e0/e0xe1norm - e0dote1*(-e0dote1*e0 + e0_norm*e0_norm*e1)/(e0xe1norm*e0xe1norm*e0xe1norm);
				Vector3 der_c03_x2 = -e0/e0xe3norm + e0dote3*(-e0dote3*e0 + e0_norm*e0_norm*e3)/(e0xe3norm*e0xe3norm*e0xe3norm);
				
				Matrix3 der_t0_x2 = -(e1*der_c03_x2.transpose() + c03*Matrix3::Identity() + e3*der_c01_x2.transpose() + c01*Matrix3::Identity());
				Vector3 der_t0dott1_x2 = der_t0_x2.transpose()*t1;
				Vector3 der_costheta_x2 = -(der_t0dott1_x2*e0_norm*e0_norm)/(e0_norm*e0_norm*e0_norm*e0_norm);
				
				#ifdef QUARTIC_TO_CUBIC
					Vector3 temp1 = -(1. + e0dote3*e0dote3/(e0xe3norm*e0xe3norm))/e0xe3norm*l_quartic_to_cubic(0,1,4,2,idx) + e0dote3*e0_norm*e0_norm/(e0xe3norm*e0xe3norm*e0xe3norm)*l_quartic_to_cubic(3,1,4,2,idx);
					Vector3 temp2 = (1. + e0dote1*e0dote1/(e0xe1norm*e0xe1norm))/e0xe1norm*l_quartic_to_cubic(0,3,4,2,idx) - e0dote1*e0_norm*e0_norm/(e0xe1norm*e0xe1norm*e0xe1norm)*l_quartic_to_cubic(1,3,4,2,idx);
					Vector3 temp3 = -(temp1 + (c03+c01)*e4.cross(e2) + temp2);
					
					Vector3 der_t0dotn_x2 = temp3/e4xe2_norm;
					Vector3 der_sintheta_x2 = (der_t0dotn_x2*e0_norm)/(e0_norm*e0_norm);
				#else
					Vector3 der_t0dotn_x2 = der_t0_x2.transpose()*(e4.cross(e2)/e4xe2_norm);
					Vector3 der_sintheta_x2 = (der_t0dotn_x2*e0_norm)/(e0_norm*e0_norm);
				#endif
				
				Vector3 force = m_nl_common(e)*(costheta0*der_costheta_x2 + sintheta0*der_sintheta_x2);
				force_omp_arrays[omp_thread_index].row(idx[2]) += force.transpose();
			}
			
			// dE/dx3
			{
				Vector3 der_c02_x3 = e0/e0xe2norm - e0dote2*(-e0dote2*e0 + e0_norm*e0_norm*e2)/(e0xe2norm*e0xe2norm*e0xe2norm);
				Vector3 der_c04_x3 = -e0/e0xe4norm + e0dote4*(-e0dote4*e0 + e0_norm*e0_norm*e4)/(e0xe4norm*e0xe4norm*e0xe4norm);
				
				Matrix3 der_t1_x3 = -(e2*der_c04_x3.transpose() + c04*Matrix3::Identity() + e4*der_c02_x3.transpose() + c02*Matrix3::Identity());
				Vector3 der_t0dott1_x3 = der_t1_x3.transpose()*t0;
				Vector3 der_costheta_x3 = -(der_t0dott1_x3*e0_norm*e0_norm)/(e0_norm*e0_norm*e0_norm*e0_norm);
				
				#ifdef QUARTIC_TO_CUBIC
					Vector3 temp1 = -c03*l_quartic_to_cubic(4,1,4,2,idx) - c01*l_quartic_to_cubic(4,3,4,2,idx);
					Vector3 temp2 = -c03*l_quartic_to_cubic(2,1,4,2,idx) - c01*l_quartic_to_cubic(2,3,4,2,idx);
					Vector3 temp3 = (-e4dote2 + e2_norm*e2_norm)/e4xe2_norm*temp1 + (e4_norm*e4_norm - e4dote2)/e4xe2_norm*temp2;
					
					Vector3 der_t0dotn_x3 = (t0.cross(e4-e2)*e4xe2_norm - temp3)/(e4xe2_norm*e4xe2_norm);
					Vector3 der_sintheta_x3 = (der_t0dotn_x3*e0_norm)/(e0_norm*e0_norm);
				#else
					Vector3 der_e4xe2norm_x3 = e4.cross(e2).cross(e4-e2)/e4xe2_norm;
					Vector3 der_t0dotn_x3 = (t0.cross(e4-e2)*e4xe2_norm - der_e4xe2norm_x3*(e4.cross(e2)).transpose()*t0)/(e4xe2_norm*e4xe2_norm);
					Vector3 der_sintheta_x3 = (der_t0dotn_x3*e0_norm)/(e0_norm*e0_norm);	
				#endif
				
				Vector3 force = m_nl_common(e)*(costheta0*der_costheta_x3 + sintheta0*der_sintheta_x3);
				force_omp_arrays[omp_thread_index].row(idx[3]) += force.transpose();
			}
		}
	}
	
}

void BendingEnergy_NonlinearHingeWithIsometry::computeDfDx(std::vector<std::vector<Jacobian> > & dfdx)
{
	#ifdef USE_OMP
	#pragma omp parallel
	#endif
	{
		int omp_thread_index = omp_get_thread_num();
		
		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (unsigned int e=0; e<m_E_unique.rows(); e++)
		{
			if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges

			int idx[4];
			idx[0] = m_E_unique(e,0);
			idx[1] = m_E_unique(e,1);
			idx[2] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
			idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
		
			Vector3 x0 = m_pos.row(idx[0]);
			Vector3 x1 = m_pos.row(idx[1]);
			Vector3 x2 = m_pos.row(idx[2]);
			Vector3 x3 = m_pos.row(idx[3]);
			
			Real costheta0 = std::cos(m_rest_angle(e));
			Real sintheta0 = std::sin(m_rest_angle(e));

			// There are 12 independent variables
			DiffScalarBase::setVariableCount(12);
			typedef DScalar2<double, Eigen::Matrix<double, 12, 1>, Eigen::Matrix<double, 12, 12> > DScalar;
			typedef Eigen::Matrix<DScalar, 3, 1> DVector3;
	
			DVector3 _x0(DScalar(0, x0(0)), DScalar(1, x0(1)), DScalar(2, x0(2)));
			DVector3 _x1(DScalar(3, x1(0)), DScalar(4, x1(1)), DScalar(5, x1(2)));
			DVector3 _x2(DScalar(6, x2(0)), DScalar(7, x2(1)), DScalar(8, x2(2)));
			DVector3 _x3(DScalar(9, x3(0)), DScalar(10, x3(1)), DScalar(11, x3(2)));

			DVector3 _e0 = _x1 - _x0;
			DVector3 _e1 = _x2 - _x0;
			DVector3 _e2 = _x3 - _x0;
			DVector3 _e3 = _x2 - _x1;
			DVector3 _e4 = _x3 - _x1;
			
			DScalar _c01 = _e0.dot(_e1)/_e0.cross(_e1).norm();
			DScalar _c02 = _e0.dot(_e2)/_e0.cross(_e2).norm();
			DScalar _c03 = -_e0.dot(_e3)/_e0.cross(_e3).norm();
			DScalar _c04 = -_e0.dot(_e4)/_e0.cross(_e4).norm();
			
			DVector3 _t0 = -_c03*_e1 - _c01*_e3;
			DVector3 _t1 = -_c04*_e2 - _c02*_e4;
			
			DScalar _e0norm = _e0.norm();
			
			DScalar _t0dott1 = _t0.dot(_t1);
			DScalar _costheta = -_t0dott1/(_e0norm*_e0norm);
			
			DVector3 _n = _e4.cross(_e2)/_e4.cross(_e2).norm();
			DScalar _t0dotn = _t0.dot(_n);
			DScalar _sintheta = _t0dotn/_e0norm;
			
			DScalar _tot_energy = m_nl_common(e)*(1. - (costheta0*_costheta + sintheta0*_sintheta));
			
			Jacobian jac;
			
			for (int i=0; i<4; i++)
			{
				if (m_per_node_dofs[idx[i]] == -1) continue;
				
				jac.index_i = m_per_node_dofs[idx[i]];
				
				for (int j=0; j<4; j++)
				{
					if (m_per_node_dofs[idx[j]] == -1) continue;
					
					jac.index_j = m_per_node_dofs[idx[j]];
					jac.jacobian = -_tot_energy.getHessian().block(i*3, j*3, 3, 3);
					
					dfdx[omp_thread_index].push_back(jac);
				}
			}
		}
	}
		
}

void BendingEnergy_NonlinearHingeWithIsometry::prepareForSubspacePolynomialPrecomputation(std::vector<std::set<int>> & stencil_dofs_per_node)
{
	for (int i=0; i<m_edges_per_node.size(); i++)
	{
		auto & stencil_dofs = stencil_dofs_per_node[i];
		
		for (auto e : m_edges_per_node[i])
		{
			int idx[4];
			idx[0] = m_E_unique(e,0);
			idx[1] = m_E_unique(e,1);
			idx[2] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
			idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
				
			stencil_dofs.insert(idx[0]*3+0);
			stencil_dofs.insert(idx[0]*3+1);
			stencil_dofs.insert(idx[0]*3+2);
			stencil_dofs.insert(idx[1]*3+0);
			stencil_dofs.insert(idx[1]*3+1);
			stencil_dofs.insert(idx[1]*3+2);
			stencil_dofs.insert(idx[2]*3+0);
			stencil_dofs.insert(idx[2]*3+1);
			stencil_dofs.insert(idx[2]*3+2);
			stencil_dofs.insert(idx[3]*3+0);
			stencil_dofs.insert(idx[3]*3+1);
			stencil_dofs.insert(idx[3]*3+2);
		}
	}
	
}

void BendingEnergy_NonlinearHingeWithIsometry::subspacePolynomialPrecomputationNodei(int node_i, const std::vector<int> & stencil_dofs_vec, ValuesForDof values_for_dof[3])
{
	for (auto e : m_edges_per_node[node_i])
	{
		int idx[4];
		idx[0] = m_E_unique(e,0);
		idx[1] = m_E_unique(e,1);
		idx[2] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
		idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
		
		// build map from local index (0,1,2,3) to node vector indices (all nodes related to the one being treated)
		std::vector<int> dofs_vec_rev(12, 0);
		for (int i=0; i<4; i++)
		{
			int pos = std::distance(stencil_dofs_vec.begin(), std::find(stencil_dofs_vec.begin(), stencil_dofs_vec.end(), idx[i]*3));
			dofs_vec_rev[i*3+0] = pos;
			dofs_vec_rev[i*3+1] = pos+1;
			dofs_vec_rev[i*3+2] = pos+2;
		}
		
		Vector3 x0_rest = m_rest_pos.row(idx[0]);
		Vector3 x1_rest = m_rest_pos.row(idx[1]);
		Vector3 x2_rest = m_rest_pos.row(idx[2]);
		Vector3 x3_rest = m_rest_pos.row(idx[3]);

		VectorX polynomial[3];
		
		if (idx[0] == node_i) // f0
		{
			get_polynomial(m_rest_angle(e), m_nl_common(e), x0_rest, x1_rest, x2_rest, x3_rest, 0, polynomial);
		}
		else if (idx[1] == node_i) // f1
		{
			get_polynomial(m_rest_angle(e), m_nl_common(e), x0_rest, x1_rest, x2_rest, x3_rest, 1, polynomial);
		}
		else if (idx[2] == node_i) // f2
		{
			get_polynomial(m_rest_angle(e), m_nl_common(e), x0_rest, x1_rest, x2_rest, x3_rest, 2, polynomial);
		}
		else if (idx[3] == node_i) // f3
		{
			get_polynomial(m_rest_angle(e), m_nl_common(e), x0_rest, x1_rest, x2_rest, x3_rest, 3, polynomial);
		}
		
		int counter = 0;
		
		if (polynomial[0](counter))
			this->cumul(dofs_vec_rev, values_for_dof[0], polynomial[0](counter));
		if (polynomial[1](counter))
			this->cumul(dofs_vec_rev, values_for_dof[1], polynomial[1](counter));
		if (polynomial[2](counter))
			this->cumul(dofs_vec_rev, values_for_dof[2], polynomial[2](counter));
		counter++;
		for (int i=0; i<12; i++)
		{
			if (polynomial[0](counter))
				this->cumul(dofs_vec_rev, values_for_dof[0], i, polynomial[0](counter));
			if (polynomial[1](counter))
				this->cumul(dofs_vec_rev, values_for_dof[1], i, polynomial[1](counter));
			if (polynomial[2](counter))
				this->cumul(dofs_vec_rev, values_for_dof[2], i, polynomial[2](counter));
			counter++;
			for (int j=i; j<12; j++)
			{
				if (polynomial[0](counter))
					this->cumul(dofs_vec_rev, values_for_dof[0], i, j, polynomial[0](counter));
				if (polynomial[1](counter))
					this->cumul(dofs_vec_rev, values_for_dof[1], i, j, polynomial[1](counter));
				if (polynomial[2](counter))
					this->cumul(dofs_vec_rev, values_for_dof[2], i, j, polynomial[2](counter));
				counter++;
				for (int k=j; k<12; k++)
				{
					if (polynomial[0](counter))
						this->cumul(dofs_vec_rev, values_for_dof[0], i, j, k, polynomial[0](counter));
					if (polynomial[1](counter))
						this->cumul(dofs_vec_rev, values_for_dof[1], i, j, k, polynomial[1](counter));
					if (polynomial[2](counter))
						this->cumul(dofs_vec_rev, values_for_dof[2], i, j, k, polynomial[2](counter));
					counter++;
				}
			}
		}
	}
}


void BendingEnergy_NonlinearHingeWithIsometry::generate_polynomial_code(std::string filename)
{
	typedef Eigen::Matrix<CubicPolynomialScalar, 3, 1> PVector3;
	typedef Eigen::Matrix<CubicPolynomialScalar, 3, 3> PMatrix3;
	
	SymbolicValue one(1);
	
	PMatrix3 identity;
	identity << CubicPolynomialScalar(one),CubicPolynomialScalar(),CubicPolynomialScalar(),
				CubicPolynomialScalar(),CubicPolynomialScalar(one),CubicPolynomialScalar(),
				CubicPolynomialScalar(),CubicPolynomialScalar(),CubicPolynomialScalar(one);
				
	SymbolicValue costheta0("costheta0");
	SymbolicValue sintheta0("sintheta0");
	
	SymbolicValue nl_c_e("nlce");
	
	SymbolicValue c01("c01");
	SymbolicValue c02("c02");
	SymbolicValue c03("c03");
	SymbolicValue c04("c04");
	
	SymbolicValue e0_norm("e0n");
	SymbolicValue e1_norm("e1n");
	SymbolicValue e2_norm("e2n");
	SymbolicValue e3_norm("e3n");
	SymbolicValue e4_norm("e4n");
	SymbolicValue e0xe1norm("e0xe1norm");
	SymbolicValue e0dote1("e0dote1");
	SymbolicValue e0xe2norm("e0xe2norm");
	SymbolicValue e0dote2("e0dote2");
	SymbolicValue e0xe3norm("e0xe3norm");
	SymbolicValue e0dote3("e0dote3");
	SymbolicValue e0xe4norm("e0xe4norm");
	SymbolicValue e0dote4("e0dote4");
	SymbolicValue e4xe2_norm("e4xe2_norm");
	SymbolicValue e4dote2("e4dote2");
	SymbolicValue e0n2("e0n2");
	SymbolicValue e0n4("e0n4");
	
	PVector3 _u0(CubicPolynomialScalar(0, one), CubicPolynomialScalar(1, one), CubicPolynomialScalar(2, one));
	PVector3 _u1(CubicPolynomialScalar(3, one), CubicPolynomialScalar(4, one), CubicPolynomialScalar(5, one));
	PVector3 _u2(CubicPolynomialScalar(6, one), CubicPolynomialScalar(7, one), CubicPolynomialScalar(8, one));
	PVector3 _u3(CubicPolynomialScalar(9, one), CubicPolynomialScalar(10, one), CubicPolynomialScalar(11, one));
	
	SymbolicValue _x0rx("x0rx");
	SymbolicValue _x0ry("x0ry");
	SymbolicValue _x0rz("x0rz");
	SymbolicValue _x1rx("x1rx");
	SymbolicValue _x1ry("x1ry");
	SymbolicValue _x1rz("x1rz");
	SymbolicValue _x2rx("x2rx");
	SymbolicValue _x2ry("x2ry");
	SymbolicValue _x2rz("x2rz");
	SymbolicValue _x3rx("x3rx");
	SymbolicValue _x3ry("x3ry");
	SymbolicValue _x3rz("x3rz");
	SymbolicValue _e0rx("e0rx");
	SymbolicValue _e0ry("e0ry");
	SymbolicValue _e0rz("e0rz");
	SymbolicValue _e1rx("e1rx");
	SymbolicValue _e1ry("e1ry");
	SymbolicValue _e1rz("e1rz");
	SymbolicValue _e2rx("e2rx");
	SymbolicValue _e2ry("e2ry");
	SymbolicValue _e2rz("e2rz");
	SymbolicValue _e3rx("e3rx");
	SymbolicValue _e3ry("e3ry");
	SymbolicValue _e3rz("e3rz");
	SymbolicValue _e4rx("e4rx");
	SymbolicValue _e4ry("e4ry");
	SymbolicValue _e4rz("e4rz");
	
	PVector3 _x0(_u0(0)+_x0rx, _u0(1)+_x0ry, _u0(2)+_x0rz);
	PVector3 _x1(_u1(0)+_x1rx, _u1(1)+_x1ry, _u1(2)+_x1rz);
	PVector3 _x2(_u2(0)+_x2rx, _u2(1)+_x2ry, _u2(2)+_x2rz);
	PVector3 _x3(_u3(0)+_x3rx, _u3(1)+_x3ry, _u3(2)+_x3rz);

	PVector3 _e0 = _x1-_x0;
	PVector3 _e1 = _x2-_x0;
	PVector3 _e2 = _x3-_x0;
	PVector3 _e3 = _x2-_x1;
	PVector3 _e4 = _x3-_x1;
	
	PVector3 _e0_r(_e0rx, _e0ry, _e0rz);
	PVector3 _e1_r(_e1rx, _e1ry, _e1rz);
	PVector3 _e2_r(_e2rx, _e2ry, _e2rz);
	PVector3 _e3_r(_e3rx, _e3ry, _e3rz);
	PVector3 _e4_r(_e4rx, _e4ry, _e4rz);

	PVector3 _t0 = -c03*_e1 - c01*_e3;
	PVector3 _t1 = -c04*_e2 - c02*_e4;
	
	PVector3 _e4xe2 = _e4.cross(_e2);
	PVector3 _e4rxe2pe4me4rxe2r = _e4_r.cross(_e2) + (_e4 - _e4_r).cross(_e2_r);
	auto l_quartic_to_cubic_P = [&] (const PVector3 & _ea, const PVector3 & _eb, const PVector3 & _ea_r, const PVector3 & _eb_r) -> PVector3
	{
		PVector3 _ea_u = _ea - _ea_r;
		PVector3 _eb_u = _eb - _eb_r;
		return _ea_r*_eb.transpose()*_e4xe2 + _ea_u*_eb_r.transpose()*_e4xe2 + _ea_u*_eb_u.transpose()*_e4rxe2pe4me4rxe2r; // + u[a]*u[b].transpose()*u[c].cross(u[d]);
	};
	
	SymbolicValue _a("a"); // (one/e0xe1norm + e0dote1*(e0dote1 - e0_norm*e0_norm)/(e0xe1norm*e0xe1norm*e0xe1norm))
	SymbolicValue _b("b"); // (one/e0xe1norm + e0dote1*(-e1_norm*e1_norm + e0dote1)/(e0xe1norm*e0xe1norm*e0xe1norm))
	SymbolicValue _c("c"); // (one/e0xe2norm + e0dote2*(e0dote2 - e0_norm*e0_norm)/(e0xe2norm*e0xe2norm*e0xe2norm))
	SymbolicValue _d("d"); // (one/e0xe2norm + e0dote2*(-e2_norm*e2_norm + e0dote2)/(e0xe2norm*e0xe2norm*e0xe2norm))
	SymbolicValue _f("f"); // (one/e0xe3norm + e0dote3*e0dote3/(e0xe3norm*e0xe3norm*e0xe3norm))
	SymbolicValue _g("g"); // e0dote3*e3_norm*e3_norm/(e0xe3norm*e0xe3norm*e0xe3norm)
	SymbolicValue _h("h"); // (one/e0xe4norm + e0dote4*(e0dote4)/(e0xe4norm*e0xe4norm*e0xe4norm))
	SymbolicValue _l("l"); // e0dote4*e4_norm*e4_norm/(e0xe4norm*e0xe4norm*e0xe4norm)
	SymbolicValue _m("m"); // (1./e0xe1norm + e0dote1*e0dote1/(e0xe1norm*e0xe1norm*e0xe1norm))
	SymbolicValue _n("n"); // e0dote1*e1_norm*e1_norm/(e0xe1norm*e0xe1norm*e0xe1norm)
	SymbolicValue _o("o"); // (1./e0xe2norm + e0dote2*e0dote2/(e0xe2norm*e0xe2norm*e0xe2norm))
	SymbolicValue _p("p"); // e0dote2*e2_norm*e2_norm/(e0xe2norm*e0xe2norm*e0xe2norm)
	SymbolicValue _q("q"); // (1./e0xe3norm + e0dote3*(e3_norm*e3_norm + e0dote3)/(e0xe3norm*e0xe3norm*e0xe3norm))
	SymbolicValue _r("r"); // (1./e0xe3norm + e0dote3*(e0dote3 + e0_norm*e0_norm)/(e0xe3norm*e0xe3norm*e0xe3norm))
	SymbolicValue _s("s"); // (1./e0xe4norm + e0dote4*(e4_norm*e4_norm + e0dote4)/(e0xe4norm*e0xe4norm*e0xe4norm))
	SymbolicValue _t("t"); // (1./e0xe4norm + e0dote4*(e0dote4 + e0_norm*e0_norm)/(e0xe4norm*e0xe4norm*e0xe4norm))
	SymbolicValue _a1("a1"); // (1./e0xe1norm + e0dote1*e0dote1/(e0xe1norm*e0xe1norm*e0xe1norm))
	SymbolicValue _a2("a2"); // e0dote1*(e0_norm*e0_norm)/(e0xe1norm*e0xe1norm*e0xe1norm)
	SymbolicValue _a3("a3"); // (1./e0xe3norm + e0dote3*e0dote3/(e0xe3norm*e0xe3norm*e0xe3norm))
	SymbolicValue _a4("a4"); // e0dote3*e0_norm*e0_norm/(e0xe3norm*e0xe3norm*e0xe3norm)
	SymbolicValue _b1("b1"); // (1./e0xe2norm + e0dote2*e0dote2/(e0xe2norm*e0xe2norm*e0xe2norm))
	SymbolicValue _b2("b2"); // e0dote2*e0_norm*e0_norm/(e0xe2norm*e0xe2norm*e0xe2norm)
	SymbolicValue _b3("b3"); // (1./e0xe4norm + e0dote4*e0dote4/(e0xe4norm*e0xe4norm*e0xe4norm))
	SymbolicValue _b4("b4"); // e0dote4*e0_norm*e0_norm*/(e0xe4norm*e0xe4norm*e0xe4norm)
	
	std::ofstream code_file_h(filename + ".h");
	
	code_file_h << "#ifndef CODE_FILE" << std::endl;
	code_file_h << "#define CODE_FILE" << std::endl;
	code_file_h << std::endl;
	code_file_h << "#include \"typedefs.h\"" << std::endl;
	code_file_h << std::endl;
	code_file_h << "void get_polynomial(Real rest_angle, Real nlce, const Vector3 & x0, const Vector3 & x1, const Vector3 & x2, const Vector3 & x3, int fn, VectorX polynomial[3]);" << std::endl;
	
	code_file_h << "#endif // CODE_FILE" << std::endl;
	
	std::ofstream code_file(filename + ".cpp");
	
	code_file << "#warning \"Compiling " << filename << ".cpp. THIS MAY TAKE SEVERAL MINUTES!!\"" << std::endl;
	code_file << "#include \"" << filename << ".h" << "\"" << std::endl;
	code_file << std::endl;
	
	code_file << "void get_polynomial(Real rest_angle, Real nlce, const Vector3 & x0, const Vector3 & x1, const Vector3 & x2, const Vector3 & x3, int fn, VectorX polynomial[3])" << std::endl;
	code_file << "{" << std::endl;
	
	code_file << "Vector3 e0 = x1-x0;" << std::endl;
	code_file << "Vector3 e1 = x2-x0;" << std::endl;
	code_file << "Vector3 e2 = x3-x0;" << std::endl;
	code_file << "Vector3 e3 = x2-x1;" << std::endl;
	code_file << "Vector3 e4 = x3-x1;" << std::endl;
	
	code_file << "Real c01 = e0.dot(e1)/e0.cross(e1).norm();" << std::endl;
	code_file << "Real c02 = e0.dot(e2)/e0.cross(e2).norm();" << std::endl;
	code_file << "Real c03 = -e0.dot(e3)/e0.cross(e3).norm();" << std::endl;
	code_file << "Real c04 = -e0.dot(e4)/e0.cross(e4).norm();" << std::endl;
		
	code_file << "Real costheta0 = std::cos(rest_angle);" << std::endl;
	code_file << "Real sintheta0 = std::sin(rest_angle);" << std::endl;
		
	code_file << "Real e0n = e0.norm();" << std::endl;
	code_file << "Real e1n = e1.norm();" << std::endl;
	code_file << "Real e2n = e2.norm();" << std::endl;
	code_file << "Real e3n = e3.norm();" << std::endl;
	code_file << "Real e4n = e4.norm();" << std::endl;
	code_file << "Real e0xe1norm = e0.cross(e1).norm();" << std::endl;
	code_file << "Real e0dote1 = e0.dot(e1);" << std::endl;
	code_file << "Real e0xe2norm = e0.cross(e2).norm();" << std::endl;
	code_file << "Real e0dote2 = e0.dot(e2);" << std::endl;
	code_file << "Real e0xe3norm = e0.cross(e3).norm();" << std::endl;
	code_file << "Real e0dote3 = e0.dot(e3);" << std::endl;
	code_file << "Real e0xe4norm = e0.cross(e4).norm();" << std::endl;
	code_file << "Real e0dote4 = e0.dot(e4);" << std::endl;
	code_file << "Real e4xe2_norm = e4.cross(e2).norm();" << std::endl;
	code_file << "Real e4dote2 = e4.dot(e2);" << std::endl;
	code_file << "Real e0n2 = e0n*e0n;" << std::endl;
	code_file << "Real e0n4 = e0n2*e0n2;" << std::endl;
			
	code_file << "Real x0rx = x0(0);" << std::endl;
	code_file << "Real x0ry = x0(1);" << std::endl;
	code_file << "Real x0rz = x0(2);" << std::endl;
	code_file << "Real x1rx = x1(0);" << std::endl;
	code_file << "Real x1ry = x1(1);" << std::endl;
	code_file << "Real x1rz = x1(2);" << std::endl;
	code_file << "Real x2rx = x2(0);" << std::endl;
	code_file << "Real x2ry = x2(1);" << std::endl;
	code_file << "Real x2rz = x2(2);" << std::endl;
	code_file << "Real x3rx = x3(0);" << std::endl;
	code_file << "Real x3ry = x3(1);" << std::endl;
	code_file << "Real x3rz = x3(2);" << std::endl;
	code_file << "Real e0rx = e0(0);" << std::endl;
	code_file << "Real e0ry = e0(1);" << std::endl;
	code_file << "Real e0rz = e0(2);" << std::endl;
	code_file << "Real e1rx = e1(0);" << std::endl;
	code_file << "Real e1ry = e1(1);" << std::endl;
	code_file << "Real e1rz = e1(2);" << std::endl;
	code_file << "Real e2rx = e2(0);" << std::endl;
	code_file << "Real e2ry = e2(1);" << std::endl;
	code_file << "Real e2rz = e2(2);" << std::endl;
	code_file << "Real e3rx = e3(0);" << std::endl;
	code_file << "Real e3ry = e3(1);" << std::endl;
	code_file << "Real e3rz = e3(2);" << std::endl;
	code_file << "Real e4rx = e4(0);" << std::endl;
	code_file << "Real e4ry = e4(1);" << std::endl;
	code_file << "Real e4rz = e4(2);" << std::endl;
	
	code_file << "Real one = 1.;" << std::endl;
	
	code_file << "Real a = (one/e0xe1norm + e0dote1*(e0dote1 - e0n*e0n)/(e0xe1norm*e0xe1norm*e0xe1norm));" << std::endl;
	code_file << "Real b = (one/e0xe1norm + e0dote1*(-e1n*e1n + e0dote1)/(e0xe1norm*e0xe1norm*e0xe1norm));" << std::endl;
	code_file << "Real c = (one/e0xe2norm + e0dote2*(e0dote2 - e0n*e0n)/(e0xe2norm*e0xe2norm*e0xe2norm));" << std::endl;
	code_file << "Real d = (one/e0xe2norm + e0dote2*(-e2n*e2n + e0dote2)/(e0xe2norm*e0xe2norm*e0xe2norm));" << std::endl;
	code_file << "Real f = (one/e0xe3norm + e0dote3*e0dote3/(e0xe3norm*e0xe3norm*e0xe3norm));" << std::endl;
	code_file << "Real g = e0dote3*e3n*e3n/(e0xe3norm*e0xe3norm*e0xe3norm);" << std::endl;
	code_file << "Real h = (one/e0xe4norm + e0dote4*(e0dote4)/(e0xe4norm*e0xe4norm*e0xe4norm));" << std::endl;
	code_file << "Real l = e0dote4*e4n*e4n/(e0xe4norm*e0xe4norm*e0xe4norm);" << std::endl;
	code_file << "Real m = (1./e0xe1norm + e0dote1*e0dote1/(e0xe1norm*e0xe1norm*e0xe1norm));" << std::endl;
	code_file << "Real n = e0dote1*e1n*e1n/(e0xe1norm*e0xe1norm*e0xe1norm);" << std::endl;
	code_file << "Real o = (1./e0xe2norm + e0dote2*e0dote2/(e0xe2norm*e0xe2norm*e0xe2norm));" << std::endl;
	code_file << "Real p = e0dote2*e2n*e2n/(e0xe2norm*e0xe2norm*e0xe2norm);" << std::endl;
	code_file << "Real q = (1./e0xe3norm + e0dote3*(e3n*e3n + e0dote3)/(e0xe3norm*e0xe3norm*e0xe3norm));" << std::endl;
	code_file << "Real r = (1./e0xe3norm + e0dote3*(e0dote3 + e0n*e0n)/(e0xe3norm*e0xe3norm*e0xe3norm));" << std::endl;
	code_file << "Real s = (1./e0xe4norm + e0dote4*(e4n*e4n + e0dote4)/(e0xe4norm*e0xe4norm*e0xe4norm));" << std::endl;
	code_file << "Real t = (1./e0xe4norm + e0dote4*(e0dote4 + e0n*e0n)/(e0xe4norm*e0xe4norm*e0xe4norm));" << std::endl;
	code_file << "Real a1 = (1./e0xe1norm + e0dote1*e0dote1/(e0xe1norm*e0xe1norm*e0xe1norm));" << std::endl;
	code_file << "Real a2 = e0dote1*(e0n*e0n)/(e0xe1norm*e0xe1norm*e0xe1norm);" << std::endl;
	code_file << "Real a3 = (1./e0xe3norm + e0dote3*e0dote3/(e0xe3norm*e0xe3norm*e0xe3norm));" << std::endl;
	code_file << "Real a4 = e0dote3*e0n*e0n/(e0xe3norm*e0xe3norm*e0xe3norm);" << std::endl;
	code_file << "Real b1 = (1./e0xe2norm + e0dote2*e0dote2/(e0xe2norm*e0xe2norm*e0xe2norm));" << std::endl;
	code_file << "Real b2 = e0dote2*e0n*e0n/(e0xe2norm*e0xe2norm*e0xe2norm);" << std::endl;
	code_file << "Real b3 = (1./e0xe4norm + e0dote4*e0dote4/(e0xe4norm*e0xe4norm*e0xe4norm));" << std::endl;
	code_file << "Real b4 = e0dote4*e0n*e0n/(e0xe4norm*e0xe4norm*e0xe4norm);" << std::endl;

	code_file << "if (fn == 0)" << std::endl;
	code_file << "{" << std::endl;
		
	// f0
	{
		PVector3 der_c01_x0 = -_e1*_a - _e0*_b;
		PVector3 der_c02_x0 = -_e2*_c - _e0*_d;
		PVector3 der_c03_x0 = _e3*_f - _e0*_g;
		PVector3 der_c04_x0 = _e4*_h - _e0*_l;
		
		PMatrix3 der_t0_x0 = -(_e1*der_c03_x0.transpose() - c03*identity + _e3*der_c01_x0.transpose());
		PMatrix3 der_t1_x0 = -(_e2*der_c04_x0.transpose() - c04*identity + _e4*der_c02_x0.transpose());
		PVector3 der_t0dott1_x0 = der_t0_x0.transpose()*_t1 + der_t1_x0.transpose()*_t0;
		PVector3 der_costheta_x0 = -(der_t0dott1_x0/e0n2 + SymbolicValue(2.)*_t0.dot(_t1)*_e0/e0n4);

		PVector3 temp1 = -e4dote2*(c03*l_quartic_to_cubic_P(_e4,_e1,_e4_r,_e1_r) + c01*l_quartic_to_cubic_P(_e4,_e3,_e4_r,_e3_r)) + e4_norm*e4_norm*(c03*l_quartic_to_cubic_P(_e2,_e1,_e2_r,_e1_r) + c01*l_quartic_to_cubic_P(_e2,_e3,_e2_r,_e3_r));
		PVector3 temp4 = (-e1_norm*e1_norm + e0dote1)/e0xe1norm*l_quartic_to_cubic_P(_e0,_e3,_e0_r,_e3_r) + (e0dote1- e0n2)/e0xe1norm*l_quartic_to_cubic_P(_e1,_e3,_e1_r,_e3_r);
		PVector3 temp3 = -e3_norm*e3_norm/e0xe3norm*l_quartic_to_cubic_P(_e0,_e1,_e0_r,_e1_r) + e0dote3/e0xe3norm*l_quartic_to_cubic_P(_e3,_e1,_e3_r,_e1_r);
		PVector3 temp5 = -c03/e0_norm/e4xe2_norm*l_quartic_to_cubic_P(_e0,_e1,_e0_r,_e1_r) - c01/e0_norm/e4xe2_norm*l_quartic_to_cubic_P(_e0,_e3,_e0_r,_e3_r);
		PVector3 temp2 = -(l_quartic_to_cubic_P(_e3,_e1,_e3_r,_e1_r)/(e4xe2_norm*e0xe3norm) + e0dote3/e4xe2_norm/(e0xe3norm*e0xe3norm)*temp3 -c03*(_e4xe2/e4xe2_norm) -(l_quartic_to_cubic_P(_e1,_e3,_e1_r,_e3_r) + l_quartic_to_cubic_P(_e0,_e3,_e0_r,_e3_r))/(e0xe1norm*e4xe2_norm) - e0dote1/(e0xe1norm*e0xe1norm)/e4xe2_norm*temp4);
		
		PVector3 der_n_x0_transp_t0 = (_e4.cross(_t0)/e4xe2_norm - temp1/(e4xe2_norm*e4xe2_norm*e4xe2_norm));
		PVector3 der_t0dotn_x0 = temp2 + der_n_x0_transp_t0;
		PVector3 der_sintheta_x0 = der_t0dotn_x0/e0_norm + temp5/e0n2;
		
		PVector3 force = nl_c_e*(costheta0*der_costheta_x0 + sintheta0*der_sintheta_x0);
		//PVector3 force = der_c01_x0;
		
		force(0).generateCode(code_file, 12, "polynomial[0]");
		force(1).generateCode(code_file, 12, "polynomial[1]");
		force(2).generateCode(code_file, 12, "polynomial[2]");
	}
	
	code_file << "}" << std::endl;
	code_file << "else if (fn == 1)" << std::endl;
	code_file << "{" << std::endl;
	
	// f1
	{
		PVector3 der_c01_x1 = _e1*_m - _e0*_n;
		PVector3 der_c02_x1 = _e2*_o - _e0*_p;
		PVector3 der_c03_x1 = _e0*_q - _e3*_r;
		PVector3 der_c04_x1 = _e0*_s -_e4*_t;
		
		PMatrix3 der_t0_x1 = -(_e1*der_c03_x1.transpose() + _e3*der_c01_x1.transpose() - c01*identity);
		PMatrix3 der_t1_x1 = -(_e2*der_c04_x1.transpose() + _e4*der_c02_x1.transpose() - c02*identity);
		PVector3 der_t0dott1_x1 = der_t0_x1.transpose()*_t1 + der_t1_x1.transpose()*_t0;
		PVector3 der_costheta_x1 = -(der_t0dott1_x1*e0n2 - SymbolicValue(2.)*_t0.dot(_t1)*_e0)/e0n4;
		
		PVector3 temp1 = c03*e2_norm*e2_norm/e4xe2_norm*l_quartic_to_cubic_P(_e4,_e1,_e4_r,_e1_r) - c03*e4dote2/e4xe2_norm*l_quartic_to_cubic_P(_e2,_e1,_e2_r,_e1_r) + c01*e2_norm*e2_norm/e4xe2_norm*l_quartic_to_cubic_P(_e4,_e3,_e4_r,_e3_r) - c01*e4dote2/e4xe2_norm*l_quartic_to_cubic_P(_e2,_e3,_e2_r,_e3_r);
		PVector3 temp2 = (one + e0dote3*(e3_norm*e3_norm+e0dote3)/(e0xe3norm*e0xe3norm))/e0xe3norm*l_quartic_to_cubic_P(_e0,_e1,_e0_r,_e1_r) - (one + e0dote3*(e0dote3+e0n2)/(e0xe3norm*e0xe3norm))/e0xe3norm*l_quartic_to_cubic_P(_e3,_e1,_e3_r,_e1_r);
		PVector3 temp3 = (one + e0dote1*e0dote1/(e0xe1norm*e0xe1norm))/e0xe1norm*l_quartic_to_cubic_P(_e1,_e3,_e1_r,_e3_r) - e0dote1*e1_norm*e1_norm/(e0xe1norm*e0xe1norm*e0xe1norm)*l_quartic_to_cubic_P(_e0,_e3,_e0_r,_e3_r);
		PVector3 temp4 = -(temp2 + temp3 - c01*_e4xe2)/e4xe2_norm;
		PVector3 temp5 = -c03*l_quartic_to_cubic_P(_e0,_e1,_e0_r,_e1_r) + -c01*l_quartic_to_cubic_P(_e0,_e3,_e0_r,_e3_r);
		
		PVector3 der_n_x1_transp_t0 = (_t0.cross(_e2)*e4xe2_norm - temp1)/(e4xe2_norm*e4xe2_norm);
		PVector3 der_t0dotn_x1 = temp4 + der_n_x1_transp_t0;
		PVector3 der_sintheta_x1 = (der_t0dotn_x1*e0_norm - temp5/e0_norm/e4xe2_norm)/e0n2;
		
		PVector3 force = nl_c_e*(costheta0*der_costheta_x1 + sintheta0*der_sintheta_x1);
		
		force(0).generateCode(code_file, 12, "polynomial[0]");
		force(1).generateCode(code_file, 12, "polynomial[1]");
		force(2).generateCode(code_file, 12, "polynomial[2]");
	}
	
	code_file << "}" << std::endl;
	code_file << "else if (fn == 2)" << std::endl;
	code_file << "{" << std::endl;
	
	// f2
	{
		PVector3 der_c01_x2 = _e0*_a1 - _e1*_a2;
		PVector3 der_c03_x2 = -_e0*_a3 + _e3*_a4;
		
		PMatrix3 der_t0_x2 = -(_e1*der_c03_x2.transpose() + c03*identity + _e3*der_c01_x2.transpose() + c01*identity);
		PVector3 der_t0dott1_x2 = der_t0_x2.transpose()*_t1;
		PVector3 der_costheta_x2 = -der_t0dott1_x2/e0n2;
		
		PVector3 temp1 = -(one + e0dote3*e0dote3/(e0xe3norm*e0xe3norm))/e0xe3norm*l_quartic_to_cubic_P(_e0,_e1,_e0_r,_e1_r) + e0dote3*e0n2/(e0xe3norm*e0xe3norm*e0xe3norm)*l_quartic_to_cubic_P(_e3,_e1,_e3_r,_e1_r);
		PVector3 temp2 = (one + e0dote1*e0dote1/(e0xe1norm*e0xe1norm))/e0xe1norm*l_quartic_to_cubic_P(_e0,_e3,_e0_r,_e3_r) - e0dote1*e0n2/(e0xe1norm*e0xe1norm*e0xe1norm)*l_quartic_to_cubic_P(_e1,_e3,_e1_r,_e3_r);
		PVector3 temp3 = -(temp1 + (c03+c01)*_e4xe2 + temp2);
		
		PVector3 der_t0dotn_x2 = temp3/e4xe2_norm;
		PVector3 der_sintheta_x2 = der_t0dotn_x2/e0_norm;
		
		PVector3 force = nl_c_e*(costheta0*der_costheta_x2 + sintheta0*der_sintheta_x2);
		
		force(0).generateCode(code_file, 12, "polynomial[0]");
		force(1).generateCode(code_file, 12, "polynomial[1]");
		force(2).generateCode(code_file, 12, "polynomial[2]");
	}
	
	code_file << "}" << std::endl;
	code_file << "else if (fn == 3)" << std::endl;
	code_file << "{" << std::endl;
	
	// f3
	{
		PVector3 der_c02_x3 = _e0*_b1 - _e2*_b2;
		PVector3 der_c04_x3 = -_e0*_b3 + _e4*_b4;
		
		PMatrix3 der_t1_x3 = -(_e2*der_c04_x3.transpose() + c04*identity + _e4*der_c02_x3.transpose() + c02*identity);
		PVector3 der_t0dott1_x3 = der_t1_x3.transpose()*_t0;
		PVector3 der_costheta_x3 = -der_t0dott1_x3/e0n2;
		
		PVector3 temp1 = -c03*l_quartic_to_cubic_P(_e4,_e1,_e4_r,_e1_r) - c01*l_quartic_to_cubic_P(_e4,_e3,_e4_r,_e3_r);
		PVector3 temp2 = -c03*l_quartic_to_cubic_P(_e2,_e1,_e2_r,_e1_r) - c01*l_quartic_to_cubic_P(_e2,_e3,_e2_r,_e3_r);
		PVector3 temp3 = (-e4dote2 + e2_norm*e2_norm)/e4xe2_norm*temp1 + (e4_norm*e4_norm - e4dote2)/e4xe2_norm*temp2;
		
		PVector3 der_t0dotn_x3 = (_t0.cross(_e4-_e2)*e4xe2_norm - temp3)/(e4xe2_norm*e4xe2_norm);
		PVector3 der_sintheta_x3 = (der_t0dotn_x3*e0_norm)/e0n2;
		
		PVector3 force = nl_c_e*(costheta0*der_costheta_x3 + sintheta0*der_sintheta_x3);
		
		force(0).generateCode(code_file, 12, "polynomial[0]");
		force(1).generateCode(code_file, 12, "polynomial[1]");
		force(2).generateCode(code_file, 12, "polynomial[2]");
	}
		
	code_file << "}" << std::endl;
	
	code_file << "}" << std::endl;
	
}
