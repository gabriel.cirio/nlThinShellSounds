
#include "ThinShell.h"
#include "GravityEnergy.h"
#include "MembraneEnergy.h"
#include "BendingEnergy.h"
#include "BendingEnergy_NonlinearHingeWithIsometry.h"
#include "BendingEnergy_Isometric.h"

#include "util/Timer.h"

#include <iostream>
#include <fstream>
#include <omp.h>

#include <igl/readOBJ.h>
#include <igl/writeOBJ.h>
#include <igl/per_vertex_normals.h>
#include <igl/vertex_triangle_adjacency.h>
#include <igl/triangle_triangle_adjacency.h>
#include <igl/unique_edge_map.h>
#include <igl/edge_flaps.h>
#include <igl/decimate.h>
#include <igl/qslim.h>

#ifdef HAVE_CUDA
#include "cuda/CudaUtilsHost.h"
#include "cuda/SubspacePolynomialPrecomputation.cuh"
#endif

#define USE_OMP

ThinShell::ThinShell()
{
	m_gravity_vector = Vector3(0., 0., -9.8);
    //m_gravity_vector = Vector3(0., 9.8, 0.);
	
	m_density = 1000.;
	m_young_modulus = 1e07;
 	m_poisson_ratio = 0.35;
	m_thickness = 0.002;
	
	m_omp_max_threads = omp_get_max_threads();
    m_force_jacobians.resize(m_omp_max_threads);
	m_damping_jacobians.resize(m_omp_max_threads);
    m_masses.resize(m_omp_max_threads);
	
	m_nb_dofs = 0;
	
	m_bending_type = 0; // 0:hingeisometric, 1:hinge, 2:isometric, 3:koiter
	
	m_use_gpu = false;
}

void ThinShell::configure(const JSONConfiguration::Object & config, const std::string & base_path)
{
    std::string thinshell_obj_filename = config["filename"].GetString();
    std::string thinshell_obj_full_filename = thinshell_obj_filename != "" ? base_path + thinshell_obj_filename : "";
    this->createThinShell(thinshell_obj_full_filename);
    if (config.HasMember("bending_type")) m_bending_type = config["bending_type"].GetInt();
}

void ThinShell::createThinShell(std::string filename, Vector3 offset)
{
    m_filename = filename;
    
	igl::readOBJ(filename, m_rest_pos, m_F);
	
	m_rest_pos.rowwise() += offset.transpose();
	
	m_pos = m_rest_pos;
	
	this->compute_helper_arrays();
	
	m_nb_nodes = m_rest_pos.rows();
	m_fixed_nodes.resize(m_nb_nodes, false);
}

void ThinShell::compute_helper_arrays()
{
	igl::vertex_triangle_adjacency(m_rest_pos.rows(), m_F, m_per_node_triangles, m_per_node_triangles_node_local_index);
	igl::triangle_triangle_adjacency(m_F, m_per_triangle_triangles, m_per_triangle_triangles_edge_local_index);
	igl::unique_edge_map(m_F, m_E_directed, m_E_unique, m_map_edge_directed_to_unique, m_map_edge_unique_to_directed);
	igl::edge_flaps(m_F, m_E_unique, m_map_edge_directed_to_unique, m_per_unique_edge_triangles, m_per_unique_edge_triangles_local_corners);
	igl::per_vertex_normals(m_rest_pos, m_F, m_rest_pos_normals);
	
	m_triangle_rest_area.resize(m_F.rows());
	m_triangle_rest_area.setZero();

	m_node_rest_area.resize(m_rest_pos.rows());
	m_node_rest_area.setZero();
	
	for (unsigned int i=0; i<m_F.rows(); i++)
	{
		Vector3 x0 = m_rest_pos.row(m_F(i,0));
		Vector3 x1 = m_rest_pos.row(m_F(i,1));
		Vector3 x2 = m_rest_pos.row(m_F(i,2));
		
		double area = 0.5*(x1 - x0).cross(x2 - x0).norm();
		
		m_triangle_rest_area(i) = area;
		
		// node area
		double angle_frac[3];
		angle_frac[0] = acos( (x1-x0).dot(x2-x0)/((x1-x0).norm()*(x2-x0).norm()) ) / PI;
		angle_frac[1] = acos( (x0-x1).dot(x2-x1)/((x0-x1).norm()*(x2-x1).norm()) ) / PI;
		angle_frac[2] = 1. - angle_frac[0] - angle_frac[1] ;
		
		for (unsigned int n=0; n<3; n++) 
			m_node_rest_area(m_F(i,n)) += angle_frac[n]*area;
	}
	
	m_per_node_edges.clear();
	m_node_neighbors.clear();
	
	m_per_node_edges.resize(m_rest_pos.rows());
	m_node_neighbors.resize(m_rest_pos.rows());
	
	for (unsigned int i=0; i<m_E_unique.rows(); i++)
	{
		m_per_node_edges[m_E_unique(i,0)].push_back(i);
		m_per_node_edges[m_E_unique(i,1)].push_back(i);
	}
	
	for (unsigned int i=0; i<m_per_node_edges.size(); i++)
	{
		for (unsigned int j=0; j<m_per_node_edges[i].size(); j++)
		{
			int node0 = m_E_unique(m_per_node_edges[i][j], 0);
			int node1 = m_E_unique(m_per_node_edges[i][j], 1);
			
			if (node0 == i) m_node_neighbors[node0].push_back(node1);
			else m_node_neighbors[node1].push_back(node0);
		}
	}
	
	m_per_triangles_unique_edges.resize(m_F.rows(), 3);
	
	for (unsigned int e=0; e<m_per_unique_edge_triangles.rows(); e++)
	{
		int va = m_E_unique(e,0);
		int vb = m_E_unique(e,1);
		
		int f = m_per_unique_edge_triangles(e,0);
		if (f != -1)
		{
			if ( ( (m_F(f,1) == va) && (m_F(f,2) == vb) ) || ( (m_F(f,2) == va) && (m_F(f,1) == vb) ) )
				m_per_triangles_unique_edges(f,0) = e;
			else if ( ( (m_F(f,0) == va) && (m_F(f,2) == vb) ) || ( (m_F(f,2) == va) && (m_F(f,0) == vb) ) )
				m_per_triangles_unique_edges(f,1) = e;
			else if ( ( (m_F(f,0) == va) && (m_F(f,1) == vb) ) || ( (m_F(f,1) == va) && (m_F(f,0) == vb) ) )
				m_per_triangles_unique_edges(f,2) = e;
			else
				std::cout << "IMPOSSIBLE!!!" << std::endl;
		}
		
		f = m_per_unique_edge_triangles(e,1);
		if (f != -1)
		{
			if ( ( (m_F(f,1) == va) && (m_F(f,2) == vb) ) || ( (m_F(f,2) == va) && (m_F(f,1) == vb) ) )
				m_per_triangles_unique_edges(f,0) = e;
			else if ( ( (m_F(f,0) == va) && (m_F(f,2) == vb) ) || ( (m_F(f,2) == va) && (m_F(f,0) == vb) ) )
				m_per_triangles_unique_edges(f,1) = e;
			else if ( ( (m_F(f,0) == va) && (m_F(f,1) == vb) ) || ( (m_F(f,1) == va) && (m_F(f,0) == vb) ) )
				m_per_triangles_unique_edges(f,2) = e;
			else
				std::cout << "IMPOSSIBLE!!!" << std::endl;
		}
		
	}
	
}

void ThinShell::computeDoFs()
{
	m_per_node_dofs.resize(m_nb_nodes, -1);
	int current_dof = 0;
	for (unsigned int i=0; i<m_nb_nodes; i++)
	{
		if (!m_fixed_nodes[i])
		{
			m_per_node_dofs[i] = current_dof;
			current_dof += 3;
		}
	}
	
	m_nb_dofs = current_dof;
}

void ThinShell::fixNode(int index)
{
	m_fixed_nodes[index] = true;
}

int ThinShell::fixNodes(Vector3 pos, Real radius)
{
	Real sq_radius = radius*radius;
	unsigned int fixed_count = 0;
	
	Vector3T posT = pos.transpose();
	
	for (unsigned int i=0; i<m_nb_nodes; i++)
	{
		if ( (m_rest_pos.row(i) - posT).squaredNorm() < sq_radius )
		{
			this->fixNode(i);
			fixed_count++;
		}
	}
	
	return fixed_count;
}

void ThinShell::prepareForSimulation()
{
	#ifndef HAVE_CUDA
		m_use_gpu = false;
	#endif
		
	m_vel.resize(m_nb_nodes, 3);
	m_vel.setZero();
	
	m_forces.resize(m_nb_nodes, 3);
	m_forces.setZero();
	
	this->computeDoFs();
	
	// setup energies
	
	if (m_bending_type == 0)
        m_energies.push_back(std::make_pair(new BendingEnergy_NonlinearHingeWithIsometry(m_rest_pos, m_pos, m_F, m_per_node_dofs, m_triangle_rest_area, m_E_unique, m_per_unique_edge_triangles, m_per_unique_edge_triangles_local_corners, m_young_modulus, m_poisson_ratio, m_thickness), "Bending"));
    else if (m_bending_type == 1)
        m_energies.push_back(std::make_pair(new BendingEnergy(m_rest_pos, m_pos, m_F, m_per_node_dofs, m_triangle_rest_area, m_E_unique, m_per_unique_edge_triangles, m_per_unique_edge_triangles_local_corners, m_per_triangles_unique_edges, m_young_modulus, m_poisson_ratio, m_thickness), "Bending"));
    else if (m_bending_type == 2)
		m_energies.push_back(std::make_pair(new IsometricBendingEnergy(m_rest_pos, m_pos, m_F, m_per_node_dofs, m_triangle_rest_area, m_E_unique, m_per_unique_edge_triangles, m_per_unique_edge_triangles_local_corners, m_young_modulus, m_poisson_ratio, m_thickness), "Bending"));
    
	m_energies.push_back(std::make_pair(new MembraneEnergy(m_rest_pos, m_pos, m_F, m_per_node_dofs, m_triangle_rest_area, m_young_modulus, m_poisson_ratio), "Membrane"));
    
	m_energy_is_active.resize(m_energies.size(), true);
	
	for (unsigned int i=0; i<m_energies.size(); i++)
		m_energies[i].first->use_gpu(m_use_gpu);
	
	for (unsigned int i=0; i<m_energies.size(); i++)
		if (m_energy_is_active[i])
			m_energies[i].first->setup();
        
    Real tot_mass = 0.;
	for (int i=0; i<m_triangle_rest_area.rows(); i++)
        tot_mass += m_triangle_rest_area(i);
    
    std::cout << "Total mass: " << tot_mass*m_density*m_thickness << " kg" << std::endl;    
}

void ThinShell::getMeshData(std::pair<MatrixX, MatrixXi> & mesh_data)
{
    mesh_data.first = m_pos;
    mesh_data.second = m_F;
}

void ThinShell::prepareForStep(Real current_time, Real dt)
{
	for (unsigned int i=0; i<m_energies.size(); i++)
		if (m_energy_is_active[i])
			m_energies[i].first->prepareForStep();
}

void ThinShell::writeMeshes(std::string base_filename)
{
    igl::writeOBJ("shell_" + base_filename + ".obj", m_pos, m_F);
}

void ThinShell::computeMass()
{
	for (unsigned int i=0; i<m_masses.size(); i++) m_masses[i].clear();
	
	#ifdef USE_OMP
	#pragma omp parallel
	#endif
	{
		// create matrices to be reused
		Matrix1 mat11;
		Jacobian mass;
		
		int omp_thread_index = omp_get_thread_num();

		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (unsigned int i=0; i<m_nb_nodes; i++)
		{
			if (m_fixed_nodes[i]) continue;
			
			mat11 << m_density*m_thickness*m_node_rest_area(i);
			mass.jacobian = mat11;
			mass.index_i = m_per_node_dofs[i];
			mass.index_j = m_per_node_dofs[i];
			m_masses[omp_thread_index].push_back(mass);
			mass.index_i++;
			mass.index_j++;
			m_masses[omp_thread_index].push_back(mass);
			mass.index_i++;
			mass.index_j++;
			m_masses[omp_thread_index].push_back(mass);
		}
	}

}

void ThinShell::computeInternalForces()
{
	this->clearForces();
	
	std::vector<MatrixX> force_omp_arrays(m_omp_max_threads, MatrixX::Zero(m_forces.rows(), m_forces.cols()));
	
	for (unsigned int i=0; i<m_energies.size(); i++)
		if (m_energy_is_active[i])
			m_energies[i].first->computeForces(force_omp_arrays);
	
	#ifdef USE_OMP
	#pragma omp parallel for
	#endif
	for (unsigned int i=0; i<m_forces.rows(); i++)
		for (unsigned int j=0; j<m_omp_max_threads; j++)
			m_forces.row(i) += force_omp_arrays[j].row(i);	
}

void ThinShell::getNodalVectors(MatrixX & vectors)
{
    this->getNodalForces(vectors);
}

void ThinShell::getNodalForces(MatrixX & forces)
{
    forces = m_forces;
}

void ThinShell::computeInternalForceJacobians()
{
	this->clearForceJacobians();
	
	for (unsigned int i=0; i<m_energies.size(); i++)
		if (m_energy_is_active[i])
			m_energies[i].first->computeDfDx(m_force_jacobians);
}

void ThinShell::computeInternalDampingJacobians()
{
	this->clearDampingJacobians();
}

void ThinShell::clearForces()
{
	m_forces.setZero();
}

void ThinShell::clearForceJacobians()
{
	for (unsigned int i=0; i<m_force_jacobians.size(); i++) m_force_jacobians[i].clear();
}

void ThinShell::clearDampingJacobians()
{
	for (unsigned int i=0; i<m_damping_jacobians.size(); i++) m_damping_jacobians[i].clear();
}

void ThinShell::getDofState(Eigen::Ref<VectorX> positions, Eigen::Ref<VectorX> velocities)
{
	#ifdef USE_OMP
	#pragma omp parallel for
	#endif
	for (unsigned int i=0; i<m_nb_nodes; i++)
	{
		if (m_per_node_dofs[i] != -1)
		{
			positions.block(m_per_node_dofs[i], 0, 3, 1) = m_pos.row(i).transpose();
			velocities.block(m_per_node_dofs[i], 0, 3, 1) = m_vel.row(i).transpose();
		}
    }
}

void ThinShell::setDofState(const Eigen::Ref<const VectorX> & positions, const Eigen::Ref<const VectorX> & velocities)
{
	#ifdef USE_OMP
	#pragma omp parallel for
	#endif
	for (unsigned int i=0; i<m_nb_nodes; i++)
	{
		if (m_per_node_dofs[i] != -1)
		{
			m_pos.row(i) = positions.block(m_per_node_dofs[i], 0, 3, 1).transpose();
			m_vel.row(i) = velocities.block(m_per_node_dofs[i], 0, 3, 1).transpose();
		}
    }
}

void ThinShell::getDofForces(Eigen::Ref<VectorX> forces)
{
	forces.setZero();
	
	#ifdef USE_OMP
	#pragma omp parallel for
	#endif
	for (unsigned int i=0; i<m_nb_nodes; i++)
	{
		if (m_per_node_dofs[i] != -1)
		{
			forces.block(m_per_node_dofs[i], 0, 3, 1) = m_forces.row(i).transpose();
		}
    }
}

void ThinShell::deactivateEnergy(std::string energy_name)
{
	bool found = false;
	for (unsigned int i=0; i<m_energies.size(); i++)
	{
		if (m_energies[i].second == energy_name)
		{
			m_energy_is_active[i] = false;
			found = true;
			std::cout << "Deactivated " << energy_name << " energy" << std::endl;
			break;
		}
	}
	
	if (!found)
		std::cout << "WARNING: Could not find " << energy_name << " energy to deactivate" << std::endl;
	
}

/// ************************************************************************** //
/// **************** SUBSPACE POLYNOMIAL PRECOMPUTATION STUFF **************** //
/// ************************************************************************** //

void ThinShell::to_mat(int num_lowfreq_modes, const MatrixX & U, RedMatData & data, int dof0, int dof1, int dof2, Real valx, Real valy, Real valz) // cubic
{
	if (m_use_gpu)
	{
		#ifdef HAVE_CUDA
		//CudaHostUtilities::EventTimer timer;
		//timer.start();
		cuda_to_mat_cubic(m_thread_map_cubic.length()/3, U.cols(), dof0, dof1, dof2, valx, valy, valz, m_thread_map_cubic.gpu(), m_Ut_gpu, data.Cr_v_gpu);
		//timer.stop();
		//std::cout << "GPU: " << timer.elapsed()/1000. << std::endl;
		#endif
	}
	else
	{
		//Timer::tic("CPU: ");
		
		int num_allfreq_modes = U.cols();
		int index = 0;
		for (int i=0; i<num_lowfreq_modes; i++)
			for (int j=i; j<num_lowfreq_modes; j++)
				for (int k=j; k<num_allfreq_modes; k++)
				{
					Real val_red = 0.;
					if ( (i != j) && (j != k) && (i != k) ) // all different
					{
						val_red = U(dof0,i)*U(dof1,j)*U(dof2,k)
								+ U(dof0,i)*U(dof1,k)*U(dof2,j)
								+ U(dof0,j)*U(dof1,i)*U(dof2,k)
								+ U(dof0,j)*U(dof1,k)*U(dof2,i)
								+ U(dof0,k)*U(dof1,i)*U(dof2,j)
								+ U(dof0,k)*U(dof1,j)*U(dof2,i);
					}
					else if ( (i == j) && (j == k) ) // all the same
					{
						val_red = U(dof0,i)*U(dof1,j)*U(dof2,k);
					}
					else // 2 same, 1 different
					{
						if (i == j)
						{
							val_red = U(dof0,i)*U(dof1,i)*U(dof2,k)
									+ U(dof0,i)*U(dof1,k)*U(dof2,i)
									+ U(dof0,k)*U(dof1,i)*U(dof2,i);
						}
						else if (i == k)
						{
							val_red = U(dof0,i)*U(dof1,i)*U(dof2,j)
									+ U(dof0,i)*U(dof1,j)*U(dof2,i)
									+ U(dof0,j)*U(dof1,i)*U(dof2,i);
						}
						else // (j == k)
						{
							val_red = U(dof0,j)*U(dof1,j)*U(dof2,i)
									+ U(dof0,j)*U(dof1,i)*U(dof2,j)
									+ U(dof0,i)*U(dof1,j)*U(dof2,j);
						}
					}
					
					data.Cr_v(index*3+0) += valx*val_red;
					data.Cr_v(index*3+1) += valy*val_red;
					data.Cr_v(index*3+2) += valz*val_red;
					
					index++;
				}
				
		//Timer::toc();
	}
	
}

void ThinShell::to_mat(int num_lowfreq_modes, const MatrixX & U, RedMatData & data, int dof0, int dof1, Real valx, Real valy, Real valz) // quadratic
{
	if (m_use_gpu)
	{
		#ifdef HAVE_CUDA
		//CudaHostUtilities::EventTimer timer;
		//timer.start();
		cuda_to_mat_quadratic(m_thread_map_quadratic.length()/2, U.cols(), dof0, dof1, valx, valy, valz, m_thread_map_quadratic.gpu(), m_Ut_gpu, data.Qr_v_gpu);
		//timer.stop();
		//std::cout << "GPU: " << timer.elapsed()/1000. << std::endl;
		#endif
	}
	else
	{
		int num_allfreq_modes = U.cols();
		int index = 0;
		for (int i=0; i<num_lowfreq_modes; i++)
			for (int j=i; j<num_allfreq_modes; j++)
			{
				Real val_red = 0.;
				if (i == j)
				{
					val_red = U(dof0,i)*U(dof1,j);
				}
				else // (i != j)
				{
					val_red = U(dof0,i)*U(dof1,j) + U(dof0,j)*U(dof1,i);
				}
				data.Qr_v(index*3+0) += valx*val_red;
				data.Qr_v(index*3+1) += valy*val_red;
				data.Qr_v(index*3+2) += valz*val_red;
				index++;
			}
	}
}

void ThinShell::to_mat(int num_lowfreq_modes, const MatrixX & U, RedMatData & data, int dof0, Real valx, Real valy, Real valz) // linear
{
	if (m_use_gpu)
	{
		#ifdef HAVE_CUDA
		//CudaHostUtilities::EventTimer timer;
		//timer.start();
		cuda_to_mat_linear(U.cols(), U.cols(), dof0, valx, valy, valz, m_Ut_gpu, data.Lr_v_gpu);
		//timer.stop();
		//std::cout << "GPU: " << timer.elapsed()/1000. << std::endl;
		#endif
	}
	else
	{
		int num_allfreq_modes = U.cols();
		int index = 0;
		for (unsigned int i=0; i<num_allfreq_modes; i++)
		{
			Real val_red = U(dof0,i);
			data.Lr_v(index*3+0) += valx*val_red;
			data.Lr_v(index*3+1) += valy*val_red;
			data.Lr_v(index*3+2) += valz*val_red;
			index++;
		}
	}
}

void ThinShell::to_mat(int num_lowfreq_modes, const MatrixX & U, RedMatData & data, Real valx, Real valy, Real valz) // constant
{
	data.Kr_v(0) += valx;
	data.Kr_v(1) += valy;
	data.Kr_v(2) += valz;
}

void ThinShell::setup_reduced_for_nodei(int node_i, const std::set<int> & stencil_dofs, int num_lowfreq_modes, const MatrixX & U, RedMatData & data)
{
	std::vector<int> stencil_dofs_vec(stencil_dofs.begin(), stencil_dofs.end());
	
	ValuesForDof values_for_dof[3];
	values_for_dof[0].cubic.resize(stencil_dofs_vec.size());
	for (int i=0; i<stencil_dofs_vec.size(); i++)
		values_for_dof[0].cubic[i].resize(stencil_dofs_vec.size());
	for (int i=0; i<stencil_dofs_vec.size(); i++)
		for (int j=0; j<stencil_dofs_vec.size(); j++)
			values_for_dof[0].cubic[i][j].resize(stencil_dofs_vec.size(), 0.);	
	
	values_for_dof[0].quadratic.resize(stencil_dofs_vec.size());
	for (int i=0; i<stencil_dofs_vec.size(); i++)
		values_for_dof[0].quadratic[i].resize(stencil_dofs_vec.size(), 0.);
	
	values_for_dof[0].linear.resize(stencil_dofs_vec.size(), 0.);
	
	values_for_dof[0].constant = 0.;
	
	values_for_dof[1] = values_for_dof[0];
	values_for_dof[2] = values_for_dof[0];
	
	for (unsigned int i=0; i<m_energies.size(); i++)
		if (m_energy_is_active[i])
			m_energies[i].first->subspacePolynomialPrecomputationNodei(node_i, stencil_dofs_vec, values_for_dof);
	
	for (int i=0; i<stencil_dofs_vec.size(); i++)
		for (int j=0; j<stencil_dofs_vec.size(); j++)
			for (int k=0; k<stencil_dofs_vec.size(); k++)
				if ( (values_for_dof[0].cubic[i][j][k] != 0) || (values_for_dof[1].cubic[i][j][k] != 0) || (values_for_dof[2].cubic[i][j][k] != 0) )
					to_mat(num_lowfreq_modes, U, data, stencil_dofs_vec[i], stencil_dofs_vec[j], stencil_dofs_vec[k], values_for_dof[0].cubic[i][j][k], values_for_dof[1].cubic[i][j][k], values_for_dof[2].cubic[i][j][k]);

	for (int i=0; i<stencil_dofs_vec.size(); i++)
		for (int j=0; j<stencil_dofs_vec.size(); j++)
			if ( (values_for_dof[0].quadratic[i][j] != 0) || (values_for_dof[1].quadratic[i][j] != 0) || (values_for_dof[2].quadratic[i][j] != 0) )
				to_mat(num_lowfreq_modes, U, data, stencil_dofs_vec[i], stencil_dofs_vec[j], values_for_dof[0].quadratic[i][j], values_for_dof[1].quadratic[i][j], values_for_dof[2].quadratic[i][j]);
			
	for (int i=0; i<stencil_dofs_vec.size(); i++)
		if ( (values_for_dof[0].linear[i] != 0) || (values_for_dof[1].linear[i] != 0) || (values_for_dof[2].linear[i] != 0) )
			to_mat(num_lowfreq_modes, U, data, stencil_dofs_vec[i], values_for_dof[0].linear[i], values_for_dof[1].linear[i], values_for_dof[2].linear[i]);
			
	if ( (values_for_dof[0].constant != 0) || (values_for_dof[1].constant != 0) || (values_for_dof[2].constant != 0) )
		to_mat(num_lowfreq_modes, U, data, values_for_dof[0].constant, values_for_dof[1].constant, values_for_dof[2].constant);
	
}

void ThinShell::setupReducedForces(const MatrixX & U, int num_lowfreq_modes, VectorX & Kr, std::vector<VectorX> & Lr, std::vector<std::vector<VectorX>> & Qr, std::vector<std::vector<std::vector<VectorX>>> & Cr)
{
	int nb_red_dofs = U.cols();
	Kr = VectorX::Zero(nb_red_dofs);
	
	Lr.resize(nb_red_dofs, VectorX::Zero(nb_red_dofs));
	
	Qr.resize(num_lowfreq_modes);
	for (unsigned int i=0; i<num_lowfreq_modes; i++)
		Qr[i].resize(nb_red_dofs);
	
	Cr.resize(num_lowfreq_modes);
	for (unsigned int i=0; i<num_lowfreq_modes; i++)
	{
		Cr[i].resize(num_lowfreq_modes);
		for (unsigned int j=0; j<num_lowfreq_modes; j++)
			Cr[i][j].resize(nb_red_dofs);
	}
	
	for (unsigned int i=0; i<num_lowfreq_modes; i++)
		for (unsigned int j=i; j<nb_red_dofs; j++)
			Qr[i][j].setZero(nb_red_dofs);
			
	for (unsigned int i=0; i<num_lowfreq_modes; i++)
		for (unsigned int j=i; j<num_lowfreq_modes; j++)
			for (unsigned int k=j; k<nb_red_dofs; k++)
				Cr[i][j][k].setZero(nb_red_dofs);
	
	int completed = 0;
	
	Timer::tic("... computing time: ");

	int num_allfreq_modes = U.cols();
	
	// build map of index to coords
	int nb_linear = num_allfreq_modes;
	int nb_quadratic = 0;
	int nb_cubic = 0;
	for (int i=0; i<num_lowfreq_modes; i++)
		for (int j=i; j<num_allfreq_modes; j++)
			nb_quadratic++;
		
	for (int i=0; i<num_lowfreq_modes; i++)
		for (int j=i; j<num_lowfreq_modes; j++)
			for (int k=j; k<num_allfreq_modes; k++)
				nb_cubic++;
	
	std::vector<std::set<int>> stencil_dofs_per_node(m_rest_pos.rows());
	for (unsigned int i=0; i<m_energies.size(); i++)
		if (m_energy_is_active[i])
			m_energies[i].first->prepareForSubspacePolynomialPrecomputation(stencil_dofs_per_node);
	
	if (m_use_gpu)
	{
		#ifdef HAVE_CUDA
		
		std::cout << "...using GPU..." << std::endl;
		
		m_thread_map_cubic.allocate(nb_cubic*3);
		m_thread_map_quadratic.allocate(nb_quadratic*2);
		int index_cubic = 0;
		int index_quadratic = 0;
		for (int i=0; i<num_lowfreq_modes; i++)
			for (int j=i; j<num_lowfreq_modes; j++)
			{
				for (int k=j; k<num_allfreq_modes; k++)
				{
					m_thread_map_cubic.vec()(index_cubic*3+0) = i;
					m_thread_map_cubic.vec()(index_cubic*3+1) = j;
					m_thread_map_cubic.vec()(index_cubic*3+2) = k;
					index_cubic++;
				}
			}
		
		for (int i=0; i<num_lowfreq_modes; i++)
			for (int j=i; j<num_allfreq_modes; j++)
			{
				m_thread_map_quadratic.vec()(index_quadratic*2+0) = i;
				m_thread_map_quadratic.vec()(index_quadratic*2+1) = j;
				index_quadratic++;
			}
			
		m_thread_map_cubic.copyCpuToGpu();
		m_thread_map_quadratic.copyCpuToGpu();
		
		#ifdef USE_OMP 
			int num_threads = m_omp_max_threads;
		#else
			int num_threads = 1;
		#endif
			
		std::vector<MemoryArray<Real>> CrUt_v_omp(num_threads);
		std::vector<MemoryArray<Real>> QrUt_v_omp(num_threads);
		std::vector<MemoryArray<Real>> LrUt_v_omp(num_threads);
		std::vector<VectorX> Kr_v_omp(num_threads);
		
		MatrixX Ut = U.transpose();
		
		cudaMalloc((void**)&m_Ut_gpu, Ut.rows()*Ut.cols()*sizeof(double));
		cudaMemcpy(m_Ut_gpu, Ut.data(), Ut.rows()*Ut.cols()*sizeof(double), cudaMemcpyHostToDevice);
			
		#ifdef USE_OMP
		#pragma omp parallel
		#endif
		{
			int omp_thread_index = omp_get_thread_num();
			
			auto & CrUt_v = CrUt_v_omp[omp_thread_index];
			auto & QrUt_v = QrUt_v_omp[omp_thread_index];
			auto & LrUt_v = LrUt_v_omp[omp_thread_index];
			auto & Kr_v = Kr_v_omp[omp_thread_index];
			
			CrUt_v.allocate(nb_cubic*num_allfreq_modes); // ~100MB for 100 modes
			QrUt_v.allocate(nb_quadratic*num_allfreq_modes);
			LrUt_v.allocate(nb_linear*num_allfreq_modes);
			CrUt_v.zeroGpu();
			QrUt_v.zeroGpu();
			LrUt_v.zeroGpu();
			Kr_v.resize(Kr.rows());
		
			RedMatData data;
			
			//cudaMalloc((void**)&data.Kr_v_gpu, 3*sizeof(double));
			cudaMalloc((void**)&data.Lr_v_gpu, nb_linear*3*sizeof(double));
			cudaMalloc((void**)&data.Qr_v_gpu, nb_quadratic*3*sizeof(double));
			cudaMalloc((void**)&data.Cr_v_gpu, nb_cubic*3*sizeof(double));
			
			data.Kr_v.resize(3);
		
			#ifdef USE_OMP
			#pragma omp for
			#endif
			for (int node_i=0; node_i<m_rest_pos.rows(); node_i++)
			{
				//cudaMemset(data.Kr_v_gpu, 0, 3*sizeof(double));
				cudaMemset(data.Lr_v_gpu, 0, nb_linear*3*sizeof(double));
				cudaMemset(data.Qr_v_gpu, 0, nb_quadratic*3*sizeof(double));
				cudaMemset(data.Cr_v_gpu, 0, nb_cubic*3*sizeof(double));
				
				data.Kr_v.setZero();
			
				//Timer::tic("Comp: ");
				// We compute the vectors per node without Ut projection, and then project them once we have touched all the triangles that have that node.
				// Doing it per node allows us to store all the pre-projected vectors only for the 3 dofs that are being treated (x,y,z of current node).
				this->setup_reduced_for_nodei(node_i, stencil_dofs_per_node[node_i], num_lowfreq_modes, U, data);
				//Timer::toc();
				
				// project (multiply by Ut)

				//Timer::tic("Mult: ");
				//CudaHostUtilities::EventTimer timer;
				//timer.start();
				cuda_cubic_prod_Ut(m_thread_map_cubic.length()/3, num_allfreq_modes, node_i*3, m_Ut_gpu, data.Cr_v_gpu, CrUt_v.gpu());
				cuda_quadratic_prod_Ut(m_thread_map_quadratic.length()/2, num_allfreq_modes, node_i*3, m_Ut_gpu, data.Qr_v_gpu, QrUt_v.gpu());
				cuda_linear_prod_Ut(nb_linear, num_allfreq_modes, node_i*3, m_Ut_gpu, data.Lr_v_gpu, LrUt_v.gpu());
				//timer.stop();
				//std::cout << "GPU: " << timer.elapsed()/1000. << std::endl;
				//Timer::toc();
					
				Kr_v += Ut.block(0, node_i*3, Ut.rows(), 3)*data.Kr_v;

				completed++;
				if ( (m_rest_pos.rows() > 10) && (completed % (m_rest_pos.rows()/10) == 0) )
					std::cout << "..." << std::ceil(Real(completed)/Real(m_rest_pos.rows())*100.) << "%" << std::endl;
				
			}
			
			CrUt_v.copyGpuToCpu();
			QrUt_v.copyGpuToCpu();
			LrUt_v.copyGpuToCpu();
		}
		
		// aggregate all omp vectors into the first one
		for (unsigned int o=1; o<num_threads; o++)
		{
			CrUt_v_omp[0].vec() += CrUt_v_omp[o].vec();
			QrUt_v_omp[0].vec() += QrUt_v_omp[o].vec();
			LrUt_v_omp[0].vec() += LrUt_v_omp[o].vec();
			Kr_v_omp[0] += Kr_v_omp[o];
		}
		
		auto & CrUt_v_aggr = CrUt_v_omp[0];
		auto & QrUt_v_aggr = QrUt_v_omp[0];
		auto & LrUt_v_aggr = LrUt_v_omp[0];
		auto & Kr_v_aggr = Kr_v_omp[0];
				
		int index = 0;
		for (unsigned int i=0; i<num_lowfreq_modes; i++)
			for (unsigned int j=i; j<num_lowfreq_modes; j++)
				for (unsigned int k=j; k<num_allfreq_modes; k++)
                {
					for (unsigned int l=0; l<num_allfreq_modes; l++)
						Cr[i][j][k](l) += CrUt_v_aggr.vec()(l*nb_cubic + index);
					
					index++;
                }
				
		index = 0;
		for (unsigned int i=0; i<num_lowfreq_modes; i++)
			for (unsigned int j=i; j<num_allfreq_modes; j++)
            {
				for (unsigned int l=0; l<num_allfreq_modes; l++)
					Qr[i][j](l) += QrUt_v_aggr.vec()(l*nb_quadratic + index);
		
				index++;
            }
			
		for (unsigned int i=0; i<num_allfreq_modes; i++)
			for (unsigned int l=0; l<num_allfreq_modes; l++)
				Lr[i](l) += LrUt_v_aggr.vec()(l*nb_linear + i);
			
		Kr += Kr_v_aggr;
		
		#endif
	}
	else
	{
		MatrixX Ut = U.transpose();
	
		#ifdef USE_OMP
		#pragma omp parallel for
		#endif
		for (int node_i=0; node_i<m_rest_pos.rows(); node_i++)
		{
			RedMatData data;
		
			data.Kr_v.setZero(3);
			data.Lr_v.setZero(nb_linear*3);
			data.Qr_v.setZero(nb_quadratic*3);
			data.Cr_v.setZero(nb_cubic*3);

			//Timer::tic("Comp: ");
			// We compute the vectors per node without Ut projection, and then project them once we have touched all the triangles that have that node.
			// Doing it per node allows us to store all the pre-projected vectors only for the 3 dofs that are being treated (x,y,z of current node).
			this->setup_reduced_for_nodei(node_i, stencil_dofs_per_node[node_i], num_lowfreq_modes, U, data);
			//Timer::toc();
		
			//Timer::tic("Mult: ");
			#ifdef USE_OMP
			#pragma omp critical
			#endif
			{
				MatrixX Ut_chunk = Ut.block(0, node_i*3, Ut.rows(), 3);
				
				// project (multiply by Ut)
				
				int index = 0;
				for (unsigned int i=0; i<num_lowfreq_modes; i++)
					for (unsigned int j=i; j<num_lowfreq_modes; j++)
						for (unsigned int k=j; k<num_allfreq_modes; k++)
						{
							Cr[i][j][k] += Ut_chunk*data.Cr_v.segment(index*3,3);
							index++;
						}
						
				index = 0;
				for (unsigned int i=0; i<num_lowfreq_modes; i++)
					for (unsigned int j=i; j<num_allfreq_modes; j++)
					{
						Qr[i][j] += Ut_chunk*data.Qr_v.segment(index*3,3);
						index++;
					}
				
				index = 0;
				for (unsigned int i=0; i<num_allfreq_modes; i++)
				{
					Lr[i] += Ut_chunk*data.Lr_v.segment(index*3,3);
					index++;
				}

				Kr += Ut_chunk*data.Kr_v;

				completed++;
				if ( (m_rest_pos.rows() > 10) && (completed % (m_rest_pos.rows()/10) == 0) )
					std::cout << "..." << std::ceil(Real(completed)/Real(m_rest_pos.rows())*100.) << "%" << std::endl;

			}
			//Timer::toc();
		}
	}
	
	Timer::toc();

	std::cout << std::endl;
	
}

