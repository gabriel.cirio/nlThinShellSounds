
#ifndef MEMORY_ARRAY_H
#define MEMORY_ARRAY_H

#include "typedefs.h"
#include "../cuda/CudaUtilsHost.h"
#include <iostream>

template <typename T>
class MemoryArray
{
	public:
		
		typedef Eigen::Map<Eigen::Matrix<T, Eigen::Dynamic, 1>> EigenMap;
		
		MemoryArray() : m_vec(nullptr, 0)
		{
			m_cpu = nullptr;
			m_gpu = nullptr;
			m_length = 0;
		}
		
		MemoryArray(int length) : MemoryArray()
		{
			allocate(length);
		}
		
		~MemoryArray()
		{
			free();
		}
		
		T* cpu() { return m_cpu; }
		T* gpu() { return m_gpu; }
		
		T* cpu() const { return m_cpu; }
		T* gpu() const { return m_gpu; }
		
		Eigen::Map<Eigen::Matrix<T, Eigen::Dynamic, 1>> & vec() { return m_vec; }
		
		int length() const { return m_length; }
		
		void allocate(int length)
		{
			m_length = length;
			
			CudaHostUtilities::allocateHostMemory((void**)&m_cpu, m_length*sizeof(T));

			// this is taken from Eigen's doc. The "new" doesn't allocate anything: it is just used to change the array pointer.
			new (&m_vec) EigenMap(m_cpu, m_length);

			#ifdef HAVE_CUDA
				cudaMalloc((void**)&m_gpu, m_length*sizeof(T));
			#endif
		}
		
		void free()
		{
			if (m_cpu != nullptr)
			{
				CudaHostUtilities::freeHostMemory((void*)m_cpu);
				m_cpu = nullptr;
			}
			#ifdef HAVE_CUDA
				if (m_gpu != nullptr)
				{
					cudaFree((void*)m_gpu);
					m_gpu = nullptr;
				}
			#endif
		}
		
		void copyCpuToGpu()
		{
			#ifdef HAVE_CUDA
			cudaMemcpy(m_gpu, m_cpu, m_length*sizeof(T), cudaMemcpyHostToDevice);
			#endif
		}
		
		void copyGpuToCpu()
		{
			#ifdef HAVE_CUDA
			cudaMemcpy(m_cpu, m_gpu, m_length*sizeof(T), cudaMemcpyDeviceToHost);
			#endif
		}
		
		void zeroGpu()
		{
			#ifdef HAVE_CUDA
			cudaMemset(m_gpu, 0, m_length*sizeof(T));
			#endif
		}
		
		void print()
		{
			std::cout << vec().transpose() << std::endl;
		}
	
	private:
		
		T* m_cpu;
		T* m_gpu;
		EigenMap m_vec;
		
		int m_length;
	
};

#endif // MEMORY_ARRAY_H

