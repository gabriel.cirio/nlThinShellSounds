#ifndef CODE_FILE
#define CODE_FILE

#include "typedefs.h"

void get_polynomial(Real rest_angle, Real nlce, const Vector3 & x0, const Vector3 & x1, const Vector3 & x2, const Vector3 & x3, int fn, VectorX polynomial[3]);
#endif // CODE_FILE
