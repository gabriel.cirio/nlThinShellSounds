#ifndef MULTISCALESOUNDS_EXPLICIT_SYMPLECTICEULER_H
#define MULTISCALESOUNDS_EXPLICIT_SYMPLECTICEULER_H

#include "Solver.h"

class ExplicitSymplecticEulerSolver : public Solver
{
	
	public:
		
		ExplicitSymplecticEulerSolver();
		virtual ~ExplicitSymplecticEulerSolver() {}
		
		void solve(std::vector<DynamicObject*> & dyn_objects);
		
		void setDtSubsteps(int dt_substeps);
		
	private:
	
		Real m_cg_tolerance;
		int m_cg_max_iterations;

};

#endif // MULTISCALESOUNDS_EXPLICIT_SYMPLECTICEULER_H
