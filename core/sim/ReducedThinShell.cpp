
#include "ReducedThinShell.h"

#include "ThinShell.h"
#include "FastBEMFileGenerator.h"

#include "util/Plotter.h"
#include "util/SoundClip.h"
#include "util/Timer.h"

#include <omp.h>
#include <iostream>
#include <fstream>

#ifdef HAVE_CUDA
#include "cuda/CudaUtilsHost.h"
#include "cuda/ReducedThinShell.cuh"
#endif

#ifdef HAVE_MATLAB
#include <igl/matlab/matlabinterface.h>
#endif

#include <igl/readOBJ.h>
#include <igl/writeOBJ.h>
#include <igl/decimate.h>

#define USE_OMP

#define RENDER_TO_AUDIO

//#define IGNORE_Kr // disables constant term to get rid of artificial rest state forces due to isometric bending

//#define DISABLE_RUNTIME_PRUNNING

ReducedThinShell::ReducedThinShell()
{
	m_computation_space = 2; // 0 - full space, 1 - linear space, 2 - subspace
	m_compute_lyapunov_exponents = true;
	
	// **************************** //
	
	m_shell = nullptr;
	
	m_masses_lowfreq.resize(1);
	m_masses_highfreq.resize(1);
	
	m_show_mode_nb = -1;
	m_show_modes_t = 0.;
	m_show_modes_freq = 0.1;
	
	m_rayleigh_alpha = 0.5;
	m_rayleigh_beta = 75e-9;
	
	m_dt_substeps = 1;
	
	m_no_stretch_forces = false;
	m_no_bending_forces = false;
	
	m_num_modes_to_compute = 10;
	
	this->setSecondsToSave(1); // default 1 second to save
	
	m_prune_rigid_modes = true;
	m_prune_rigid_modes_threshold = -1; // -1 means prune first 6 modes
	m_num_lowfreq_modes = 0;
	m_num_highfreq_modes = 0;
	
	m_first_lowfreq_mode_to_plot = -1;
	m_last_lowfreq_mode_to_plot = -1;
	m_first_highfreq_mode_to_plot = -1;
	m_last_highfreq_mode_to_plot = -1;
	
    m_render_to_screen = false;
    
	m_post_step_plot_modes = false;
	m_post_step_write_audio = false;
	m_post_step_plot_lyapunov = false;
	
	m_sound_base_name = "test";
    m_base_folder = std::string(MULTISCALESOUNDS_RESULTS_FOLDER);
    
    m_prunning_threshold = 1e-3;
    #ifdef DISABLE_RUNTIME_PRUNNING
        m_prunning_threshold = 0.;
    #endif    
	
	m_up_rest_norm = -1;
    
    m_spectrogram_scale = 1.;
    
    m_contact_damping_scale = 10.;
}

ReducedThinShell::~ReducedThinShell()
{
	#ifdef HAVE_CUDA
	cublasDestroy(m_cublas_handle);
	#endif
}

void ReducedThinShell::configure(const JSONConfiguration::Object & config, const std::string & base_path)
{
    auto json_thinshell = config["ThinShell"].GetObject();
    m_shell = new ThinShell();
    m_shell->configure(json_thinshell, base_path);
    
    this->setMaterial(config["material"].GetString());
    this->setNumModesToCompute(config["modes_to_compute"].GetInt());
    this->setNumLowFrequencyModes(config["lowfreq_modes"].GetInt());
    this->setNumHighFrequencyModes(config["highfreq_modes"].GetInt());
    
    std::string shell_filename = m_shell->getFilename();
    std::string name = shell_filename.substr(0,shell_filename.find_last_of("."));
    name = name.substr(name.find_last_of("/\\")+1);
    std::cout << name << std::endl;
    this->setSoundBaseName(name);
    m_base_folder = base_path;
    
    // impacts
    if (config.HasMember("Impacts"))
    {
        if (m_animation_object.state.size())
        {
            std::cout << "ERROR! Cannot load impacts if an animation exists!" << std::endl;
            return;
        }
        std::cout << config["Impacts"].Size() << " impacts found." << std::endl;
        for (int i = 0; i < config["Impacts"].Size(); ++i) 
        {
            const auto & impact_config = config["Impacts"][i];
            Real time = impact_config["time"].GetDouble();
            Real radius = impact_config["radius"].GetDouble();
            Real magnitude = impact_config["magnitude"].GetDouble();
            Real spread = impact_config["spread"].GetDouble();
            Vector3 center_vec;
            if (impact_config.HasMember("center_idx")) center_vec = m_shell->getRestPos(impact_config["center_idx"].GetInt());
            else center_vec = Vector3(impact_config["center_vec"][0].GetDouble(), impact_config["center_vec"][1].GetDouble(), impact_config["center_vec"][2].GetDouble());
            
            this->addImpact(Impact(time, center_vec, radius, magnitude, spread));
            
            if (impact_config.HasMember("repeat")) 
            {
                int num_repetitions = impact_config["repeat"].GetInt();
                Real repeat_dt = impact_config["repeat_dt"].GetDouble();
                Real repeat_df = impact_config.HasMember("repeat_df") ? impact_config["repeat_df"].GetDouble() : 0.;
                for (int j=0; j<num_repetitions; j++)
                {
                    time += repeat_dt;
                    magnitude += repeat_df;
                    this->addImpact(Impact(time, center_vec, radius, magnitude, spread));
                }
            }
                    
        }
    }
    
    // turbulence
    if (config.HasMember("Turbulence"))
    {
        const auto & turbulence_config = config["Turbulence"];
        if (turbulence_config.HasMember("scale")) m_spectrogram_scale = turbulence_config["scale"].GetDouble();
    }
    
    if (config.HasMember("contact_damping_scale")) m_contact_damping_scale = config["contact_damping_scale"].GetDouble();
	
	if (config.HasMember("prunning_threshold")) m_prunning_threshold = config["prunning_threshold"].GetDouble();
    
}

void ReducedThinShell::setComputationSpace(int space)
{
    m_computation_space = space;
}

void ReducedThinShell::setSecondsToSave(Real seconds_to_save) 
{ 
	m_seconds_to_save = seconds_to_save; 
	m_saved_modal_positions_lowfreq.reserve(44100*m_dt_substeps*m_seconds_to_save);
	m_saved_modal_positions_highfreq.reserve(44100*m_dt_substeps*m_seconds_to_save);
	m_saved_modal_velocities_lowfreq.reserve(44100*m_dt_substeps*m_seconds_to_save);
	m_saved_modal_velocities_highfreq.reserve(44100*m_dt_substeps*m_seconds_to_save);
}

void ReducedThinShell::getWorldVectors(std::pair<MatrixX, MatrixX> & vectors)
{
    if (m_render_to_screen)
    {
        std::vector<Vector3> start;
        std::vector<Vector3> end;
        
        VectorX disp = VectorX(m_external_impact_sparse);
        
        for (int i=0; i<disp.rows()/3; i++)
        {
            if (disp.segment(i*3,3).squaredNorm())
            {
                start.push_back(m_rest_positions.segment(i*3,3));
                end.push_back(start.back()+disp.segment(i*3,3));
            }
        }
        
        vectors.first.resize(start.size(), 3);
        vectors.second.resize(end.size(), 3);
        
        for (int i=0; i<start.size(); i++)
        {
            vectors.first.row(i) = start[i];
            vectors.second.row(i) = end[i];
        }
    }
}

void ReducedThinShell::getNodalVectors(MatrixX & vectors)
{
    if (m_render_to_screen)
    {
        //this->getNodalForces(vectors);
        //this->getNodalImpulses(vectors);
    }
}

void ReducedThinShell::getNodalForces(MatrixX & forces)
{
    if (m_evalues_lowfreq.rows() && (m_show_mode_nb != -1))
    {
        Real amplitude = 0.1*sqrt(m_span);
        VectorX disp = amplitude*m_evecs_lowfreq.col(m_show_mode_nb);
        Eigen::Map<MatrixX> disp_mat_t(disp.data(), 3, disp.rows()/3);
        forces = disp_mat_t.transpose();
    }
    else
        return m_shell->getNodalVectors(forces);
}

void ReducedThinShell::getNodalImpulses(MatrixX & forces)
{
    VectorX disp = VectorX(m_external_impact_sparse);
    Eigen::Map<MatrixX> disp_mat_t(disp.data(), 3, disp.rows()/3);
    forces = disp_mat_t.transpose();
}

void ReducedThinShell::computeUnreducedMatrices(Eigen::SparseMatrix<Real> & M_full, VectorX & M_full_diag, Eigen::SparseMatrix<Real> & K_full)
{
	std::cout << "Computing K and M..." << std::endl;
	
	m_shell->prepareForStep(0.,0.);
	
    int nb_unreduced_dof = m_shell->getNbDof();
	
	// stiffness matrix
	
	m_shell->computeInternalForceJacobians();
	
	std::vector<std::vector<std::vector<Jacobian> >* > force_jacobians;
	force_jacobians.resize(1);
	m_shell->getDofForceJacobians(force_jacobians[0]);
	std::vector<Eigen::Triplet<Real> > KtripletList;
		
	// create K
	for (unsigned int j=0; j<force_jacobians.size(); j++)
	{
		for (unsigned int a=0; a<force_jacobians[j]->size(); a++)
		{
			for (unsigned int k=0; k<force_jacobians[j]->at(a).size(); k++)
			{
				MatrixX a33 = -force_jacobians[j]->at(a)[k].jacobian;

				int index_i = force_jacobians[j]->at(a)[k].index_i;
				int index_j = force_jacobians[j]->at(a)[k].index_j;
				
				// get dimension of jacobian (3x3, 3x1, 1x1, ...)
				unsigned int size_jac_i = a33.rows();
				unsigned int size_jac_j = a33.cols();
				
				for (unsigned int l=0; l<size_jac_i; l++)
					for (unsigned int m=0; m<size_jac_j; m++)
					{
						int local_index_i = index_i+l;
						int local_index_j = index_j+m;
						
						KtripletList.push_back(Eigen::Triplet<Real>(local_index_i, local_index_j, a33(l,m)));
					}
			}
		}
	}

	K_full.resize(nb_unreduced_dof, nb_unreduced_dof);
	K_full.setFromTriplets(KtripletList.begin(), KtripletList.end());
		
	// mass matrix
	
	m_shell->computeMass();
	
	std::vector<std::vector< std::vector<Jacobian> >* > masses;
	masses.resize(1);
	m_shell->getDofMasses(masses[0]);
	std::vector<Eigen::Triplet<Real> > MtripletList;
	M_full_diag.resize(nb_unreduced_dof);
	M_full_diag.setZero();
	
	// finish by adding the mass entries
	for (unsigned int j=0; j<masses.size(); j++)
	{
		for (unsigned int a=0; a<masses[j]->size(); a++)
		{
			for (unsigned int k=0; k<masses[j]->at(a).size(); k++)
			{
				MatrixX mass = masses[j]->at(a)[k].jacobian;
				
				int index_i = masses[j]->at(a)[k].index_i;
				int index_j = masses[j]->at(a)[k].index_j;
				
				// get dimension of jacobian (3x3, 3x1, 1x1, ...)
				unsigned int size_jac_i = mass.rows();
				unsigned int size_jac_j = mass.cols();
				
				for (unsigned int l=0; l<size_jac_i; l++)
				{
					for (unsigned int m=0; m<size_jac_j; m++)
					{
						int local_index_i = index_i+l;
						int local_index_j = index_j+m;
						
						MtripletList.push_back(Eigen::Triplet<Real>(local_index_i, local_index_j, mass(l,m)));
						if (local_index_i == local_index_j)
							M_full_diag(local_index_i) += mass(l,m);
					}
				}
			}
		}
	}
	
	M_full.resize(nb_unreduced_dof, nb_unreduced_dof); // mass matrix
	M_full.setFromTriplets(MtripletList.begin(), MtripletList.end());
}

void ReducedThinShell::computeModes(int num_modes, Eigen::SparseMatrix<Real> & M_full, Eigen::SparseMatrix<Real> & K_full, VectorX & evalues, MatrixX & evecs)
{
	int nb_unreduced_dof = m_shell->getNbDof();
	
	std::cout << "Computing " << (num_modes == -1 ? "all" : std::to_string(num_modes)) << " modes from " << nb_unreduced_dof << " dofs..." << std::endl;
	 
	// modes (they come out mass-normalized!)
	
	if (num_modes == -1) // eig (all modes!)
	{
		// For eig behavior (ALL eigenvalues)
		Eigen::GeneralizedSelfAdjointEigenSolver<MatrixX> es(K_full, M_full); // mass-normalizes the eigenvectors!
		evalues = es.eigenvalues();
		evecs = es.eigenvectors();
	}
	else // eigs (subset of modes)
	{
		#ifdef HAVE_MATLAB
		
			std::cout << "Using matlab for eigendecomposition..." << std::endl;
			Engine* matlab_engine;
			std::cout << "   Initializing matlab engine..." << std::endl;
			igl::matlab::mlinit(&matlab_engine);
			std::cout << "   Sending matrices..." << std::endl;
			igl::matlab::mlsetmatrix(&matlab_engine, "M", M_full);
			igl::matlab::mlsetmatrix(&matlab_engine, "K", K_full);
			std::cout << "   Computing eigendecomposition..." << std::endl;
			std::string code = "[V,D] = eigs(K,M," + std::to_string(num_modes) + ",'SM');";
			igl::matlab::mleval(&matlab_engine, code);
			code = "evalues = real(diag(D));";
			igl::matlab::mleval(&matlab_engine, code);
			code = "evecs = real(V);";
			igl::matlab::mleval(&matlab_engine, code);
			std::cout << "   Retrieving matrices..." << std::endl;
			MatrixX evalues_mat;
			igl::matlab::mlgetmatrix(&matlab_engine, "evalues", evalues_mat);
			evalues = evalues_mat.col(0);
			igl::matlab::mlgetmatrix(&matlab_engine, "evecs", evecs);
			
		#else
			
			std::cout << "FATAL ERROR: could not find Matlab. Make sure it's enabled in the CMakeLists.txt!" << std::endl;
			
		#endif
	}
	
	std::stringstream suffix;
	suffix << "_" << nb_unreduced_dof << "_" << (num_modes == -1 ? "all" : std::to_string(num_modes)) << (m_shell->getBendingType() == 0 ? "_iso" : "") << ".bin";
	
	std::ofstream out_modes(m_base_folder + "modes" + suffix.str(), std::ios::binary);
	int rows = evalues.rows();
	int cols = evalues.cols();
    out_modes.write((char*) (&rows), sizeof(int));
    out_modes.write((char*) (&cols), sizeof(int));
    out_modes.write((char*) evalues.data(), rows*cols*sizeof(Real) );
    rows = evecs.rows();
	cols = evecs.cols();
    out_modes.write((char*) (&rows), sizeof(int));
    out_modes.write((char*) (&cols), sizeof(int));
    out_modes.write((char*) evecs.data(), rows*cols*sizeof(Real) );
    out_modes.close();
	std::cout << "Saved file " << "modes" + suffix.str() << std::endl;
}

void ReducedThinShell::loadModes(int num_modes, VectorX & evalues, MatrixX & evecs)
{
	int nb_unreduced_dof = m_shell->getNbDof();
	
	std::stringstream suffix;
	suffix << "_" << nb_unreduced_dof << "_" << (num_modes == -1 ? "all" : std::to_string(num_modes)) << (m_shell->getBendingType() == 0 ? "_iso" : "") << ".bin";
	
	std::ifstream in_modes(m_base_folder + "modes" + suffix.str(), std::ios::binary);
	if (!in_modes)
	{
		std::cout << "File " << "modes" + suffix.str() << "not found..." << std::endl;
		return;
	}
    int rows = 0;
	int cols = 0;
    in_modes.read((char*) (&rows),sizeof(int));
    in_modes.read((char*) (&cols),sizeof(int));
    evalues.resize(rows, cols);
    in_modes.read( (char *) evalues.data() , rows*cols*sizeof(Real) );
	rows = 0;
	cols = 0;
    in_modes.read((char*) (&rows),sizeof(int));
    in_modes.read((char*) (&cols),sizeof(int));
    evecs.resize(rows, cols);
    in_modes.read( (char *) evecs.data() , rows*cols*sizeof(Real) );
    in_modes.close();
	std::cout << "Loaded file " << "modes" + suffix.str() << std::endl;
}

void ReducedThinShell::loadOrComputeModes(int num_modes, Eigen::SparseMatrix<Real> & M_full, Eigen::SparseMatrix<Real> & K_full, VectorX & evalues, MatrixX & evecs)
{
	int nb_unreduced_dof = m_shell->getNbDof();
	
	std::stringstream suffix;
	suffix << "_" << nb_unreduced_dof << "_" << (num_modes == -1 ? "all" : std::to_string(num_modes)) << (m_shell->getBendingType() == 0 ? "_iso" : "") << ".bin";
	std::string fname = "modes" + suffix.str();
	std::ifstream in_modes(m_base_folder + fname, std::ios::binary);
	bool exists = in_modes.good();
	in_modes.close();
	
	if (exists)
	{
		std::cout << "Found existing " << fname << ". Loading..." << std::endl;
		this->loadModes(num_modes, evalues, evecs);
	}
	else
	{
		std::cout << "Could not find existing " << fname << ". Creating..." << std::endl;
		this->computeModes(num_modes, M_full, K_full, evalues, evecs);
	}
}

void ReducedThinShell::pruneRigidModes(Real rigid_modes_threshold, VectorX & evalues, MatrixX & evecs)
{
	// prune rigid body modes
	int cut;
	
	if (rigid_modes_threshold == -1)
		cut = 6;
	else
	{
		for (cut=0; cut<evalues.rows(); cut++)
			if (evalues(cut) > rigid_modes_threshold) break;
	}
		
	std::cout << "Pruning rigid body modes!" << std::endl;
	if (cut != 6)
		std::cout << "    WARNING: pruning " << cut << " modes." << std::endl;
	
	VectorX temp = evalues.block(cut,0,evalues.rows()-cut,1);
	evalues = temp;
	MatrixX temp_m = evecs.block(0,cut,evecs.rows(),evecs.cols()-cut);
	evecs = temp_m;
	
	std::cout << "    Pruned eigenvalues: " << std::endl;
	std::cout << evalues.transpose() << std::endl;
}

void ReducedThinShell::selectModalRange(int first_mode, int num_modes, const VectorX & evalues, const MatrixX & evecs, VectorX & selected_evalues, MatrixX & selected_evectors, bool verbose)
{
	if (verbose)
		std::cout << "Selecting mode " << first_mode << " through mode " << first_mode+num_modes-1 << "..." << std::endl;
	
	selected_evalues = evalues.block(first_mode,0,num_modes,1);
	selected_evectors = evecs.block(0,first_mode,evecs.rows(),num_modes);
	
	if (verbose)
	{
		std::cout << "    Selected eigenvalues: " << std::endl;
		std::cout << selected_evalues.transpose() << std::endl;
	}
}

void ReducedThinShell::computeReducedRayleighMatrix(const VectorX & evalues, VectorX & reduced_rayleigh_vec)
{
	//reduced_rayleigh = m_rayleigh_alpha*(evecs.transpose()*M_full*evecs) + m_rayleigh_beta*(evecs.transpose()*K_full*evecs);
	reduced_rayleigh_vec = m_rayleigh_alpha*VectorX::Ones(evalues.rows()) + m_rayleigh_beta*evalues;
}

void ReducedThinShell::convertReducedForceVectorsToSingleMatrix()
{
	int nb_dofs = m_evecs_lowfreq.rows();
	
	int tot_el = 1; // for constant term
	for (unsigned int i=0; i<nb_dofs; i++)
	{
		tot_el++;
		for (unsigned int j=i; j<nb_dofs; j++)
		{
			tot_el++;
			for (unsigned int k=j; k<nb_dofs; k++)
				tot_el++;
		}
	}
			
	m_reduced_forces_matrix.resize(nb_dofs, tot_el);
	m_reduced_forces_matrix.setZero();

	tot_el = 0;
	
	for (unsigned int i=0; i<nb_dofs; i++)
	{
		for (unsigned int j=i; j<nb_dofs; j++)
		{
			for (unsigned int k=j; k<nb_dofs; k++)
			{
				m_reduced_forces_matrix.col(tot_el) = m_Cr[i][j][k]; // cubic
				tot_el++;
			}
			
			m_reduced_forces_matrix.col(tot_el) = m_Qr[i][j]; // quadratic
			tot_el++;
		}
		
		m_reduced_forces_matrix.col(tot_el) = m_Lr[i]; // linear
		tot_el++;
	}
	
	m_reduced_forces_matrix.col(tot_el) = m_Kr; // constant
	tot_el++;
	
	#ifdef HAVE_CUDA
		cudaMalloc((void**)&m_reduced_forces_matrix_gpu, m_reduced_forces_matrix.rows()*m_reduced_forces_matrix.cols()*sizeof(double));
		cublasSetMatrix(m_reduced_forces_matrix.rows(), m_reduced_forces_matrix.cols(), sizeof(double), m_reduced_forces_matrix.data(), m_reduced_forces_matrix.rows(), m_reduced_forces_matrix_gpu, m_reduced_forces_matrix.rows());
		cudaMalloc((void**)&m_fake_dofs_gpu, (m_reduced_forces_matrix.rows()+1)*sizeof(double)); // dofs + 1
		cudaMalloc((void**)&m_cubic_elem_vector_gpu, m_reduced_forces_matrix.cols()*sizeof(double));
		cudaMalloc((void**)&m_ret_gpu, m_reduced_forces_matrix.rows()*sizeof(double));
		VectorX ones(m_reduced_forces_matrix.rows()+1);
		ones.setOnes();
		cudaMemcpy(m_fake_dofs_gpu, ones.data(), (m_reduced_forces_matrix.rows()+1)*sizeof(double), cudaMemcpyHostToDevice); // this is just to set the last element to 1.
	#endif

}

void ReducedThinShell::prepareForSimulation()
{
    std::cout << std::endl;
    std::cout << "**** Preparing to simulate " << m_sound_base_name << "*****" << std::endl;
    
    #ifdef HAVE_CUDA
	cublasCreate_v2(&m_cublas_handle);
	#endif
	
	m_shell->prepareForSimulation();
	
	VectorX M_full_diag;
	Eigen::SparseMatrix<Real> M_full;
	Eigen::SparseMatrix<Real> K_full;
	
	this->computeUnreducedMatrices(M_full, M_full_diag, K_full);
	
	// full Rayleigh damping matrix
	//m_rayleigh_full = m_rayleigh_alpha*(M_full) + m_rayleigh_beta*(K_full);
	
	int nb_unreduced_dof = m_shell->getNbDof();
	
	if ( (m_num_modes_to_compute != -1) && (m_num_modes_to_compute > nb_unreduced_dof-1) )
		m_num_modes_to_compute = nb_unreduced_dof-1;
	
	VectorX evalues;
	MatrixX evecs;
	
	//this->computeModes(m_num_modes, evecs, evalues);
	//this->loadModes(m_num_modes, evecs, evalues);
	this->loadOrComputeModes(m_num_modes_to_compute, M_full, K_full, evalues, evecs);
	
	std::cout << "Eigenvalues:" << std::endl;
	std::cout << evalues.transpose() << std::endl;
// 	std::cout << "Eigenvectors:" << std::endl;
// 	std::cout << evecs << std::endl;
	
	// prune rigid body bodes
	if (m_prune_rigid_modes) 
        this->pruneRigidModes(m_prune_rigid_modes_threshold, evalues, evecs);
	
	if (m_no_stretch_forces)
		m_shell->deactivateEnergy("Membrane");
	
	if (m_no_bending_forces)
		m_shell->deactivateEnergy("Bending");
	
	// recompute K if we disabled some forces since mode computations
	if (m_no_stretch_forces || m_no_bending_forces)
		this->computeUnreducedMatrices(M_full, M_full_diag, K_full);
	
	int nb_dofs_unreduced = evecs.rows();
	m_rest_positions.resize(nb_dofs_unreduced);
	m_rest_positions.setZero();
	m_rest_velocities.resize(nb_dofs_unreduced);
	m_rest_velocities.setZero();
	m_shell->getDofState(m_rest_positions, m_rest_velocities);
	
	m_bb_min = 1e10*Vector3::Ones();
	m_bb_max = -m_bb_min;
	for (unsigned int i=0; i<m_rest_positions.rows()/3; i++)
	{
		for (unsigned int j=0; j<3; j++)
		{
			if (m_bb_min(j) > m_rest_positions(i*3+0)) m_bb_min(j) = m_rest_positions(i*3+0);
			if (m_bb_max(j) < m_rest_positions(i*3+0)) m_bb_max(j) = m_rest_positions(i*3+0);
		}
	}
	
	m_span = 0.;
	for (unsigned int j=0; j<3; j++)
		if ( (m_bb_max(j)-m_bb_min(j)) > m_span ) 
			m_span = m_bb_max(j)-m_bb_min(j);

	// process impacts
	int num_nodes = m_rest_positions.rows()/3;
	for (auto & impact : m_impacts)
	{
		Real sq_radius = impact.radius*impact.radius;
		for (int i=0; i<num_nodes; i++)
		{
			if ( (m_rest_positions.segment(i*3,3) - impact.center).squaredNorm() < sq_radius )
			{
				impact.nodes.push_back(i);
				impact.directions.push_back(m_shell->getRestNormal(i));
			}
		}
	}
		
	// if linear space requested, then combine low and high freq modes into high freq and set lowfreq to -1
	if (m_computation_space == 1) // linear space
	{
		m_num_highfreq_modes += m_num_lowfreq_modes;
		m_num_lowfreq_modes = 0;
	}

	// low frequencies
	if (m_num_lowfreq_modes != 0)
	{
		if (m_num_lowfreq_modes > evalues.rows())
		{
			std::cout << "Requesting more low frequency modes than available modes. Adjusted to max number of modes." << std::endl;
			m_num_lowfreq_modes = evalues.rows();
		}
		
		this->selectModalRange(0, m_num_lowfreq_modes, evalues, evecs, m_evalues_lowfreq, m_evecs_lowfreq);
		
		// identity matrix for the mass
		Matrix1 mat11;
		mat11 << 1.;
		Jacobian mass;
		mass.jacobian = mat11;
		
		int nb_dofs_reduced = m_evecs_lowfreq.cols();
		
		m_masses_lowfreq.reserve(nb_dofs_reduced);
		for (unsigned int i=0; i<nb_dofs_reduced; i++)
		{
			mass.index_i = i;
			mass.index_j = i;
			m_masses_lowfreq[0].push_back(mass);
		}
		
		m_q_lowfreq.resize(nb_dofs_reduced);
		m_q_lowfreq.setZero();
		
		m_qv_lowfreq.resize(nb_dofs_reduced);
		m_qv_lowfreq.setZero();
		
		m_qf_lowfreq.resize(nb_dofs_reduced);
		m_qf_lowfreq.setZero();
	}
	
	// high frequencies
	if (m_num_highfreq_modes != 0)
	{
		int mode_range_start = m_num_lowfreq_modes;
		
		if ((mode_range_start+m_num_highfreq_modes) > evalues.rows())
		{
			std::cout << "Requesting more high frequency modes than available modes. Adjusted to max number of modes." << std::endl;
			m_num_highfreq_modes = evalues.rows()-mode_range_start;
		}
		
		this->selectModalRange(mode_range_start, m_num_highfreq_modes, evalues, evecs, m_evalues_highfreq, m_evecs_highfreq);
		
		// identity matrix for the mass
		Matrix1 mat11;
		mat11 << 1.;
		Jacobian mass;
		mass.jacobian = mat11;
		
		int nb_dofs_reduced = m_evecs_highfreq.cols();
		
		m_masses_highfreq.reserve(nb_dofs_reduced);
		for (unsigned int i=0; i<nb_dofs_reduced; i++)
		{
			mass.index_i = i;
			mass.index_j = i;
			m_masses_highfreq[0].push_back(mass);
		}
		
		m_q_highfreq.resize(nb_dofs_reduced);
		m_q_highfreq.setZero();
		
		m_qv_highfreq.resize(nb_dofs_reduced);
		m_qv_highfreq.setZero();
		
		m_qf_highfreq.resize(nb_dofs_reduced);
		m_qf_highfreq.setZero();
	}
	
	// all freq
	
	m_evecs_allfreq.resize( (m_num_lowfreq_modes != 0) ? m_evecs_lowfreq.rows() : m_evecs_highfreq.rows(), m_evecs_lowfreq.cols()+m_evecs_highfreq.cols());
	m_evalues_allfreq.resize(m_evalues_lowfreq.rows()+m_evalues_highfreq.rows());
	
	if (m_num_lowfreq_modes != 0)
	{
		m_evecs_allfreq.block(0,0,m_evecs_lowfreq.rows(),m_evecs_lowfreq.cols()) = m_evecs_lowfreq;
		m_evalues_allfreq.segment(0,m_evalues_lowfreq.rows()) = m_evalues_lowfreq;
	}
	
	if (m_evecs_highfreq.cols())
	{
		m_evecs_allfreq.block(0,m_evecs_lowfreq.cols(),m_evecs_highfreq.rows(),m_evecs_highfreq.cols()) = m_evecs_highfreq;
		m_evalues_allfreq.segment(m_evalues_lowfreq.rows(),m_evalues_highfreq.rows()) = m_evalues_highfreq;
	}
	
	m_evecs_allfreq_transposed = m_evecs_allfreq.transpose();
	
	this->computeReducedRayleighMatrix(m_evalues_allfreq, m_rayleigh_reduced_vec_allfreq);
	
	// store remaining frequencies and their damping
	this->selectModalRange(m_evalues_allfreq.rows(), evalues.rows()-m_evalues_allfreq.rows(), evalues, evecs, m_evalues_extrafreq, m_evecs_extrafreq, false);
	this->computeReducedRayleighMatrix(m_evalues_extrafreq, m_rayleigh_reduced_vec_extrafreq);
	
	std::cout << "################################################" << std::endl;
	if (m_num_lowfreq_modes != 0)
	{
		Real lowfreq_low = std::sqrt(m_evalues_lowfreq(0))/(2.*PI);
		Real lowfreq_high = std::sqrt(m_evalues_lowfreq(m_evalues_lowfreq.rows()-1))/(2.*PI);
		std::cout << "Low frequency simulation will span " << lowfreq_low << " Hz to " << lowfreq_high << " Hz." << std::endl;
		if (lowfreq_high > 7000)
		{
			std::cout << "############################################################################################################" << std::endl;
			std::cout << "WARNING! Your low frequency range goes pretty high, which might lead to instabilities! Try lower than 7kHz." << std::endl;
			std::cout << "############################################################################################################" << std::endl;
		}
	}
	
	if (m_num_highfreq_modes != 0)
	{
		Real highfreq_low = std::sqrt(m_evalues_highfreq(0))/(2.*PI);
		Real highfreq_high = std::sqrt(m_evalues_highfreq(m_evalues_highfreq.rows()-1))/(2.*PI);
		std::cout << "High frequency simulation will span " << highfreq_low << " Hz to " << highfreq_high << " Hz." << std::endl;
		if (highfreq_high > 7000)
		{
			std::cout << "############################################################################################################" << std::endl;
			std::cout << "WARNING! Your high frequency range goes pretty high, which might lead to instabilities! Try lower than 7kHz." << std::endl;
			std::cout << "############################################################################################################" << std::endl;
		}
	}
	std::cout << "################################################" << std::endl;
	
	if (m_computation_space >= 2) // subspace, turbulent subspace
	{
		// low freq + coupling
		if (m_num_lowfreq_modes != 0)
		{
			int nb_unreduced_dof = m_shell->getNbDof();
			
			std::stringstream suffix;
			suffix << "_" << nb_unreduced_dof << "_" << std::to_string(m_q_lowfreq.rows()) << "_" << std::to_string(m_q_highfreq.rows()) << ".bin";
			std::string fname = "reduced_force_vectors" + suffix.str();
			std::ifstream in_forcevectors(m_base_folder + fname, std::ios::binary);
			bool exists = in_forcevectors.good();
			
			int num_lowfreq_modes = m_q_lowfreq.rows();
			int num_allfreq_modes = m_q_lowfreq.rows() + m_q_highfreq.rows();
			
			if (exists) // load vectors from disk
			{
				std::cout << "Loading matrices for low freq + coupling..." << std::endl;
				
				m_Kr.resize(num_allfreq_modes);
				
				m_Lr.resize(num_allfreq_modes, VectorX::Zero(num_allfreq_modes));
				
				m_Qr.resize(num_lowfreq_modes);
				for (int i=0; i<num_lowfreq_modes; i++)
					m_Qr[i].resize(num_allfreq_modes);
				
				m_Cr.resize(num_lowfreq_modes);
				for (int i=0; i<num_lowfreq_modes; i++)
				{
					m_Cr[i].resize(num_lowfreq_modes);
					for (int j=i; j<num_lowfreq_modes; j++)
						m_Cr[i][j].resize(num_allfreq_modes);
				}
				
				for (int i=0; i<num_lowfreq_modes; i++)
					for (int j=i; j<num_allfreq_modes; j++)
						m_Qr[i][j].setZero(num_allfreq_modes);
						
				for (int i=0; i<num_lowfreq_modes; i++)
					for (int j=i; j<num_lowfreq_modes; j++)
						for (int k=j; k<num_allfreq_modes; k++)
							m_Cr[i][j][k].setZero(num_allfreq_modes);
						
				// cubic
				for (int i=0; i<num_lowfreq_modes; i++)
					for (int j=i; j<num_lowfreq_modes; j++)
						for (int k=j; k<num_allfreq_modes; k++)
							in_forcevectors.read((char*)m_Cr[i][j][k].data(), num_allfreq_modes*sizeof(double));
						
				//  quadratic
				for (int i=0; i<num_lowfreq_modes; i++)
					for (int j=i; j<num_allfreq_modes; j++)
						in_forcevectors.read((char*)m_Qr[i][j].data(), num_allfreq_modes*sizeof(double));

				// linear
				for (int i=0; i<num_allfreq_modes; i++)
					in_forcevectors.read((char*)m_Lr[i].data(), num_allfreq_modes*sizeof(double));
				
				// constant
				in_forcevectors.read((char*)m_Kr.data(), num_allfreq_modes*sizeof(double));
				
				std::cout << "...done." << std::endl;
			}
			else // compute vectors
			{
				in_forcevectors.close();
			
				std::cout << "Precomputing subspace polynomial forces..." << std::endl;
				
				MatrixX evecs_allfreq(m_evecs_lowfreq.rows(), m_evecs_lowfreq.cols()+m_evecs_highfreq.cols());
				evecs_allfreq.block(0,0,m_evecs_lowfreq.rows(),m_evecs_lowfreq.cols()) = m_evecs_lowfreq;
				evecs_allfreq.block(0,m_evecs_lowfreq.cols(),m_evecs_lowfreq.rows(),m_evecs_highfreq.cols()) = m_evecs_highfreq;
		
				m_shell->setupReducedForces(evecs_allfreq, m_evecs_lowfreq.cols(), m_Kr, m_Lr, m_Qr, m_Cr);
				
				// save
				std::ofstream out_forcevectors(m_base_folder + fname, std::ios::out | std::ios::binary | std::ios::trunc);
				
				// cubic
				for (int i=0; i<num_lowfreq_modes; i++)
					for (int j=i; j<num_lowfreq_modes; j++)
						for (int k=j; k<num_allfreq_modes; k++)
							out_forcevectors.write((char*)m_Cr[i][j][k].data(), num_allfreq_modes*sizeof(double));
				
				//  quadratic
				for (int i=0; i<num_lowfreq_modes; i++)
					for (int j=i; j<num_allfreq_modes; j++)
						out_forcevectors.write((char*)m_Qr[i][j].data(), num_allfreq_modes*sizeof(double));

				// linear
				for (int i=0; i<num_allfreq_modes; i++)
					out_forcevectors.write((char*)m_Lr[i].data(), num_allfreq_modes*sizeof(double));
					
				// constant
				out_forcevectors.write((char*)m_Kr.data(), num_allfreq_modes*sizeof(double));
			}
				
			// precompute infinity norm of vectors
            m_Lr_norms.resize(num_allfreq_modes);
			m_Qr_norms.resize(num_lowfreq_modes);
			m_Cr_norms.resize(num_lowfreq_modes);
			for (int i=0; i<num_lowfreq_modes; i++)
			{
				m_Cr_norms[i].resize(num_lowfreq_modes);
				for (int j=i; j<num_lowfreq_modes; j++)
				{
					m_Cr_norms[i][j].resize(num_allfreq_modes);
					for (int k=j; k<num_allfreq_modes; k++)
					{
						m_Cr_norms[i][j][k] = m_Cr[i][j][k].lpNorm<Eigen::Infinity>();
					}
				}
				
				m_Qr_norms[i].resize(num_allfreq_modes);
				for (int j=i; j<num_allfreq_modes; j++)
				{
					m_Qr_norms[i][j] = m_Qr[i][j].lpNorm<Eigen::Infinity>();
				}
			}
			
			for (int i=0; i<num_allfreq_modes; i++)
			{
                m_Lr_norms[i] = m_Lr[i].lpNorm<Eigen::Infinity>();
            }
		}
	}
	
	if (m_computation_space != 2) // if not subspace, disable lyapunov unless forced
		m_compute_lyapunov_exponents = false;
	
	std::cout << (m_compute_lyapunov_exponents ? "Enabled" : "Disabled") << " lyapunov exponents" << std::endl;
    
    // runtime prunning
    m_qpeaks = VectorX::Zero(m_q_lowfreq.rows() + m_q_highfreq.rows());
    m_prevq = VectorX::Zero(m_q_lowfreq.rows() + m_q_highfreq.rows());
    m_osc_dir = VectorXi::Zero(m_q_lowfreq.rows() + m_q_highfreq.rows());
    
    // load spectrogram
    if (m_computation_space == 3) // turbulent subspace
    {
        std::cout << "Loading turbulent spectrogram..." << std::endl;
        
        std::string spectrogram_filename = m_base_folder+m_sound_base_name+"_turbulent_spectrogram.bin";
        std::ifstream binspectrogram(spectrogram_filename, std::ios::binary);

        if (!binspectrogram)
        {
            std::cout << "Could not load " << spectrogram_filename << ". Aborting!" << std::endl;
            std::exit(1);
        }
        
        int num_freqs;
        int num_timechunks;
        binspectrogram.read((char*)(&m_timechunk_size),sizeof(int));
        binspectrogram.read((char*)(&num_freqs),sizeof(int));
        binspectrogram.read((char*)(&num_timechunks),sizeof(int));
        m_spectrogram.resize(num_freqs, num_timechunks);
        binspectrogram.read((char*)m_spectrogram.data(), num_freqs*num_timechunks*sizeof(Real));
        
        int num_allfreq_modes = m_q_lowfreq.rows() + m_q_highfreq.rows();
        
        m_prevprevvel = VectorX::Zero(num_allfreq_modes);
        m_prevvel = VectorX::Zero(num_allfreq_modes);
        m_last_vel_per_freq = VectorX::Zero(num_allfreq_modes);
        m_last_timestep_per_freq = VectorXi::Zero(num_allfreq_modes);
        
        std::cout << "...done." << std::endl;
    }
    
    if (m_computation_space >= 2) // reduced, turbulent subspace
    {
		std::cout << "Prunning threshold: " << m_prunning_threshold << std::endl;
	}
	
}

void ReducedThinShell::setupLyapunovExponents()
{
	m_up_rest_norm = 0;
	
	if (m_num_lowfreq_modes != 0)
	{
		// lyapunov exponents: prepare normalized perturbation
	
		int nb_lowfreq_dofs = m_evalues_lowfreq.rows();
		m_up.setOnes(nb_lowfreq_dofs); // ones or random, it's the same
		m_up.normalize();
		m_vp.setZero(nb_lowfreq_dofs);
		
		// compute K for low freq modes (KL) at rest
		
		MatrixX KL(nb_lowfreq_dofs, nb_lowfreq_dofs);
		KL.setZero();
		
		if (m_computation_space >= 2) // subspace, turbulent subspace
		{
			for (int i=0; i<nb_lowfreq_dofs; i++)
			{
				KL.col(i) += m_Lr[i];
				
				for (int j=i; j<nb_lowfreq_dofs; j++)
				{
					KL.col(j) += m_q_lowfreq(i)*m_Qr[i][j];	
					Real qiqj = m_q_lowfreq(i)*m_q_lowfreq(j);
					
					for (int k=j; k<nb_lowfreq_dofs; k++)
						KL.col(k) += qiqj*m_Cr[i][j][k];
				}
			}
		}
		else // full space or linear space
		{
			KL.diagonal() = -m_evalues_lowfreq;
		}
			
		// Then integrate M*ap + K*up = 0 to get new up (use explicit euler).
		// Since we are in subspace, masses are identity.
		// Loop until norm converges.

		Real oldpnorm = 0.;
		do
		{
			VectorX dv = -KL*m_up*m_dt;
			m_vp += dv;
			m_up += m_vp*m_dt;
			
			oldpnorm = m_up_rest_norm;
			m_up_rest_norm = m_up.norm();
			
			m_up /= m_up_rest_norm;
			m_vp /= m_up_rest_norm;
		} 
		while (std::fabs(m_up_rest_norm-oldpnorm) > 1e-8);
		
		std::cout << "Perturbation rest norm: " << m_up_rest_norm << std::endl;
		
		// reset pertubation
		
		m_up.setOnes(); // ones or random, it's the same
		m_up.normalize();
		m_vp.setZero();
		
		m_accum_perturb_ln_norm = 0.;
		m_accum_perturb_ln_all.setZero(nb_lowfreq_dofs);
		
		m_lyapunov.reserve(44100*m_dt_substeps*m_seconds_to_save);
		m_lyapunov_all.reserve(44100*m_dt_substeps*m_seconds_to_save);
	}
}

void ReducedThinShell::updateLyapunovExponents()
{
	int num_lowfreq_modes = m_evalues_lowfreq.rows();
	
	// First compute K for low freq modes (KL)
	
	MatrixX KL(num_lowfreq_modes, num_lowfreq_modes);
	KL.setZero();
	
	if (m_computation_space >= 2) // subspace, turbulent subspace
	{
		#ifdef USE_OMP
		#pragma omp parallel
		#endif
		{
			MatrixX KL_omp(num_lowfreq_modes, num_lowfreq_modes);
			KL_omp.setZero();
			
			#ifdef USE_OMP
			#pragma omp for
			#endif
			for (int i=0; i<num_lowfreq_modes; i++)
			{
				KL_omp.col(i) += m_Lr[i];
				
				for (int j=i; j<num_lowfreq_modes; j++)
				{
					KL_omp.col(j) += m_q_lowfreq(i)*m_Qr[i][j];
					
					Real qiqj = m_q_lowfreq(i)*m_q_lowfreq(j);
					
					for (int k=j; k<num_lowfreq_modes; k++)
						KL_omp.col(k) += qiqj*m_Cr[i][j][k];
				}
			}
			
			#pragma omp critical
			{
				KL += KL_omp;
			}
		}
	}
	else if (m_computation_space == 0) // full space
	{
		VectorX M_full_diag;
		Eigen::SparseMatrix<Real> M_full;
		Eigen::SparseMatrix<Real> K_full;
		this->computeUnreducedMatrices(M_full, M_full_diag, K_full);
	
		KL = -m_evecs_lowfreq.transpose()*K_full*m_evecs_lowfreq;
	}
	else // linear space
	{
		KL.diagonal() = -m_evalues_lowfreq;
	}
	
	// Then integrate M*ap + K*up = 0 to get new up (use explicit euler).
	// Since we are in subspace, masses are identity.
	
	// scale by the rest norm
	m_up /= m_up_rest_norm;
	m_vp /= m_up_rest_norm;
	
	// time integration
	VectorX dv = -KL*m_up*m_dt;
	m_vp += dv;
	m_up += m_vp*m_dt;
	
	Real pnorm = m_up.norm();
	m_accum_perturb_ln_all = m_q_lowfreq;
	m_accum_perturb_ln_norm += std::log(pnorm);
	
	m_up /= pnorm;
	m_vp /= pnorm;
}

void ReducedThinShell::addImpact(Impact impact)
{
	m_impacts.push_back(impact);
}

void ReducedThinShell::prepareForStep(Real current_time, Real dt)
{
    m_dt = dt;

    if (m_compute_lyapunov_exponents && (m_up_rest_norm == -1))
        this->setupLyapunovExponents(); // we need to do it here since we need dt value

    // external forces
        
    if (m_external_impact_sparse.rows() != m_shell->getNbDof()) 
        m_external_impact_sparse.resize(m_shell->getNbDof());
    
    m_contact_in_this_timestep = false;
    
    if (m_computation_space == 3) // doing turbulence
    {
        int num_lowfreq_modes = m_q_lowfreq.rows();
		int num_highfreq_modes = m_q_highfreq.rows();
		int num_allfreq_modes = num_lowfreq_modes + num_highfreq_modes;
	
        VectorX qv_allfreq(num_allfreq_modes);
		qv_allfreq.segment(0,num_lowfreq_modes) = m_qv_lowfreq;
		qv_allfreq.segment(num_lowfreq_modes, num_highfreq_modes) = m_qv_highfreq;
        
        int timestep = std::floor(current_time*m_dt_substeps*44100.);
        
        for (int freq_idx=0; freq_idx<qv_allfreq.rows(); freq_idx++)
        {
            Real sign1 = (m_prevvel(freq_idx)-m_prevprevvel(freq_idx)) > 0 ? 1 : -1;
            Real sign2 = (qv_allfreq(freq_idx)-m_prevvel(freq_idx)) > 0 ? 1 : -1;
            if ( (sign1 != sign2) || ( (m_prevvel(freq_idx) == 0.) && (qv_allfreq(freq_idx) == 0.) ) )
            {
                // retrieve energies of previous and next timechunks for interpolation
                int tchunk_left = std::floor(Real(timestep)/Real(m_timechunk_size));
                Real energy_left = m_spectrogram(freq_idx,tchunk_left);
                if (tchunk_left+1 >= m_spectrogram.cols()) continue; // reached the end of the spectrogram
                Real energy_right = m_spectrogram(freq_idx,tchunk_left+1);
                
                // interpolate left and right to get current energy/velocity
                int idx_in_tchunk = timestep - tchunk_left*m_timechunk_size;
                Real vel = std::sqrt((1.-Real(idx_in_tchunk)/Real(m_timechunk_size))*energy_left + (Real(idx_in_tchunk)/Real(m_timechunk_size))*energy_right);
                
                Real vel_last = m_last_vel_per_freq(freq_idx);
                int timestep_last = m_last_timestep_per_freq(freq_idx);
                
                Real dv = vel - vel_last;
                // compensate for damping
                dv += Real(timestep-timestep_last)*1./44100./Real(m_dt_substeps)*m_rayleigh_reduced_vec_allfreq(freq_idx)*(vel+vel_last)/2.;
                
                if (dv > 0.) // check without this
                {
                    Real sign = qv_allfreq(freq_idx) > 0. ? 1. : -1.;
                    qv_allfreq(freq_idx) += sign*m_spectrogram_scale*dv;
                    
                    // std::cout << timestep << " " << freq_idx << " " << dv << std::endl;
                }
                
                m_last_vel_per_freq(freq_idx) = vel;
                m_last_timestep_per_freq(freq_idx) = timestep;
            }
        }
        
        m_prevprevvel = m_prevvel;
        m_prevvel = qv_allfreq;
        
        m_qv_lowfreq = qv_allfreq.segment(0,num_lowfreq_modes);
        m_qv_highfreq = qv_allfreq.segment(num_lowfreq_modes, num_highfreq_modes);
    }
    else // use external forces
    {
        m_external_impact_sparse.setZero();
        VectorX impact_dense(m_rest_positions.rows());
        impact_dense.setZero();
        
        if (m_animation_object.state.size()) // from animation
        {
            int anim_timestep_nb = std::round(current_time/m_animation_object.dt);
            
            // add new impacts to impact list
            if ( (anim_timestep_nb < m_animation_object.state.size()) && m_animation_object.collisions[anim_timestep_nb].size() )
            {
                for (const auto & col : m_animation_object.collisions[anim_timestep_nb])
                {
                    Real impact_magnitude = col.local_force.norm();
                    if (!impact_magnitude) continue; // this contact from scisim is erroneous (skip it otherwise crash)
                    Impact impact(current_time, Vector3::Zero(), 0, impact_magnitude, 0.001);
                    impact.nodes.push_back(col.node);
                    impact.directions.push_back(col.local_force/impact_magnitude);
                    m_impacts.push_back(impact);                
                }
            }
            
            // process impact list
            for (auto & impact : m_impacts)
            {
                // raised cosine
                Real half_spread = impact.spread/2.;
                double val = impact.magnitude*0.5*(1.+std::cos(M_PI*(impact.local_time-half_spread)/half_spread));
                Vector3 nodal_impact = val*impact.directions[0];
                impact_dense.segment(impact.nodes[0]*3,3) += nodal_impact;
                impact.local_time += dt;
            }
            
            if (m_impacts.size()) m_contact_in_this_timestep = true;
            
            // remove elapsed impacts
            auto impact_it = m_impacts.begin();
            while (impact_it != m_impacts.end())
            {
                if (impact_it->local_time >= impact_it->spread)
                    impact_it = m_impacts.erase(impact_it);
                else
                    impact_it++;
            }
            
        }
        else  // from preset impacts
        {
            for (auto & impact : m_impacts)
            {
                if (current_time >= impact.time)
                {
                    if (impact.local_time < impact.spread)
                    {
                        // raised cosine
                        Real half_spread = impact.spread/2.;
                        double val = impact.magnitude*0.5*(1.+std::cos(M_PI*(impact.local_time-half_spread)/half_spread));
                        
                        for (int i=0; i<impact.nodes.size(); i++)
                        {
                            Real scaling = 1.-(m_rest_positions.segment(impact.nodes[i]*3,3)-impact.center).norm()/impact.radius;
                            Vector3 nodal_impact = scaling*val*impact.directions[i];
                            impact_dense.segment(impact.nodes[i]*3,3) += nodal_impact;
                        }
                    }
                    impact.local_time += dt;
                }
            }
        }
        
        // convert dense to sparse
        for (int i=0; i<impact_dense.rows(); i++)
            if (impact_dense(i) != 0.)
                m_external_impact_sparse.coeffRef(i) = impact_dense(i);
    }
    
    // update q peaks for runtime vector prunning
    if (m_computation_space > 1) // subspace simulation
    {
        int num_lowfreq_modes = m_q_lowfreq.rows();
        int num_highfreq_modes = m_q_highfreq.rows();
        int num_allfreq_modes = num_lowfreq_modes + num_highfreq_modes;
    
        VectorX q_allfreq(num_allfreq_modes);
        q_allfreq.segment(0,num_lowfreq_modes) = m_q_lowfreq;
        q_allfreq.segment(num_lowfreq_modes, num_highfreq_modes) = m_q_highfreq;
        
        for (int i=0; i<num_allfreq_modes; i++)
        {
            int new_osc_dir = (q_allfreq(i)-m_prevq(i)) > 0 ? 1 : -1;
            
            if (m_osc_dir(i) != new_osc_dir)
            {
                m_qpeaks(i) = std::fabs(m_prevq(i));
                m_osc_dir(i) = new_osc_dir;
            }
        }
        m_prevq = q_allfreq;
        
        for (int i=0; i<num_allfreq_modes; i++)
        {
            if (std::fabs(q_allfreq(i)) > m_qpeaks(i))
                m_qpeaks(i) = std::fabs(q_allfreq(i));
        }
    }

}

void ReducedThinShell::computeInternalForces()
{ 
    int num_lowfreq_modes = m_q_lowfreq.rows();
    int num_highfreq_modes = m_q_highfreq.rows();
    int num_allfreq_modes = num_lowfreq_modes + num_highfreq_modes;

    VectorX qv_allfreq(num_allfreq_modes);
    if (num_lowfreq_modes) qv_allfreq.segment(0,num_lowfreq_modes) = m_qv_lowfreq;
    if (num_highfreq_modes) qv_allfreq.segment(num_lowfreq_modes, num_highfreq_modes) = m_qv_highfreq;

    // harmless hack to simulate damping due to contact
    Real contact_damping_scale = m_contact_in_this_timestep ? m_contact_damping_scale : 1.;
    
    VectorX qf_allfreq = m_evecs_allfreq_transposed*m_external_impact_sparse - contact_damping_scale*m_rayleigh_reduced_vec_allfreq.cwiseProduct(qv_allfreq);
    
    if (m_computation_space == 0) // full space
	{
		VectorX unreduced_velocities = m_rest_velocities;
		VectorX unreduced_positions = m_rest_positions;
            
		// add low frequencies
		if (m_q_lowfreq.rows())
		{
			unreduced_velocities += m_evecs_lowfreq*m_qv_lowfreq;
			unreduced_positions += m_evecs_lowfreq*m_q_lowfreq;
		}
		
		// add high frequencies
		if (m_q_highfreq.rows())
		{
			unreduced_velocities += m_evecs_highfreq*m_qv_highfreq;
			unreduced_positions += m_evecs_highfreq*m_q_highfreq;
		}

		m_shell->setDofState(unreduced_positions, unreduced_velocities);
		m_shell->computeInternalForces();
		VectorX FuL = VectorX::Zero(m_rest_positions.rows());
		m_shell->getDofForces(FuL);
		
		qf_allfreq += m_evecs_allfreq_transposed*FuL;
	}
	else
	{
		if (m_computation_space == 1) // simulation is linear only
		{
            qf_allfreq += -m_evalues_allfreq.cwiseProduct(m_q_highfreq); // q_allfreq = m_q_highfreq when linear (we moved all the modes to high freq vector)
		}
		else
		{
			VectorX FqL(num_allfreq_modes);
			FqL.setZero();
				
			VectorX FKqH(num_allfreq_modes);
			FKqH.setZero();
			
			#ifdef USE_OMP
			#pragma omp parallel
			#endif
			{
				int omp_thread_index = omp_get_thread_num();
				
				VectorX FqL_omp(num_allfreq_modes);
				FqL_omp.setZero();
				
				VectorX FKqH_omp(num_allfreq_modes);
				FKqH_omp.setZero();
				
				#ifdef USE_OMP
				#pragma omp for
				#endif
				for (int i=0; i<num_lowfreq_modes; i++)
				{
                    if (std::fabs(m_qpeaks(i)*m_Lr_norms[i]) > m_prunning_threshold)
                        FqL_omp += m_q_lowfreq(i)*m_Lr[i]; // linear
					
					for (int j=i; j<num_lowfreq_modes; j++)
					{
						Real qiqj = m_q_lowfreq(i)*m_q_lowfreq(j);
                        Real qiqj_peaks = m_qpeaks(i)*m_qpeaks(j);
						
                        if (std::fabs(qiqj_peaks*m_Qr_norms[i][j]) > m_prunning_threshold)
                            FqL_omp += qiqj*m_Qr[i][j]; // quadratic
						
						for (int k=j; k<num_lowfreq_modes; k++)
                            if (std::fabs(m_qpeaks(k)*qiqj_peaks*m_Cr_norms[i][j][k]) > m_prunning_threshold)
                                FqL_omp += qiqj*m_q_lowfreq(k)*m_Cr[i][j][k]; // cubic
						
						for (int k=num_lowfreq_modes; k<num_allfreq_modes; k++)
							if (std::fabs(m_qpeaks(k)*qiqj_peaks*m_Cr_norms[i][j][k]) > m_prunning_threshold)
								FKqH_omp += m_q_highfreq(k-num_lowfreq_modes)*qiqj*m_Cr[i][j][k];
					}
					
					for (int j=num_lowfreq_modes; j<num_allfreq_modes; j++)
						if (std::fabs(m_qpeaks(j)*m_qpeaks(i)*m_Qr_norms[i][j]) > m_prunning_threshold)
							FKqH_omp += m_q_highfreq(j-num_lowfreq_modes)*m_q_lowfreq(i)*m_Qr[i][j];
				}
				
				if (omp_thread_index == 0)
				{
					#ifndef IGNORE_Kr
					FqL_omp += m_Kr; // constant
					#endif
					
					for (int i=num_lowfreq_modes; i<num_allfreq_modes; i++)
                        if (std::fabs(m_qpeaks(i)*m_Lr_norms[i]) > m_prunning_threshold)
                            FKqH_omp += m_q_highfreq(i-num_lowfreq_modes)*m_Lr[i];
				}
				
				#pragma omp critical
				{
					FqL += FqL_omp;
					FKqH += FKqH_omp;
				}
			}
			
			// FqL + K*qH
			qf_allfreq += FqL + FKqH;
			
			// Perturbation computations
			if (m_compute_lyapunov_exponents)
				this->updateLyapunovExponents();
			
		}
	}
	
	if (num_lowfreq_modes) m_qf_lowfreq = qf_allfreq.segment(0, num_lowfreq_modes);
    if (num_highfreq_modes) m_qf_highfreq = qf_allfreq.segment(num_lowfreq_modes, num_highfreq_modes);
	
}

void ReducedThinShell::getDofState(Eigen::Ref<VectorX> positions, Eigen::Ref<VectorX> velocities)
{
	if (m_num_lowfreq_modes != 0)
	{
		positions.segment(0,m_q_lowfreq.rows()) = m_q_lowfreq;
		velocities.segment(0,m_q_lowfreq.rows()) = m_qv_lowfreq;
	}
	
	if (m_num_highfreq_modes != 0)
	{
		positions.segment(m_q_lowfreq.rows(),m_q_highfreq.rows()) = m_q_highfreq;
		velocities.segment(m_q_lowfreq.rows(),m_q_highfreq.rows()) = m_qv_highfreq;
	}
}

void ReducedThinShell::setDofState(const Eigen::Ref<const VectorX> & positions, const Eigen::Ref<const VectorX> & velocities)
{
	// input positions and velocities are actually "displacement" positions and velocities
	
	if (m_num_lowfreq_modes != 0)
	{
		m_q_lowfreq = positions.segment(0,m_q_lowfreq.rows());
		m_qv_lowfreq = velocities.segment(0,m_q_lowfreq.rows());
	}
	
	if (m_num_highfreq_modes != 0)
	{
		m_q_highfreq = positions.segment(m_q_lowfreq.rows(),m_q_highfreq.rows());
		m_qv_highfreq = velocities.segment(m_q_lowfreq.rows(),m_q_highfreq.rows());
	}
	
	if (m_render_to_screen) // THIS IS VERY EXPENSIVE!!
	{
		VectorX unreduced_velocities = m_rest_velocities;
		VectorX unreduced_positions = m_rest_positions;
		
		if (m_num_lowfreq_modes != 0)
		{
			unreduced_velocities += m_evecs_lowfreq*m_qv_lowfreq;
			unreduced_positions += m_evecs_lowfreq*m_q_lowfreq;
		}
		
		if (m_num_highfreq_modes != 0)
		{
			unreduced_velocities += m_evecs_highfreq*m_qv_highfreq;
			unreduced_positions += m_evecs_highfreq*m_q_highfreq;
		}
		
		m_shell->setDofState(unreduced_positions, unreduced_velocities);
	}
	
}

void ReducedThinShell::getDofForces(Eigen::Ref<VectorX> forces)
{
	if (m_num_lowfreq_modes != 0)
	{
		forces.segment(0,m_qf_lowfreq.rows()) = m_qf_lowfreq;
	}
	
	if (m_num_highfreq_modes != 0)
	{
		forces.segment(m_qf_lowfreq.rows(),m_qf_highfreq.rows()) = m_qf_highfreq;
	}
	
}

void ReducedThinShell::getMeshData(std::pair<MatrixX, MatrixXi> & mesh_data) 
{ 
	if (m_render_to_screen)
    {
		m_shell->getMeshData(mesh_data); 
		
		if (m_evalues_lowfreq.rows() && (m_show_mode_nb != -1))
		{
			Real amplitude = 0.1*sqrt(m_span); //0.1*m_span;
			Real frequency = m_show_modes_freq;
			VectorX positions = m_rest_positions + amplitude*sin(2.*PI*frequency*m_show_modes_t)*m_evecs_lowfreq.col(m_show_mode_nb);
			m_show_modes_t += 0.001;
			
			Eigen::Map<MatrixX> positions_mat_t(positions.data(), 3, positions.rows()/3);
			mesh_data.first = positions_mat_t.transpose();
		}
    }
}

void ReducedThinShell::set_key(unsigned char key)
{
	if (key == '=')
	{
		if (m_show_mode_nb < m_evalues_lowfreq.rows()-1)
			m_show_mode_nb++;
		std::cout << "Mode: " << m_show_mode_nb << std::endl;
	}
	else if (key == '-')
	{
		if (m_show_mode_nb > -1)
		{
			m_show_mode_nb--;
			std::cout << "Mode: " << m_show_mode_nb << std::endl;
		}
		else
			std::cout << "Mode display OFF" << std::endl;
	}
	else if (key == 'D')
	{
		m_post_step_plot_modes = true;
	}
	else if (key == 'W')
	{
		m_post_step_write_audio = true;
	}
	else if (key == 'L')
	{
		m_post_step_plot_lyapunov = true;
	}
	else if (key == 'V')
	{
		m_render_to_screen = !m_render_to_screen;
	}
	else if (key == '1')
	{
		m_show_modes_freq /= 10;
		std::cout << "Show modes using a frequency of " << m_show_modes_freq << " Hz" << std::endl;
	}
	else if (key == '2')
	{
		m_show_modes_freq *= 10;
		std::cout << "Show modes using a frequency of " << m_show_modes_freq << " Hz" << std::endl;
	}
	else if (key == 'R')
	{
		this->writeFastBEMFiles();
	}
}

void ReducedThinShell::postStep()
{
	// save modal data for audio
	#ifdef RENDER_TO_AUDIO
		if ( (m_saved_modal_positions_lowfreq.size() < m_seconds_to_save*m_dt_substeps*44100) &&
			 (m_saved_modal_positions_highfreq.size() < m_seconds_to_save*m_dt_substeps*44100) )
		{
			if (m_num_lowfreq_modes != 0)
			{
				m_saved_modal_positions_lowfreq.push_back(m_q_lowfreq);
				m_saved_modal_velocities_lowfreq.push_back(m_qv_lowfreq);
			}
			if (m_num_highfreq_modes != 0)
			{
				m_saved_modal_positions_highfreq.push_back(m_q_highfreq);
				m_saved_modal_velocities_highfreq.push_back(m_qv_highfreq);
			}
		}
	#endif
	
	// lyapunov
	if (m_compute_lyapunov_exponents)
	{
		if (m_lyapunov.size() < m_seconds_to_save*m_dt_substeps*44100)
		{
			m_lyapunov.push_back(m_accum_perturb_ln_norm);
			m_lyapunov_all.push_back(m_accum_perturb_ln_all);
		}
	}
	
	// deal with events
	
	if (m_post_step_plot_modes)
	{
		this->plotModes();
		m_post_step_plot_modes = false;
	}
	
	if (m_post_step_write_audio)
	{
		this->writeAudio();
		m_post_step_write_audio = false;
	}
	
	if (m_post_step_plot_lyapunov)
	{
		this->plotLyapunov();
		m_post_step_plot_lyapunov = false;
	}
	
}

void ReducedThinShell::plotModes()
{
	if (m_num_lowfreq_modes != 0)
	{
		std::cout << "Plotting low frequencies..." << std::endl;
		if (m_first_lowfreq_mode_to_plot != -1)
		{
			std::vector<VectorX> selected_amps(m_saved_modal_positions_lowfreq.size());
			for (int i=0; i<m_saved_modal_positions_lowfreq.size(); i++)
				selected_amps[i] = m_saved_modal_positions_lowfreq[i].block(m_first_lowfreq_mode_to_plot, 0, m_last_lowfreq_mode_to_plot-m_first_lowfreq_mode_to_plot, 1);
			Plotter::plotToFile(selected_amps, "amplitudes_lowfreq.jpg");
			Plotter::showPlot("amplitudes_lowfreq.jpg");
		}
		else 
		{
			Plotter::plotToFile(m_saved_modal_positions_lowfreq, "amplitudes_lowfreq.jpg");
			Plotter::showPlot("amplitudes_lowfreq.jpg");
		}
		std::cout << "...done" << std::endl;
	}
	
	if (m_num_highfreq_modes != 0)
	{
		std::cout << "Plotting high frequencies..." << std::endl;
		if (m_first_highfreq_mode_to_plot != -1)
		{
			std::vector<VectorX> selected_amps(m_saved_modal_positions_highfreq.size());
			for (int i=0; i<m_saved_modal_positions_highfreq.size(); i++)
				selected_amps[i] = m_saved_modal_positions_highfreq[i].block(m_first_highfreq_mode_to_plot, 0, m_last_highfreq_mode_to_plot-m_first_highfreq_mode_to_plot, 1);
			Plotter::plotToFile(selected_amps, "amplitudes_highfreq.jpg");
			Plotter::showPlot("amplitudes_highfreq.jpg");
		}
		else 
		{
			Plotter::plotToFile(m_saved_modal_positions_highfreq, "amplitudes_highfreq.jpg");
			Plotter::showPlot("amplitudes_highfreq.jpg");
		}
		std::cout << "...done" << std::endl;
	}
}
	
void ReducedThinShell::writeAudio()
{
	std::cout << "Saving WAV file for " << m_sound_base_name << "..." << std::endl;
    
    int num_amps = m_saved_modal_positions_lowfreq.size() > m_saved_modal_positions_highfreq.size() ? m_saved_modal_positions_lowfreq.size() : m_saved_modal_positions_highfreq.size();
	std::vector<double> audible_data(num_amps, 0.);
    
    int lf_min = -1;
    int lf_max = -1;
    int hf_min = -1;
    int hf_max = -1;
    
    // find frequency ranges
    
	// low frequency modes
	if (m_num_lowfreq_modes != 0)
	{
		// keep only audible modes
		for (int i=0; i<m_evalues_lowfreq.size(); i++)
		{
			if ( (lf_min == -1) && (m_evalues_lowfreq(i) > 1.58e4) ) lf_min = i; // lambda = w^2 = (2*pi*f)^2 and f_min = 20Hz
			if ( (lf_max == -1) && (m_evalues_lowfreq(i) > 1.92e10) ) lf_max = i-1; // lambda = w^2 = (2*pi*f)^2 and f_max = 22050Hz
		}
		if (lf_max == -1) lf_max = m_evalues_lowfreq.size()-1;
		
		std::cout << "  Low freq range: from mode " << lf_min << " (" << std::sqrt(m_evalues_lowfreq(lf_min))/(2.*PI) << " Hz) to mode " << lf_max << " (" << std::sqrt(m_evalues_lowfreq(lf_max))/(2.*PI) << " Hz)" << std::endl;
    }
    
    // high frequency modes
	if (m_num_highfreq_modes != 0)
	{
		// keep only audible modes
		for (int i=0; i<m_evalues_highfreq.size(); i++)
		{
			if ( (hf_min == -1) && (m_evalues_highfreq(i) > 1.58e4) ) hf_min = i; // lambda = w^2 = (2*pi*f)^2 and f_min = 20Hz
			if ( (hf_max == -1) && (m_evalues_highfreq(i) > 1.92e10) ) hf_max = i-1; // lambda = w^2 = (2*pi*f)^2 and f_max = 22050Hz
		}
		if (hf_max == -1) hf_max = m_evalues_highfreq.size()-1;
		
		int num_low_freq_modes = m_num_lowfreq_modes;
		std::cout << "  High freq range: from mode " << num_low_freq_modes+hf_min << " (" << std::sqrt(m_evalues_highfreq(hf_min))/(2.*PI) << " Hz) to mode " << num_low_freq_modes+hf_max << " (" << std::sqrt(m_evalues_highfreq(hf_max))/(2.*PI) << " Hz)" << std::endl;
    }
    
    std::string is_turbulent = (m_computation_space == 3) ? "_turbulence" : "";
    
    // compute sound and save modal positions in binary format
    std::ofstream binaudio(m_base_folder+m_sound_base_name+is_turbulent+".binaudio", std::ios::binary);
    int tot_f = 0;
    if (lf_min != -1) tot_f += lf_max-lf_min+1;
    if (hf_min != -1) tot_f += hf_max-hf_min+1;
    binaudio.write((char*)&tot_f, sizeof(int));
    binaudio.write((char*)&num_amps, sizeof(int));
    
    for (int i=0; i<num_amps; i++)
    {
        if (lf_min != -1) // low frequencies
        {
            VectorX subamp = m_saved_modal_positions_lowfreq[i].segment(lf_min, lf_max-lf_min+1);
            audible_data[i] = subamp.sum();
            binaudio.write((char*)subamp.data(), subamp.rows()*sizeof(Real));
        }
        if (hf_min != -1) // high frequencies
        {
            VectorX subamp = m_saved_modal_positions_highfreq[i].segment(hf_min, hf_max-hf_min+1);
            audible_data[i] += subamp.sum();
            binaudio.write((char*)subamp.data(), subamp.rows()*sizeof(Real));
        }
    }

    // save sound
    Real max_amp = SoundClip::writeWAV(m_base_folder+m_sound_base_name+is_turbulent+".wav", audible_data, 44100*m_dt_substeps);
    std::cout << "  Max amplitude: " << max_amp << std::endl;
	std::cout << "...done" << std::endl;
	
    // write turbulence stuff
	if (m_compute_lyapunov_exponents)
		this->writeTurbulenceData();
}

void ReducedThinShell::writeTurbulenceData()
{
	std::cout << "Saving turbulence data..." << std::endl;
	
	// save all the other stuff
	std::ofstream output(m_base_folder + m_sound_base_name + "_turbulence_data.bin", std::ios::binary);
	
	// modal frequencies (all freq)
	int lowf = m_evalues_lowfreq.rows();
	int highf = m_evalues_highfreq.rows();
	int extraf = m_evalues_extrafreq.rows();
	output.write((char*)(&lowf), sizeof(int));
	output.write((char*)(&highf), sizeof(int));
	output.write((char*)(&extraf), sizeof(int));
	int num_timesteps = m_saved_modal_positions_lowfreq.size();
	output.write((char*)(&num_timesteps), sizeof(int));
	
	VectorX freqs(lowf+highf+extraf);
	freqs << m_evalues_lowfreq, m_evalues_highfreq, m_evalues_extrafreq;
	freqs = freqs.cwiseSqrt() / (2.*PI);
    output.write((char*)freqs.data(), freqs.rows()*sizeof(Real));
	
	VectorX damping(lowf+highf+extraf);
	damping << m_rayleigh_reduced_vec_allfreq, m_rayleigh_reduced_vec_extrafreq;
    output.write((char*)damping.data(), damping.rows()*sizeof(Real));
	
	// modal velocities per dt
	for (int i=0; i<num_timesteps; i++)
	{
		output.write((char*)m_saved_modal_velocities_lowfreq[i].data(), lowf*sizeof(Real));
		output.write((char*)m_saved_modal_velocities_highfreq[i].data(), highf*sizeof(Real));
	}
	
	// lyapunov exponent per dt
	output.write((char*)m_lyapunov.data(), num_timesteps*sizeof(Real));
	
	// lyapunov exponent per mode per dt
	for (int i=0; i<num_timesteps; i++)
		output.write((char*)m_lyapunov_all[i].data(), lowf*sizeof(Real));
	
	std::cout << "...done" << std::endl;
}

void ReducedThinShell::writeMeshes(std::string base_filename)
{
    // update mesh dofs form reduced coordinates
    
    VectorX unreduced_velocities = m_rest_velocities;
    VectorX unreduced_positions = m_rest_positions;

    if (m_num_lowfreq_modes != 0)
    {
        unreduced_velocities += m_evecs_lowfreq*m_qv_lowfreq;
        unreduced_positions += m_evecs_lowfreq*m_q_lowfreq;
    }
    
    if (m_num_highfreq_modes != 0)
    {
        unreduced_velocities += m_evecs_highfreq*m_qv_highfreq;
        unreduced_positions += m_evecs_highfreq*m_q_highfreq;
    }
    m_shell->setDofState(unreduced_positions, unreduced_velocities);
	
    // get updated mesh positions
    std::pair<MatrixX, MatrixXi> mesh_data;
    m_shell->getMeshData(mesh_data);
    igl::writeOBJ(m_sound_base_name + "_" + base_filename + ".obj", mesh_data.first, mesh_data.second);
        
    #ifdef WRITE_FORCES
    // write forces also
    std::ofstream force_file(m_sound_base_name + "_force_" + base_filename + ".txt");
    for (auto & impact : m_impacts)
    {
        if ( (impact.local_time) && (impact.local_time < 3.*1./30.) )
        {
            Vector3 f = impact.magnitude*impact.directions[0];
            force_file << mesh_data.first(impact.nodes[0],0) << std::endl;
            force_file << mesh_data.first(impact.nodes[0],1) << std::endl;
            force_file << mesh_data.first(impact.nodes[0],2) << std::endl;
            force_file << f(0) << std::endl;
            force_file << f(1) << std::endl;
            force_file << f(2) << std::endl;
        }
    }
    #endif
}

void ReducedThinShell::plotLyapunov()
{
	if (m_num_lowfreq_modes != 0)
	{
		std::cout << "Plotting local lyapunov exponents..." << std::endl;
		Plotter::plotToFile(m_lyapunov, "lyapunov.jpg");
		Plotter::showPlot("lyapunov.jpg");
		//Plotter::plotToFile(m_lyapunov_all, "lyapunov_all.jpg");
		//Plotter::showPlot("lyapunov_all.jpg");
		std::cout << "...done" << std::endl;
	}
}

void ReducedThinShell::writeFastBEMFiles()
{
    std::string fastbem_folder = m_base_folder + "fastbem_" + m_sound_base_name;
    std::string call = "mkdir " + fastbem_folder;
    int ret =  std::system(call.c_str());
    std::cout << "mkdir call return: " << ret << std::endl;
    
    int lowf = m_evalues_lowfreq.rows();
    int highf = m_evalues_highfreq.rows();
    
    VectorX evalues(lowf+highf);
    if (lowf) evalues.segment(0, lowf) = m_evalues_lowfreq;
    if (highf) evalues.segment(lowf, highf) = m_evalues_highfreq;
    
    MatrixX evectors(m_rest_positions.rows(), lowf+highf);
    if (lowf) evectors.block(0, 0, m_rest_positions.rows(), lowf) = m_evecs_lowfreq;
    if (highf) evectors.block(0, lowf, m_rest_positions.rows(), highf) = m_evecs_highfreq;
    
    MatrixX fieldpoints(8,3);
    fieldpoints.row(0) << 2,0,0;
    fieldpoints.row(1) << 0,2,0;
    fieldpoints.row(2) << -2,0,0;
    fieldpoints.row(3) << -2,-2,0;
    fieldpoints.row(4) << 2,0,2;
    fieldpoints.row(5) << 0,2,2;
    fieldpoints.row(6) << -2,0,2;
    fieldpoints.row(7) << -2,-2,2;
    
    FastBEMFileGenerator::writeFiles(fastbem_folder, m_shell->getRestPos(), m_shell->getFaces(), evectors, evalues, fieldpoints);
}

void ReducedThinShell::setMaterial(std::string material_name)
{
	Real density;
	Real poisson;
	Real young;
	Real thickness;
	
	if (material_name == "GalvanizedSteel")
	{
		density = 7850.;
		young =  190e9;
		poisson = 0.30;
		thickness = 0.001;
		m_rayleigh_alpha = 0.5;
		m_rayleigh_beta = 75e-9;
	}
    else if (material_name == "Polycarbonate")
	{
		density = 1200.;
		young =  2.4e9;
		poisson = 0.37;
		thickness = 0.0015; //to get ~0.9kg for water bottle
		m_rayleigh_alpha = 1.;
		m_rayleigh_beta = 500e-9;
	}
	else if (material_name == "Polyethylene") // LDPE
	{
		density = 916.;
		young =  2.4e9;
		poisson = 0.4;
		thickness = 0.0025;
		m_rayleigh_alpha = 4.;
		m_rayleigh_beta = 300e-9;
	}
	else if (material_name == "Bronze")
	{
		density = 8400.;
		young =  124e9;
		poisson = 0.33;
		thickness = 0.0007;
		m_rayleigh_alpha = 1.;
		m_rayleigh_beta = 6.25e-9;
	}
	else if (material_name == "Bronze_Thick") // tam-tam
	{
		density = 8400.;
		young =  110e9;
		poisson = 0.33;
		thickness = 0.003;
		//m_rayleigh_alpha = 1.;
		//m_rayleigh_beta = 6.25e-9;
        m_rayleigh_alpha = 0.1;
		m_rayleigh_beta = 6.25e-9;
	}
	else if (material_name == "Aluminium") // for sheet
	{
		density = 2700.;
		young =  70e9;
		poisson = 0.35;
		thickness = 0.00025;
        m_rayleigh_alpha = 0.5;
		m_rayleigh_beta = 400e-9;
	}
    else if (material_name == "Steel")
	{
		density = 7850.;
		young =  190e9;
		poisson = 0.30;
		thickness = 0.0016;
		m_rayleigh_alpha = 0.5;
		m_rayleigh_beta = 75e-9;
	}
	else if (material_name == "Steel2")
	{
		density = 7850.;
		young =  190e9;
		poisson = 0.30;
		thickness = 0.0016;
		m_rayleigh_alpha = 0.5;
		m_rayleigh_beta = 6.25e-9;
	}
	else
	{
		std::cout << "Error: unknown material " << material_name << std::endl;
        std::exit(1);
	}
	
	m_shell->setDensity(density);
	m_shell->setPoissonRatio(poisson);
	m_shell->setYoungModulus(young);
	m_shell->setThickness(thickness);
}

