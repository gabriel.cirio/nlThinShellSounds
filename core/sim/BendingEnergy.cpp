
#include "BendingEnergy.h"

#include <igl/per_face_normals.h>
#include <omp.h>
#include <iostream>

#define USE_OMP

// use theta, tantheta or sintheta
//#define HINGE_BENDING_USE_TANTHETA
#define HINGE_BENDING_USE_SINTHETA

BendingEnergy::BendingEnergy(MatrixX & rest_pos, MatrixX & pos, MatrixXi & F, std::vector<int> & per_node_dofs, VectorX & triangle_rest_area, MatrixXi & E_unique,
							 MatrixXi & per_unique_edge_triangles, MatrixXi & per_unique_edge_triangles_local_corners, MatrixXi & per_triangles_unique_edges,
							 Real young_modulus, Real poisson_ratio, Real thickness)
:m_rest_pos(rest_pos), m_pos(pos), m_F(F), m_per_node_dofs(per_node_dofs), m_triangle_rest_area(triangle_rest_area), m_E_unique(E_unique), m_per_unique_edge_triangles(per_unique_edge_triangles), m_per_unique_edge_triangles_local_corners(per_unique_edge_triangles_local_corners), m_per_triangles_unique_edges(per_triangles_unique_edges), m_young_modulus(young_modulus), m_poisson_ratio(poisson_ratio), m_thickness(thickness)
{
}

void BendingEnergy::setup()
{
	m_per_edge_rest_phi.resize(m_E_unique.rows());
	m_per_edge_rest_phi.setZero();
	this->computeBendingRestPhi(m_per_edge_rest_phi);
	
	m_bending_stiffness = m_young_modulus*m_thickness*m_thickness*m_thickness/(24.*(1.-m_poisson_ratio*m_poisson_ratio));
	
	std::cout << "Bending stiffness: " << m_bending_stiffness << std::endl;
}

void BendingEnergy::computeBendingRestPhi(VectorX & rest_phis)
{
	auto l_bending_rest_phi = [this] (int idx[4], Real & rest_phi)
	{
		auto l_compute_tantheta = [this] (const Vector3 & n, const Vector3 & n_tilde, const Vector3 & e0, Real & rest_phi)
		{
			Real tan_half_theta = (n - n_tilde).norm()/(n + n_tilde).norm();
			Real sign_angle = n.cross(n_tilde).dot(e0);
			if (std::isnan(sign_angle)) sign_angle = 1.;
			else sign_angle = sign_angle > 0 ? 1. : -1.;
			rest_phi = 2.*sign_angle*tan_half_theta;
		};
		
		auto l_compute_sintheta = [this] (const Vector3 & n, const Vector3 & n_tilde, const Vector3 & e0, Real & rest_phi)
		{
			Real theta = std::atan2((n.cross(n_tilde)).dot(e0.normalized()), n.dot(n_tilde));
			rest_phi = std::sin(theta/2.);
		};
		
		auto l_compute_theta = [this] (const Vector3 & n, const Vector3 & n_tilde, const Vector3 & e0, Real & rest_phi)
		{
			Real theta = std::atan2((n.cross(n_tilde)).dot(e0.normalized()), n.dot(n_tilde));
			rest_phi = theta;
		};
		
		Vector3 x0 = m_pos.row(idx[0]);
		Vector3 x1 = m_pos.row(idx[1]);
		Vector3 x2 = m_pos.row(idx[2]);
		Vector3 x3 = m_pos.row(idx[3]);
		
		Vector3 e0 = x2 - x1;
		Vector3 e1 = x0 - x2;
		Vector3 e1_tilde = x3 - x2;
		
		Vector3 n = e0.cross(e1);
		Real n_length = n.norm();
		Vector3 n_tilde = -e0.cross(e1_tilde);
		Real n_tilde_length = n_tilde.norm();
		n /= n_length;
		n_tilde /= n_tilde_length;
		
		#if defined(HINGE_BENDING_USE_TANTHETA)
		l_compute_tantheta(n, n_tilde, e0, rest_phi);
		#elif defined(HINGE_BENDING_USE_SINTHETA)
		l_compute_sintheta(n, n_tilde, e0, rest_phi);
		#else
		l_compute_theta(n, n_tilde, e0, rest_phi);
		#endif
	};
	
	#ifdef USE_OMP
	#pragma omp parallel for
	#endif
	for (unsigned int e=0; e<m_E_unique.rows(); e++)
	{
		if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges
		
		int idx[4];
		idx[0] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
		idx[1] = m_E_unique(e,0);
		idx[2] = m_E_unique(e,1);
		idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
		
		l_bending_rest_phi(idx, rest_phis(e));
	}
	
}

void BendingEnergy::computeForces(std::vector<MatrixX> & force_omp_arrays)
{
	auto l_bending_stencil = [this] (int e, int idx[4], Vector3 forces[4])
	{
		auto l_compute_dPsi_tantheta = [this] (const Vector3 & n, const Vector3 & n_tilde, const Vector3 & e0, const Real rest_phi, const Real ka, Real & dPsi_dTheta)
		{
			Real tan_half_theta = (n - n_tilde).norm()/(n + n_tilde).norm();
			Real sign_angle = n.cross(n_tilde).dot(e0);
			if (std::isnan(sign_angle)) sign_angle = 1.;
			else sign_angle = sign_angle > 0 ? 1. : -1.;
			Real phi_theta = 2.*sign_angle*tan_half_theta;
			dPsi_dTheta = 2.*ka*(phi_theta - rest_phi)*(1.+tan_half_theta*tan_half_theta);
		};
		
		auto l_compute_dPsi_sintheta = [this] (const Vector3 & n, const Vector3 & n_tilde, const Vector3 & e0, const Real rest_phi, const Real ka, Real & dPsi_dTheta)
		{
			Real theta = std::atan2((n.cross(n_tilde)).dot(e0.normalized()), n.dot(n_tilde));
			Real phi_theta = std::sin(theta/2.);
			dPsi_dTheta = 2.*ka*(phi_theta - rest_phi)*(0.5*std::cos(theta/2.));
		};
		
		auto l_compute_dPsi_theta = [this] (const Vector3 & n, const Vector3 & n_tilde, const Vector3 & e0, const Real rest_phi, const Real ka, Real & dPsi_dTheta)
		{
			Real theta = std::atan2((n.cross(n_tilde)).dot(e0.normalized()), n.dot(n_tilde));
			Real phi_theta = theta;
			dPsi_dTheta = 2.*ka*(phi_theta - rest_phi);
		};
		
		Vector3 x0 = m_pos.row(idx[0]);
		Vector3 x1 = m_pos.row(idx[1]);
		Vector3 x2 = m_pos.row(idx[2]);
		Vector3 x3 = m_pos.row(idx[3]);
		
		Real restareas = m_triangle_rest_area(m_per_unique_edge_triangles(e,0)) + m_triangle_rest_area(m_per_unique_edge_triangles(e,1));
		Real e0_rest_sqnorm = (m_rest_pos.row(idx[2]) - m_rest_pos.row(idx[1])).squaredNorm();
		
		Vector3 e0 = x2 - x1;
		Vector3 e1 = x0 - x2;
		Vector3 e2 = x0 - x1;
		Vector3 e1_tilde = x3 - x2;
		Vector3 e2_tilde = x3 - x1;
		
		Vector3 n = e0.cross(e1);
		Real n_length = n.norm();
		Vector3 n_tilde = -e0.cross(e1_tilde);
		Real n_tilde_length = n_tilde.norm();
		n /= n_length;
		n_tilde /= n_tilde_length;
		
		Real ka = m_bending_stiffness*3.*e0_rest_sqnorm/restareas;
		
		Real rest_phi = m_per_edge_rest_phi(e);
		
		Real dPsi_dTheta;
		
		#if defined(HINGE_BENDING_USE_TANTHETA)
		l_compute_dPsi_tantheta(n, n_tilde, e0, rest_phi, ka, dPsi_dTheta);
		#elif defined(HINGE_BENDING_USE_SINTHETA)
		l_compute_dPsi_sintheta(n, n_tilde, e0, rest_phi, ka, dPsi_dTheta);
		#else
		l_compute_dPsi_theta(n, n_tilde, e0, rest_phi, ka, dPsi_dTheta);
		#endif
		
		Real e0_length = e0.norm();
		
		forces[0] = -dPsi_dTheta*(-e0_length/n_length*n.transpose());
		forces[1] = -dPsi_dTheta*((-e0/e0_length).dot(e1)/n_length*n.transpose() + (-e0/e0_length).dot(e1_tilde)/n_tilde_length*n_tilde.transpose());
		forces[2] = -dPsi_dTheta*((e0/e0_length).dot(e2)/n_length*n.transpose() + (e0/e0_length).dot(e2_tilde)/n_tilde_length*n_tilde.transpose());
		forces[3] = -dPsi_dTheta*(-e0_length/n_tilde_length*n_tilde.transpose());
	};
	
	#ifdef USE_OMP
	#pragma omp parallel
	#endif
	{
		int omp_thread_index = omp_get_thread_num();

		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (unsigned int e=0; e<m_E_unique.rows(); e++)
		{
			if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges
			
			int idx[4];
			idx[0] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
			idx[1] = m_E_unique(e,0);
			idx[2] = m_E_unique(e,1);
			idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
			
			Vector3 forces[4];
			l_bending_stencil(e, idx, forces);
			
			force_omp_arrays[omp_thread_index].row(idx[0]) += forces[0];
			force_omp_arrays[omp_thread_index].row(idx[1]) += forces[1];
			force_omp_arrays[omp_thread_index].row(idx[2]) += forces[2];
			force_omp_arrays[omp_thread_index].row(idx[3]) += forces[3];
		}
	}
}

void BendingEnergy::computeDfDx(std::vector<std::vector<Jacobian> > & dfdx)
{
	auto l_bending_stencil_gradientPart = [this] (int e, int idx[4], MatrixX & dfdx_bending_gradpart, Real & dPsi_dTheta)
	{
		auto l_compute_dPsi_tantheta = [this] (const Vector3 & n, const Vector3 & n_tilde, const Vector3 & e0, const Real rest_phi, const Real ka, Real & dPsi_dTheta, Real & dPsi_dTheta_dTheta)
		{
			Real tan_half_theta = (n - n_tilde).norm()/(n + n_tilde).norm();
			Real sign_angle = n.cross(n_tilde).dot(e0);
			if (std::isnan(sign_angle)) sign_angle = 1.;
			else sign_angle = sign_angle > 0 ? 1. : -1.;
			Real phi_theta = 2.*sign_angle*tan_half_theta;
			Real dPhi_dTheta = 1 + tan_half_theta*tan_half_theta;
			dPsi_dTheta = 2.*ka*(phi_theta - rest_phi)*dPhi_dTheta;
			dPsi_dTheta_dTheta = 2.*ka*(dPhi_dTheta*dPhi_dTheta + (phi_theta-rest_phi)*tan_half_theta*dPhi_dTheta);
		};
		
		auto l_compute_dPsi_sintheta = [this] (const Vector3 & n, const Vector3 & n_tilde, const Vector3 & e0, const Real rest_phi, const Real ka, Real & dPsi_dTheta, Real & dPsi_dTheta_dTheta)
		{
			Real theta = std::atan2((n.cross(n_tilde)).dot(e0.normalized()), n.dot(n_tilde));
			Real phi_theta = std::sin(theta/2.);
			dPsi_dTheta = 2.*ka*(phi_theta - rest_phi)*(0.5*std::cos(theta/2.));
			dPsi_dTheta_dTheta = 2.*ka*( (0.5*std::cos(theta/2.))*(0.5*std::cos(theta/2.)) + (phi_theta - rest_phi)*(0.5*0.5*-std::sin(theta/2.)) );
		};
		
		auto l_compute_dPsi_theta = [this] (const Vector3 & n, const Vector3 & n_tilde, const Vector3 & e0, const Real rest_phi, const Real ka, Real & dPsi_dTheta, Real & dPsi_dTheta_dTheta)
		{
			Real theta = atan2((n.cross(n_tilde)).dot(e0.normalized()), n.dot(n_tilde));
			Real phi_theta = theta;
			dPsi_dTheta = 2.*ka*(phi_theta - rest_phi);
			dPsi_dTheta_dTheta = 2.*ka;
		};
	
		Vector3 x0 = m_pos.row(idx[0]);
		Vector3 x1 = m_pos.row(idx[1]);
		Vector3 x2 = m_pos.row(idx[2]);
		Vector3 x3 = m_pos.row(idx[3]);
		
		Real restareas = m_triangle_rest_area(m_per_unique_edge_triangles(e,0)) + m_triangle_rest_area(m_per_unique_edge_triangles(e,1));
		Real e0_rest_sqnorm = (m_rest_pos.row(idx[2]) - m_rest_pos.row(idx[1])).squaredNorm();
	
		Vector3 e0 = x2 - x1;
		Vector3 e1 = x0 - x2;
		Vector3 e2 = x0 - x1;
		Vector3 e1_tilde = x3 - x2;
		Vector3 e2_tilde = x3 - x1;
		
		Vector3 n = e0.cross(e1);
		Real n_length = n.norm();
		Vector3 n_tilde = -e0.cross(e1_tilde);
		Real n_tilde_length = n_tilde.norm();
		n /= n_length;
		n_tilde /= n_tilde_length;
		
		Real ka = m_bending_stiffness*3.*e0_rest_sqnorm/restareas;
		
		Real rest_phi = m_per_edge_rest_phi(e);
		
		Real dPsi_dTheta_dTheta;
		
		#if defined(HINGE_BENDING_USE_TANTHETA)
		l_compute_dPsi_tantheta(n, n_tilde, e0, rest_phi, ka, dPsi_dTheta, dPsi_dTheta_dTheta);
		#elif defined(HINGE_BENDING_USE_SINTHETA)
		l_compute_dPsi_sintheta(n, n_tilde, e0, rest_phi, ka, dPsi_dTheta, dPsi_dTheta_dTheta);
		#else
		l_compute_dPsi_theta(n, n_tilde, e0, rest_phi, ka, dPsi_dTheta, dPsi_dTheta_dTheta);
		#endif
		
		Real e0_length = e0.norm();
		
		VectorX dthetadx(12);
		dthetadx.block(0,0,3,1) = -e0_length/n_length*n;
		dthetadx.block(3,0,3,1) = (-e0/e0_length).dot(e1)/n_length*n + (-e0/e0_length).dot(e1_tilde)/n_tilde_length*n_tilde;
		dthetadx.block(6,0,3,1) = (e0/e0_length).dot(e2)/n_length*n + (e0/e0_length).dot(e2_tilde)/n_tilde_length*n_tilde;
		dthetadx.block(9,0,3,1) = -e0_length/n_tilde_length*n_tilde;
		
		dfdx_bending_gradpart = dPsi_dTheta_dTheta*dthetadx*dthetadx.transpose();
	};
	
	auto l_bending_stencil_hessianPart = [this] (int f, int idx[3], MatrixX & dfdx_bending_hesspart, const VectorX & e_length, const VectorX & dPsi_dTheta)
	{
		Vector3 x0 = m_pos.row(idx[0]);
		Vector3 x1 = m_pos.row(idx[1]);
		Vector3 x2 = m_pos.row(idx[2]);
		
		Real e0_length = e_length(m_per_triangles_unique_edges(f,0));
		Real e1_length = e_length(m_per_triangles_unique_edges(f,1));
		Real e2_length = e_length(m_per_triangles_unique_edges(f,2));
		
		Vector3 e0_normalized = (x2-x1)/e0_length;
		Vector3 e1_normalized = (x2-x0)/e1_length;
		Vector3 e2_normalized = (x1-x0)/e2_length;
		
		Real cos_alpha0 = e1_normalized.dot(e2_normalized);
		Real cos_alpha1 = -e2_normalized.dot(e0_normalized);
		Real cos_alpha2 = -e1_normalized.dot(-e0_normalized);
		
		Real A = 0.5*(x2-x0).cross(x1-x0).norm();
		
		Real inv_h0 = e0_length/(2.*A);
		Real inv_h1 = e1_length/(2.*A);
		Real inv_h2 = e2_length/(2.*A);
		
		Vector3 n = e2_normalized.cross(e1_normalized).normalized();
		
		Vector3 m0 = e0_normalized.cross(n).normalized();
		Vector3 m1 = -e1_normalized.cross(n).normalized();
		Vector3 m2 = e2_normalized.cross(n).normalized();
		
		Real w[3][3];
		w[0][0] = inv_h0*inv_h0;
		w[0][1] = inv_h0*inv_h1;
		w[0][2] = inv_h0*inv_h2;
		w[1][0] = inv_h1*inv_h0;
		w[1][1] = inv_h1*inv_h1;
		w[1][2] = inv_h1*inv_h2;
		w[2][0] = inv_h2*inv_h0;
		w[2][1] = inv_h2*inv_h1;
		w[2][2] = inv_h2*inv_h2;
		
		Matrix3 M[3];
		M[0] = n*m0.transpose();
		M[1] = n*m1.transpose();
		M[2] = n*m2.transpose();
		
		Matrix3 N0 = M[0]/(e0_length*e0_length);
		Matrix3 N1 = M[1]/(e1_length*e1_length);
		Matrix3 N2 = M[2]/(e2_length*e2_length);

		Real c0 = dPsi_dTheta(m_per_triangles_unique_edges(f,0));
		Real c1 = dPsi_dTheta(m_per_triangles_unique_edges(f,1));
		Real c2 = dPsi_dTheta(m_per_triangles_unique_edges(f,2));
		
		Real d[3];
		d[0] = c2*cos_alpha1 + c1*cos_alpha2 - c0;
		d[1] = c0*cos_alpha2 + c2*cos_alpha0 - c1;
		d[2] = c1*cos_alpha0 + c0*cos_alpha1 - c2;
		
		Matrix3 R[3];
		R[0] = c0*N0;
		R[1] = c1*N1;
		R[2] = c2*N2;
		
		dfdx_bending_hesspart.resize(9,9);
		
		for (unsigned int i=0; i<3; i++)
		{
			for (unsigned int k=i; k<i+2; k++)
			{
				int j = k%3;
				
				Matrix3 hij = w[i][j]*(d[i]*M[j].transpose() + d[j]*M[i]);
				if (i == j)
				{
					hij += -R[(i+1)%3] - R[(i+2)%3];
				}
				else
				{
					// check if we need to transpose. Needs transpose for one of the two triangles of the hinge (doesn't matter which as long as it is consistent throughout sim).
					int e = m_per_triangles_unique_edges(f,(i+2)%3); // get edge id
					if (m_per_unique_edge_triangles(e,0) == f)
						hij += R[(i+2)%3].transpose();
					else 
						hij += R[(i+2)%3];
				}
				
				dfdx_bending_hesspart.block(i*3, j*3, 3, 3) = hij;
			}
		}
		
		dfdx_bending_hesspart.block(3, 0, 3, 3) = dfdx_bending_hesspart.block(0, 3, 3, 3).transpose();
		dfdx_bending_hesspart.block(0, 6, 3, 3) = dfdx_bending_hesspart.block(6, 0, 3, 3).transpose();
		dfdx_bending_hesspart.block(6, 3, 3, 3) = dfdx_bending_hesspart.block(3, 6, 3, 3).transpose();
		
	};
	
	VectorX e_length(m_E_unique.rows());
	VectorX dPsi_dTheta(m_E_unique.rows());
	dPsi_dTheta.setZero(); // same effect as sigma (boundary boolean)
	
	#ifdef USE_OMP
	#pragma omp parallel
	#endif
	{
		int omp_thread_index = omp_get_thread_num();

		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (unsigned int e=0; e<m_E_unique.rows(); e++)
		{
			e_length(e) = (m_pos.row(m_E_unique(e,0)) - m_pos.row(m_E_unique(e,1))).norm();
			
			if ( (m_per_unique_edge_triangles(e,0) == -1) || (m_per_unique_edge_triangles(e,1) == -1) ) continue; // skip boundary edges
			
			int idx[4];
			idx[0] = m_F(m_per_unique_edge_triangles(e,0), m_per_unique_edge_triangles_local_corners(e,0));
			idx[1] = m_E_unique(e,0);
			idx[2] = m_E_unique(e,1);
			idx[3] = m_F(m_per_unique_edge_triangles(e,1), m_per_unique_edge_triangles_local_corners(e,1));
			
			MatrixX dfdx_bending_gradpart;
			l_bending_stencil_gradientPart(e, idx, dfdx_bending_gradpart, dPsi_dTheta(e));
			
			Jacobian jac;
			
			for (unsigned int i=0; i<4; i++)
			{
				if (m_per_node_dofs[idx[i]] == -1) continue;
				
				jac.index_i = m_per_node_dofs[idx[i]];
				
				for (unsigned int j=0; j<4; j++)
				{
					if (m_per_node_dofs[idx[j]] == -1) continue;
					
					jac.index_j = m_per_node_dofs[idx[j]];
					jac.jacobian = -dfdx_bending_gradpart.block(i*3, j*3, 3, 3);
					
					dfdx[omp_thread_index].push_back(jac);
				}
			}
		}
	}
	
	#ifdef USE_OMP
	#pragma omp parallel
	#endif
	{
		int omp_thread_index = omp_get_thread_num();

		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (unsigned int f=0; f<m_F.rows(); f++)
		{
			int idx[3];
			idx[0] = m_F(f,0);
			idx[1] = m_F(f,1);
			idx[2] = m_F(f,2);
			
			MatrixX dfdx_bending_hesspart;
			l_bending_stencil_hessianPart(f, idx, dfdx_bending_hesspart, e_length, dPsi_dTheta);

			Jacobian jac;
			
			for (unsigned int i=0; i<3; i++)
			{
				if (m_per_node_dofs[idx[i]] == -1) continue;
				
				jac.index_i = m_per_node_dofs[idx[i]];
				
				for (unsigned int j=0; j<3; j++)
				{
					if (m_per_node_dofs[idx[j]] == -1) continue;
					
					jac.index_j = m_per_node_dofs[idx[j]];
					jac.jacobian = -dfdx_bending_hesspart.block(i*3, j*3, 3, 3);
					
					dfdx[omp_thread_index].push_back(jac);
				}
			}
		}
	}
}


