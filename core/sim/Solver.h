
#ifndef MULTISCALESOUNDS_SOLVER_H
#define MULTISCALESOUNDS_SOLVER_H

#include "typedefs.h"

#include <vector>

class ThinShell;
class DynamicObject;

class Solver
{
	
	public:
		
		Solver(): m_dt(0.) {}
		virtual ~Solver() {}
		
		virtual void solve(std::vector<DynamicObject*> & dyn_objects) = 0;
		
		virtual void setDt(Real dt) { m_dt = dt; }
		virtual Real getDt() { return m_dt; }
		virtual Real getCurrentDt() { return m_dt; }
		virtual void setDtSubsteps(int dt_substeps) {}
		
	protected:
		
		Real m_dt;
		
};

#endif // MULTISCALESOUNDS_SOLVER_H
