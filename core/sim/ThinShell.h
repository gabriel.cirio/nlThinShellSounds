
#ifndef MULTISCALESOUNDS_FABRIC_H
#define MULTISCALESOUNDS_FABRIC_H

#include "typedefs.h"
#include "DynamicObject.h"
#include "MemoryArray.h"

#include "util/JSONConfiguration.h"

#include <list>
#include <set>

class Energy;

class ThinShell : public DynamicObject
{
	public:
		
		ThinShell();
		virtual ~ThinShell() {}
		
		void configure(const JSONConfiguration::Object & config, const std::string & base_path);
        
		void createThinShell(std::string filename="", Vector3 offset=Vector3::Zero());
		
		void getMeshData(std::pair<MatrixX, MatrixXi> & mesh_data);
        void getNodalVectors(MatrixX & vectors);
        
		int fixNodes(Vector3 pos, Real radius);
		void fixNode(int index);
		
		void prepareForSimulation();
		void prepareForStep(Real current_time, Real dt);
		void computeInternalForces();
		void computeInternalForceJacobians();
		void computeInternalDampingJacobians();
		void computeMass();
		
		void setupReducedForces(const MatrixX & U, int num_lowfreq_modes, VectorX & Kr, std::vector<VectorX> & Lr, std::vector<std::vector<VectorX>> & Qr, std::vector<std::vector<std::vector<VectorX>>> & Cr);
		
		void getDofState(Eigen::Ref<VectorX> positions, Eigen::Ref<VectorX> velocities);
		void setDofState(const Eigen::Ref<const VectorX> & positions, const Eigen::Ref<const VectorX> & velocities);
		void getDofForces(Eigen::Ref<VectorX> forces);
		void getDofMasses(std::vector<std::vector<Jacobian> >* & masses) { masses = &m_masses; }
		void getDofForceJacobians(std::vector<std::vector<Jacobian> >* & force_jacobians) { force_jacobians = &m_force_jacobians; }
		void getDofDampingJacobians(std::vector<std::vector<Jacobian> >* & damping_jacobians) { damping_jacobians = &m_damping_jacobians; }
		unsigned int getNbDof() { return m_nb_dofs; }
		Vector3 getRestNormal(int node_index) { return m_rest_pos_normals.row(node_index); }
		Vector3 getRestPos(int node_index) { return m_rest_pos.row(node_index); }
		
		void setBendingType(int type) { m_bending_type = type; }
		int getBendingType() { return m_bending_type; }
		std::vector<std::vector<int>> getNodeNeighbors() { return m_node_neighbors; }
		MatrixX getRestPos() { return m_rest_pos; }
		MatrixX getPos() { return m_pos; }
		MatrixXi getFaces() { return m_F; }
		MatrixXi getUniqueEdges() { return m_E_unique; }
		void deactivateEnergy(std::string energy_name);
		
		void setDensity(Real density) { m_density = density; }
		void setPoissonRatio(Real poisson) { m_poisson_ratio = poisson; }
		void setYoungModulus(Real young) { m_young_modulus = young; }
		void setThickness(Real thickness) { m_thickness = thickness; }
		Real getDensity() { return m_density; }
		Real getPoissonRatio() { return m_poisson_ratio; }
		Real getYoungModulus() { return m_young_modulus; }
		Real getThickness() { return m_thickness; }
		
		void setUseGpu(bool use) { m_use_gpu = use; }
		
		std::string getFilename() { return m_filename; }
		void writeMeshes(std::string base_filename);

	private:
		
		void clearForces();
		void clearForceJacobians();
		void clearDampingJacobians();
		
		void computeDoFs();
		void compute_helper_arrays();
        
        void getNodalForces(MatrixX & forces);
		
		struct RedMatData
		{
			VectorX Kr_v;
			VectorX Lr_v;
			VectorX Qr_v;
			VectorX Cr_v;
			
			double* Kr_v_gpu;
			double* Lr_v_gpu;
			double* Qr_v_gpu;
			double* Cr_v_gpu;
			
		};
	
		inline void setup_reduced_for_nodei(int node_i, const std::set<int> & stencil_dofs, int num_lowfreq_modes, const MatrixX & U, RedMatData & data);
		
		inline void to_mat(int num_lowfreq_modes, const MatrixX & U, RedMatData & data, int dof0, int dof1, int dof2, Real valx, Real valy, Real valz);
		inline void to_mat(int num_lowfreq_modes, const MatrixX & U, RedMatData & data, int dof0, int dof1, Real valx, Real valy, Real valz);
		inline void to_mat(int num_lowfreq_modes, const MatrixX & U, RedMatData & data, int dof0, Real valx, Real valy, Real valz);
		inline void to_mat(int num_lowfreq_modes, const MatrixX & U, RedMatData & data, Real valx, Real valy, Real valz);

	private:
		
		MatrixX m_rest_pos;
		MatrixX m_pos;
		MatrixX m_vel;
		MatrixXi m_F;
		MatrixX m_forces;
		MatrixX m_rest_pos_normals;
		
		std::vector<std::pair<Energy*, std::string>> m_energies;
		std::vector<bool> m_energy_is_active;
		
		unsigned int m_nb_nodes;
		unsigned int m_nb_dofs;
		std::vector<int> m_per_node_dofs;
		
		VectorX m_triangle_rest_area;
		VectorX m_node_rest_area;
		
		std::vector<bool> m_fixed_nodes;
		
		// Helper structures
		std::vector<std::vector<int> > m_node_neighbors;
		std::vector<std::vector<int> > m_per_node_triangles;
		std::vector<std::vector<int> > m_per_node_triangles_node_local_index; // node local index (0, 1 or 2) for each node's incident triangle
		
		MatrixXi m_per_triangle_triangles; // #F by #3 adjacent matrix, the element i,j is the id of the triangle adjacent to the j edge of triangle i
		MatrixXi m_per_triangle_triangles_edge_local_index; // #F by #3 adjacent matrix, the element i,j is the id of edge of the triangle m_per_triangle_triangles(i,j) that is adjacent with triangle i. NOTE: the first edge of a triangle is [0,1] the second [1,2] and the third [2,3].
		
		MatrixXi m_E_directed; // #F*3 by 2 list of all of directed edges
		MatrixXi m_E_unique; // #uE by 2 list of unique undirected edges
		MatrixXi m_map_edge_directed_to_unique; // #F*3 list of indices into uE, mapping each directed edge to unique undirected edge
		std::vector<std::vector<int>> m_map_edge_unique_to_directed; // #uE list of lists of indices into E of coexisting edges
		MatrixXi m_per_unique_edge_triangles; // #E by 2 list of edge flaps, EF(e,0)=f means e=(i-->j) is the edge of F(f,:) opposite the vth corner, where EI(e,0)=v. Similarly EF(e,1) e=(j->i)
		MatrixXi m_per_unique_edge_triangles_local_corners; // #E by 2 list of edge flap corners
		std::vector<std::vector<int> > m_per_node_edges;
		MatrixXi m_per_triangles_unique_edges;
		
		// Parameters
		Real m_density;
		Real m_thickness;
		Real m_poisson_ratio;
		Real m_young_modulus;
		Vector3 m_gravity_vector;
				
		// Runtime containers (each container is multiple vectors so that all OpenMP threads can write into them at the same time)
		std::vector<std::vector<Jacobian> > m_masses;
		std::vector<std::vector<Jacobian> > m_force_jacobians;
		std::vector<std::vector<Jacobian> > m_damping_jacobians;
		
		int m_bending_type;
		
		int m_omp_max_threads;
		bool m_use_gpu;
        std::string m_filename;
		
		// cuda
		double* m_Ut_gpu;
		MemoryArray<int> m_thread_map_cubic;
		MemoryArray<int> m_thread_map_quadratic;
		
};

#endif // MULTISCALESOUNDS_FABRIC_H
