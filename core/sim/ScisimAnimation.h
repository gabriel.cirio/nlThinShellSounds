
#ifndef MULTISCALESOUNDS_SCISIM_ANIMATION_H
#define MULTISCALESOUNDS_SCISIM_ANIMATION_H

#include "typedefs.h"
#include "DynamicObject.h"

#include "util/JSONConfiguration.h"

#include <vector>

class ScisimAnimationObject
{
    
    public:
        
        struct State
        {
            Vector3 pos;
            Matrix3 rot;
            Vector3 vel;
            Vector3 w;
        };
        
        struct Collision
        {
            int other_body_index;
            Vector3 world_point;
            Vector3 world_normal;
            Vector3 world_force;
            int node;
            // this local data is w.r.t. this projet's meshes, not scisim (scisim uses version rotated by R.transpose())
            //  (local_ours) = R*(local_scisim)
            // state.rot already has that R factored in
            Vector3 local_point;
            Vector3 local_normal;
            Vector3 local_force;
            
        };
        
        ScisimAnimationObject() {}
        ~ScisimAnimationObject() {}
        
        void addTimestep(const Vector3 & pos, const Matrix3 & rot, const Vector3 & vel, const Vector3 & w);
        void addCollisionAtCurrentStep(const Collision & col);
        void computeCollisionLocalData(Real force_scale);
        
    public:
    
        std::vector<State> state;
        std::vector<std::vector<Collision>> collisions;
            
        // geometry
        MatrixX restV;
        MatrixXi F;
        MatrixX coarse_restV;
        MatrixXi coarse_F;
        Vector3 cm;
        Matrix3 R;
        
        // time
        Real dt;
};

class ScisimAnimation : public DynamicObject
{
    
    public:
        
        ScisimAnimation();
        ~ScisimAnimation() {}
    
        void configure(const JSONConfiguration::Object & config, const std::string & base_path);
        void read(std::string sbf_filename);
        
        void getMeshData(std::pair<MatrixX, MatrixXi> & mesh_data);
        void getNodalVectors(MatrixX & vectors);
        void getWorldVectors(std::pair<MatrixX, MatrixX> & vectors);
        
        void prepareForSimulation();
        void prepareForStep(Real current_time, Real dt);
        void computeInternalForces() {}
        void computeInternalForceJacobians() {}
        void computeInternalDampingJacobians() {}
        void computeMass() {}
        void postStep() {}
        
        void getDofState(Eigen::Ref<VectorX> positions, Eigen::Ref<VectorX> velocities) {}
        void setDofState(const Eigen::Ref<const VectorX> & positions, const Eigen::Ref<const VectorX> & velocities) {}
        void getDofForces(Eigen::Ref<VectorX> forces) {}
        void getDofMasses(std::vector<std::vector<Jacobian> >* & masses) {}
        void getDofForceJacobians(std::vector<std::vector<Jacobian> >* & force_jacobians) {}
        void getDofDampingJacobians(std::vector<std::vector<Jacobian> >* & damping_jacobians) {}
        unsigned int getNbDof() { return 0; }
        
        bool is_reduced() { return true; } // to keep mass and other stuff in solver
        
        const ScisimAnimationObject & getObject(int index) { return m_objects[index]; }
        Real getDuration() { return m_animation_timesteps*m_animation_dt; }
        
        void writeMeshes(std::string base_filename);
        
    private:
    
        std::vector<ScisimAnimationObject> m_objects;
        int m_timestep_nb;
        double m_animation_dt;
        int m_animation_timesteps;
        Real m_force_scale;
};

#endif // MULTISCALESOUNDS_SCISIM_ANIMATION_H
