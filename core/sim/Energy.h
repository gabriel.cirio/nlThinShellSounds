
#ifndef MULTISCALESOUNDS_ENERGY_H
#define MULTISCALESOUNDS_ENERGY_H

#include "typedefs.h"
#include <vector>
#include <set>

class Energy
{
	public:
		
		Energy() : m_use_gpu(false) {}
		virtual ~Energy() {}
		
		virtual void setup() = 0;
		virtual void setup_reduced(const MatrixX & U, int num_lowfreq_modes, VectorX & Kr, std::vector<VectorX> & Lr, std::vector<std::vector<VectorX>> & Qr, std::vector<std::vector<std::vector<VectorX>>> & Cr) {}
		
		virtual void prepareForStep() {}
		virtual void computeForces(std::vector<MatrixX> & force_omp_arrays) = 0;
		
		void use_gpu(bool use) { m_use_gpu = use; }
		
		virtual void computeDfDx(std::vector<std::vector<Jacobian> > & dfdx) {}
		
		virtual void prepareForSubspacePolynomialPrecomputation(std::vector<std::set<int>> & stencil_dofs_per_node) {}
		virtual void subspacePolynomialPrecomputationNodei(int node_i, const std::vector<int> & stencil_dofs_vec, ValuesForDof values_for_dof[3]) {}
		
	protected:
	
		void cumul(const std::vector<int> & nodes_vec_rev, ValuesForDof & values_for_dof, int dof0, int dof1, int dof2, Real val) // cubic
		{
			std::vector<int> sorted_nodes = { nodes_vec_rev[dof0], nodes_vec_rev[dof1], nodes_vec_rev[dof2] };
			std::sort(sorted_nodes.begin(), sorted_nodes.end());
			values_for_dof.cubic[sorted_nodes[0]][sorted_nodes[1]][sorted_nodes[2]] += val;
		}

		void cumul(const std::vector<int> & nodes_vec_rev, ValuesForDof & values_for_dof, int dof0, int dof1, Real val) // quadratic
		{
			if (nodes_vec_rev[dof0] < nodes_vec_rev[dof1]) values_for_dof.quadratic[nodes_vec_rev[dof0]][nodes_vec_rev[dof1]] += val;
			else values_for_dof.quadratic[nodes_vec_rev[dof1]][nodes_vec_rev[dof0]] += val;
		}

		void cumul(const std::vector<int> & nodes_vec_rev, ValuesForDof & values_for_dof, int dof0, Real val) // linear
		{
			values_for_dof.linear[nodes_vec_rev[dof0]] += val;
		}
			
		void cumul(const std::vector<int> & nodes_vec_rev, ValuesForDof & values_for_dof, Real val) // constant
		{
			values_for_dof.constant += val;
		}
		
	protected:
		
		bool m_use_gpu;
	
};

#endif // MULTISCALESOUNDS_ENERGY_H
