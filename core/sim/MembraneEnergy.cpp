
#include "MembraneEnergy.h"

#include <igl/per_face_normals.h>
#include <Eigen/Sparse>

#include <omp.h>
#include <iostream>

#define USE_OMP

MembraneEnergy::MembraneEnergy(MatrixX & rest_pos, MatrixX & pos, MatrixXi & F, std::vector<int> & per_node_dofs, VectorX & triangle_rest_area, Real young_modulus, Real poisson_ratio) 
:m_rest_pos(rest_pos), m_pos(pos), m_F(F), m_per_node_dofs(per_node_dofs), m_triangle_rest_area(triangle_rest_area), m_young_modulus(young_modulus), m_poisson_ratio(poisson_ratio)
{
}

void MembraneEnergy::setup()
{
	igl::per_face_normals(m_rest_pos, m_F, m_triangle_normals);
	m_membrane_ru.resize(m_F.rows(), 3);
	m_membrane_rv.resize(m_F.rows(), 3);
	
    for (unsigned int f=0; f<m_F.rows(); f++)
    {
        Vector3 x0 = m_rest_pos.row(m_F(f,0));
		Vector3 x1 = m_rest_pos.row(m_F(f,1));
		Vector3 x2 = m_rest_pos.row(m_F(f,2));
		
		Vector3 normal = m_triangle_normals.row(f);
		
		//Define (u,v) coords by LOCALLY rotating the triangle to align it with the Z axis
		Quaternion rot = Quaternion::FromTwoVectors(normal, Vector3::UnitZ());

		// compute uv of each vertex
		Vector2 uvi = Vector2::Zero();
		Vector2 uvj = (rot * (x1 - x0)).segment(0,2);
		Vector2 uvk = (rot * (x2 - x0)).segment(0,2);
		
		//Determine vertex weights for strain computation
		Real dinv = 1./(uvi(0) * (uvj(1) - uvk(1)) + uvj(0) * (uvk(1) - uvi(1)) + uvk(0) * (uvi(1) - uvj(1)));
		m_membrane_ru.row(f) << dinv * (uvj(1) - uvk(1)), dinv * (uvk(1) - uvi(1)), dinv * (uvi(1) - uvj(1));
		m_membrane_rv.row(f) << dinv * (uvk(0) - uvj(0)), dinv * (uvi(0) - uvk(0)), dinv * (uvj(0) - uvi(0));
    }
    
    //m_membrane_material_tensor << 1, m_poisson_ratio, 0, m_poisson_ratio, 1, 0, 0, 0, 0.5*(1-m_poisson_ratio);
    m_membrane_material_tensor << 1, m_poisson_ratio, 0, m_poisson_ratio, 1, 0, 0, 0, 0.5*(1-m_poisson_ratio);
    m_membrane_material_tensor = m_membrane_material_tensor*(m_young_modulus/(1-m_poisson_ratio*m_poisson_ratio));
	
	// compute triangles per node
	m_triangles_per_node.resize(m_rest_pos.rows());
	for (int i=0; i<m_F.rows(); i++)
	{
		m_triangles_per_node[m_F(i,0)].push_back(i);
		m_triangles_per_node[m_F(i,1)].push_back(i);
		m_triangles_per_node[m_F(i,2)].push_back(i);
	}

}

void MembraneEnergy::prepareForStep()
{
	igl::per_face_normals(m_pos, m_F, m_triangle_normals);
}

void MembraneEnergy::computeForces(std::vector<MatrixX> & force_omp_arrays)
{
	#ifdef USE_OMP
	#pragma omp parallel
	#endif
	{
		int omp_thread_index = omp_get_thread_num();

		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (unsigned int f=0; f<m_F.rows(); f++)
		{
			Vector3 x0 = m_pos.row(m_F(f,0));
			Vector3 x1 = m_pos.row(m_F(f,1));
			Vector3 x2 = m_pos.row(m_F(f,2));

			Vector3 U = x0*m_membrane_ru(f,0) + x1*m_membrane_ru(f,1) + x2*m_membrane_ru(f,2);
			Vector3 V = x0*m_membrane_rv(f,0) + x1*m_membrane_rv(f,1) + x2*m_membrane_rv(f,2);
			
			Vector3 strain = Vector3(0.5*(U.dot(U) - 1), 0.5*(V.dot(V) - 1), U.dot(V));
			Vector3 stress = m_membrane_material_tensor*strain;
		
			for (unsigned int i=0; i<3; i++)
			{
				force_omp_arrays[omp_thread_index].row(m_F(f,i)) += -m_triangle_rest_area(f)*( stress(0)*(m_membrane_ru(f,i)*U) + stress(1)*(m_membrane_rv(f,i)*V) + stress(2)*(m_membrane_ru(f,i)*V + m_membrane_rv(f,i)*U) );
			}
		}
	}
	
}

void MembraneEnergy::computeDfDx(std::vector<std::vector<Jacobian> > & dfdx)
{
	auto l_membrane_stencil = [this] (int f, Matrix3 dfdx_membrane[3][3])
	{
		Vector3 x0 = m_pos.row(m_F(f,0));
		Vector3 x1 = m_pos.row(m_F(f,1));
		Vector3 x2 = m_pos.row(m_F(f,2));

        Vector3 U = x0*m_membrane_ru(f,0) + x1*m_membrane_ru(f,1) + x2*m_membrane_ru(f,2);
		Vector3 V = x0*m_membrane_rv(f,0) + x1*m_membrane_rv(f,1) + x2*m_membrane_rv(f,2);
		
		Vector3 strain = Vector3(0.5*(U.dot(U) - 1), 0.5*(V.dot(V) - 1), U.dot(V));
        Vector3 stress = m_membrane_material_tensor*strain;
		
		for (unsigned int i=0; i<3; i++)
		{
			for (unsigned int j=0; j<3; j++)
			{
				dfdx_membrane[i][j] = -m_triangle_rest_area(f)*(m_membrane_material_tensor(0,0)*(m_membrane_ru(f,i)*m_membrane_ru(f,j)*U*U.transpose()) 
										+ m_membrane_material_tensor(1,1)*(m_membrane_rv(f,i)*m_membrane_rv(f,j)*V*V.transpose())
										+ m_membrane_material_tensor(2,2)*( (m_membrane_rv(f,i)*m_membrane_ru(f,j)*U*V.transpose() + m_membrane_ru(f,i)*m_membrane_rv(f,j)*V*U.transpose()) +
																	(m_membrane_rv(f,i)*m_membrane_rv(f,j)*U*U.transpose() + m_membrane_ru(f,i)*m_membrane_ru(f,j)*V*V.transpose()) )
										+ m_membrane_material_tensor(0,1)*(m_membrane_ru(f,i)*m_membrane_rv(f,j)*U*V.transpose())
										+ m_membrane_material_tensor(1,0)*(m_membrane_rv(f,i)*m_membrane_ru(f,j)*V*U.transpose())
										+ (stress(0)*(m_membrane_ru(f,i)*m_membrane_ru(f,j)) + stress(1)*(m_membrane_rv(f,i)*m_membrane_rv(f,j)) + stress(2)*(m_membrane_ru(f,i)*m_membrane_rv(f,j) + m_membrane_rv(f,i)*m_membrane_ru(f,j)))*Matrix3::Identity());
				
			}
		}      
	};
	
	#ifdef USE_OMP
	#pragma omp parallel
	#endif
	{
		int omp_thread_index = omp_get_thread_num();

		#ifdef USE_OMP
		#pragma omp for
		#endif
		for (unsigned int f=0; f<m_F.rows(); f++)
		{
			Matrix3 dfdx_membrane[3][3];
			l_membrane_stencil(f, dfdx_membrane);
			
			Jacobian jac;
			
			for (unsigned int i=0; i<3; i++)
			{
				if (m_per_node_dofs[m_F(f,i)] == -1) continue;
				
				jac.index_i = m_per_node_dofs[m_F(f,i)];
				
				for (unsigned int j=0; j<3; j++)
				{
					if (m_per_node_dofs[m_F(f,j)] == -1) continue;
					
					jac.index_j = m_per_node_dofs[m_F(f,j)];
					jac.jacobian = dfdx_membrane[i][j];
					
					dfdx[omp_thread_index].push_back(jac);
				}
			}
		}
	}
}

void MembraneEnergy::prepareForSubspacePolynomialPrecomputation(std::vector<std::set<int>> & stencil_dofs_per_node)
{
	for (int i=0; i<m_triangles_per_node.size(); i++)
	{
		auto & stencil_dofs = stencil_dofs_per_node[i];
		
		for (auto f : m_triangles_per_node[i])
		{
			stencil_dofs.insert(m_F(f,0)*3+0);
			stencil_dofs.insert(m_F(f,0)*3+1);
			stencil_dofs.insert(m_F(f,0)*3+2);
			stencil_dofs.insert(m_F(f,1)*3+0);
			stencil_dofs.insert(m_F(f,1)*3+1);
			stencil_dofs.insert(m_F(f,1)*3+2);
			stencil_dofs.insert(m_F(f,2)*3+0);
			stencil_dofs.insert(m_F(f,2)*3+1);
			stencil_dofs.insert(m_F(f,2)*3+2);
		}
	}
	
}

void MembraneEnergy::subspacePolynomialPrecomputationNodei(int node_i, const std::vector<int> & stencil_dofs_vec, ValuesForDof values_for_dof[3])
{
	auto l_square = [] (double val) -> double
	{
		return val*val;
	};
	
	std::vector<int> dofs_vec_rev(m_rest_pos.rows()*3, 0);
	for (int i=0; i<stencil_dofs_vec.size(); i++)
		dofs_vec_rev[stencil_dofs_vec[i]] = i;
	
	for (auto f : m_triangles_per_node[node_i])
	{
		int ux0 = m_F(f,0)*3+0;
		int ux1 = m_F(f,1)*3+0;
		int ux2 = m_F(f,2)*3+0;
		int uy0 = m_F(f,0)*3+1;
		int uy1 = m_F(f,1)*3+1;
		int uy2 = m_F(f,2)*3+1;
		int uz0 = m_F(f,0)*3+2;
		int uz1 = m_F(f,1)*3+2;
		int uz2 = m_F(f,2)*3+2;
		
		Real rx0 = m_rest_pos(m_F(f,0), 0);
		Real rx1 = m_rest_pos(m_F(f,1), 0);
		Real rx2 = m_rest_pos(m_F(f,2), 0);
		Real ry0 = m_rest_pos(m_F(f,0), 1);
		Real ry1 = m_rest_pos(m_F(f,1), 1);
		Real ry2 = m_rest_pos(m_F(f,2), 1);
		Real rz0 = m_rest_pos(m_F(f,0), 2);
		Real rz1 = m_rest_pos(m_F(f,1), 2);
		Real rz2 = m_rest_pos(m_F(f,2), 2);

		Real A = m_membrane_ru(f,0);
		Real B = m_membrane_ru(f,1);
		Real C = m_membrane_ru(f,2);
		Real D = m_membrane_rv(f,0);
		Real E = m_membrane_rv(f,1);
		Real F = m_membrane_rv(f,2);
		
		Real T00 = m_membrane_material_tensor(0,0);
		Real T01 = m_membrane_material_tensor(0,1);
		Real T10 = m_membrane_material_tensor(1,0);
		Real T11 = m_membrane_material_tensor(1,1);
		Real T22 = m_membrane_material_tensor(2,2);
		
		Real L = -m_triangle_rest_area(f);
		
		Real X;
		Real Y;
		
		if (m_F(f,0) == node_i)
		{
			X = A;
			Y = D;
		}
		else if (m_F(f,1) == node_i)
		{
			X = B;
			Y = E;
		}
		else
		{
			X = C;
			Y = F;
		}
			
		{
			cumul(dofs_vec_rev, values_for_dof[0], ux0, ux0, ux0, L*(A*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+D*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, ux1, ux1, L*(B*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+E*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, ux2, ux2, L*(C*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+F*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, ux0, ux1, L*(B*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+E*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+T22*(A*E+B*D)*(A*Y+D*X)+A*X*(A*B*T00+D*E*T01)+D*Y*(A*B*T10+D*E*T11)+A*D*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, ux1, ux1, L*(A*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+D*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+T22*(A*E+B*D)*(B*Y+E*X)+B*X*(A*B*T00+D*E*T01)+E*Y*(A*B*T10+D*E*T11)+B*E*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, ux0, ux2, L*(C*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+F*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+T22*(A*F+C*D)*(A*Y+D*X)+A*X*(A*C*T00+D*F*T01)+D*Y*(A*C*T10+D*F*T11)+A*D*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, ux2, ux2, L*(A*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+D*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+T22*(A*F+C*D)*(C*Y+F*X)+C*X*(A*C*T00+D*F*T01)+F*Y*(A*C*T10+D*F*T11)+C*F*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, ux1, ux2, L*(C*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+F*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+T22*(B*F+C*E)*(B*Y+E*X)+B*X*(B*C*T00+E*F*T01)+E*Y*(B*C*T10+E*F*T11)+B*E*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, ux2, ux2, L*(B*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+E*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+T22*(B*F+C*E)*(C*Y+F*X)+C*X*(B*C*T00+E*F*T01)+F*Y*(B*C*T10+E*F*T11)+C*F*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uy0, uy0, L*(A*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+D*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uz0, uz0, L*(A*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+D*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, ux1, ux2, L*(T22*(A*E+B*D)*(C*Y+F*X)+T22*(A*F+C*D)*(B*Y+E*X)+T22*(B*F+C*E)*(A*Y+D*X)+C*X*(A*B*T00+D*E*T01)+B*X*(A*C*T00+D*F*T01)+A*X*(B*C*T00+E*F*T01)+F*Y*(A*B*T10+D*E*T11)+E*Y*(A*C*T10+D*F*T11)+D*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uy0, uy1, L*(T22*(A*E+B*D)*(A*Y+D*X)+A*X*(A*B*T00+D*E*T01)+D*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uz0, uz1, L*(T22*(A*E+B*D)*(A*Y+D*X)+A*X*(A*B*T00+D*E*T01)+D*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uy0, uy1, L*(T22*(A*E+B*D)*(B*Y+E*X)+B*X*(A*B*T00+D*E*T01)+E*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uz0, uz1, L*(T22*(A*E+B*D)*(B*Y+E*X)+B*X*(A*B*T00+D*E*T01)+E*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uy0, uy2, L*(T22*(A*F+C*D)*(A*Y+D*X)+A*X*(A*C*T00+D*F*T01)+D*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uz0, uz2, L*(T22*(A*F+C*D)*(A*Y+D*X)+A*X*(A*C*T00+D*F*T01)+D*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uy0, uy1, L*(T22*(A*E+B*D)*(C*Y+F*X)+C*X*(A*B*T00+D*E*T01)+F*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uz0, uz1, L*(T22*(A*E+B*D)*(C*Y+F*X)+C*X*(A*B*T00+D*E*T01)+F*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uy0, uy2, L*(T22*(A*F+C*D)*(B*Y+E*X)+B*X*(A*C*T00+D*F*T01)+E*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uz0, uz2, L*(T22*(A*F+C*D)*(B*Y+E*X)+B*X*(A*C*T00+D*F*T01)+E*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uy1, uy2, L*(T22*(B*F+C*E)*(A*Y+D*X)+A*X*(B*C*T00+E*F*T01)+D*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uz1, uz2, L*(T22*(B*F+C*E)*(A*Y+D*X)+A*X*(B*C*T00+E*F*T01)+D*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uy0, uy2, L*(T22*(A*F+C*D)*(C*Y+F*X)+C*X*(A*C*T00+D*F*T01)+F*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uz0, uz2, L*(T22*(A*F+C*D)*(C*Y+F*X)+C*X*(A*C*T00+D*F*T01)+F*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uy1, uy2, L*(T22*(B*F+C*E)*(B*Y+E*X)+B*X*(B*C*T00+E*F*T01)+E*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uz1, uz2, L*(T22*(B*F+C*E)*(B*Y+E*X)+B*X*(B*C*T00+E*F*T01)+E*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uy1, uy2, L*(T22*(B*F+C*E)*(C*Y+F*X)+C*X*(B*C*T00+E*F*T01)+F*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uz1, uz2, L*(T22*(B*F+C*E)*(C*Y+F*X)+C*X*(B*C*T00+E*F*T01)+F*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uy0, uy0, L*(B*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+E*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uz0, uz0, L*(B*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+E*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uy1, uy1, L*(A*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+D*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uz1, uz1, L*(A*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+D*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uy0, uy0, L*(C*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+F*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uz0, uz0, L*(C*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+F*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uy1, uy1, L*(B*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+E*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uz1, uz1, L*(B*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+E*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uy2, uy2, L*(A*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+D*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uz2, uz2, L*(A*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+D*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uy1, uy1, L*(C*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+F*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uz1, uz1, L*(C*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+F*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uy2, uy2, L*(B*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+E*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uz2, uz2, L*(B*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+E*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uy2, uy2, L*(C*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+F*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uz2, uz2, L*(C*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+F*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(C*Y+F*X)));
			
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uy0, L*(A*X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))+D*Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))+T22*(A*Y+D*X)*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uz0, L*(A*X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))+D*Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))+T22*(A*Y+D*X)*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uy0, L*(B*X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))+E*Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))+T22*(B*Y+E*X)*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uz0, L*(B*X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))+E*Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))+T22*(B*Y+E*X)*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uy1, L*(A*X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))+D*Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))+T22*(A*Y+D*X)*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uz1, L*(A*X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))+D*Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))+T22*(A*Y+D*X)*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uy0, L*(C*X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))+F*Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))+T22*(C*Y+F*X)*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uz0, L*(C*X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))+F*Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))+T22*(C*Y+F*X)*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uy1, L*(B*X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))+E*Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))+T22*(B*Y+E*X)*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uz1, L*(B*X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))+E*Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))+T22*(B*Y+E*X)*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uy2, L*(A*X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))+D*Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))+T22*(A*Y+D*X)*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, uz2, L*(A*X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))+D*Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))+T22*(A*Y+D*X)*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uy1, L*(C*X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))+F*Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))+T22*(C*Y+F*X)*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uz1, L*(C*X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))+F*Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))+T22*(C*Y+F*X)*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uy2, L*(B*X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))+E*Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))+T22*(B*Y+E*X)*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, uz2, L*(B*X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))+E*Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))+T22*(B*Y+E*X)*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uy2, L*(C*X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))+F*Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))+T22*(C*Y+F*X)*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, uz2, L*(C*X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))+F*Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))+T22*(C*Y+F*X)*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, ux1, L*(X*(A*B*T00+D*E*T01)*(A*rx0+B*rx1+C*rx2)+Y*(A*B*T10+D*E*T11)*(D*rx0+E*rx1+F*rx2)+B*X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))+A*X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))+E*Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))+D*Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(A*E+B*D)+T22*(A*Y+D*X)*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))+T22*(B*Y+E*X)*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, uy1, L*(X*(A*B*T00+D*E*T01)*(A*rx0+B*rx1+C*rx2)+Y*(A*B*T10+D*E*T11)*(D*rx0+E*rx1+F*rx2)+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(A*E+B*D)));
			cumul(dofs_vec_rev, values_for_dof[0], uz0, uz1, L*(X*(A*B*T00+D*E*T01)*(A*rx0+B*rx1+C*rx2)+Y*(A*B*T10+D*E*T11)*(D*rx0+E*rx1+F*rx2)+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(A*E+B*D)));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, uy2, L*(X*(A*C*T00+D*F*T01)*(A*rx0+B*rx1+C*rx2)+Y*(A*C*T10+D*F*T11)*(D*rx0+E*rx1+F*rx2)+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(A*F+C*D)));
			cumul(dofs_vec_rev, values_for_dof[0], uz0, uz2, L*(X*(A*C*T00+D*F*T01)*(A*rx0+B*rx1+C*rx2)+Y*(A*C*T10+D*F*T11)*(D*rx0+E*rx1+F*rx2)+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(A*F+C*D)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, ux2, L*(X*(A*C*T00+D*F*T01)*(A*rx0+B*rx1+C*rx2)+Y*(A*C*T10+D*F*T11)*(D*rx0+E*rx1+F*rx2)+C*X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))+A*X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))+F*Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))+D*Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(A*F+C*D)+T22*(A*Y+D*X)*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))+T22*(C*Y+F*X)*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], uy1, uy2, L*(X*(B*C*T00+E*F*T01)*(A*rx0+B*rx1+C*rx2)+Y*(B*C*T10+E*F*T11)*(D*rx0+E*rx1+F*rx2)+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(B*F+C*E)));
			cumul(dofs_vec_rev, values_for_dof[0], uz1, uz2, L*(X*(B*C*T00+E*F*T01)*(A*rx0+B*rx1+C*rx2)+Y*(B*C*T10+E*F*T11)*(D*rx0+E*rx1+F*rx2)+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(B*F+C*E)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, L*(T22*(A*Y+D*X)*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))+A*X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+D*Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))*(A*rx0+B*rx1+C*rx2)+Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))*(D*rx0+E*rx1+F*rx2)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, L*(T22*(B*Y+E*X)*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))+B*X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+E*Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))*(A*rx0+B*rx1+C*rx2)+Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))*(D*rx0+E*rx1+F*rx2)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, L*(T22*(C*Y+F*X)*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))+C*X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+F*Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))*(A*rx0+B*rx1+C*rx2)+Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))*(D*rx0+E*rx1+F*rx2)));
			cumul(dofs_vec_rev, values_for_dof[0], ux0, ux0, L*(X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))*(A*rx0+B*rx1+C*rx2)+Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))*(D*rx0+E*rx1+F*rx2)+A*X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))+D*Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))+T22*(A*Y+D*X)*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))+A*D*T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, uy0, L*(X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))*(A*rx0+B*rx1+C*rx2)+Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))*(D*rx0+E*rx1+F*rx2)+A*D*T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], uz0, uz0, L*(X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))*(A*rx0+B*rx1+C*rx2)+Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))*(D*rx0+E*rx1+F*rx2)+A*D*T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], uy0, L*(T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))+X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))*(A*rx0+B*rx1+C*rx2)+Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))*(D*rx0+E*rx1+F*rx2)));
			cumul(dofs_vec_rev, values_for_dof[0], uz0, L*(T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))+X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))*(A*rx0+B*rx1+C*rx2)+Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))*(D*rx0+E*rx1+F*rx2)));
			cumul(dofs_vec_rev, values_for_dof[0], uy1, uy1, L*(X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))*(A*rx0+B*rx1+C*rx2)+Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))*(D*rx0+E*rx1+F*rx2)+B*E*T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], uz1, uz1, L*(X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))*(A*rx0+B*rx1+C*rx2)+Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))*(D*rx0+E*rx1+F*rx2)+B*E*T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], uy1, L*(T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))+X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))*(A*rx0+B*rx1+C*rx2)+Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))*(D*rx0+E*rx1+F*rx2)));
			cumul(dofs_vec_rev, values_for_dof[0], uz1, L*(T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))+X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))*(A*rx0+B*rx1+C*rx2)+Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))*(D*rx0+E*rx1+F*rx2)));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, ux1, L*(X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))*(A*rx0+B*rx1+C*rx2)+Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))*(D*rx0+E*rx1+F*rx2)+B*X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))+E*Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))+T22*(B*Y+E*X)*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))+B*E*T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], uy2, uy2, L*(X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))*(A*rx0+B*rx1+C*rx2)+Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))*(D*rx0+E*rx1+F*rx2)+C*F*T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], uz2, uz2, L*(X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))*(A*rx0+B*rx1+C*rx2)+Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))*(D*rx0+E*rx1+F*rx2)+C*F*T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], uy2, L*(T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))+X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))*(A*rx0+B*rx1+C*rx2)+Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))*(D*rx0+E*rx1+F*rx2)));
			cumul(dofs_vec_rev, values_for_dof[0], uz2, L*(T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))+X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))*(A*rx0+B*rx1+C*rx2)+Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))*(D*rx0+E*rx1+F*rx2)));
			cumul(dofs_vec_rev, values_for_dof[0], ux2, ux2, L*(X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))*(A*rx0+B*rx1+C*rx2)+Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))*(D*rx0+E*rx1+F*rx2)+C*X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))+F*Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))+T22*(C*Y+F*X)*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))+C*F*T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], L*(T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[0], ux1, ux2, L*(X*(B*C*T00+E*F*T01)*(A*rx0+B*rx1+C*rx2)+Y*(B*C*T10+E*F*T11)*(D*rx0+E*rx1+F*rx2)+C*X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))+B*X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))+F*Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))+E*Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))+T22*(Y*(A*rx0+B*rx1+C*rx2)+X*(D*rx0+E*rx1+F*rx2))*(B*F+C*E)+T22*(B*Y+E*X)*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))+T22*(C*Y+F*X)*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[0], L*(X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))*(A*rx0+B*rx1+C*rx2)+Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))*(D*rx0+E*rx1+F*rx2)));
		}
		
		{
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uy0, uy0, L*(A*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+D*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uy1, uy1, L*(B*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+E*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uy2, uy2, L*(C*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+F*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, L*(T22*(A*Y+D*X)*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))+A*X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+D*Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))*(A*ry0+B*ry1+C*ry2)+Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))*(D*ry0+E*ry1+F*ry2)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, L*(T22*(B*Y+E*X)*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))+B*X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+E*Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))*(A*ry0+B*ry1+C*ry2)+Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))*(D*ry0+E*ry1+F*ry2)));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, L*(T22*(C*Y+F*X)*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))+C*X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+F*Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))*(A*ry0+B*ry1+C*ry2)+Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))*(D*ry0+E*ry1+F*ry2)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uy0, L*(X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))*(A*ry0+B*ry1+C*ry2)+Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))*(D*ry0+E*ry1+F*ry2)+A*X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))+D*Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))+T22*(A*Y+D*X)*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))+A*D*T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux0, L*(X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))*(A*ry0+B*ry1+C*ry2)+Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))*(D*ry0+E*ry1+F*ry2)+A*D*T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], uz0, uz0, L*(X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))*(A*ry0+B*ry1+C*ry2)+Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))*(D*ry0+E*ry1+F*ry2)+A*D*T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, L*(T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))+X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))*(A*ry0+B*ry1+C*ry2)+Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))*(D*ry0+E*ry1+F*ry2)));
			cumul(dofs_vec_rev, values_for_dof[1], uz0, L*(T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))+X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))*(A*ry0+B*ry1+C*ry2)+Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))*(D*ry0+E*ry1+F*ry2)));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, ux1, L*(X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))*(A*ry0+B*ry1+C*ry2)+Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))*(D*ry0+E*ry1+F*ry2)+B*E*T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], uz1, uz1, L*(X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))*(A*ry0+B*ry1+C*ry2)+Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))*(D*ry0+E*ry1+F*ry2)+B*E*T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, L*(T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))+X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))*(A*ry0+B*ry1+C*ry2)+Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))*(D*ry0+E*ry1+F*ry2)));
			cumul(dofs_vec_rev, values_for_dof[1], uz1, L*(T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))+X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))*(A*ry0+B*ry1+C*ry2)+Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))*(D*ry0+E*ry1+F*ry2)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uy1, L*(X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))*(A*ry0+B*ry1+C*ry2)+Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))*(D*ry0+E*ry1+F*ry2)+B*X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))+E*Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))+T22*(B*Y+E*X)*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))+B*E*T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, ux2, L*(X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))*(A*ry0+B*ry1+C*ry2)+Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))*(D*ry0+E*ry1+F*ry2)+C*F*T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], uz2, uz2, L*(X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))*(A*ry0+B*ry1+C*ry2)+Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))*(D*ry0+E*ry1+F*ry2)+C*F*T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, L*(T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))+X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))*(A*ry0+B*ry1+C*ry2)+Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))*(D*ry0+E*ry1+F*ry2)));
			cumul(dofs_vec_rev, values_for_dof[1], uz2, L*(T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))+X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))*(A*ry0+B*ry1+C*ry2)+Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))*(D*ry0+E*ry1+F*ry2)));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uy2, L*(X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))*(A*ry0+B*ry1+C*ry2)+Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))*(D*ry0+E*ry1+F*ry2)+C*X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))+F*Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))+T22*(C*Y+F*X)*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))+C*F*T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], L*(T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uy2, L*(X*(B*C*T00+E*F*T01)*(A*ry0+B*ry1+C*ry2)+Y*(B*C*T10+E*F*T11)*(D*ry0+E*ry1+F*ry2)+C*X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))+B*X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))+F*Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))+E*Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(B*F+C*E)+T22*(B*Y+E*X)*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))+T22*(C*Y+F*X)*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], L*(X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))*(A*ry0+B*ry1+C*ry2)+Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))*(D*ry0+E*ry1+F*ry2)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uy0, uy1, L*(B*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+E*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+T22*(A*E+B*D)*(A*Y+D*X)+A*X*(A*B*T00+D*E*T01)+D*Y*(A*B*T10+D*E*T11)+A*D*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uy1, uy1, L*(A*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+D*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+T22*(A*E+B*D)*(B*Y+E*X)+B*X*(A*B*T00+D*E*T01)+E*Y*(A*B*T10+D*E*T11)+B*E*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uy0, uy2, L*(C*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+F*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+T22*(A*F+C*D)*(A*Y+D*X)+A*X*(A*C*T00+D*F*T01)+D*Y*(A*C*T10+D*F*T11)+A*D*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uy2, uy2, L*(A*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+D*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+T22*(A*F+C*D)*(C*Y+F*X)+C*X*(A*C*T00+D*F*T01)+F*Y*(A*C*T10+D*F*T11)+C*F*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uy1, uy2, L*(C*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+F*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+T22*(B*F+C*E)*(B*Y+E*X)+B*X*(B*C*T00+E*F*T01)+E*Y*(B*C*T10+E*F*T11)+B*E*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uy2, uy2, L*(B*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+E*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+T22*(B*F+C*E)*(C*Y+F*X)+C*X*(B*C*T00+E*F*T01)+F*Y*(B*C*T10+E*F*T11)+C*F*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux0, uy0, L*(A*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+D*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uz0, uz0, L*(A*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+D*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, uy0, L*(A*X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))+D*Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))+T22*(A*Y+D*X)*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uz0, L*(A*X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))+D*Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))+T22*(A*Y+D*X)*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux0, uy1, L*(B*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+E*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uz0, uz0, L*(B*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+E*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, uy1, L*(B*X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))+E*Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))+T22*(B*Y+E*X)*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uz0, L*(B*X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))+E*Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))+T22*(B*Y+E*X)*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, ux1, uy0, L*(A*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+D*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uz1, uz1, L*(A*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+D*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, uy0, L*(A*X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))+D*Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))+T22*(A*Y+D*X)*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uz1, L*(A*X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))+D*Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))+T22*(A*Y+D*X)*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux0, uy2, L*(C*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+F*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uz0, uz0, L*(C*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+F*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, uy2, L*(C*X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))+F*Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))+T22*(C*Y+F*X)*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uz0, L*(C*X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))+F*Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))+T22*(C*Y+F*X)*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, ux1, uy1, L*(B*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+E*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uz1, uz1, L*(B*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+E*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, ux2, uy0, L*(A*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+D*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uz2, uz2, L*(A*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+D*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, uy1, L*(B*X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))+E*Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))+T22*(B*Y+E*X)*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uz1, L*(B*X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))+E*Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))+T22*(B*Y+E*X)*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, uy0, L*(A*X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))+D*Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))+T22*(A*Y+D*X)*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uz2, L*(A*X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))+D*Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))+T22*(A*Y+D*X)*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, ux1, uy2, L*(C*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+F*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uz1, uz1, L*(C*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+F*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, uy2, L*(C*X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))+F*Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))+T22*(C*Y+F*X)*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uz1, L*(C*X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))+F*Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))+T22*(C*Y+F*X)*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, ux2, uy1, L*(B*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+E*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uz2, uz2, L*(B*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+E*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, uy1, L*(B*X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))+E*Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))+T22*(B*Y+E*X)*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uz2, L*(B*X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))+E*Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))+T22*(B*Y+E*X)*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, ux2, uy2, L*(C*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+F*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uz2, uz2, L*(C*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+F*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[1], ux2, uy2, L*(C*X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))+F*Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))+T22*(C*Y+F*X)*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uz2, L*(C*X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))+F*Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))+T22*(C*Y+F*X)*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uy1, L*(X*(A*B*T00+D*E*T01)*(A*ry0+B*ry1+C*ry2)+Y*(A*B*T10+D*E*T11)*(D*ry0+E*ry1+F*ry2)+B*X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))+A*X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))+E*Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))+D*Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(A*E+B*D)+T22*(A*Y+D*X)*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))+T22*(B*Y+E*X)*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux1, L*(X*(A*B*T00+D*E*T01)*(A*ry0+B*ry1+C*ry2)+Y*(A*B*T10+D*E*T11)*(D*ry0+E*ry1+F*ry2)+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(A*E+B*D)));
			cumul(dofs_vec_rev, values_for_dof[1], uz0, uz1, L*(X*(A*B*T00+D*E*T01)*(A*ry0+B*ry1+C*ry2)+Y*(A*B*T10+D*E*T11)*(D*ry0+E*ry1+F*ry2)+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(A*E+B*D)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux2, L*(X*(A*C*T00+D*F*T01)*(A*ry0+B*ry1+C*ry2)+Y*(A*C*T10+D*F*T11)*(D*ry0+E*ry1+F*ry2)+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(A*F+C*D)));
			cumul(dofs_vec_rev, values_for_dof[1], uz0, uz2, L*(X*(A*C*T00+D*F*T01)*(A*ry0+B*ry1+C*ry2)+Y*(A*C*T10+D*F*T11)*(D*ry0+E*ry1+F*ry2)+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(A*F+C*D)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uy2, L*(X*(A*C*T00+D*F*T01)*(A*ry0+B*ry1+C*ry2)+Y*(A*C*T10+D*F*T11)*(D*ry0+E*ry1+F*ry2)+C*X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))+A*X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))+F*Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))+D*Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(A*F+C*D)+T22*(A*Y+D*X)*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))+T22*(C*Y+F*X)*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, ux2, L*(X*(B*C*T00+E*F*T01)*(A*ry0+B*ry1+C*ry2)+Y*(B*C*T10+E*F*T11)*(D*ry0+E*ry1+F*ry2)+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(B*F+C*E)));
			cumul(dofs_vec_rev, values_for_dof[1], uz1, uz2, L*(X*(B*C*T00+E*F*T01)*(A*ry0+B*ry1+C*ry2)+Y*(B*C*T10+E*F*T11)*(D*ry0+E*ry1+F*ry2)+T22*(Y*(A*ry0+B*ry1+C*ry2)+X*(D*ry0+E*ry1+F*ry2))*(B*F+C*E)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uy1, uy2, L*(T22*(A*E+B*D)*(C*Y+F*X)+T22*(A*F+C*D)*(B*Y+E*X)+T22*(B*F+C*E)*(A*Y+D*X)+C*X*(A*B*T00+D*E*T01)+B*X*(A*C*T00+D*F*T01)+A*X*(B*C*T00+E*F*T01)+F*Y*(A*B*T10+D*E*T11)+E*Y*(A*C*T10+D*F*T11)+D*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux1, uy0, L*(T22*(A*E+B*D)*(A*Y+D*X)+A*X*(A*B*T00+D*E*T01)+D*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uz0, uz1, L*(T22*(A*E+B*D)*(A*Y+D*X)+A*X*(A*B*T00+D*E*T01)+D*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux1, uy1, L*(T22*(A*E+B*D)*(B*Y+E*X)+B*X*(A*B*T00+D*E*T01)+E*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uz0, uz1, L*(T22*(A*E+B*D)*(B*Y+E*X)+B*X*(A*B*T00+D*E*T01)+E*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux2, uy0, L*(T22*(A*F+C*D)*(A*Y+D*X)+A*X*(A*C*T00+D*F*T01)+D*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uz0, uz2, L*(T22*(A*F+C*D)*(A*Y+D*X)+A*X*(A*C*T00+D*F*T01)+D*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux1, uy2, L*(T22*(A*E+B*D)*(C*Y+F*X)+C*X*(A*B*T00+D*E*T01)+F*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uz0, uz1, L*(T22*(A*E+B*D)*(C*Y+F*X)+C*X*(A*B*T00+D*E*T01)+F*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux2, uy1, L*(T22*(A*F+C*D)*(B*Y+E*X)+B*X*(A*C*T00+D*F*T01)+E*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uz0, uz2, L*(T22*(A*F+C*D)*(B*Y+E*X)+B*X*(A*C*T00+D*F*T01)+E*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, ux2, uy0, L*(T22*(B*F+C*E)*(A*Y+D*X)+A*X*(B*C*T00+E*F*T01)+D*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], uy0, uz1, uz2, L*(T22*(B*F+C*E)*(A*Y+D*X)+A*X*(B*C*T00+E*F*T01)+D*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], ux0, ux2, uy2, L*(T22*(A*F+C*D)*(C*Y+F*X)+C*X*(A*C*T00+D*F*T01)+F*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uz0, uz2, L*(T22*(A*F+C*D)*(C*Y+F*X)+C*X*(A*C*T00+D*F*T01)+F*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, ux2, uy1, L*(T22*(B*F+C*E)*(B*Y+E*X)+B*X*(B*C*T00+E*F*T01)+E*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], uy1, uz1, uz2, L*(T22*(B*F+C*E)*(B*Y+E*X)+B*X*(B*C*T00+E*F*T01)+E*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], ux1, ux2, uy2, L*(T22*(B*F+C*E)*(C*Y+F*X)+C*X*(B*C*T00+E*F*T01)+F*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[1], uy2, uz1, uz2, L*(T22*(B*F+C*E)*(C*Y+F*X)+C*X*(B*C*T00+E*F*T01)+F*Y*(B*C*T10+E*F*T11)));
		}
		
		{
			cumul(dofs_vec_rev, values_for_dof[2], uz0, uz0, uz0, L*(A*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+D*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uz1, uz1, uz1, L*(B*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+E*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uz2, uz2, uz2, L*(C*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+F*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uz0, L*(T22*(A*Y+D*X)*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))+A*X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+D*Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))*(A*rz0+B*rz1+C*rz2)+Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))*(D*rz0+E*rz1+F*rz2)));
			cumul(dofs_vec_rev, values_for_dof[2], uz1, L*(T22*(B*Y+E*X)*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))+B*X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+E*Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))*(A*rz0+B*rz1+C*rz2)+Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))*(D*rz0+E*rz1+F*rz2)));
			cumul(dofs_vec_rev, values_for_dof[2], uz2, L*(T22*(C*Y+F*X)*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))+C*X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+F*Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))+X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))*(A*rz0+B*rz1+C*rz2)+Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))*(D*rz0+E*rz1+F*rz2)));
			cumul(dofs_vec_rev, values_for_dof[2], uz0, uz0, L*(X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))*(A*rz0+B*rz1+C*rz2)+Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))*(D*rz0+E*rz1+F*rz2)+A*X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))+D*Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))+T22*(A*Y+D*X)*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))+A*D*T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux0, L*(X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))*(A*rz0+B*rz1+C*rz2)+Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))*(D*rz0+E*rz1+F*rz2)+A*D*T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy0, L*(X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))*(A*rz0+B*rz1+C*rz2)+Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))*(D*rz0+E*rz1+F*rz2)+A*D*T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, L*(T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))+X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))*(A*rz0+B*rz1+C*rz2)+Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))*(D*rz0+E*rz1+F*rz2)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, L*(T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))+X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))*(A*rz0+B*rz1+C*rz2)+Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))*(D*rz0+E*rz1+F*rz2)));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, ux1, L*(X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))*(A*rz0+B*rz1+C*rz2)+Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))*(D*rz0+E*rz1+F*rz2)+B*E*T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uy1, L*(X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))*(A*rz0+B*rz1+C*rz2)+Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))*(D*rz0+E*rz1+F*rz2)+B*E*T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, L*(T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))+X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))*(A*rz0+B*rz1+C*rz2)+Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))*(D*rz0+E*rz1+F*rz2)));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, L*(T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))+X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))*(A*rz0+B*rz1+C*rz2)+Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))*(D*rz0+E*rz1+F*rz2)));
			cumul(dofs_vec_rev, values_for_dof[2], uz1, uz1, L*(X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))*(A*rz0+B*rz1+C*rz2)+Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))*(D*rz0+E*rz1+F*rz2)+B*X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))+E*Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))+T22*(B*Y+E*X)*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))+B*E*T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, ux2, L*(X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))*(A*rz0+B*rz1+C*rz2)+Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))*(D*rz0+E*rz1+F*rz2)+C*F*T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, uy2, L*(X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))*(A*rz0+B*rz1+C*rz2)+Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))*(D*rz0+E*rz1+F*rz2)+C*F*T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, L*(T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))+X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))*(A*rz0+B*rz1+C*rz2)+Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))*(D*rz0+E*rz1+F*rz2)));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, L*(T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))+X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))*(A*rz0+B*rz1+C*rz2)+Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))*(D*rz0+E*rz1+F*rz2)));
			cumul(dofs_vec_rev, values_for_dof[2], uz2, uz2, L*(X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))*(A*rz0+B*rz1+C*rz2)+Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))*(D*rz0+E*rz1+F*rz2)+C*X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))+F*Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))+T22*(C*Y+F*X)*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))+C*F*T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], L*(T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*((A*rx0+B*rx1+C*rx2)*(D*rx0+E*rx1+F*rx2)+(A*ry0+B*ry1+C*ry2)*(D*ry0+E*ry1+F*ry2)+(A*rz0+B*rz1+C*rz2)*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], uz1, uz2, L*(X*(B*C*T00+E*F*T01)*(A*rz0+B*rz1+C*rz2)+Y*(B*C*T10+E*F*T11)*(D*rz0+E*rz1+F*rz2)+C*X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))+B*X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))+F*Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))+E*Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(B*F+C*E)+T22*(B*Y+E*X)*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))+T22*(C*Y+F*X)*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], L*(X*(T00*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T01*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))*(A*rz0+B*rz1+C*rz2)+Y*(T10*(l_square(A*rx0+B*rx1+C*rx2)*(0.5)+l_square(A*ry0+B*ry1+C*ry2)*(0.5)+l_square(A*rz0+B*rz1+C*rz2)*(0.5)-0.5)+T11*(l_square(D*rx0+E*rx1+F*rx2)*(0.5)+l_square(D*ry0+E*ry1+F*ry2)*(0.5)+l_square(D*rz0+E*rz1+F*rz2)*(0.5)-0.5))*(D*rz0+E*rz1+F*rz2)));
			cumul(dofs_vec_rev, values_for_dof[2], uz0, uz0, uz1, L*(B*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+E*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+T22*(A*E+B*D)*(A*Y+D*X)+A*X*(A*B*T00+D*E*T01)+D*Y*(A*B*T10+D*E*T11)+A*D*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uz0, uz1, uz1, L*(A*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+D*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+T22*(A*E+B*D)*(B*Y+E*X)+B*X*(A*B*T00+D*E*T01)+E*Y*(A*B*T10+D*E*T11)+B*E*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uz0, uz0, uz2, L*(C*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+F*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+T22*(A*F+C*D)*(A*Y+D*X)+A*X*(A*C*T00+D*F*T01)+D*Y*(A*C*T10+D*F*T11)+A*D*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uz0, uz2, uz2, L*(A*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+D*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+T22*(A*F+C*D)*(C*Y+F*X)+C*X*(A*C*T00+D*F*T01)+F*Y*(A*C*T10+D*F*T11)+C*F*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uz1, uz1, uz2, L*(C*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+F*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+T22*(B*F+C*E)*(B*Y+E*X)+B*X*(B*C*T00+E*F*T01)+E*Y*(B*C*T10+E*F*T11)+B*E*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uz1, uz2, uz2, L*(B*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+E*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+T22*(B*F+C*E)*(C*Y+F*X)+C*X*(B*C*T00+E*F*T01)+F*Y*(B*C*T10+E*F*T11)+C*F*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux0, uz0, L*(A*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+D*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy0, uz0, L*(A*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+D*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, uz0, L*(A*X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))+D*Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))+T22*(A*Y+D*X)*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uz0, L*(A*X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))+D*Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))+T22*(A*Y+D*X)*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux0, uz1, L*(B*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+E*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy0, uz1, L*(B*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+E*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, uz1, L*(B*X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))+E*Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))+T22*(B*Y+E*X)*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uz1, L*(B*X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))+E*Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))+T22*(B*Y+E*X)*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, ux1, uz0, L*(A*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+D*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uy1, uz0, L*(A*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+D*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, uz0, L*(A*X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))+D*Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))+T22*(A*Y+D*X)*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uz0, L*(A*X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))+D*Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))+T22*(A*Y+D*X)*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux0, uz2, L*(C*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+F*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy0, uz2, L*(C*X*((A*A)*T00*(0.5)+(D*D)*T01*(0.5))+F*Y*((A*A)*T10*(0.5)+(D*D)*T11*(0.5))+A*D*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, uz2, L*(C*X*(A*T00*(A*rx0+B*rx1+C*rx2)+D*T01*(D*rx0+E*rx1+F*rx2))+F*Y*(A*T10*(A*rx0+B*rx1+C*rx2)+D*T11*(D*rx0+E*rx1+F*rx2))+T22*(C*Y+F*X)*(D*(A*rx0+B*rx1+C*rx2)+A*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uz2, L*(C*X*(A*T00*(A*ry0+B*ry1+C*ry2)+D*T01*(D*ry0+E*ry1+F*ry2))+F*Y*(A*T10*(A*ry0+B*ry1+C*ry2)+D*T11*(D*ry0+E*ry1+F*ry2))+T22*(C*Y+F*X)*(D*(A*ry0+B*ry1+C*ry2)+A*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, ux1, uz1, L*(B*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+E*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uy1, uz1, L*(B*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+E*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, ux2, uz0, L*(A*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+D*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, uy2, uz0, L*(A*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+D*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(A*Y+D*X)));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, uz1, L*(B*X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))+E*Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))+T22*(B*Y+E*X)*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uz1, L*(B*X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))+E*Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))+T22*(B*Y+E*X)*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, uz0, L*(A*X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))+D*Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))+T22*(A*Y+D*X)*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, uz0, L*(A*X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))+D*Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))+T22*(A*Y+D*X)*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, ux1, uz2, L*(C*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+F*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uy1, uz2, L*(C*X*((B*B)*T00*(0.5)+(E*E)*T01*(0.5))+F*Y*((B*B)*T10*(0.5)+(E*E)*T11*(0.5))+B*E*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, uz2, L*(C*X*(B*T00*(A*rx0+B*rx1+C*rx2)+E*T01*(D*rx0+E*rx1+F*rx2))+F*Y*(B*T10*(A*rx0+B*rx1+C*rx2)+E*T11*(D*rx0+E*rx1+F*rx2))+T22*(C*Y+F*X)*(E*(A*rx0+B*rx1+C*rx2)+B*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uz2, L*(C*X*(B*T00*(A*ry0+B*ry1+C*ry2)+E*T01*(D*ry0+E*ry1+F*ry2))+F*Y*(B*T10*(A*ry0+B*ry1+C*ry2)+E*T11*(D*ry0+E*ry1+F*ry2))+T22*(C*Y+F*X)*(E*(A*ry0+B*ry1+C*ry2)+B*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, ux2, uz1, L*(B*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+E*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, uy2, uz1, L*(B*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+E*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(B*Y+E*X)));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, uz1, L*(B*X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))+E*Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))+T22*(B*Y+E*X)*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, uz1, L*(B*X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))+E*Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))+T22*(B*Y+E*X)*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, ux2, uz2, L*(C*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+F*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, uy2, uz2, L*(C*X*((C*C)*T00*(0.5)+(F*F)*T01*(0.5))+F*Y*((C*C)*T10*(0.5)+(F*F)*T11*(0.5))+C*F*T22*(C*Y+F*X)));
			cumul(dofs_vec_rev, values_for_dof[2], ux2, uz2, L*(C*X*(C*T00*(A*rx0+B*rx1+C*rx2)+F*T01*(D*rx0+E*rx1+F*rx2))+F*Y*(C*T10*(A*rx0+B*rx1+C*rx2)+F*T11*(D*rx0+E*rx1+F*rx2))+T22*(C*Y+F*X)*(F*(A*rx0+B*rx1+C*rx2)+C*(D*rx0+E*rx1+F*rx2))));
			cumul(dofs_vec_rev, values_for_dof[2], uy2, uz2, L*(C*X*(C*T00*(A*ry0+B*ry1+C*ry2)+F*T01*(D*ry0+E*ry1+F*ry2))+F*Y*(C*T10*(A*ry0+B*ry1+C*ry2)+F*T11*(D*ry0+E*ry1+F*ry2))+T22*(C*Y+F*X)*(F*(A*ry0+B*ry1+C*ry2)+C*(D*ry0+E*ry1+F*ry2))));
			cumul(dofs_vec_rev, values_for_dof[2], uz0, uz1, L*(X*(A*B*T00+D*E*T01)*(A*rz0+B*rz1+C*rz2)+Y*(A*B*T10+D*E*T11)*(D*rz0+E*rz1+F*rz2)+B*X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))+A*X*(B*T00*(A*rz0+B*rz1+C*rz2)+E*T01*(D*rz0+E*rz1+F*rz2))+E*Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))+D*Y*(B*T10*(A*rz0+B*rz1+C*rz2)+E*T11*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(A*E+B*D)+T22*(A*Y+D*X)*(E*(A*rz0+B*rz1+C*rz2)+B*(D*rz0+E*rz1+F*rz2))+T22*(B*Y+E*X)*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux1, L*(X*(A*B*T00+D*E*T01)*(A*rz0+B*rz1+C*rz2)+Y*(A*B*T10+D*E*T11)*(D*rz0+E*rz1+F*rz2)+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(A*E+B*D)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy1, L*(X*(A*B*T00+D*E*T01)*(A*rz0+B*rz1+C*rz2)+Y*(A*B*T10+D*E*T11)*(D*rz0+E*rz1+F*rz2)+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(A*E+B*D)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux2, L*(X*(A*C*T00+D*F*T01)*(A*rz0+B*rz1+C*rz2)+Y*(A*C*T10+D*F*T11)*(D*rz0+E*rz1+F*rz2)+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(A*F+C*D)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy2, L*(X*(A*C*T00+D*F*T01)*(A*rz0+B*rz1+C*rz2)+Y*(A*C*T10+D*F*T11)*(D*rz0+E*rz1+F*rz2)+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(A*F+C*D)));
			cumul(dofs_vec_rev, values_for_dof[2], uz0, uz2, L*(X*(A*C*T00+D*F*T01)*(A*rz0+B*rz1+C*rz2)+Y*(A*C*T10+D*F*T11)*(D*rz0+E*rz1+F*rz2)+C*X*(A*T00*(A*rz0+B*rz1+C*rz2)+D*T01*(D*rz0+E*rz1+F*rz2))+A*X*(C*T00*(A*rz0+B*rz1+C*rz2)+F*T01*(D*rz0+E*rz1+F*rz2))+F*Y*(A*T10*(A*rz0+B*rz1+C*rz2)+D*T11*(D*rz0+E*rz1+F*rz2))+D*Y*(C*T10*(A*rz0+B*rz1+C*rz2)+F*T11*(D*rz0+E*rz1+F*rz2))+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(A*F+C*D)+T22*(A*Y+D*X)*(F*(A*rz0+B*rz1+C*rz2)+C*(D*rz0+E*rz1+F*rz2))+T22*(C*Y+F*X)*(D*(A*rz0+B*rz1+C*rz2)+A*(D*rz0+E*rz1+F*rz2))));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, ux2, L*(X*(B*C*T00+E*F*T01)*(A*rz0+B*rz1+C*rz2)+Y*(B*C*T10+E*F*T11)*(D*rz0+E*rz1+F*rz2)+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(B*F+C*E)));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uy2, L*(X*(B*C*T00+E*F*T01)*(A*rz0+B*rz1+C*rz2)+Y*(B*C*T10+E*F*T11)*(D*rz0+E*rz1+F*rz2)+T22*(Y*(A*rz0+B*rz1+C*rz2)+X*(D*rz0+E*rz1+F*rz2))*(B*F+C*E)));
			cumul(dofs_vec_rev, values_for_dof[2], uz0, uz1, uz2, L*(T22*(A*E+B*D)*(C*Y+F*X)+T22*(A*F+C*D)*(B*Y+E*X)+T22*(B*F+C*E)*(A*Y+D*X)+C*X*(A*B*T00+D*E*T01)+B*X*(A*C*T00+D*F*T01)+A*X*(B*C*T00+E*F*T01)+F*Y*(A*B*T10+D*E*T11)+E*Y*(A*C*T10+D*F*T11)+D*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux1, uz0, L*(T22*(A*E+B*D)*(A*Y+D*X)+A*X*(A*B*T00+D*E*T01)+D*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy1, uz0, L*(T22*(A*E+B*D)*(A*Y+D*X)+A*X*(A*B*T00+D*E*T01)+D*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux1, uz1, L*(T22*(A*E+B*D)*(B*Y+E*X)+B*X*(A*B*T00+D*E*T01)+E*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy1, uz1, L*(T22*(A*E+B*D)*(B*Y+E*X)+B*X*(A*B*T00+D*E*T01)+E*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux2, uz0, L*(T22*(A*F+C*D)*(A*Y+D*X)+A*X*(A*C*T00+D*F*T01)+D*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy2, uz0, L*(T22*(A*F+C*D)*(A*Y+D*X)+A*X*(A*C*T00+D*F*T01)+D*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux1, uz2, L*(T22*(A*E+B*D)*(C*Y+F*X)+C*X*(A*B*T00+D*E*T01)+F*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy1, uz2, L*(T22*(A*E+B*D)*(C*Y+F*X)+C*X*(A*B*T00+D*E*T01)+F*Y*(A*B*T10+D*E*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux2, uz1, L*(T22*(A*F+C*D)*(B*Y+E*X)+B*X*(A*C*T00+D*F*T01)+E*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy2, uz1, L*(T22*(A*F+C*D)*(B*Y+E*X)+B*X*(A*C*T00+D*F*T01)+E*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, ux2, uz0, L*(T22*(B*F+C*E)*(A*Y+D*X)+A*X*(B*C*T00+E*F*T01)+D*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uy2, uz0, L*(T22*(B*F+C*E)*(A*Y+D*X)+A*X*(B*C*T00+E*F*T01)+D*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], ux0, ux2, uz2, L*(T22*(A*F+C*D)*(C*Y+F*X)+C*X*(A*C*T00+D*F*T01)+F*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], uy0, uy2, uz2, L*(T22*(A*F+C*D)*(C*Y+F*X)+C*X*(A*C*T00+D*F*T01)+F*Y*(A*C*T10+D*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, ux2, uz1, L*(T22*(B*F+C*E)*(B*Y+E*X)+B*X*(B*C*T00+E*F*T01)+E*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uy2, uz1, L*(T22*(B*F+C*E)*(B*Y+E*X)+B*X*(B*C*T00+E*F*T01)+E*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], ux1, ux2, uz2, L*(T22*(B*F+C*E)*(C*Y+F*X)+C*X*(B*C*T00+E*F*T01)+F*Y*(B*C*T10+E*F*T11)));
			cumul(dofs_vec_rev, values_for_dof[2], uy1, uy2, uz2, L*(T22*(B*F+C*E)*(C*Y+F*X)+C*X*(B*C*T00+E*F*T01)+F*Y*(B*C*T10+E*F*T11)));
		}
	}
	
}
