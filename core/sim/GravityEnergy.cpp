
#include "GravityEnergy.h"

#include <omp.h>

#define USE_OMP

GravityEnergy::GravityEnergy(VectorX & node_rest_area, Real density, Real thickness, Vector3 gravity_vector)
:m_node_rest_area(node_rest_area), m_density(density), m_thickness(thickness), m_gravity_vector(gravity_vector)
{
}

void GravityEnergy::computeForces(std::vector<MatrixX> & force_omp_arrays)
{
	#ifdef USE_OMP
	#pragma omp parallel for
	#endif
	for (unsigned int i=0; i<m_node_rest_area.size(); i++)
	{
		force_omp_arrays[0].row(i) += m_density*m_thickness*m_node_rest_area(i)*m_gravity_vector;
	}
}

