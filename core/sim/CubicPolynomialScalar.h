
#ifndef CUBIC_POLYNOMIAL_SCALAR
#define CUBIC_POLYNOMIAL_SCALAR

/*
// ******************************************** //
// ************** USAGE EXAMPLE *************** //
// ******************************************** //

typedef Eigen::Matrix<CubicPolynomialScalar, 3, 1> PVector3;
typedef Eigen::Matrix<CubicPolynomialScalar, 3, 3> PMatrix3;

CubicPolynomialScalar two(2.);
PVector3 twovec(two, two, two);
PVector3 x0(CubicPolynomialScalar(0, 1.), CubicPolynomialScalar(1, 1.), CubicPolynomialScalar(2, 1.));
PVector3 x1(CubicPolynomialScalar(3, 1.), CubicPolynomialScalar(4, 1.), CubicPolynomialScalar(5, 1.));
PVector3 x2(CubicPolynomialScalar(6, 1.), CubicPolynomialScalar(7, 1.), CubicPolynomialScalar(8, 1.));
CubicPolynomialScalar res = 10.*(x0+x2).cross(3.*x1).dot(x2/2.) + 5.*x0.transpose()*x1 + x2(0) + 1.;

std::cout << res << std::endl;

// ******************************************** //
// ******************************************** //
*/

#include "typedefs.h"

#include <string>
#include <iostream>
#include <vector>
#include <fstream>

struct SymbolicValue
{
	public:
		
		SymbolicValue() {}
		
		SymbolicValue(int val)
		{
			if (val != 0)
				m_val = std::to_string(val) + ".";
		}
		
		SymbolicValue(const std::string & val) : m_val(val) { }
		
		SymbolicValue(const SymbolicValue & val) : m_val(val.m_val) {}
		
		std::string get() const { return m_val; }
		
		bool isOne() const
		{
			return (m_val == "1.") ? true : false;
		}
		
		bool isMinusOne() const
		{
			return ( (m_val == "-1.") || (m_val == "(-1.)") ) ? true : false;
		}
		
		bool isZero() const
		{
			return ( (m_val == "0.") || (m_val == "") ) ? true : false;
		}
		
		//operator bool() const { return m_val != ""; }
	
		friend SymbolicValue operator+(const SymbolicValue &lhs, const SymbolicValue &rhs) 
		{
			SymbolicValue res;
			if ( lhs.isZero() || rhs.isZero() )
			{
				if (!lhs.isZero()) res = lhs;
				else if (!rhs.isZero()) res = rhs;
				// if both 0, then empty
			}
			else
				res.m_val = "(" + lhs.m_val + "+" + rhs.m_val + ")";
			
			return res;
		}

		inline SymbolicValue& operator+=(const SymbolicValue &s) 
		{
			if ( this->isZero() || s.isZero() )
			{
				if (!s.isZero()) *this = s;
			}
			else
				m_val = "(" + m_val + "+" + s.m_val + ")";
			
			return *this;
		}

		friend SymbolicValue operator-(const SymbolicValue &lhs, const SymbolicValue &rhs) 
		{
			SymbolicValue res;
			if ( lhs.isZero() || rhs.isZero() )
			{
				if (!lhs.isZero()) res = lhs;
				else if (!rhs.isZero()) res = rhs;
				// if both 0, then empty
			}
			else
				res.m_val = "(" + lhs.m_val + "-" + rhs.m_val + ")";
			
			return res;
		}

		friend SymbolicValue operator-(const SymbolicValue &s)
		{
			SymbolicValue res;
			
			if (!s.isZero())
				res.m_val = "(-" + s.m_val + ")";
			
			return res;
		}

		inline SymbolicValue& operator-=(const SymbolicValue &s) 
		{
			if ( this->isZero() || s.isZero() )
			{
				if (!s.isZero()) *this = s;
			}
			else
				m_val = "(" + m_val + "-" + s.m_val + ")";
			
			return *this;
		}

		friend SymbolicValue operator/(const SymbolicValue &lhs, const SymbolicValue &rhs)
		{
			SymbolicValue res;
			if ( lhs.isZero() || rhs.isOne() )
			{
				if ( rhs.isOne() && (!lhs.isZero()) ) res = lhs;
			}
			else
				res.m_val = "(" + lhs.m_val + "/" + rhs.m_val + ")";
			
			return res;
		}
		
		inline SymbolicValue& operator/=(const SymbolicValue &s) 
		{
			if ( (!this->isZero()) && (!s.isOne()) )
				m_val = "(" + m_val + "/" + s.m_val + ")";
			
			return *this;
		}

		inline friend SymbolicValue operator*(const SymbolicValue &lhs, const SymbolicValue &rhs) 
		{
			SymbolicValue res;
			if ( !lhs.isZero() && !rhs.isZero() )
			{
				if (lhs.isOne()) res = rhs;
				else if (lhs.isMinusOne()) res = "(-" + rhs.m_val + ")";
				else if (rhs.isOne()) res = lhs;
				else if (rhs.isMinusOne()) res = "(-" + lhs.m_val + ")";
				else res.m_val = "(" + lhs.m_val + "*" + rhs.m_val + ")";
			}
			
			return res;
		}

		inline SymbolicValue& operator*=(const SymbolicValue &s) 
		{
			if (this->isZero() || s.isZero()) m_val = "";
			else if (!s.isOne())
			{
				if (this->isOne()) m_val = s.m_val;
				else m_val = "(" + m_val + "*" + s.m_val + ")";
			}
			
			return *this;
		}

		inline void operator=(const SymbolicValue& s) 
		{
			m_val = s.m_val;
		}
		
	private:
		
		std::string m_val;
};

class CubicPolynomialScalar
{

public:
	
    friend std::ostream & operator<<(std::ostream&, const CubicPolynomialScalar&);
	
	CubicPolynomialScalar()
	{
		m_l.reserve(12);
		m_q.reserve(12*12);
		m_c.reserve(12*12*12);
	}
	
	explicit CubicPolynomialScalar(int value) : m_k(value) 
	{
		m_l.reserve(12);
		m_q.reserve(12*12);
		m_c.reserve(12*12*12);
	}
	
	CubicPolynomialScalar(const SymbolicValue & value) : m_k(value) 
	{
		m_l.reserve(12);
		m_q.reserve(12*12);
		m_c.reserve(12*12*12);
	}
	
	CubicPolynomialScalar(int var, const SymbolicValue & value)
	{
		m_l.reserve(12);
		m_q.reserve(12*12);
		m_c.reserve(12*12*12);
		
		m_l.push_back(Term(var, value));
	}
	
	// Copy constructor
	CubicPolynomialScalar(const CubicPolynomialScalar & s)
	{
		m_k = s.m_k;
		m_l = s.m_l;
		m_q = s.m_q;
		m_c = s.m_c;
	}

	// ======================================================================
	// Addition
	// ======================================================================
	
	friend CubicPolynomialScalar operator+(const CubicPolynomialScalar &lhs, const CubicPolynomialScalar &rhs)
	{
		CubicPolynomialScalar res(lhs);
		
		res.m_k += rhs.m_k;
		res.m_l.insert(res.m_l.end(), rhs.m_l.begin(), rhs.m_l.end());
		res.m_q.insert(res.m_q.end(), rhs.m_q.begin(), rhs.m_q.end());
		res.m_c.insert(res.m_c.end(), rhs.m_c.begin(), rhs.m_c.end());
		
		return res;
	}

	friend CubicPolynomialScalar operator+(const CubicPolynomialScalar &lhs, const SymbolicValue &rhs)
	{
		CubicPolynomialScalar res(lhs);
		res.m_k += rhs;
		return res;
	}

	friend CubicPolynomialScalar operator+(const SymbolicValue &lhs, const CubicPolynomialScalar &rhs) 
	{
		CubicPolynomialScalar res(rhs);
		res.m_k += lhs;
		return res;
	}

	inline CubicPolynomialScalar& operator+=(const CubicPolynomialScalar &s) 
	{
		m_k += s.m_k;
		m_l.insert(m_l.end(), s.m_l.begin(), s.m_l.end());
		m_q.insert(m_q.end(), s.m_q.begin(), s.m_q.end());
		m_c.insert(m_c.end(), s.m_c.begin(), s.m_c.end());
		
		return *this;
	}

	inline CubicPolynomialScalar& operator+=(const SymbolicValue &v) 
	{
		m_k += v;
		return *this;
	}

	// ======================================================================
	// Substraction
	// ======================================================================
	
	friend CubicPolynomialScalar operator-(const CubicPolynomialScalar &lhs, const CubicPolynomialScalar &rhs) 
	{
		CubicPolynomialScalar res(lhs);
		
		res.m_k -= rhs.m_k;
		for (const auto & term : rhs.m_l) res.m_l.push_back(Term(term.vars, -term.value));
		for (const auto & term : rhs.m_q) res.m_q.push_back(Term(term.vars, -term.value));
		for (const auto & term : rhs.m_c) res.m_c.push_back(Term(term.vars, -term.value));

		return res;
	}

	friend CubicPolynomialScalar operator-(const CubicPolynomialScalar &lhs, const SymbolicValue &rhs)
	{
		CubicPolynomialScalar res(lhs);
		res.m_k -= rhs;
		return res;
	}

	friend CubicPolynomialScalar operator-(const SymbolicValue &lhs, const CubicPolynomialScalar &rhs) 
	{
		CubicPolynomialScalar res(rhs);
		res.m_k -= lhs;
		return res;
	}

	friend CubicPolynomialScalar operator-(const CubicPolynomialScalar &s) 
	{
		CubicPolynomialScalar res;
		
		res.m_k = -s.m_k;
		for (const auto & term : s.m_l) res.m_l.push_back(Term(term.vars, -term.value));
		for (const auto & term : s.m_q) res.m_q.push_back(Term(term.vars, -term.value));
		for (const auto & term : s.m_c) res.m_c.push_back(Term(term.vars, -term.value));

		return res;
	}

	inline CubicPolynomialScalar& operator-=(const CubicPolynomialScalar &s) 
	{
		m_k -= s.m_k;
		for (const auto & term : s.m_l) m_l.push_back(Term(term.vars, -term.value));
		for (const auto & term : s.m_q) m_q.push_back(Term(term.vars, -term.value));
		for (const auto & term : s.m_c) m_c.push_back(Term(term.vars, -term.value));

		return *this;
	}

	inline CubicPolynomialScalar& operator-=(const SymbolicValue &v) 
	{
		m_k -= v;
		return *this;
	}
	
	// ======================================================================
	// Division
	// ======================================================================
	
	friend CubicPolynomialScalar operator/(const CubicPolynomialScalar &lhs, const SymbolicValue &rhs)
	{
		if (rhs.isZero())
			throw std::runtime_error("CubicPolynomialScalar: Division by zero!");
		
		CubicPolynomialScalar res;
		
		res.m_k = lhs.m_k/rhs;
		for (const auto & term : lhs.m_l) res.m_l.push_back(Term(term.vars, term.value/rhs));
		for (const auto & term : lhs.m_q) res.m_q.push_back(Term(term.vars, term.value/rhs));
		for (const auto & term : lhs.m_c) res.m_c.push_back(Term(term.vars, term.value/rhs));

		return res;
	}

	inline CubicPolynomialScalar& operator/=(const SymbolicValue &v)
	{
		m_k /= v;
		for (auto & term : m_l) term.value /= v;
		for (auto & term : m_q) term.value /= v;
		for (auto & term : m_c) term.value /= v;
		
		return *this;
	}
	
	// The only reason this one is defined is to support division of an eigen vector of CPScalars by a scalar
	friend CubicPolynomialScalar operator/(const CubicPolynomialScalar &lhs, const CubicPolynomialScalar &rhs) 
	{
		if (rhs.m_l.size() || rhs.m_q.size() || rhs.m_c.size())
			std::cout << "WARNING!! Denominator has non-constant values! Your equation is not polynomial!" << std::endl;
		
		CubicPolynomialScalar res;
		
		SymbolicValue v = rhs.m_k;
		
		res.m_k = lhs.m_k/v;
		for (const auto & term : lhs.m_l) res.m_l.push_back(Term(term.vars, term.value/v));
		for (const auto & term : lhs.m_q) res.m_q.push_back(Term(term.vars, term.value/v));
		for (const auto & term : lhs.m_c) res.m_c.push_back(Term(term.vars, term.value/v));

		return res;
	}

	// ======================================================================
	// Multiplication
	// ======================================================================
	
	inline friend CubicPolynomialScalar operator*(const CubicPolynomialScalar &lhs, const SymbolicValue &rhs) 
	{
		CubicPolynomialScalar res;
		
		res.m_k = lhs.m_k*rhs;
		for (const auto & term : lhs.m_l) res.m_l.push_back(Term(term.vars, term.value*rhs));
		for (const auto & term : lhs.m_q) res.m_q.push_back(Term(term.vars, term.value*rhs));
		for (const auto & term : lhs.m_c) res.m_c.push_back(Term(term.vars, term.value*rhs));

		return res;
	}

	inline friend CubicPolynomialScalar operator*(const SymbolicValue &lhs, const CubicPolynomialScalar &rhs)
	{
		CubicPolynomialScalar res;
		
		res.m_k = rhs.m_k*lhs;
		for (const auto & term : rhs.m_l) res.m_l.push_back(Term(term.vars, term.value*lhs));
		for (const auto & term : rhs.m_q) res.m_q.push_back(Term(term.vars, term.value*lhs));
		for (const auto & term : rhs.m_c) res.m_c.push_back(Term(term.vars, term.value*lhs));

		return res;
	}

	inline friend CubicPolynomialScalar operator*(const CubicPolynomialScalar &lhs, const CubicPolynomialScalar &rhs) 
	{
		if ( (lhs.m_l.size() && rhs.m_c.size()) || (lhs.m_q.size() && (rhs.m_q.size() || rhs.m_c.size())) || (lhs.m_c.size() && (rhs.m_l.size() || rhs.m_q.size() || rhs.m_c.size())) )
			std::cout << "WARNING!! Multiplication produces terms beyond cubic order! Your equation is not cubic!" << std::endl;
		
		CubicPolynomialScalar res;
		
		// k*k, k*l, k*q, k*c don't change polynomial order
		if (!lhs.m_k.isZero())
		{
			if (!rhs.m_k.isZero()) res.m_k = lhs.m_k*rhs.m_k;
			for (const auto & term_rhs : rhs.m_l) res.m_l.push_back(Term(term_rhs.vars, term_rhs.value*lhs.m_k));
			for (const auto & term_rhs : rhs.m_q) res.m_q.push_back(Term(term_rhs.vars, term_rhs.value*lhs.m_k));
			for (const auto & term_rhs : rhs.m_c) res.m_c.push_back(Term(term_rhs.vars, term_rhs.value*lhs.m_k));
		}
		
		// l*k, l*l, l*q increase polynomial order of rhs by 1. l*c fails since cubic max.
		for (const auto & term_lhs : lhs.m_l)
		{
			if (!rhs.m_k.isZero()) res.m_l.push_back(Term(term_lhs.vars, term_lhs.value*rhs.m_k));
			for (const auto & term_rhs : rhs.m_l) res.m_q.push_back(Term(term_lhs.vars[0],term_rhs.vars[0], term_lhs.value*term_rhs.value));
			for (const auto & term_rhs : rhs.m_q) res.m_c.push_back(Term(term_lhs.vars[0],term_rhs.vars[0],term_rhs.vars[1], term_lhs.value*term_rhs.value));
		}
		
		// q*k, q*l increase polynomial order of rhs by 2. q*q and q*c fail since cubic max.
		for (const auto & term_lhs : lhs.m_q)
		{
			if (!rhs.m_k.isZero()) res.m_q.push_back(Term(term_lhs.vars, term_lhs.value*rhs.m_k));
			for (const auto & term_rhs : rhs.m_l) res.m_c.push_back(Term(term_lhs.vars[0],term_lhs.vars[1],term_rhs.vars[0], term_lhs.value*term_rhs.value));
		}
		
		// c*k increases polynomial order of rhs by 3. c*l, c*q and c*c fail since cubic max.
		for (const auto & term_lhs : lhs.m_c)
			if (!rhs.m_k.isZero()) res.m_c.push_back(Term(term_lhs.vars, term_lhs.value*rhs.m_k));
		
		return res;
	}

	inline CubicPolynomialScalar& operator*=(const SymbolicValue &v) 
	{
		m_k *= v;
		for (auto & term : m_l) term.value *= v;
		for (auto & term : m_q) term.value *= v;
		for (auto & term : m_c) term.value *= v;

		return *this;
	}

	// ======================================================================
	// Assignment
	// ======================================================================
	
	inline void operator=(const CubicPolynomialScalar& s)
	{ 
		m_k = s.m_k;
		m_l = s.m_l;
		m_q = s.m_q;
		m_c = s.m_c;
	}
	
	inline void operator=(const SymbolicValue &v)
	{ 
		m_k = v;
		m_l.clear();
		m_q.clear();
		m_c.clear();
	}
	
	// ======================================================================
	// Evaluation
	// ======================================================================
	
	/*Real eval(const VectorX & vars_values)
	{
		SymbolicValue val = m_k;
		
		for (const auto & term : m_l) val += vars_values(term.vars[0])*term.value;
		for (const auto & term : m_q) val += vars_values(term.vars[0])*vars_values(term.vars[1])*term.value;
		for (const auto & term : m_c) 
			val += vars_values(term.vars[0])*vars_values(term.vars[1])*vars_values(term.vars[2])*term.value;
		
		return val;
	}*/
	
	void getPolynomial(VectorX & value_per_term, int nb_vars)
	{
		/*int nb_terms_l = nb_vars;
		int nb_terms_q = 0;
		int nb_terms_c = 0;
		
		for (int i=0; i<nb_vars; i++)
		{
			for (int j=i; j<nb_vars; j++)
			{
				nb_terms_q++;
				for (int k=j; k<nb_vars; k++)
					nb_terms_c++;
			}
		}
		
		int nb_terms_tot = 1 + nb_terms_l + nb_terms_q + nb_terms_c;
		
		// accumulation without grouping terms
		
		int nb_vars_2 = nb_vars*nb_vars;
		int nb_vars_3 = nb_vars*nb_vars*nb_vars;
		
		VectorX full_accum_l = VectorX::Zero(nb_vars);
		VectorX full_accum_q = VectorX::Zero(nb_vars_2);
		VectorX full_accum_c = VectorX::Zero(nb_vars_3);
		
		for (const auto & term : m_l) 
			full_accum_l(term.vars[0]) += term.value;
		
		for (const auto & term : m_q) 
			full_accum_q(term.vars[0]*nb_vars + term.vars[1]) += term.value;
		
		for (const auto & term : m_c) 
			full_accum_c(term.vars[0]*nb_vars_2 + term.vars[1]*nb_vars + term.vars[2]) += term.value;
		
		// now reduce by grouping terms
		
		value_per_term.setZero(nb_terms_tot);
		
		int counter = 0;
		
		value_per_term(0) = m_k;
		counter++;
		
		for (int i=0; i<nb_vars; i++)
		{
			value_per_term(counter) += full_accum_l(i);
			counter++;
			
			for (int j=i; j<nb_vars; j++)
			{
				value_per_term(counter) += full_accum_q(i*nb_vars+j);
				if (i != j)
					value_per_term(counter) += full_accum_q(j*nb_vars+i);
				
				counter++;
			
				for (int k=j; k<nb_vars; k++)
				{
					if ( (i != j) && (j != k) && (i != k) ) // all different
					{
						value_per_term(counter) += full_accum_c(i*nb_vars_2 + j*nb_vars + k);
						value_per_term(counter) += full_accum_c(i*nb_vars_2 + k*nb_vars + j);
						value_per_term(counter) += full_accum_c(j*nb_vars_2 + i*nb_vars + k);
						value_per_term(counter) += full_accum_c(j*nb_vars_2 + k*nb_vars + i);
						value_per_term(counter) += full_accum_c(k*nb_vars_2 + i*nb_vars + j);
						value_per_term(counter) += full_accum_c(k*nb_vars_2 + j*nb_vars + i);
					}
					else if ( (i == j) && (j == k) ) // all the same
					{
						value_per_term(counter) += full_accum_c(i*nb_vars_2 + j*nb_vars + k);
					}
					else // 2 same, 1 different
					{
						if (i == j)
						{
							value_per_term(counter) += full_accum_c(i*nb_vars_2 + j*nb_vars + k);
							value_per_term(counter) += full_accum_c(i*nb_vars_2 + k*nb_vars + j);
							value_per_term(counter) += full_accum_c(k*nb_vars_2 + i*nb_vars + j);
						}
						else if (i == k)
						{
							value_per_term(counter) += full_accum_c(i*nb_vars_2 + j*nb_vars + k);
							value_per_term(counter) += full_accum_c(i*nb_vars_2 + k*nb_vars + j);
							value_per_term(counter) += full_accum_c(j*nb_vars_2 + i*nb_vars + k);
						}
						else // (j == k)
						{
							value_per_term(counter) += full_accum_c(i*nb_vars_2 + j*nb_vars + k);
							value_per_term(counter) += full_accum_c(j*nb_vars_2 + i*nb_vars + k);
							value_per_term(counter) += full_accum_c(j*nb_vars_2 + k*nb_vars + i);
						}
					}
					
					counter++;
				}
			}
		}*/
			
		// ********************************************** //
		// *** HOW TO USE THE OUTPUT OF THIS FUNCTION *** //
		// ********************************************** //
		/*Real accum = 0;
		int counter = 0;
		int nb_vars = 9;
		
		accum += polynomial(0);
		counter++;
		
		for (int i=0; i<nb_vars; i++)
		{
			accum += polynomial(counter)*values(i);
			counter++;
			
			for (int j=i; j<nb_vars; j++)
			{
				accum += polynomial(counter)*values(i)*values(j);
				counter++;
			
				for (int k=j; k<nb_vars; k++)
				{
					accum += polynomial(counter)*values(i)*values(j)*values(k);
					counter++;
				}
			}
		}*/
		// ********************************************** //
		// ********************************************** //

	}
	
	void generateCode(std::ofstream & code_file, int nb_vars, std::string polynomial_name)
	{
		int nb_terms_l = nb_vars;
		int nb_terms_q = 0;
		int nb_terms_c = 0;
		
		for (int i=0; i<nb_vars; i++)
		{
			for (int j=i; j<nb_vars; j++)
			{
				nb_terms_q++;
				for (int k=j; k<nb_vars; k++)
					nb_terms_c++;
			}
		}
		
		int nb_terms_tot = 1 + nb_terms_l + nb_terms_q + nb_terms_c;
		
		// accumulation without grouping terms
		
		int nb_vars_2 = nb_vars*nb_vars;
		int nb_vars_3 = nb_vars*nb_vars*nb_vars;
		
		std::vector<std::string> full_accum_l(nb_vars);
		std::vector<std::string> full_accum_q(nb_vars_2);
		std::vector<std::string> full_accum_c(nb_vars_3);
		
		for (const auto & term : m_l) 
		{
			std::string & final_val = full_accum_l[term.vars[0]];
			std::string current_val = final_val;
			std::string new_val = term.value.get();
			if (new_val != "")
			{
				if (current_val != "") final_val = "(" + current_val + "+" + new_val + ")";
				else final_val = new_val;
			}
		}
		
		for (const auto & term : m_q)
		{
			std::string & final_val = full_accum_q[term.vars[0]*nb_vars + term.vars[1]];
			std::string current_val = final_val;
			std::string new_val = term.value.get();
			if (new_val != "")
			{
				if (current_val != "") final_val = "(" + current_val + "+" + new_val + ")";
				else final_val = new_val;
			}
		}
		
		for (const auto & term : m_c)
		{
			std::string & final_val = full_accum_c[term.vars[0]*nb_vars_2 + term.vars[1]*nb_vars + term.vars[2]];
			std::string current_val = final_val;
			std::string new_val = term.value.get();
			if (new_val != "")
			{
				if (current_val != "") final_val = "(" + current_val + "+" + new_val + ")";
				else final_val = new_val;
			}
		}
		
		// now reduce by grouping terms
		
		auto accum_vals = [] (std::string & lhs, const std::string & rhs)
		{
			if (rhs != "")
			{
				if (lhs != "") lhs = lhs + "+" + rhs;
				else lhs = rhs;
			}
		};
		
		std::vector<std::string> value_per_term(nb_terms_tot);
		
		int counter = 0;
		
		value_per_term[0] = m_k.get();
		counter++;
		
		for (int i=0; i<nb_vars; i++)
		{
			value_per_term[counter] = full_accum_l[i];
			counter++;
			
			for (int j=i; j<nb_vars; j++)
			{
				value_per_term[counter] = full_accum_q[i*nb_vars+j];
				if (i != j)
					accum_vals(value_per_term[counter], full_accum_q[j*nb_vars+i]);
				counter++;
			
				for (int k=j; k<nb_vars; k++)
				{
					if ( (i != j) && (j != k) && (i != k) ) // all different
					{
						accum_vals(value_per_term[counter], full_accum_c[i*nb_vars_2 + j*nb_vars + k]);
						accum_vals(value_per_term[counter], full_accum_c[i*nb_vars_2 + k*nb_vars + j]);
						accum_vals(value_per_term[counter], full_accum_c[j*nb_vars_2 + i*nb_vars + k]);
						accum_vals(value_per_term[counter], full_accum_c[j*nb_vars_2 + k*nb_vars + i]);
						accum_vals(value_per_term[counter], full_accum_c[k*nb_vars_2 + i*nb_vars + j]);
						accum_vals(value_per_term[counter], full_accum_c[k*nb_vars_2 + j*nb_vars + i]);
					}
					else if ( (i == j) && (j == k) ) // all the same
					{
						accum_vals(value_per_term[counter], full_accum_c[i*nb_vars_2 + j*nb_vars + k]);
					}
					else // 2 same, 1 different
					{
						if (i == j)
						{
							accum_vals(value_per_term[counter], full_accum_c[i*nb_vars_2 + j*nb_vars + k]);
							accum_vals(value_per_term[counter], full_accum_c[i*nb_vars_2 + k*nb_vars + j]);
							accum_vals(value_per_term[counter], full_accum_c[k*nb_vars_2 + i*nb_vars + j]);
						}
						else if (i == k)
						{
							accum_vals(value_per_term[counter], full_accum_c[i*nb_vars_2 + j*nb_vars + k]);
							accum_vals(value_per_term[counter], full_accum_c[i*nb_vars_2 + k*nb_vars + j]);
							accum_vals(value_per_term[counter], full_accum_c[j*nb_vars_2 + i*nb_vars + k]);
						}
						else // (j == k)
						{
							accum_vals(value_per_term[counter], full_accum_c[i*nb_vars_2 + j*nb_vars + k]);
							accum_vals(value_per_term[counter], full_accum_c[j*nb_vars_2 + i*nb_vars + k]);
							accum_vals(value_per_term[counter], full_accum_c[j*nb_vars_2 + k*nb_vars + i]);
						}
					}
					
					counter++;
				}
			}
		}
		
		// now dump to file
		
		code_file << polynomial_name << ".setZero(" << std::to_string(nb_terms_tot) << ");" << std::endl;
		
		counter = 0;
		
		if (value_per_term[counter] != "") 
			code_file << polynomial_name << "(" << std::to_string(counter) << ") = " << value_per_term[counter] << ";" << std::endl;
		counter++;
		
		for (int i=0; i<nb_vars; i++)
		{
			if (value_per_term[counter] != "") 
				code_file << polynomial_name << "(" << std::to_string(counter) << ") = " << value_per_term[counter] << ";" << std::endl;
			counter++;
			
			for (int j=i; j<nb_vars; j++)
			{
				if (value_per_term[counter] != "") 
					code_file << polynomial_name << "(" << std::to_string(counter) << ") = " << value_per_term[counter] << ";" << std::endl;
				counter++;
			
				for (int k=j; k<nb_vars; k++)
				{
					if (value_per_term[counter] != "") 
						code_file << polynomial_name << "(" << std::to_string(counter) << ") = " << value_per_term[counter] << ";" << std::endl;
					
					counter++;
				}
			}
		}
	}

protected:
	
	struct Term
	{
		Term(int val) : value(val) {}
		//Term(const SymbolicValue & val) : value(val) {}
		Term(const std::vector<int> & _vars, const SymbolicValue & val) : vars(_vars), value(val) {}
		Term(int i, const SymbolicValue & val) : value(val)
		{
			vars = {i};
		}
		Term(int i, int j, const SymbolicValue & val) : value(val)
		{
			vars = {i,j};
		}
		Term(int i, int j, int k, const SymbolicValue & val) : value(val)
		{
			vars = {i,j,k};
		}
		
		std::vector<int> vars;
		SymbolicValue value;
	};
	
	SymbolicValue m_k; // constant term
	std::vector<Term> m_l; // linear terms
	std::vector<Term> m_q; // quadratic terms
	std::vector<Term> m_c; // cubic terms
	
};

std::ostream & operator<<(std::ostream & out, const CubicPolynomialScalar & s)
{
	if (!s.m_k.isZero())
	{
		out << "Constant: " << s.m_k.get() << std::endl;
	}
	
	if (s.m_l.size())
	{
		out << "Linear: ";
		for (const auto & term : s.m_l) 
			out << "(" << term.vars[0] << ")," << term.value.get() << " ";
		out << std::endl;
	}
	
	if (s.m_q.size())
	{
		out << "Quadratic: ";
		for (const auto & term : s.m_q)
			out << "(" << term.vars[0] << "," << term.vars[1] << ")," << term.value.get() << " ";
		out << std::endl;
	}
	
	if (s.m_c.size())
	{
		out << "Cubic: ";
		for (const auto & term : s.m_c)
			out << "(" << term.vars[0] << "," << term.vars[1] << "," << term.vars[2] << ")," << term.value.get() << " ";
		out << std::endl;
	}
	
	return out;
}

#endif // CUBIC_POLYNOMIAL_SCALAR
