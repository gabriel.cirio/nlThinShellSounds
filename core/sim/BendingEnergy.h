
#ifndef BENDING_ENERGY_H
#define BENDING_ENERGY_H

#include "Energy.h"

class BendingEnergy : public Energy
{
	public:
		
		BendingEnergy(MatrixX & rest_pos, MatrixX & pos, MatrixXi & F, std::vector<int> & per_node_dofs, VectorX & triangle_rest_area, MatrixXi & E_unique,
					  MatrixXi & per_unique_edge_triangles, MatrixXi & per_unique_edge_triangles_local_corners, MatrixXi & per_triangles_unique_edges,
					  Real young_modulus, Real poisson_ratio, Real thickness);
		
		~BendingEnergy() {}
		
		void setup();
		void prepareForStep() {}
		void computeForces(std::vector<MatrixX> & force_omp_arrays);
		
		void computeDfDx(std::vector<std::vector<Jacobian> > & dfdx);
		
	private:
	
		void computeBendingRestPhi(VectorX & rest_angles);
		
	private:
		
		MatrixX & m_rest_pos;
		MatrixX & m_pos;
		MatrixXi & m_F;
		std::vector<int> & m_per_node_dofs;
		VectorX & m_triangle_rest_area;
		
		MatrixXi & m_E_unique;
		MatrixXi & m_per_unique_edge_triangles;
		MatrixXi & m_per_unique_edge_triangles_local_corners;
		MatrixXi & m_per_triangles_unique_edges;
		
		Real m_young_modulus;
		Real m_poisson_ratio;
		Real m_thickness;
		
		Real m_bending_stiffness;
		VectorX m_per_edge_rest_phi; // theta or tantheta, depending on choice of bending formulation
		
};

#endif // BENDING_ENERGY_H
