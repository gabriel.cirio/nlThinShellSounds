
#ifndef MULTISCALESOUNDS_DYNAMICOBJECT_H
#define MULTISCALESOUNDS_DYNAMICOBJECT_H

#include "typedefs.h"

class DynamicObject
{
	public:
		
		DynamicObject() {}
		virtual ~DynamicObject() {}
		
		virtual void getMeshData(std::pair<MatrixX, MatrixXi> & mesh_data) = 0;
        virtual void getNodalVectors(MatrixX & vectors) {}
        virtual void getWorldVectors(std::pair<MatrixX, MatrixX> & vectors) {}
        
		virtual int fixNodes(Vector3 pos, Real radius) { return -1; }
		virtual void fixNode(int index) {}
		
		virtual void prepareForSimulation() = 0;
		virtual void prepareForStep(Real current_time, Real dt) = 0;
		virtual void computeInternalForces() = 0;
		virtual void computeInternalForceJacobians() = 0;
		virtual void computeInternalDampingJacobians() = 0;
		virtual void computeMass() = 0;
		virtual void postStep() {}
		virtual void setDtSubsteps(int dt_substeps) {}
		
		virtual void getDofState(Eigen::Ref<VectorX> positions, Eigen::Ref<VectorX> velocities) = 0;
		virtual void setDofState(const Eigen::Ref<const VectorX> & positions, const Eigen::Ref<const VectorX> & velocities) = 0;
		virtual void getDofForces(Eigen::Ref<VectorX> forces) = 0;
		virtual void getDofMasses(std::vector<std::vector<Jacobian> >* & masses) = 0;
		virtual void getDofForceJacobians(std::vector<std::vector<Jacobian> >* & force_jacobians) = 0;
		virtual void getDofDampingJacobians(std::vector<std::vector<Jacobian> >* & damping_jacobians) = 0;
		virtual unsigned int getNbDof() = 0;
		
		virtual bool is_reduced() { return false; }
		virtual void set_key(unsigned char key) {}
		
		virtual void writeToDisk() {}
		
		virtual void writeMeshes(std::string base_filename) {}
};


#endif // MULTISCALESOUNDS_DYNAMICOBJECT_H
