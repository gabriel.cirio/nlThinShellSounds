
#include "FastBEMFileGenerator.h"

#include <igl/per_face_normals.h>

#include <iostream>
#include <fstream>

void FastBEMFileGenerator::writeFiles(std::string path_prefix, const MatrixX & V, const MatrixXi & F, const MatrixX & evectors, const VectorX & evalues, const MatrixX & fieldpoints)
{
    
    // Write data in pairs of files (as many as requested chunks)
    
    // constants
    const Real _c = 343.0;
    const Real _density = 1.29;
    int _nrule = 8;
    int _ngauss = 1;
     
    const int FILE_CHUNKS = 40;
    
    MatrixX Fn;
    igl::per_face_normals(V, F, Fn);
    
    for (unsigned int chunk=0; chunk<FILE_CHUNKS; chunk++)
    {
        std::vector<int> chunk_indices;
        chunk_indices.reserve(evalues.rows()/FILE_CHUNKS+1);
        for (int i=chunk; i<evalues.rows(); i+=FILE_CHUNKS)
            if ( (evalues(i)>1.6e4) && (evalues(i)<1.92e10) ) // between 20Hz and 22050Hz
                chunk_indices.push_back(i);
            
        // input.dat : common stuff
        {
            Real min_angularFrequency = std::sqrt(evalues(chunk_indices[0]));
            Real min_frequency = min_angularFrequency/(2.*PI);
            
            Real max_angularFrequency = std::sqrt(evalues(chunk_indices.back()));
            Real max_frequency = max_angularFrequency/(2.*PI);
            
            std::string filename = path_prefix + "/chunk" + std::to_string(chunk+1) + ".dat";

            // Create a new file
            std::ofstream of(filename);

            if (!of)
            {
                std::cout << "ERROR: could not open file " << filename << std::endl;
                return;
            }

            // Output basic information at the start of the file
            of << "Acoustic radiation input data for " << path_prefix << " for frequencies " << min_frequency << " -- " << max_frequency << std::endl;

            // Solve the complete problem with Fast Multipole BEM solver
            of << "Complete      1" << std::endl;

            // Full problem space
            of << "Full     0     0.d0" << std::endl;

            // Output element counts
            of << F.rows()*2 << " "; // To accomodate both sides
            of << V.rows() << " ";
            of << fieldpoints.rows() << " ";
            
            of << "0" << std::endl;		// 0 field cells

            // No incident plane waves, no user-defined sources
            of << "0     0" << std::endl;
            //of << "(1., 0.)     -1.     0.     0." << std::endl;

            // No point sources
            of << "0" << std::endl;

            of << _c << " " << _density;
            of << "  2.d-5  1.d-12    ! speed of sound, medium density" << std::endl;

            // Frequency information: from min to max, number of frequencies, no octaves and specify we update the boundary conditions for each frequency
            of << min_frequency << " " << max_frequency << " " << chunk_indices.size() << " 0 1   ! Boundary is updated in jbc file" << std::endl;

            // HBIE and integration information
            if (_nrule > 6) _nrule = 6; // nrule and ngauss go from 1 to 6
            of << "0 " << _nrule << " " << _ngauss << " 0" << std::endl;

            // Begin writing node positions
            of << "$ Nodes:" << std::endl;

            for (unsigned int j=0; j<V.rows(); j++)
                of << (j+1) << " " << V(j,0) << " " << V(j,1) << " " << V(j,2) << std::endl;
            
            // Write out elements and default boundary conditions ==> won't be used since we are specifying BCs for each frequency
            of << "$ Elements and Boundary Conditions:" << std::endl;

            for (unsigned int j=0; j<F.rows(); j++)
            {
                of << j*2+1 << " ";

                for (int k=0; k<3; k++)
                    of << F(j,k) + 1 << " ";

                // Set up the default boundary condition
                std::complex<Real> boundaryCondition;
                boundaryCondition.real(0.0);
                boundaryCondition.imag(0.0);

                // Neumann BC
                of << "2 ";

                of << "( " << boundaryCondition.real() << ", " << boundaryCondition.imag() << ")" << std::endl;

                // Write an entry for the opposite side also
                of << j*2 + 2 << " ";

                for (int k=2; k>=0; k--)
                    of << F(j,k) + 1 << " ";

                // Neumann BC
                of << "2 ";

                boundaryCondition *= -1.0;
                of << "( " << boundaryCondition.real() << ", " << boundaryCondition.imag() << ")" << std::endl;
            }

            // Write out field points
            of << "$ Field Points:" << std::endl;

            for (unsigned int j=0; j<fieldpoints.rows(); j++)
                of << (j+1) << " " << fieldpoints(j,0) << " " << fieldpoints(j,1) << " " << fieldpoints(j,2) << std::endl;

            // Finish off the file
            of << "$ Field Cells:" << std::endl;
            of << "$ End of file" << std::endl;
            of.close();
        }

        // Now write another file with the frequencies and the boundary conditions for each frequency
        // Also write yet another file with information about the chunk: frequencies, number of boundary elements and of field points
        // Otherwise, parsing this info is slow and Fastbem outputs the wrong frequencies in the output files.
        {
            // Create BC file
            std::string filename = path_prefix + "/chunk" + std::to_string(chunk+1) + ".jbc";
            std::ofstream of_BC(filename);
            if (!of_BC)
            {
                std::cout << "ERROR: could not open file " << filename << std::endl;
                return;
            }
            
            // Create info file
            filename = path_prefix + "/chunk" + std::to_string(chunk+1) + ".info";
            std::ofstream of_info(filename);
            if (!of_info)
            {
                std::cout << "ERROR: could not open file " << filename << std::endl;
                return;
            }
        
            of_info << F.rows()*2 << std::endl;
            of_info << fieldpoints.rows() << std::endl;
            
            for (auto index : chunk_indices)
            {
                if ( index < 0 || index >= evectors.cols() )
                {
                    std::cout << "ERROR writing FastBEM data" << std::endl;
                    break;
                }

                Real angularFrequency = std::sqrt(evalues(index));
                Real frequency = angularFrequency/(2.*PI);

                std::cout << "Writing BCs for frequency " << index+1 << " => " << frequency << " Hz" << std::endl;
                
                of_info << frequency << std::endl;
                
                of_BC << "$ BC Values for Frequency No. " << index+1 << ":" << std::endl;
                of_BC << frequency << std::endl;

                of_BC << "$ Boundary Elem No., BC Type, BC Values" << std::endl;

                // Write out elements and boundary conditions
                
                for (int j=0; j<F.rows(); j++)
                {
                    of_BC << j*2 + 1 << " ";

                    Vector3 modalOffset = Vector3::Zero();

                    for (int k=0; k<3; k++)
                        modalOffset += evectors.block(F(j,k)*3, index, 3, 1);

                    // Normalize the modal offset
                    modalOffset /= 3.0;

                    // Set up the boundary condition
                    std::complex<Real> boundaryCondition;
                    boundaryCondition.real(0.0);
                    boundaryCondition.imag(angularFrequency * modalOffset.dot(Fn.row(j)));

                    // Neumann BC
                    of_BC << "2 ";

                    of_BC << "( " << boundaryCondition.real() << ", " << boundaryCondition.imag() << ")" << std::endl;

                    // Write an entry for the opposite side also
                    of_BC << j*2 + 2 << " ";

                    // Neumann BC
                    of_BC << "2 ";

                    boundaryCondition *= -1.0;
                    of_BC << "( " << boundaryCondition.real() << ", " << boundaryCondition.imag() << ")" << std::endl;
                }
            }
            
            of_BC << "$ End of file" << std::endl;
            of_BC.close();

            std::cout << "Done" << std::endl;
        }
    }

}
