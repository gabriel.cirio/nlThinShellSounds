
#ifndef ISOMETRIC_BENDING_ENERGY_H
#define ISOMETRIC_BENDING_ENERGY_H

#include "Energy.h"

#include <Eigen/Sparse>

class IsometricBendingEnergy : public Energy
{
	public:
		
		IsometricBendingEnergy(MatrixX & rest_pos, MatrixX & pos, MatrixXi & F, std::vector<int> & per_node_dofs, VectorX & triangle_rest_area, MatrixXi & E_unique,
					  MatrixXi & per_unique_edge_triangles, MatrixXi & per_unique_edge_triangles_local_corners, Real young_modulus, Real poisson_ratio, Real thickness);
		
		~IsometricBendingEnergy() {}
		
		void setup();
		void prepareForStep() {}
		void computeForces(std::vector<MatrixX> & force_omp_arrays);
		
		void computeDfDx(std::vector<std::vector<Jacobian> > & dfdx);
		
		void prepareForSubspacePolynomialPrecomputation(std::vector<std::set<int>> & stencil_dofs_per_node);
		void subspacePolynomialPrecomputationNodei(int node_i, const std::vector<int> & stencil_dofs_vec, ValuesForDof values_for_dof[3]);
	
	private:
		
		MatrixX & m_rest_pos;
		MatrixX & m_pos;
		MatrixXi & m_F;
		std::vector<int> & m_per_node_dofs;
		VectorX & m_triangle_rest_area;
		
		MatrixXi & m_E_unique;
		MatrixXi & m_per_unique_edge_triangles;
		MatrixXi & m_per_unique_edge_triangles_local_corners;
		
		Real m_young_modulus;
		Real m_poisson_ratio;
		Real m_thickness;
		
		Real m_bending_stiffness;
		
		Eigen::SparseMatrix<Real> m_L;
		std::vector<Eigen::SparseMatrix<Real>> m_Qi;
		VectorX m_nonflat_common;
		VectorX m_flat_common;
		MatrixX m_cot;
		
		std::vector<std::vector<int>> m_edges_per_node;
};

#endif // ISOMETRIC_BENDING_ENERGY_H




