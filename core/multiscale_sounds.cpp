
#include "multiscale_sounds.h"
#include "util/JSONConfiguration.h"
#include "sim/DynamicObject.h"
#include "sim/ThinShell.h"
#include "sim/ScisimAnimation.h"
#include "sim/ReducedThinShell.h"
#include "sim/ExplicitSymplecticEulerSolver.h"

#ifdef HAVE_CUDA
#include "cuda/CudaUtilsHost.h"
#endif

#include <iostream>

MultiScaleSounds::MultiScaleSounds(bool is_cli) : m_is_cli(is_cli)
{
	m_is_valid = false;
	m_is_prepared = false;
	m_solver = nullptr;
	m_elapsed_time = 0.;
	m_duration = 0;
	m_dt_substeps = 1;
    m_mesh_write_fps = 30.;
    m_mesh_write_last_frame = -1;
    m_mesh_write = false;
}

bool MultiScaleSounds::load_config(std::string config_filename)
{
    std::string base_path = config_filename.substr(0,config_filename.find_last_of("/\\")+1);
  
    JSONConfiguration json_configuration;
    bool loaded = json_configuration.loadSceneFile(config_filename);
    
    if (!loaded) return false;
    
    const rapidjson::Document &config_tree = json_configuration.getSceneConfigurationTree();
        
    bool use_gpu = false;
    if (config_tree.HasMember("use_gpu"))
        use_gpu = config_tree["use_gpu"].GetBool();
    
    int computation_space = config_tree["computation_space"].GetInt(); // 0 (full), 1 (linear), 2 (reduced)
    std::cout << "Computing in ";
    if (computation_space == 0) std::cout << "FULL SPACE" << std::endl;
    else if (computation_space == 1) std::cout << "LINEAR SPACE" << std::endl;
    else if (computation_space == 2) std::cout << "SUBSPACE" << std::endl;
    else if (computation_space == 3) std::cout << "TURBULENT SUBSPACE" << std::endl;
    else 
    {
        std::cout << "Unknown computation space. Aborting!" << std::endl;
        std::exit(1);
    }
    
    bool compute_lyapunov = true;
    if (config_tree.HasMember("compute_lyapunov"))
        compute_lyapunov = config_tree["compute_lyapunov"].GetBool();
    
    m_duration = config_tree["duration"].GetDouble();
    
    // render to screen
    bool render_to_screen = false;
    if (config_tree.HasMember("render")) render_to_screen = config_tree["render"].GetBool();
    
    if (config_tree.HasMember("write_meshes")) 
        m_mesh_write = config_tree["write_meshes"].GetBool();
	
	if (config_tree.HasMember("dt_substeps")) 
        m_dt_substeps = config_tree["dt_substeps"].GetInt();
    
    ScisimAnimation* scisim_anim = nullptr;
    if (config_tree.HasMember("Animation")) 
    {
        auto json_animation = config_tree["Animation"].GetObject();
        scisim_anim = new ScisimAnimation();
        scisim_anim->configure(json_animation, base_path);
        
        bool add_to_sim = false;
        if (json_animation.HasMember("add_to_sim"))
            add_to_sim = json_animation["add_to_sim"].GetBool();
        if (add_to_sim)
            m_dynamic_objects.push_back(scisim_anim);
        
        if (m_duration != scisim_anim->getDuration())
        {
            std::cout << "Adjusting simulation requested duration (" << m_duration << ") to animation duration (" << scisim_anim->getDuration() << ")." << std::endl;
            m_duration = scisim_anim->getDuration();
        }
    }
    
    if (config_tree.HasMember("ReducedThinShells"))
    {
        std::cout << config_tree["ReducedThinShells"].Size() << " reduced thin shells found." << std::endl;
        for (int i = 0; i < config_tree["ReducedThinShells"].Size(); ++i) 
        {
            auto json_reducedthinshell = config_tree["ReducedThinShells"][i].GetObject();
            ReducedThinShell* reduced_thinshell = new ReducedThinShell();
            if (scisim_anim) reduced_thinshell->setAnimationObject(scisim_anim->getObject(i));
            reduced_thinshell->configure(json_reducedthinshell, base_path);
            reduced_thinshell->getThinShell()->setUseGpu(use_gpu);
            reduced_thinshell->setSecondsToSave(m_duration);
            reduced_thinshell->setComputationSpace(computation_space);
            reduced_thinshell->enableLyapunovExponents(compute_lyapunov);
            reduced_thinshell->renderToScreen(render_to_screen);
            m_dynamic_objects.push_back(reduced_thinshell);
        }
    }
    else if (config_tree.HasMember("ThinShells"))
    {
        std::cout << config_tree["ThinShells"].Size() << " thin shells found." << std::endl;
        for (int i = 0; i < config_tree["ThinShells"].Size(); ++i) 
        {
            auto json_thinshell = config_tree["ThinShells"][i].GetObject();
            ThinShell* thinshell = new ThinShell();
            thinshell->configure(json_thinshell, base_path);
            m_dynamic_objects.push_back(thinshell);
        }
    }
    
    m_solver = new ExplicitSymplecticEulerSolver();
    //m_solver = new VelocityVerletSolver();
    //m_solver = new ExplicitNewmarkSolver();
    //m_solver = new ExplicitRK4Solver();
    //m_solver = new NewtonBackwardEulerSolver();
	
	#ifdef HAVE_CUDA
        int device_num = CudaHostUtilities::findCudaDevice();
        //device_num = 0; // to force using the Tesla
        CudaHostUtilities::gpuDeviceInit(device_num);
    #endif

    m_is_valid = true;

    return true;
}

void MultiScaleSounds::prepare()
{
	for (auto & dyn_obj : m_dynamic_objects)
		dyn_obj->setDtSubsteps(m_dt_substeps);
	
	m_solver->setDtSubsteps(m_dt_substeps);
	
	for (auto & dyn_obj : m_dynamic_objects)
		dyn_obj->prepareForSimulation();
	
	m_is_prepared = true;
}

void MultiScaleSounds::simulate_step()
{
    if (!m_is_prepared) this->prepare();

	for (auto & dyn_obj : m_dynamic_objects)
		dyn_obj->prepareForStep(m_elapsed_time, m_solver->getCurrentDt());
	
    m_solver->solve(m_dynamic_objects);
	
	for (auto & dyn_obj : m_dynamic_objects)
		dyn_obj->postStep();
    
    if (m_mesh_write)
    {
        int current_frame = std::floor(m_elapsed_time * m_mesh_write_fps);
        if (current_frame != m_mesh_write_last_frame)
        {
            for (auto & dyn_obj : m_dynamic_objects)
                dyn_obj->writeMeshes("frame_" + std::to_string(current_frame));
            m_mesh_write_last_frame = current_frame;
        }
    }
	
    if (m_is_cli && (m_elapsed_time > m_duration))
	{
		for (auto & dyn_obj : m_dynamic_objects)
			dyn_obj->writeToDisk();
		m_is_valid = false;
	}
	
	m_elapsed_time += m_solver->getCurrentDt();
	
}

void MultiScaleSounds::get_data_meshes(std::vector<std::pair<MatrixX, MatrixXi>> & meshes)
{
	meshes.resize(m_dynamic_objects.size());
	for (unsigned int i=0; i<m_dynamic_objects.size(); i++)
		m_dynamic_objects[i]->getMeshData(meshes[i]);
}

void MultiScaleSounds::get_data_nodal_vectors(std::vector<MatrixX> & vectors)
{
    vectors.resize(m_dynamic_objects.size());
    for (unsigned int i=0; i<m_dynamic_objects.size(); i++)
        m_dynamic_objects[i]->getNodalVectors(vectors[i]);
}

void MultiScaleSounds::get_data_world_vectors(std::vector<std::pair<MatrixX, MatrixX>> & vectors)
{
    vectors.resize(m_dynamic_objects.size());
    for (unsigned int i=0; i<m_dynamic_objects.size(); i++)
        m_dynamic_objects[i]->getWorldVectors(vectors[i]);
}

void MultiScaleSounds::set_key(unsigned char key)
{ 
	for (auto & dyn_obj : m_dynamic_objects) 
		dyn_obj->set_key(key);
}

