# nlThinShellSounds

Source code of the project corresponding to the paper &#34;Multi-Scale Simulation of Nonlinear Thin-Shell Sound with Wave Turbulence&#34;

Copyright Inria and Columbia University.
Free for private and academic use.

Dependencies: libigl, cuda, matlab, linsndfile, MathGL