
#include "multiscale_sounds.h"

#include "util/Timer.h"

#include <iostream>
#include <thread>

MultiScaleSounds* g_sim;
bool g_thread_running = false;

void sim_thread()
{
	int frame_stepper = 0;
    
	while (g_sim->is_valid())
	{
		g_sim->simulate_step();
		
        if (frame_stepper < 100) frame_stepper++;
		else
		{
			std::cout << "Elasped: " << g_sim->getElapsedTime() << std::endl;
			frame_stepper = 0;
		}
	}
	
	g_thread_running = false;
}

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		std::cout << "Usage: ./multiscale_sounds <filename>" << std::endl;
		return 0;
	}

	std::string conf_filename = argv[1];
    
	g_sim = new MultiScaleSounds(true);
    
	if (!g_sim->load_config(conf_filename)) return 1;
    
    g_thread_running = true;
    std::thread stdthread(sim_thread);
	stdthread.detach();

    Timer::tic("Total elapsed time:");
	while (g_thread_running)
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
    Timer::toc();
	
	return 0;
}
